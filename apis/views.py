from __future__ import division
from django.contrib.auth.models import User, Group
from django.contrib.auth import logout
from rest_framework import viewsets, permissions
from allauth.account import app_settings
from django.core.exceptions import ObjectDoesNotExist
from rest_framework.authtoken.models import Token
from serializers import AboutSerializer, FaqSerializers, AdminMessageSerializer, PickupCancellationReasonsSerializers, DriversLocationSerializer, SizeSerializer, PackageSerializer, MessageSeralizer, AddressSerializer, PickUpPriceSerializer, CustomDriverSerializer, UpdateAccountSerializer, VechileTypeSerializer, DriverSerializer, DeleveryServiceSerializer, LogSerializer, VerificationSerializer, CustomerSerializer, PickupServiceSerializer, PaymentSerializer, PackageSizeInfoSerializer, ExpressWindowsSerializer, CreditCardSerializer
from customer.models import Customer, UserProfile, CreditCard
from driver.models import Driver, Vechile, BankAccount
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.shortcuts import get_object_or_404
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.authentication import TokenAuthentication
from service.models import AdminMessage, PickupCancellationReasons, Payment, PickupJobLogDriver, Pricing, PackageSize, AdminMessage, Tracking, VehicleType, DeliveryService, ActivityLog, PickupService, Package, PickUpRate, HolidayList, DriverRating, PackageSize
from rest_framework.views import APIView
from rest_framework.decorators import api_view, authentication_classes
from django.views.decorators.csrf import csrf_exempt
import datetime
import json
from django.db.models import Q
from django.http import JsonResponse, HttpResponse
from rest_framework.decorators import detail_route, list_route
from PIL import Image
from django.conf import settings
from rest_framework.authtoken.models import Token
from twilio.rest import TwilioRestClient
from address.models import Address, Postalcode, Street
import googlemaps
import ast
from base.utils import send_email_notification, send_email_invoice_for_pickup
import math
import requests
from base import constant
import os
import googlemaps
from superadmin.models import CommissionSetup, DeliveryTiming, ExpressWindows, Faq, AboutPage
from django.contrib.auth import authenticate
import stripe
from cart.cart import Cart
from operator import attrgetter
from allauth.account.forms import ResetPasswordForm
from datetime import date
import base64
from django.core.files.base import ContentFile
from django.core.mail import send_mail
# import ast

#geo django imports
from geo.models import DriverLocation
from django.contrib.gis import geos

#dispatch 
from dispatch.dispatch import Dispatch
from dispatch import tasks





def log_view_pickup(pickup, driver):
    try:
        instance = PickupJobLogDriver()
        instance.pickup = pickup
        instance.driver = driver
        instance.action = "view"
        instance.save()
        return True
    except:
        return False

def log_accept_pickup(pickup, driver):
    try:
        instance = PickupJobLogDriver()
        instance.pickup = pickup
        instance.driver = driver
        instance.action = "accept"
        instance.save()
        return True
    except:
        return False

def log_cancell_pickup(pickup, driver):
    try:
        instance = PickupJobLogDriver()
        instance.pickup = pickup
        instance.driver = driver
        instance.action = "cancell"
        instance.save()
        return True
    except:
        return False

def get_acceptacce_rate(driver):
    data = PickupJobLogDriver.objects.filter(driver=driver, action="view")
    acceptted = PickupJobLogDriver.objects.filter(driver=driver, action="accept")
    if data.count() == 0 or acceptted.count() == 0:
        return 0.0
    else:
        return float(acceptted.count())/float(data.count())*100.00


def get_cencell_rate(driver):
    data = PickupJobLogDriver.objects.filter(driver=driver, action="view")
    cancelled = PickupJobLogDriver.objects.filter(driver=driver, action="cancell")
    if data.count() == 0 or cancelled.count() == 0:
        return 0.0
    else:
        return float(cancelled.count())/float(data.count())*100.00

def _get_admin_fees_for_pickup(pickup):
    vechile_type = pickup.vehicle_type
    return 0

def _calculate_trip_price(pickup):
    datype = "Weekday"
    today = datetime.datetime.today()
    if today.weekday() == 6:
        datype = "Weekend"
    elif HolidayList.objects.filter(date=today).count() > 0:
        datype = "Weekend"
    price = PickUpRate.objects.filter(vehicle_type=pickup.vehicle_type, day_type=datype)
    return price
def calculate_fare_for_passenger_pickup(pickup):
    price = _calculate_trip_price(pickup)
    base_fare = price[0].base_fare
    driver_catogory = pickup.pickedup_by.driver_category
    comission_data = CommissionSetup.objects.filter(category=driver_catogory)
    try:
        commission_percentacge = comission_data[0]
    except:
        commission_percentacge = 0

    rate = price[0].rate
    distance_str = pickup.distance
    distance = float(distance_str.split()[0])
    fare = float(base_fare) + float(rate) * distance
    car_fee = 0.0
    online_fees = 0.0
    toll_fee = 0.0
    admin_fees = round(fare * (float(commission_percentacge.commission_pickup) / 100.00), 2)
    tatal_fee = fare+ car_fee + online_fees + admin_fees
    return {"fare": fare, "car_fee": car_fee, "online_fee": online_fees, "admine_fee": admin_fees, "total": tatal_fee, "toll_fee":toll_fee}
# make payment for passenger pickup
def make_payment_for_pickup(pickup):
    get_price = calculate_fare_for_passenger_pickup(pickup)
    total = int(get_price['total'] * 100)
    stripe.api_key = constant.STRIPE_SECRECT_KEY
    cards = CreditCard.objects.filter(custmer=pickup.customer)
    # try:
    payment = Payment()
    payment.customer = pickup.customer
    payment.pickup_service = pickup
    payment.total_cost = get_price['total']
    payment.paid_amount = get_price['total']
    payment.online_fee = get_price['online_fee']
    payment.toll_fee = get_price['toll_fee']
    payment.admin_fee = get_price['admine_fee']
    payment.save()
    log = ActivityLog()
    log.log_message = "Your payment has been received."
    log.pickup_service = pickup
    log.log_type = "The payment is done."
    log.delevery_status = "paid"
    log.save()
    stripe.Charge.create(
        amount=total,
        currency="sgd",
        customer=cards[0].strite_id)
    return True
    # send_email_notification(request.user, "Your payment has been received.")
    # except:
    #     return False

# # make refund
def make_refund_for_courier():
    return True



@api_view(['post'])
@authentication_classes([TokenAuthentication])
def test_email(request):
    send_email_notification(request.user, "This Test")
    return HttpResponse("Ok")



def get_nearest_drivers(location, radius, driver_type):
    device_token_list = []
    drivers = Driver.objects.filter(Q(driver_service_type=driver_type) | Q(driver_service_type="both") & Q(is_active=True))
    for driver in drivers:
        # distance = distance_between_locations(location[0], drivers.last_lat, location[1], derver.last_lng)
        if True:
            if driver.device_token is not "":
                device_token_list.append(driver.device_token)
    return device_token_list
# Job post notification category = 1
# New Pickup job posted category = 2
def send_push_message(device_token_list, message, title, category = 0):
    url = "https://fcm.googleapis.com/fcm/send"
    noti = {}
    noti['content_available'] = True
    noti['notification'] = {}
    noti['notification']['body'] = message
    noti['notification']['title'] = title
    noti['notification']['category'] = category
    noti['data'] = {}
    noti['data']['message'] = message
    noti['data']['title'] = title
    noti['notification']['sound'] = 'default'
    noti['registration_ids'] = get_nearest_drivers(None, None, "courier")
    noti['priority'] = 'high'

    headers = {
        'authorization': "key=AIzaSyDZWqkNJXm11ilon9e8j2F7tecYQd12flU",
        'content-type': "application/json",
        'cache-control': "no-cache",
    }

    return  requests.request("POST", url, data=json.dumps(noti), headers=headers)

def send_push_message_for_pickeup_job_post(device_token_list, message, title, category = 0, job_id= 0):
    url = "https://fcm.googleapis.com/fcm/send"
    noti = {}
    noti['content_available'] = True
    noti['notification'] = {}
    noti['notification']['body'] = message
    noti['notification']['title'] = title
    noti['notification']['category'] = category
    noti['notification']['job_id'] = job_id
    noti['data'] = {}
    noti['data']['message'] = message
    noti['data']['title'] = title
    noti['notification']['sound'] = 'default'
    noti['registration_ids'] = get_nearest_drivers(None, None, "pickup")
    noti['priority'] = 'high'

    headers = {
        'authorization': "key=AIzaSyDZWqkNJXm11ilon9e8j2F7tecYQd12flU",
        'content-type': "application/json",
        'cache-control': "no-cache",
    }
    return  requests.request("POST", url, data=json.dumps(noti), headers=headers)

@api_view(['GET'])
def test_push(request):
    res = send_push_message_for_pickeup_job_post(["edRtBlloBmk:APA91bEzv62104lA7PC3f96sA76Jpgvj2_27V5bepeQw6E3foYMCWREn1fnHt3VAdboCKbxn5k2MLiKLG-Ccvlgtie3xrcfEDBG2tIbZC33cJxdqfvPWX_TBD6jR0dd_yp9OdVzx7Pxz"], "This is message body", "This is message title", 1)
    return HttpResponse(res)


@api_view(['POST'])
def create_driver(request):
    serializer = DriverSerializer(data=request.data)
    if serializer.is_valid():
        user = User.objects.filter(email=request.data['email'])
        # Need to use this part later #
        # phone = UserProfile.objects.filter(phone_number=request.data['mobile_no'])
        # if phone.count() == 0 and user.count() == 0:
        ###
        if user.count() == 0:
            try:
                serializer.save()
                driver_query = Driver.objects.get(email=request.data['email'])
                code = driver_query.verification
                phone_number = driver_query.mobile_no
                send_sms(code, phone_number)
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            except Exception, e:
                return Response({'status':'error', 'message':e}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        else:
            return Response({'status': 'error', 'message': "Phone no or email already exists."},
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    return Response({'status':'error', 'message': serializer.errors}, status=status.HTTP_400_BAD_REQUEST)




def send_sms(message, send_to):
	send_from = '+12027937265'
	client = TwilioRestClient(settings.TWILIO_ACCOUNT_SID, settings.TWILIO_AUTH_TOKEN)
	response = client.messages.create(body='Your UF verfication code is: '+message, to=send_to, from_=send_from)

def get_avarage_rattings(driver):
    rating = DriverRating.objects.filter(driver=driver.id)
    if rating.count() == 0:
        avg_rating = 5
    else:
        total_rating = 0
        for rate in rating:
            total_rating = total_rating + int(rate.rating)
        avg_rating = total_rating / rating.count()
        avg_rating = float("{0:.1f}".format(avg_rating))
    return avg_rating


@api_view(['POST'])
def custom_authentication(request):
    authentication_classes = (TokenAuthentication,)
    email_phone_number = request.POST['email_phone_number']
    device_token = request.POST.get("device_token", "")
    email = False
    phone_number = False
    customer_user = False
    driver_user = False
    try:
        try:
            verify = User.objects.get(email=email_phone_number)
            email = True
            if email:
                try:
                    token = Token.objects.get(user=verify.id)
                    token.delete()
                except:
                    pass
            try:
                driver = Driver.objects.get(user=verify.id)
                driver.device_token = device_token
                driver.save()
                # weekly_earning = 900.00
                # print 'before'
                weekly_earning = get_weekly_earnings(driver)
                # print 'after'
                acceptacce = get_acceptacce_rate(driver)
                cancellation = get_cencell_rate(driver)
                avg_rating = get_avarage_rattings(driver)
                message = AdminMessage.objects.filter(message_to_driver=driver)
                message_serializer = MessageSeralizer(message, many=True)

                driver_user = True
                try:
                    valid = verify.check_password(request.POST['password'])
                    if valid:
                        token = Token.objects.create(user=User.objects.get(email=email_phone_number))
                        serializer = DriverSerializer(driver)

                        return Response({'token': token.key, 'type': 'Driver', 'user_data': serializer.data, 'rating': avg_rating,
                                         'weekly_earning': weekly_earning, 'acceptance': acceptacce,
                                         'cancellation': cancellation, 'message': message_serializer.data}, status=status.HTTP_200_OK)
                    else:
                        return Response({'message': 'Invalid User'}, status=status.HTTP_400_BAD_REQUEST)
                except Exception, e:
                    return Response({'message':  e}, status=status.HTTP_400_BAD_REQUEST)
            except Exception,e:
                try:
                    customer = Customer.objects.get(user=verify.id)
                    customer_user = True
                    customer.device_token = device_token
                    customer.save()
                    try:
                        valid = verify.check_password(request.POST['password'])
                        if valid:
                            token = Token.objects.create(user=User.objects.get(email=email_phone_number))
                            serializer = CustomerSerializer(customer, context={"request": request})
                            return Response({'token': token.key, 'type': 'Customer', 'user_data': serializer.data}, status=status.HTTP_200_OK)
                        else:
                            return Response({'message': e}, status=status.HTTP_400_BAD_REQUEST)
                    except Exception, e:
                        return Response({'message': e}, status=status.HTTP_400_BAD_REQUEST)
                except Exception, e:
                    return Response({'message': e}, status=status.HTTP_400_BAD_REQUEST)

        except Exception, e:
            return Response({'message': e}, status=status.HTTP_400_BAD_REQUEST)

            # Need to use this part later #
            # phone_number = True
            # try:
            #     driver = Driver.objects.get(mobile_no=email_phone_number)
            #     driver.device_token = token
            #     driver.save()
            #     driver_user = True
            #     if driver_user:
            #         try:
            #             token = Token.objects.get(user=driver.user.id)
            #             token.delete()
            #         except:
            #             pass
            #     user_info = User.objects.get(id=driver.user.id)
            #     # print user_info.is_active
            #     if user_info.is_active == 1:
            #         try:
            #             valid = user_info.check_password(request.POST['password'])
            #             if valid:
            #                 token = Token.objects.create(user=User.objects.get(id=user_info.id))
            #                 serializer = DriverSerializer(driver)
            #                 rating = DriverRating.objects.filter(driver=driver.id)
            #                 if rating.count() == 0:
            #                     avg_rating = 0
            #                 else:
            #                     total_rating = 0
            #                     for rate in rating:
            #                         total_rating = total_rating + int(rate.rating)
            #                     avg_rating = total_rating / rating.count()
            #                     avg_rating = float("{0:.1f}".format(avg_rating))
            #                 return Response({'token': token.key, 'type': 'Driver', 'user_data': serializer.data,
            #                                  'rating': avg_rating, 'weekly_earning': weekly_earning, 'acceptance': acceptacce, 'cencelation':cencelation, 'message': message_serializer.data}, status=status.HTTP_200_OK)
            #         except Exception, e:
            #             print e
            #             return Response({'message': 'Invalid Information'}, status=status.HTTP_400_BAD_REQUEST)
            #     else:
            #         return Response({'message': 'Not activated account'}, status=status.HTTP_400_BAD_REQUEST)
            #         return Response({'message': 'Not activated account'}, status=status.HTTP_400_BAD_REQUEST)
                # return Response({'Driver': driver_user}, status=status.HTTP_200_OK)
            # except:
            #     try:
            #         customer = Customer.objects.get(contact_no=email_phone_number)
            #         customer.device_token = token
            #         customer.save()
            #         customer_user = True
            #         if customer_user:
            #             try:
            #                 token = Token.objects.get(user=customer.user.id)
            #                 token.delete()
            #             except:
            #                 pass
            #         user_info = User.objects.get(id=customer.user.id)
            #         # print user_info.id
            #         if user_info.is_active == 1:
            #             try:
            #                 valid = user_info.check_password(request.POST['password'])
            #                 if valid:
            #                     token = Token.objects.create(user=User.objects.get(id=user_info.id))
            #                     serializer = CustomerSerializer(customer)
            #                     return Response({'token': token.key, 'type': 'Customer', 'user_data': serializer.data},
            #                                     status=status.HTTP_200_OK)
            #                 else:
            #                     return Response({'message': 'Invalid Information'}, status=status.HTTP_400_BAD_REQUEST)
            #             except Exception, e:
            #                 return Response({'message': 'Invalid Information'}, status=status.HTTP_400_BAD_REQUEST)
            #         else:
            #             return Response({'message': 'Not activated account'}, status=status.HTTP_400_BAD_REQUEST)
            #     except Exception, e:
            #         return Response({'message': 'Invalid information'}, status=status.HTTP_400_BAD_REQUEST)
    except Exception, e:
        return Response({"message": e}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
@authentication_classes([TokenAuthentication])
def update_drivers_image(request):
    # driver_id = request.POST['driver_id']
    driver_id = request.user.id
    image_type = request.POST['image_type']
    image = request.FILES['image']
    try:
        driver = Driver.objects.get(user=driver_id)
    except:
        return Response({'status':'error', 'message': "Unable to Add Image"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    if image_type == 'nric_photo':
        driver.nric_photo = image
        driver.save()
        return Response({'message':'NRIC Image Updated'}, status=status.HTTP_200_OK)
    elif image_type == 'driving_licence_photo':
        driver.driving_licence_photo = image
        driver.save()
        return Response({'message' : 'Driving Licence Photo Updated'}, status=status.HTTP_200_OK)
    elif image_type == 'profile_image':
        driver.profile_image = image
        driver.save()
        return Response({'message': 'Profile Photo Updated'}, status=status.HTTP_200_OK)
    elif image_type == 'plate':
        try:
            vechile = Vechile.objects.get(driver= driver)
            vechile.plate = image
            return Response({'message' :'Plate image Updated'}, status=status.HTTP_200_OK)
        except:
            return Response({'status':'error', 'message': "Unable to Add Plate Image"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
    elif image_type == 'vechile_photo':
        try:
            vechile = Vechile.objects.get(driver= driver)
            vechile.vechile_photo = image
            return Response({'message' : 'Vechile image Updated'}, status=status.HTTP_200_OK)
        except:
            return Response({'status':'error', 'message': "Unable to Add Vechile Image"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
    else:
        return Response({'status':'error', 'message': "Unable to Add Image"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class VechileTypeViewSet(viewsets.ModelViewSet):
    """
    API endpoint for get vechile type
    """
    queryset = VehicleType.objects.all()
    serializer_class = VechileTypeSerializer
    http_method_names = ['get']

class DeleveryServiceViewSet(viewsets.ModelViewSet):
    """
    API endpoint that
    """
    queryset = DeliveryService.objects.all()
    serializer_class = DeleveryServiceSerializer
    http_method_names = ['get']



@api_view(['POST'])
def update_your_account(request):
    serializer = UpdateAccountSerializer(data=request.data)
    if serializer.is_valid():
        if serializer.save():
            return Response({"message": "success"}, status=status.HTTP_202_ACCEPTED)
    else:
        return Response(serializer.errors)
    return Response({"message": "Unable you Update your account"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

@api_view(['POST'])
@authentication_classes([TokenAuthentication])
def take_delevery_service(request, job_id):
    lat = request.POST['lat']
    lng = request.POST['lng']
    try:
        service = DeliveryService.objects.get(id=job_id)
        driver = Driver.objects.get(user=request.user)
        if service.delevery_status == "taken":
            return Response({"status": "falure", "message": "The Job Already Taken"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        service.delevery_status = "taken"
        service.taken_by = driver
        service.job_start_time = datetime.datetime.now()
        service.save()
        log = ActivityLog()
        log.delevery_service = service
        log.log_message = constant.DELEVERY_DERVICE_TAKEN_MESSAGE
        log.delevery_status = "taken"
        log.logger_name = driver.name
        log.log_type = constant.ACCEPTED_JOB
        log.lat = lat
        log.lng = lng
        log.save()
        # send_email_notification(service.customer.user, "Courrier requet has been accepted by "+driver.name)
        return Response({"status": "success"}, status=status.HTTP_200_OK)
    except:
        return Response({"message": constant.VALIDATION_ERROR}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)




@api_view(['POST'])
@authentication_classes([TokenAuthentication])
def complete_delevery_job(request, job_id):
    lat = request.POST['lat']
    lng = request.POST['lng']
    try:
        note = request.POST['completion_note']
        driver = Driver.objects.get(user=request.user)
        signature = request.FILES['signature']
        service = DeliveryService.objects.get(id=job_id)
        if service.delevery_status != "taken" or service.taken_by != Driver.objects.get(user=request.user):
            return Response({"message":"You are now allowed to perform operation."}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        service.delevery_status = "completed"
        service.completion_note = note
        service.job_complete_time = datetime.datetime.now()
        service.signature = signature
        # service.confirm_delevery = True
        service.save()
        log = ActivityLog()
        log.delevery_service = service
        log.delevery_status = "completed"
        log.log_message = constant.DELEVERY_DERVICE_COMPLETED_MESSAGE
        log.logger_name = driver.name
        log.log_type = constant.COMPLETED_JOB
        log.lat = lat
        log.lng = lng
        log.save()
        send_email_notification(service.customer.user, "Delevery requet has been completed ")
        return Response({"status": "success"})
    except Exception, e:
        return Response({"message": e},status=status.HTTP_500_INTERNAL_SERVER_ERROR)

@api_view(['POST'])
@authentication_classes([TokenAuthentication])
def cancell_delevery_job(request, job_id):
    lat = request.POST['lat']
    lng = request.POST['lng']
    try:
        note = request.POST['cancell_note']
        driver = Driver.objects.get(user=request.user)
        service = DeliveryService.objects.get(id=job_id)
        if service.delevery_status != "taken" or service.taken_by != Driver.objects.get(user=request.user):
            return Response({"message":"You are not allowed to perform this operation."},status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        service.delevery_status = "pending"
        service.cancell_note = note
        service.job_cancelled_time = datetime.datetime.now()
        service.taken_by = None
        service.save()
        log = ActivityLog()
        log.delevery_service = service
        log.delevery_status = "cancelled"
        log.log_message = constant.DELEVERY_DERVICE_CANCELL_MESSAGE
        log.logger_name = driver.name
        log.log_type = constant.CANCELLED_JOB
        log.lat = lat
        log.lng = lng
        log.save()
        send_email_notification(service.customer.user, "Delevery requet has been cancelled")
        return Response({"status": "success"})
    except:
        return Response({"message": constant.VALIDATION_ERROR},status=status.HTTP_500_INTERNAL_SERVER_ERROR)

@api_view(['POST'])
@authentication_classes([TokenAuthentication])
def confirm_pickup(request):
    ids = request.POST.get('package_ids')
    lat = request.POST['lat']
    lng = request.POST['lng']
    packages = []
    try:
        for i in ast.literal_eval(ids):
            package = Package.objects.get(id=i)
            if package.service.taken_by != Driver.objects.get(user=request.user):
                return Response({"message": "You are now allowed to perform this operation."}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
            package.confirm_pickup = True
            packages.append(package)
        for pack in packages:
            pack.save()
        log = ActivityLog()
        log.log_type = constant.DELEVERY_DERVICE_PICKEDUP_MESSAGE
        log.delevery_service = packages[0].service
        log.delevery_status = 'pickedup'
        log.logger_name = packages[0].service.taken_by.name
        log.log_message = 'Your packages have been picked up.'
        log.date_time = datetime.datetime.now()
        log.lat = lat
        log.lng = lng
        log.save()
        send_email_notification(packages[0].service.customer.user, "Your Packeges has been picked up by "+ Driver.objects.get(
            user=request.user).name)
    except Exception, e:
        return Response({"message": e},status=status.HTTP_500_INTERNAL_SERVER_ERROR)
    return Response({"status": "success"})


@api_view(['POST'])
@authentication_classes([TokenAuthentication])
def take_pickup_service(request, job_id):
    try:
        service = PickupService.objects.get(id=job_id)
        serializer = PickupServiceSerializer(service)
        driver = Driver.objects.get(user=request.user)
        if service.pickup_status == "taken":
            return Response({"status": "falure", "message": "The Job Already Taken"}, status=status.HTTP_400_BAD_REQUEST)
        service.pickup_status = "taken"
        service.pickedup_by = driver
        service.job_start_time = datetime.datetime.now()
        service.save()
        dispatch = Dispatch(request)
        dispatch.confirm_customer_fo_pickup_taken(service.customer.device_token, "Your Pickup Taken", "Your Pickup Taken", driver.id)
        log = ActivityLog()
        log.pickup_service = service
        log.log_message = constant.DELEVERY_DERVICE_TAKEN_MESSAGE
        log.pickup_status = "taken"
        log.logger_name = driver.name
        log.log_type = constant.ACCEPTED_JOB
        log.save()
        log_accept_pickup(service, driver)

        return Response(serializer.data, status=status.HTTP_200_OK)
    except Exception, e:
        return Response({"message": e}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


@api_view(['POST'])
@authentication_classes([TokenAuthentication])
def pickedup_pickup_service(request, job_id):
    service = PickupService.objects.get(id=job_id)
    driver = Driver.objects.get(user=request.user)
    if service.pickup_status == "pickedup":
        return Response({"status": "falure", "message": "The Job Already Picked Up."}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
    service.pickup_status = "pickedup"
    service.pickedup_by = driver
    service.job_start_time = datetime.datetime.now()
    service.save()
    log = ActivityLog()
    log.pickup_service = service
    log.log_message = constant.PICKEDUP_JOB
    log.pickup_status = "pickedup"
    log.logger_name = driver.name
    log.log_type = constant.ACCEPTED_JOB
    log.save()
    return Response({"status": "success"})

@api_view(['POST'])
@authentication_classes([TokenAuthentication])
def cancell_pickup_job(request, job_id):
    try:
        note = request.POST['cancell_note']
        service = PickupService.objects.get(id=job_id)
        driver = Driver.objects.get(user=request.user)
        if service.pickup_status != "taken" or service.pickedup_by != Driver.objects.get(user=request.user):
            return Response({"message":"You are not allowed to perform this operation."},status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        service.pickup_status = "pending"
        service.job_cancelled_time = datetime.datetime.now()
        service.cancell_note = note
        service.pickedup_by = None
        service.save()
        log = ActivityLog()
        log.pickup_service = service
        log.pickup_status = "cancelled"
        log.log_message = constant.DELEVERY_DERVICE_CANCELL_MESSAGE
        log.logger_name = driver.name
        log.log_type = constant.CANCELLED_JOB
        log.save()
        log_cancell_pickup(service, driver )
        return Response({"status": "success"})
    except:
        return Response({"message": constant.VALIDATION_ERROR},status=status.HTTP_500_INTERNAL_SERVER_ERROR)

@api_view(['POST'])
@authentication_classes([TokenAuthentication])
def complete_pickup_job(request, job_id):
    try:
        note = request.POST['completion_note']
        service = PickupService.objects.get(id=job_id)
        if not make_payment_for_pickup(service):
            return Response({"status": "falure", "message": "Unable to make payment right now"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        driver = Driver.objects.get(user=request.user)
        if service.pickup_status != "pickedup" or service.pickedup_by != Driver.objects.get(user=request.user):
            return Response({"message": "You are not allowed to perform this operation."},status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        service.pickup_status = "completed"
        service.job_complete_time = datetime.datetime.now()
        service.completion_note = note
        service.save()
        log = ActivityLog()
        log.pickup_service = service
        log.pickup_status = "completed"
        log.log_message = constant.DELEVERY_DERVICE_COMPLETED_MESSAGE
        log.logger_name = driver.name
        log.log_type = constant.COMPLETED_JOB
        log.save()
        dispatch = Dispatch(request)
        dispatch.confirm_customer_fo_pickup_completed(service.customer.device_token, 'Your trip has been completed', 'Completed trip')
        #payment
        prices = calculate_fare_for_passenger_pickup(service)
        send_email_invoice_for_pickup(service.customer.user, "", prices)
        # calculate_fare_for_passenger_pickup(service)
        return Response({"status": "success"})
    except Exception, e:
        return Response({"message": e}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

@api_view(['GET'])
@authentication_classes([TokenAuthentication])
def get_activity_log(request, job_id):
    service = DeliveryService.objects.get(id=job_id)
    if service.taken_by != Driver.objects.get(user=request.user):
        return Response({"message":"You are now allowed to perform this operation."},status=status.HTTP_500_INTERNAL_SERVER_ERROR)
    logs = ActivityLog.objects.filter(delevery_service=service)
    serializer = LogSerializer(logs)
    return Response(serializer.data)

@api_view(['POST'])
@authentication_classes([TokenAuthentication])
def update_account_info(request, driver_id):
    try:
        driver = Driver.objects.get(id=driver_id)
        if 'plate' in request.FILES:
            plate = request.FILES['plate']
        else:
            plate = None
        if 'vechile_photo' in request.FILES:
            vechile_photo = request.FILES['vechile_photo']
        else:
            vechile_photo = None
        vechile_number = request.POST['vechile_number']
        brand = request.POST['brand']
        name_of_bank = request.POST['name_of_bank']
        account_number = request.POST['account_number']
        bank_code = request.POST['bank_code']
        vechile_type = request.POST['vechile_type']
        vechile = Vechile()
        vechile.driver = driver
        vechile.plate = plate
        vechile.vechile_type = vechile_type
        vechile.vechile_photo = vechile_photo
        vechile.vechile_number = vechile_number
        vechile.brand = brand
        vechile.save()
        bank_ac = BankAccount()
        bank_ac.driver = driver
        bank_ac.name_of_bank = name_of_bank
        bank_ac.account_number = account_number
        bank_ac.bank_code = bank_code
        bank_ac.save()
        return Response({"status": "success"})
    except:
        return Response({"message": constant.VALIDATION_ERROR},status=status.HTTP_500_INTERNAL_SERVER_ERROR)



@api_view(['GET', 'PUT'])
def get_customer_info(request, customer_id):
    if request.method == 'GET':
        try:
            serializer = CustomerSerializer(Customer.objects.get(id=customer_id))
            return Response(serializer.data, status=status.HTTP_200_OK)
        except Exception, e:
            return Response({"message": constant.VALIDATION_ERROR},status=status.HTTP_500_INTERNAL_SERVER_ERROR)
    elif request.method == 'PUT':
        try:
            customer = Customer.objects.get(id=customer_id)
            customer.name = request.POST['name']
            customer.address = request.POST['address']
            customer.contact_no = request.POST['contact_no']
            # customer.nric = request.POST['nric']
            customer.save()
            serializer = CustomerSerializer(customer)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        except Exception, e:
            return Response({'message': "Unable to update customer"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
    else:
        return Response(status=status.HTTP_400_BAD_REQUEST)




@api_view(['GET'])
def dashboard(request, id):
    pass

@api_view(['GET'])
def verification_check(request, user_id):
    driver = Driver.objects.get(id=user_id)
    verify = VerificationSerializer(driver)
    try:
        verify.check(user_id)
        return Response(verify.data, status=status.HTTP_200_OK)
    except:
        return Response(verify.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
def customer_verification(request, verification_id):
    from serializers import CustomerVerificationSerializer
    verification = CustomerVerificationSerializer(data=request.data)
    if verification.is_valid():
        try:
            if verification.verify(verification_id):
                # return Response(verification.data, status=status.HTTP_200_OK)
                return Response({'message': "Successfully verified account"}, status=status.HTTP_200_OK)
        except:
            return Response({'message': "Unable to verify"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    return Response(verification.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
def verification(request, verification_id):
    verification = VerificationSerializer(data=request.data)
    if verification.is_valid():
        try:
            if verification.verify(verification_id):
                # return Response(verification.data, status=status.HTTP_200_OK)
                return Response({'message': "Successfully verified account"}, status=status.HTTP_200_OK)
        except:
            return Response({'message': "Unable to verify"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    return Response(verification.errors, status=status.HTTP_400_BAD_REQUEST)


# Token Logout implemented. I can change the method later if wanted.
@api_view(['GET'])
@authentication_classes([TokenAuthentication])
def logout(request):
    # permission_classes = (permissions.IsAuthenticated,)
    # token_auth = request.POST['auth_token']
    try:
        token = Token.objects.get(user=request.user)
        token.delete()
        return Response({"success": "Successfully logged out."},
                        status=status.HTTP_200_OK)
    except:
        return Response({"falied":"Failed to logout"}, status=status.HTTP_400_BAD_REQUEST)



@api_view(['POST'])
@authentication_classes([TokenAuthentication])
def update_driver_location(request):
    from django.contrib.gis.geos import Point
    try:
        driver = Driver.objects.get(user=request.user)
        try:
            drivers_location = DriverLocation.objects.get(driver_id=driver.id)
        except:
            drivers_location = DriverLocation()
        drivers_location.driver_id = driver.id
        current_point = Point(float(request.POST['last_lng']), float(request.POST['last_lat']))
        drivers_location.location = current_point
        drivers_location.updated_at = datetime.datetime.now()
        drivers_location.save()
        return Response({"status": "success"}, status=status.HTTP_200_OK)
    except Exception, e:
        return Response({"failed": e}, status=status.HTTP_400_BAD_REQUEST)

def image_verify(pic, extensions, the_size):
    ext = os.path.splitext(pic.name)[1]
    if not ext in extensions or pic.size/1024 > the_size:
        return False
    return True

@api_view(['GET'])
def get_driver_location(request):
    """
        List all the active driver
    """
    dispatch = Dispatch(request)
    driver_location = dispatch.get_all_drivers_location()
    serializer = DriversLocationSerializer(driver_location, many=True)
    return Response(serializer.data, status=status.HTTP_200_OK)



#get pricing for vechiletype
@api_view(['GET'])
def get_pickup_price(request, type_id):
    datype = "Weekday"
    today = datetime.datetime.today()
    if today.weekday() == 6:
        datype = "Weekend"
    elif HolidayList.objects.filter(date=today).count() > 0:
        datype = "Weekend"
    price = PickUpRate.objects.filter(vehicle_type=VehicleType.objects.get(id=type_id), day_type=datype)
    serializer = PickUpPriceSerializer(price[0], many=False)
    return Response(serializer.data, status=status.HTTP_200_OK)

@api_view(['GET'])
def get_pickup_status(request, pickup_id):
    try:
        pickup = PickupService.objects.get(id=pickup_id)
        return Response({'pickup_status': pickup.pickup_status}, status=status.HTTP_200_OK)
    except:
        return Response({"failed": "Failed to post the driver's location."}, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET'])
def get_address(request, postal_code):
    try:
        address = Postalcode.objects.get(post_code=postal_code)
        street = Street.objects.get(street_key=address.street_key)
        return Response([{"post_code": address.post_code, "bldg_no": address.buliding_no, "street_name": street.street_name, "bldg_name": "", "floor": "", "unit": "" }], status=status.HTTP_200_OK)
        # Get drivers distance for pickup location
    except:
        return Response([])
@api_view(['GET'])
def get_updated_distance(request, pickup_id):
    pickup = PickupService.objects.get(id=pickup_id)
    driver_location = DriverLocation.objects.get(driver_id=pickup.pickedup_by.id)
    # driver = Driver.objects.get(id=pickup.pickedup_by.id)
    driver_lat = driver_location.get_lat()
    driver_lng = driver_location.get_lng()
    pickup_lat = pickup.pickup_address_lat
    pickup_lng = pickup.pickup_address_lng
    try:
        gmaps = googlemaps.Client(key=constant.GOOGLE_MAP_KEY)

        directions_results = gmaps.distance_matrix(
            origins=pickup_lat + ',' + pickup_lng,
            destinations=str(driver_lat) + ',' + str(driver_lng),
            mode=None,
            language=None,
            avoid=None,
            units="metric",
            departure_time=None,
            arrival_time=None,
            transit_mode=None,
            transit_routing_preference=None,
            traffic_model=None
        )
        approx_distance =  str(directions_results['rows'][0]['elements'][0]['distance']['text'])
        approx_time = str(directions_results['rows'][0]['elements'][0]['duration']['text'])
        request.session['eta'] = approx_time
        request.session['distance'] = approx_distance
        return Response({"distance": approx_distance, "time": approx_time, "driver_lat": str(driver_lat), "driver_lng": str(driver_lng), "pickup_lat": str(pickup_lat), "pickup_lng": str(pickup_lng)}, status=status.HTTP_200_OK)
    except Exception, e:
        return Response({"Error": e}, status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
@authentication_classes([TokenAuthentication])
def get_listed_jobs(request):
    try:
        date_filter = request.POST['date']
        dates = datetime.datetime.strptime(date_filter, '%d %b %Y').date()
    except:
        date_filter = None
    driver = Driver.objects.get(user=request.user.id)
    if request.POST['job_type'] == 'courier':
        if request.POST['job_status'] == 'pending':
            try:
                if date_filter is None:
                    # delevery filter for active job expaires
                    delivery_jobs = DeliveryService.objects.filter(delevery_status='pending').order_by('-id')
                    delivery = []
                    for d in delivery_jobs:
                        if not d.customer.is_corporate:
                            delivery.append(d)
                else:
                    # delevery filter for active job expaires
                    delivery_jobs = DeliveryService.objects.filter(delevery_status='pending', collection_date=dates).order_by('-id')
                    delivery = []
                    for d in delivery_jobs:
                        if not d.customer.is_corporate:
                            delivery.append(d)
                delivery_serializer = DeleveryServiceSerializer(delivery, many=True)
                return Response(delivery_serializer.data, status=status.HTTP_200_OK)
            except Exception, e:
                return Response({'message': e}, status=status.HTTP_400_BAD_REQUEST)

        elif request.POST['job_status'] == 'assigned':
            try:
                delivery_jobs = DeliveryService.objects.filter(delevery_status='assigned', taken_by=driver.id, auto_assigned=True).order_by('-id')
                delivery_serializer = DeleveryServiceSerializer(delivery_jobs, many=True)
                return Response(delivery_serializer.data, status=status.HTTP_200_OK)
            except Exception, e:
                return Response({'message': e}, status=status.HTTP_400_BAD_REQUEST)

        elif request.POST['job_status'] == 'super-assigned':
            try:
                delivery_jobs = DeliveryService.objects.filter(delevery_status='assigned', taken_by=driver.id,
                                                               auto_assigned=False).order_by('-id')
                delivery_serializer = DeleveryServiceSerializer(delivery_jobs, many=True)
                return Response(delivery_serializer.data, status=status.HTTP_200_OK)
            except Exception, e:
                return Response({'message': e}, status=status.HTTP_400_BAD_REQUEST)

        elif request.POST['job_status'] == 'taken':
            try:
                delivery_jobs = DeliveryService.objects.filter(delevery_status='taken', taken_by=driver.id).order_by('-id')
                delivery_serializer = DeleveryServiceSerializer(delivery_jobs, many=True)
                return Response(delivery_serializer.data, status=status.HTTP_200_OK)
            except Exception, e:
                return Response({'message': e}, status=status.HTTP_400_BAD_REQUEST)

        elif request.POST['job_status'] == 'completed':
            try:
                delivery_jobs = DeliveryService.objects.filter(delevery_status='completed', taken_by=driver.id).order_by('id')
                delivery_serializer = DeleveryServiceSerializer(delivery_jobs, many=True)
                return Response(delivery_serializer.data, status=status.HTTP_200_OK)
            except Exception, e:
                return Response({'message': e}, status=status.HTTP_400_BAD_REQUEST)

        elif request.POST['job_status'] == 'cancelled':
            try:
                delivery_jobs = DeliveryService.objects.filter(delevery_status='cancelled', taken_by=driver.id).order_by('-id')
                delivery_serializer = DeleveryServiceSerializer(delivery_jobs, many=True)
                return Response(delivery_serializer.data, status=status.HTTP_200_OK)
            except Exception, e:
                return Response({'message': e}, status=status.HTTP_400_BAD_REQUEST)

        else:
            return Response({'message': 'Wrong job status search.'}, status=status.HTTP_400_BAD_REQUEST)

    #
    #
    #   Pickup drivers
    #
    #

    elif request.POST['job_type'] == 'pickup':
        if request.POST['job_status'] == 'pending':
            try:
                vehicle = Vechile.objects.get(driver=driver.id)
                pickup_jobs = PickupService.objects.filter(pickup_status='pending', vehicle_type=vehicle.vechile_type).order_by('-id')
                try:
                    pickup_serializer = PickupServiceSerializer(pickup_jobs, many=True)
                    return Response(pickup_serializer.data, status=status.HTTP_200_OK)
                except Exception, e:
                    return Response({'message': e}, status=status.HTTP_400_BAD_REQUEST)
            except Exception, e:
                return Response({'message': e}, status=status.HTTP_400_BAD_REQUEST)

        elif request.POST['job_status'] == 'assigned':
            try:
                pickup_jobs = PickupService.objects.filter(pickup_status='assigned', pickedup_by=driver.id).order_by('-id')
                pickup_serializer = PickupServiceSerializer(pickup_jobs, many=True)
                return Response(pickup_serializer.data, status=status.HTTP_200_OK)
            except Exception, e:
                return Response({'message': e}, status=status.HTTP_400_BAD_REQUEST)

        elif request.POST['job_status'] == 'taken':
            try:
                pickup_jobs = PickupService.objects.filter(pickup_status='taken', pickedup_by=driver.id).order_by('-id')
                pickup_serializer = PickupServiceSerializer(pickup_jobs, many=True)
                return Response(pickup_serializer.data, status=status.HTTP_200_OK)
            except Exception, e:
                return Response({'message': e}, status=status.HTTP_400_BAD_REQUEST)

        elif request.POST['job_status'] == 'completed':
            try:
                pickup_jobs = PickupService.objects.filter(pickup_status='completed', pickedup_by=driver.id).order_by('-id')
                pickup_serializer = PickupServiceSerializer(pickup_jobs, many=True)
                return Response(pickup_serializer.data, status=status.HTTP_200_OK)
            except Exception, e:
                return Response({'message': e}, status=status.HTTP_400_BAD_REQUEST)

        elif request.POST['job_status'] == 'cancelled':
            try:
                pickup_jobs = PickupService.objects.filter(pickup_status='cancelled', pickedup_by=driver.id).order_by('-id')
                pickup_serializer = PickupServiceSerializer(pickup_jobs, many=True)
                return Response(pickup_serializer.data, status=status.HTTP_200_OK)
            except Exception, e:
                return Response({'message': e}, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({'message': 'Wrong job status search.'}, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
@authentication_classes([TokenAuthentication])
def refined_job_list(request):
    # start_date = request.POST['start_date']
    start_date = datetime.datetime.strptime(request.POST['start_date'], '%d %b %Y').date()
    # end_date = request.POST['end_date']
    end_date = datetime.datetime.strptime(request.POST['end_date'], '%d %b %Y').date() + datetime.timedelta(days=1)
    job_type = request.POST['job_type']
    if job_type == 'courier':
        try:
            try:
                driver = Driver.objects.get(user=request.user)
                courier = DeliveryService.objects.filter(taken_by=driver.id, delevery_status='completed',
                                                         job_complete_time__range=[start_date, end_date]).order_by('-id')
            except:
                courier = None
            courier_serializer = DeleveryServiceSerializer(courier, many=True)
            return Response(courier_serializer.data, status=status.HTTP_200_OK)
        except Exception, e:
            return Response({'message': e}, status=status.HTTP_400_BAD_REQUEST)

    elif job_type == 'pickup':
        try:
            try:
                driver = Driver.objects.get(user=request.user)
                pickup = PickupService.objects.filter(pickedup_by=driver.id, pickup_status='completed',
                                                         job_complete_time__range=[start_date, end_date]).order_by('-id')
            except:
                pickup = None
            pickup_serializer = PickupServiceSerializer(pickup, many=True)
            return Response(pickup_serializer.data, status=status.HTTP_200_OK)
        except Exception, e:
            return Response({'message': e}, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
@authentication_classes([TokenAuthentication])
def available_toggle(request):
    activity_status = request.POST['status']
    try:
        if activity_status == '0':
            try:
                driver = Driver.objects.get(user=request.user.id)
                driver.is_active = False
                driver.save()
                return Response({'message': 'Your status is changed to inactive'}, status=status.HTTP_200_OK)
            except Exception, e:
                return Response({'message': e}, status=status.HTTP_400_BAD_REQUEST)
        else:
            try:
                driver = Driver.objects.get(user=request.user.id)
                driver.is_active = True
                driver.save()
                return Response({'message': 'Your status is changed to active'}, status=status.HTTP_200_OK)
            except Exception, e:
                return Response({'message': e}, status=status.HTTP_400_BAD_REQUEST)
    except Exception, e:
        return Response({'message': e}, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
@authentication_classes([TokenAuthentication])
def posted_job_by_date(request):
    start_date = datetime.datetime.strptime(request.POST['start_date'], '%d %b %Y').date()
    end_date = datetime.datetime.strptime(request.POST['end_date'], '%d %b %Y').date()
    job_type = request.POST['job_type']
    if job_type == 'courier':
        try:
            try:
                courier = DeliveryService.objects.filter(delevery_status='pending',
                                                         delevery_date__range=[start_date, end_date]).order_by('-id')
            except:
                courier = None
            courier_serializer = DeleveryServiceSerializer(courier, many=True)
            return Response(courier_serializer.data, status=status.HTTP_200_OK)
        except Exception, e:
            return Response({'message': e}, status=status.HTTP_400_BAD_REQUEST)

    elif job_type == 'pickup':
        try:
            try:
                # driver = Driver.objects.get(user=request.user)
                pickup = PickupService.objects.filter(pickup_status='pending',
                                                         created_at__range=[start_date, end_date]).order_by('-id')
            except:
                pickup = None
            pickup_serializer = PickupServiceSerializer(pickup, many=True)
            return Response(pickup_serializer.data, status=status.HTTP_200_OK)
        except Exception, e:
            return Response({'message': e}, status=status.HTTP_400_BAD_REQUEST)


#api for tracking location of a tripe
@api_view(['post'])
@authentication_classes([TokenAuthentication])
def location_tracker(request, job_id):
    # try:
    data = ast.literal_eval(request.POST['data'])
    trip = PickupService.objects.get(id= job_id)
    for location in data:
        track = Tracking()
        track.trip = trip
        track.lat = location['lat']
        track.lng = location['lng']
        track.data_id = location['id']
        track.save()
    return Response({'status': 'success'}, status=status.HTTP_200_OK)
    # except:
    #     return Response({'status': 'falure'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

@api_view(['post'])
# @authentication_classes([TokenAuthentication])
def get_pickup_job_details(request):
    try:
        pick_up_job = request.POST['job_id']
        pickup = PickupService.objects.get(id=pick_up_job)
        searilizer = PickupServiceSerializer(pickup, many=False)
        return Response(searilizer.data, status=status.HTTP_200_OK)
    except:
        return Response({"statuse": "falure"}, status=status.HTTP_400_BAD_REQUEST)


@api_view(['post'])
@authentication_classes([TokenAuthentication])
def get_fare_details(request, job_id):
    try:
        pickup = PickupService.objects.get(id=job_id)
        prices = calculate_fare_for_passenger_pickup(pickup)
        return Response(prices, status=status.HTTP_200_OK)
    except:
        return Response({"status": "falure"}, status= status.HTTP_400_BAD_REQUEST)

@api_view(['post'])
@authentication_classes([TokenAuthentication])
def update_account_settings(request):
    try:
        driver_id = request.POST.get("driver_id", "")
        profile_image = request.FILES['profile_image']
        name = request.POST.get("name", "")
        mobile_no = request.POST.get("mobile_no", "")
        email_address = request.POST.get("email_address", "")
        company_name = request.POST.get("company_name", "")
        name_of_bank = request.POST.get("name_of_bank", "")
        bank_account_no = request.POST.get("bank_account_no", "")
        bank_code = request.POST.get("bank_code", "")
        bank_branch_code = request.POST.get("bank_branch_code", "")
        #Create Driver Inatace
        driver = Driver.objects.get(id=driver_id)
        #Update User
        user = driver.user
        user.email = email_address
        user.save()
        #Update Driver
        driver.name = name
        driver.email = email_address
        driver.profile_image = profile_image
        driver.mobile_no = mobile_no
        driver.company = company_name
        driver.save()
        #Update Bank Account Info
        bank = BankAccount.objects.get(driver=driver)
        bank.name_of_bank = name_of_bank
        bank.account_number = bank_account_no
        bank.bank_code = bank_code
        bank.branch_code = bank_branch_code
        bank.save()
        return Response({"status": "success"}, status= status.HTTP_200_OK)
    except Exception, e:
        return Response({"status": "failure"}, status = status.HTTP_500_INTERNAL_SERVER_ERROR)

@api_view(['post'])
@authentication_classes([TokenAuthentication])
def change_password_for_driver(request):
    driver_id = request.POST.get("driver_id", "")
    current_pass = request.POST.get("current_pass", "")
    new_pass = request.POST.get("new_pass", "")
    # Get Driver Inatance
    driver = Driver.objects.get(id=driver_id)
    user = driver.user
    user = authenticate(username=user.username, password=current_pass)
    if user is not None:
        #change Pass
        user.set_password(new_pass)
        user.save()
        return Response({"status": "success"}, status=status.HTTP_200_OK)
    else:
        # Raise Error
        return Response({"status": "falure"}, status=status.HTTP_401_UNAUTHORIZED)

@api_view(['get'])
@authentication_classes([TokenAuthentication])
def get_courier_price_details(request):
    packages = PackageSize.objects.all()
    serializer = SizeSerializer(packages, many=True)
    return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(['post'])
@authentication_classes([TokenAuthentication])
def pickup_job_view(request):
    pickup_id = request.POST.get("pickup_id", "")
    driver_id = request.POST.get("driver_id", "")
    if pickup_id is not "" and driver_id is not "":
        try:
            pickup = PickupService.objects.get(id=pickup_id)
            driver = Driver.objects.get(id=driver_id)
            if log_view_pickup(pickup, driver):
                return Response({"status": "success"},status=status.HTTP_200_OK)
        except:
            pass
    return Response({"status": "falure"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


@api_view(['get'])
@authentication_classes([TokenAuthentication])
def get_drivers_details(request):
    driver = Driver.objects.get(user=request.user)
    weekly_earning = get_weekly_earnings(driver)
    acceptacce = get_acceptacce_rate(driver)
    cancellation = get_cencell_rate(driver)
    avg_rating = get_avarage_rattings(driver)
    message = AdminMessage.objects.filter(message_to_driver=driver)
    message_serializer = MessageSeralizer(message, many=True)
    token = Token.objects.get(user=request.user)
    serializer = DriverSerializer(driver)

    return Response({'token': token.key, 'type': 'Driver', 'user_data': serializer.data, 'rating': avg_rating,
                                         'weekly_earning': weekly_earning, 'acceptance': acceptacce,
                                         'cancellation': cancellation, 'message': message_serializer.data}, status=status.HTTP_200_OK)


def test_gmap(request):
    gmaps = googlemaps.Client(key=constant.GOOGLE_MAP_KEY)
    # gmaps.
    return HttpResponse("test")


def get_weekly_earnings(driver):
    datetoday = datetime.datetime.today()
    first_day = date(week_magic(datetoday)[0].year, week_magic(datetoday)[0].month, week_magic(datetoday)[0].day)
    last_day = date(week_magic(datetoday)[1].year, week_magic(datetoday)[1].month, week_magic(datetoday)[1].day)
    earnings = earning_from_date_range(driver.id, first_day, last_day)
    # return earnings
    total_money = 0
    # print 'ok'
    if earnings:
        print 'test'
        for earning in earnings:
            try:
                drivers_money = earning.payment.total_cost - earning.payment.admin_fee
                total_money = total_money + drivers_money
            except:
                total_money = total_money + 0
    return total_money


def earning_from_date_range(id, first, last):
    type = get_driver_type(id)
    if type == 'courier':
        earnings = delivery_earnings_from_date(id, first, last)
        return earnings
    elif type == 'pickup':
        earnings = pickup_earnings_from_date(id, first, last)
        return earnings
    elif type == 'both':
        delivery_earnings = delivery_earnings_from_date(id, first, last)
        pickup_earnings = pickup_earnings_from_date(id, first, last)
        if delivery_earnings and not pickup_earnings:
            # print 'No delivery'
            return delivery_earnings
        if pickup_earnings and not delivery_earnings:
            # print 'No pickup'
            return pickup_earnings
        if not delivery_earnings and not pickup_earnings:
            value = None
            return value
        else:
            return delivery_earnings + pickup_earnings


def get_driver_type(id):
    driver = Driver.objects.get(id=id)
    return driver.driver_service_type


def delivery_earnings_from_date(id, start, end):
    start_date = datetime.datetime.strptime(start.strftime('%d-%m-%Y'), '%d-%m-%Y')
    # print start_date
    end_date = datetime.datetime.strptime(end.strftime('%d-%m-%Y'), '%d-%m-%Y')
    earnings = []
    delivery = DeliveryService.objects.filter(taken_by=id, delevery_status='completed', job_complete_time__range=[start_date, end_date])
    if delivery.count() > 0:
        for obj in delivery:
            try:
                payment = Payment.objects.get(delevery_service=obj)
                obj.payment = payment
                obj.type = 'DELIVERY'
            except:
                obj.type = 'DELIVERY'
                obj.payment = None
            earnings.append(obj)
            # print earnings
        return earnings
    else:
        earnings = None
        return earnings


def pickup_earnings_from_date(id, start, end):
    start_date = datetime.datetime.strptime(start.strftime('%d-%m-%Y'), '%d-%m-%Y')
    end_date = datetime.datetime.strptime(end.strftime('%d-%m-%Y'), '%d-%m-%Y')
    earnings = []
    pickup = PickupService.objects.filter(pickedup_by=id, pickup_status='completed', job_complete_time__range=[start_date, end_date])
    if pickup.count() > 0:
        for obj in pickup:
            try:
                payment = Payment.objects.get(pickup_service=obj)
                obj.type = 'PICKUP'
                obj.payment = payment
            except:
                obj.type = 'PICKUP'
                obj.payment = None
            earnings.append(obj)
        return earnings
    else:
        earnings = None
        return earnings


def week_magic(day):
    day_of_week = day.weekday()

    to_beginning_of_week = datetime.timedelta(days=day_of_week)
    beginning_of_week = day - to_beginning_of_week

    to_end_of_week = datetime.timedelta(days=6 - day_of_week)
    end_of_week = day + to_end_of_week

    return (beginning_of_week, end_of_week)

#########Customer API#####################
@api_view(['POST'])
def create_customer(request):
    serializer = CustomerSerializer(data=request.data)
    user = User.objects.filter(email=request.data['email'])
    phone = UserProfile.objects.filter(phone_number=request.data['contact_no'])
    if serializer.is_valid():
        try:
            if user.count() == 0:
                serializer.save()
                ######SEND SMS TO CUSTOMER HERE######
                number = request.data['contact_no']
                customer = Customer.objects.get(email=request.data['email'])
                code = customer.verification
                send_sms(code, number)
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            else:
                return Response({"message": "Email already exists"}, status=status.HTTP_400_BAD_REQUEST)
        except Exception, e:
            return Response({'message': e}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def verify_code(request):
    try:
        code = request.POST['code']
        customer = Customer.objects.filter(verification=code)
        if customer.count() > 0:
            customer = Customer.objects.get(verification=code)
            serializer = CustomerSerializer(customer)
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            return Response({'message': "Invalid verification code"}, status=status.HTTP_404_NOT_FOUND)
    except Exception, e:
        return Response({'message': "Unable to verify customer code"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


@api_view(['POST'])
def complete_registration(request, customer_id):
    try:
        customer = Customer.objects.get(id=customer_id)
        customer.address = request.POST['address']
        customer.postal_code = request.POST['postal_code']
        customer.save()
        serializer = CustomerSerializer(customer)
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    except Exception, e:
        return Response({'message': e}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)



@api_view(['GET'])
@authentication_classes([TokenAuthentication])
def get_customer_couriers(request):
    try:
        complete_delivery = DeleveryServiceSerializer(DeliveryService.objects.filter(Q(delevery_status="completed") |
                                                                                     Q(delevery_status="cancelled"),
                                                                                     customer=request.user.id), many=True)
        active_delivery = DeleveryServiceSerializer(DeliveryService.objects.filter(Q(delevery_status="assigned") |
                                                                                     Q(delevery_status="pending") |
                                                                                     Q(delevery_status="taken") |
                                                                                     Q(delevery_status="pickedup"),
                                                                                     customer=request.user.id), many=True)
        return Response({"active": active_delivery.data, "completed": complete_delivery.data}, status=status.HTTP_200_OK)
    except Exception, e:
        return Response({'message': "Unable to fetch bookings"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)



@api_view(['GET'])
@authentication_classes([TokenAuthentication])
def get_customer_pickups(request):
    try:
        complete_pickup = PickupServiceSerializer(PickupService.objects.filter(Q(pickup_status="completed") |
                                                                                     Q(pickup_status="cancelled"),
                                                                                     customer=request.user.id), many=True)
        active_pickup = PickupServiceSerializer(PickupService.objects.filter(Q(pickup_status="assigned") |
                                                                                   Q(pickup_status="pending") |
                                                                                   Q(pickup_status="taken") |
                                                                                   Q(pickup_status="pickedup"),
                                                                                   customer=request.user.id), many=True)
        return Response({"active": active_pickup.data, "completed": complete_pickup.data},
                        status=status.HTTP_200_OK)
    except Exception, e:
        return Response({'message': "Unable to fetch bookings"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


@api_view(['GET'])
def active_bookings(request, customer_id):
    try:
        delivery_serializer = DeleveryServiceSerializer(DeliveryService.objects.filter(Q(delevery_status="pending") |
                                                                                       Q(delevery_status="assigned") | Q(delevery_status="taken"),
                                                                                       customer=customer_id).order_by('-id'), many=True)

        # completed_delivery_serializer = DeleveryServiceSerializer(DeliveryService.objects.filter(delevery_status="completed",
        #                                                                                customer=customer_id).order_by('-id'), many=True)

        pickup_serializer = PickupServiceSerializer(PickupService.objects.filter(pickup_status="completed",
                                                                                 customer=customer_id).order_by('-id'), many=True)
        # try:
        delivery = DeliveryService.objects.filter(delevery_status="completed", rated="not_rated")
        pickup = PickupService.objects.filter(pickup_status="completed", rated="not_rated")
        if delivery.count() > 0:
            completed_delivery = DeleveryServiceSerializer(delivery[0])
            if pickup.count() > 0:
                completed_pickup = PickupServiceSerializer(pickup[0])
                return Response({'delivery': delivery_serializer.data, 'pickup': pickup_serializer.data,
                                 'completed_delivery': completed_delivery.data,
                                 'completed_pickup': completed_pickup.data},
                                status=status.HTTP_200_OK)
                # return Response({'dashboard': {'active': delivery_serializer.data,
                #                                'completed': {'pickup': pickup_serializer.data,
                #                                              'courier': delivery_serializer.data}}},
                #                 status=status.HTTP_200_OK)

            else:
                print "NONE"
                completed_pickup = None
                # return Response({'dashboard': {'active': delivery_serializer.data,
                #                                'completed': {'pickup': pickup_serializer.data,
                #                                              'courier': delivery_serializer.data}}},
                #                 status=status.HTTP_200_OK)

                return Response({'delivery': delivery_serializer.data, 'pickup': pickup_serializer.data,
                                 'completed_delivery': completed_delivery.data,
                                 'completed_pickup': completed_pickup},
                                status=status.HTTP_200_OK)
        else:
            completed_delivery = None
            if pickup.count() > 0:
                completed_pickup = PickupServiceSerializer(pickup[0])
                return Response({'delivery': delivery_serializer.data, 'pickup': pickup_serializer.data,
                                 'completed_delivery': completed_delivery,
                                 'completed_pickup': completed_pickup.data},
                                status=status.HTTP_200_OK)
                # return Response({'dashboard': {'active': delivery_serializer.data,
                #                                'completed': {'pickup': pickup_serializer.data,
                #                                              'courier': delivery_serializer.data}}},
                #                 status=status.HTTP_200_OK)

            else:
                completed_pickup = None
                return Response({'delivery': delivery_serializer.data, 'pickup': pickup_serializer.data,
                                 'completed_delivery': completed_delivery,
                                 'completed_pickup': completed_pickup},
                                status=status.HTTP_200_OK)
                # return Response({'dashboard': {'active': delivery_serializer.data,
                #                                'completed': {'pickup': pickup_serializer.data,
                #                                              'courier': delivery_serializer.data}}},
                #                 status=status.HTTP_200_OK)

    except Exception, e:
        return Response({'message': e}, status=status.HTTP_400_BAD_REQUEST)



@api_view(['GET'])
def active_bookings_alternative(request, customer_id):
    try:
        delivery_serializer = DeleveryServiceSerializer(DeliveryService.objects.filter(Q(delevery_status="pending") |
                                                                                       Q(delevery_status="assigned") | Q(delevery_status="taken"),
                                                                                       customer=customer_id).order_by('-id'), many=True)

        completed_delivery_serializer = DeleveryServiceSerializer(DeliveryService.objects.filter(delevery_status="completed",
                                                                                       customer=customer_id).order_by('-id'), many=True)

        pickup_serializer = PickupServiceSerializer(PickupService.objects.filter(pickup_status="completed",
                                                                                 customer=customer_id).order_by('-id'), many=True)
        # try:
        delivery = DeliveryService.objects.filter(delevery_status="completed", rated="not_rated")
        pickup = PickupService.objects.filter(pickup_status="completed", rated="not_rated")
        if delivery.count() > 0:
            completed_delivery = DeleveryServiceSerializer(delivery[0])
            if pickup.count() > 0:
                completed_pickup = PickupServiceSerializer(pickup[0])
                return Response({'dashboard': {'active': delivery_serializer.data,
                                               'completed': {'pickup': pickup_serializer.data,
                                                             'courier': completed_delivery_serializer.data}}},
                                status=status.HTTP_200_OK)

            else:
                # print "NONE"
                completed_pickup = None
                return Response({'dashboard': {'active': delivery_serializer.data,
                                               'completed': {'pickup': pickup_serializer.data,
                                                             'courier': completed_delivery_serializer.data}}},
                                status=status.HTTP_200_OK)
        else:
            completed_delivery = None
            if pickup.count() > 0:
                completed_pickup = PickupServiceSerializer(pickup[0])
                return Response({'dashboard': {'active': delivery_serializer.data,
                                               'completed': {'pickup': pickup_serializer.data,
                                                             'courier': completed_delivery_serializer.data}}},
                                status=status.HTTP_200_OK)

            else:
                completed_pickup = None
                return Response({'dashboard': {'active': delivery_serializer.data,
                                               'completed': {'pickup': pickup_serializer.data,
                                                             'courier': completed_delivery_serializer.data}}},
                                status=status.HTTP_200_OK)

    except Exception, e:
        return Response({'message': e}, status=status.HTTP_400_BAD_REQUEST)



@api_view(['POST'])
@authentication_classes([TokenAuthentication])
def rate_driver(request):
    try:
        job_type = request.POST['job_type']
        job_id = request.POST['job_id']
        rate = request.POST['rating']
        comment = request.POST['comment']
        if job_type == 'delivery':
            delivery = DeliveryService.objects.get(id=job_id)
            rating = DriverRating()
            rating.driver = delivery.taken_by
            rating.delivery = delivery
            rating.rating = rate
            rating.rated_by = delivery.customer
            rating.comment = comment
            rating.save()
            delivery.rated = 'rated'
            delivery.save()
        elif job_type == 'pickup':
            pickup = PickupService.objects.get(id=job_id)
            rating = DriverRating()
            rating.driver = pickup.pickedup_by
            rating.pickup = pickup
            rating.rated_by = pickup.customer
            rating.rating = rate
            rating.comment = comment
            rating.save()
            pickup.rated = 'rated'
            pickup.save()
        else:
            return Response({'message': 'Wrong keyword'}, status=status.HTTP_400_BAD_REQUEST)
        return Response({'message': 'Driver has been successfully rated'}, status=status.HTTP_200_OK)

    except Exception, e:
        return Response({'message': e}, status=status.HTTP_400_BAD_REQUEST)





@api_view(['GET'])
@authentication_classes([TokenAuthentication])
def get_pickup_detail(request, pickup_id):
    try:
        pickup_serializer = PickupServiceSerializer(PickupService.objects.get(id=pickup_id))
        log_serializer = LogSerializer(ActivityLog.objects.filter(pickup_service=pickup_id), many=True)
        try:
            payment_serializer = PaymentSerializer(Payment.objects.get(pickup_service=pickup_id))
            return Response({'pickup': pickup_serializer.data, 'payment': payment_serializer.data, 'log': log_serializer.data},
                        status=status.HTTP_200_OK)
        except:
            return Response({'pickup': pickup_serializer.data, 'payment': 'not available', 'log': log_serializer.data},
                            status=status.HTTP_200_OK)
    except Exception, e:
        return Response({'error': e}, status=status.HTTP_400_BAD_REQUEST)



@api_view(['GET'])
@authentication_classes([TokenAuthentication])
def get_delivery_detail(request, delivery_id):
    try:
        delivery_serializer = DeleveryServiceSerializer(DeliveryService.objects.get(id=delivery_id))
        log_serializer = LogSerializer(ActivityLog.objects.filter(delevery_service=delivery_id), many=True)
        try:
            payment_serializer = PaymentSerializer(Payment.objects.filter(delevery_service=delivery_id), many=True)
            try:
                message_serializer = AdminMessageSerializer(AdminMessage.objects.filter(delivery=delivery_id), many=True)
                return Response({'delivery': delivery_serializer.data, 'payment': payment_serializer.data,
                                 'log': log_serializer.data, 'message': message_serializer.data},
                                status=status.HTTP_200_OK)
            except:
                return Response({'delivery': delivery_serializer.data, 'payment': payment_serializer.data,
                                 'log': log_serializer.data, 'message': 'no messages'},
                                status=status.HTTP_200_OK)
        except Exception, e:
            print str(e)
            try:
                message_serializer = AdminMessageSerializer(AdminMessage.objects.filter(delivery=delivery_id),
                                                            many=True)
                return Response({'delivery': delivery_serializer.data, 'payment': 'not available',
                                 'log': log_serializer.data, 'message': message_serializer.data},
                                status=status.HTTP_200_OK)
            except:
                return Response({'delivery': delivery_serializer.data, 'payment': 'not available',
                                 'log': log_serializer.data, 'message': 'no messages'},
                                status=status.HTTP_200_OK)
    except Exception, e:
        return Response({'error': e}, status=status.HTTP_400_BAD_REQUEST)



@api_view(['POST'])
@authentication_classes([TokenAuthentication])
def courier_booking(request):
    try:
        total_cost = float(request.POST['cost'])
        collection_address = request.POST['collection_address']
        delivery_address = request.POST['delivery_address']
        collection_name = request.POST['collection_name']
        delivery_name = request.POST['delivery_name']
        collection_number = request.POST['collection_number']
        delivery_number = request.POST['delivery_number']
        try:
            collection_remarks = request.POST['collection_remarks']
        except:
            collection_remarks = None
        try:
            delivery_remarks = request.POST['delivery_remarks']
        except:
            delivery_remarks = None
        collection_date = datetime.datetime.strptime(request.POST['collection_date'], '%d %b %Y').date()
        delivery_date = datetime.datetime.strptime(request.POST['delivery_date'], '%d %b %Y').date()
        collection_time = request.POST['collection_time']
        delivery_time = request.POST['delivery_time']

        customer = Customer.objects.get(user=request.user)

        delivery = DeliveryService()
        delivery.customer = customer
        delivery.collection_name = collection_name
        delivery.delivery_name = delivery_name
        delivery.collection_address = collection_address
        delivery.delivery_address = delivery_address
        delivery.collection_contact_no = collection_number
        delivery.delivery_contact_no = delivery_number
        delivery.collection_remark = collection_remarks
        delivery.delevery_remarks = delivery_remarks
        delivery.collection_date = collection_date
        delivery.delevery_date = delivery_date
        delivery.collection_time = collection_time
        delivery.delevery_time = delivery_time
        delivery.rated = 'not_rated'
        delivery.total_cost = total_cost
        delivery.status = False
        delivery.payment_status = False
        try:
            gmaps = googlemaps.Client(key='AIzaSyD4qpAFSCX_E_hTOkpw0t53Dv3ULOQ2lw4')

            directions_results = gmaps.distance_matrix(
                request.POST['collection_address'],
                request.POST['delivery_address'],
                mode=None,
                language=None,
                avoid=None,
                units="metric",
                departure_time=None,
                arrival_time=None,
                transit_mode=None,
                transit_routing_preference=None,
                traffic_model=None
            )

            approx_distance = str(directions_results['rows'][0]['elements'][0]['distance']['text'])
            approx_time = str(directions_results['rows'][0]['elements'][0]['duration']['text'])
            delivery.eta = approx_time
            delivery.distance = approx_distance
        except Exception, e:
            print e
            delivery.eta = None
            delivery.distance = None
            # return Response({"error": e}, status=status.HTTP_400_BAD_REQUEST)
        delivery.save()

        try:
            data = ast.literal_eval(request.POST['package'])
        except Exception, e:
            return Response({"error": str(e)}, status=status.HTTP_400_BAD_REQUEST)

        cart = Cart(request)
        scheType = ""


        period = cart.get_time_difference(request.POST['delivery_date'], request.POST['collection_date'])
        # print 'period:' + str(period)

        if period == 0:
            scheType = "Same Day"
        elif period == 1:
            scheType = "Next Day"
        else:
            scheType = "Two Working Days"

        pack = []
        try:
            for package in data:
                # print package['size']
                size = PackageSize.objects.get(id=int(package['size']))
                package['weight'] = size.weight
                package['length'] = size.length
                package['width'] = size.width
                package['height'] = size.height
                pack.append(package)
            # print data['size']
        except Exception, e:
            print e
            return Response({"error": str(e)}, status=status.HTTP_400_BAD_REQUEST)


        try:
            # print "OK"
            card_id = request.POST['card_id']
            card = CreditCard.objects.get(id=card_id)
            stripe.api_key = constant.STRIPE_SECRECT_KEY
            stripe.Charge.create(
                amount=int(total_cost * 100),
                currency="sgd",
                customer=card.strite_id)
            payment = Payment()
            payment.customer = customer
            payment.delevery_service = delivery
            payment.total_cost = total_cost
            payment.paid_amount = total_cost
            payment.save()
            delivery.status = True
            delivery.payment_status = True
            delivery.save()
            log = ActivityLog()
            log.log_message = "Your payment has been received."
            log.delevery_service = delivery
            log.log_type = "The payment is done."
            log.delevery_status = "paid"
            log.save()
        except Exception, e:
            print e
            pass
            # if request.POST['card_token']:
            try:
                card_token = request.POST['card_token']
                stripe.api_key = constant.STRIPE_SECRECT_KEY
                # try:
                stripe.Charge.create(
                    amount=int(total_cost * 100),
                    currency='sgd',
                    source=card_token,
                    description='courier payment'
                )
                # except Exception, e:
                #     return Response({"error": str(e)}, status=status.HTTP_400_BAD_REQUEST)
                payment = Payment()
                payment.customer = customer
                payment.delevery_service = delivery
                payment.total_cost = total_cost
                payment.paid_amount = total_cost
                payment.save()
                delivery.status = True
                delivery.payment_status = True
                delivery.save()
                log = ActivityLog()
                log.log_message = "Your payment has been received."
                log.delevery_service = delivery
                log.log_type = "The payment is done."
                log.delevery_status = "paid"
                log.save()
            except Exception, e:
                print e
                return Response({"error": str(e)}, status=status.HTTP_400_BAD_REQUEST)
            # else:
            #     return Response({"error": str(e)}, status=status.HTTP_400_BAD_REQUEST)


        for item in pack:
            try:
                package = Package()
                package.service = delivery
                package.quantity = item['quantity']
                package.user = request.user

                if item['image']:
                    try:
                        image_data = item['image']
                        name = 'temp.jpg'
                        data = ContentFile(base64.b64decode(str(image_data)), name)
                        package.image = data
                        # print 'done'
                    except Exception, e:
                        print e
                        return Response({"error": str(e)}, status=status.HTTP_400_BAD_REQUEST)
                else:
                    package.image = None
                package.weight = item['weight']
                package.length = item['length']
                package.width = item['width']
                package.height = item['height']
                package.size = PackageSize.objects.get(id=item['size'])
                package.unit_price = item['unit_price']
                package.save()

            except Exception, e:
                return Response({"error": e}, status=status.HTTP_400_BAD_REQUEST)

        delivery_serializer = DeleveryServiceSerializer(delivery)

        return Response({"order": delivery_serializer.data},
                        status=status.HTTP_200_OK)
    except Exception, e:
        return Response({"error": e}, status=status.HTTP_400_BAD_REQUEST)



@api_view(['GET'])
@authentication_classes([TokenAuthentication])
def get_credit_card_information(request):
    try:
        customer = Customer.objects.get(user=request.user.id)
        credit_card = CreditCard.objects.filter(custmer=customer)
        credit_card_serializer = CreditCardSerializer(credit_card, many=True)
        return Response(credit_card_serializer.data, status=status.HTTP_200_OK)
    except Exception, e:
        return Response({"message": str(e)}, status=status.HTTP_400_BAD_REQUEST)



@api_view(['GET'])
def get_pricing(request):
    pricing = PackageSize.objects.all().order_by('start_weight')
    pricings = []
    counter = 1
    for p in pricing:
        p.weight_index = counter
        counter = counter + 1
        pricings.append(p)
    # print pricing[0].corp_range_rat
    packageSizeInfoSerializer = PackageSizeInfoSerializer(pricings, many=True)
    express = ExpressWindows.objects.all()
    expressPricingSerializer = ExpressWindowsSerializer(express, many=True)
    return Response({'pricing': packageSizeInfoSerializer.data, 'additional_pricing': expressPricingSerializer.data}, status=status.HTTP_200_OK)

################ CUSTOMER RELATED APIS END #######################

############################# Reset Password api. For customer and driver both" ########
@api_view(['POST'])
def reset_password(request):
    is_exists = False
    try:
        User.objects.get(email=request.POST['email'])
        is_exists = True
    except:
        pass
    pass_reste_form = ResetPasswordForm(request.POST)
    if pass_reste_form.is_valid() and is_exists:
        pass_reste_form.save(request)
        return Response({"status": "success"})
    return Response({"error": "Could not find your email address"}, status=status.HTTP_400_BAD_REQUEST)

################################ get Available delivery window ########################

@api_view(['GET'])
def get_deliver_window(request):
    # try:
    delibery_window = DeliveryTiming.objects.all()
    express_window = ExpressWindows.objects.all()
    windows = []
    for window in delibery_window:
        windows.append(window.window_name)
    for window in express_window:
        windows.append(window.window_name)
    return Response({"data": windows}, status=status.HTTP_200_OK)


@api_view(['GET'])
def get_collection_window(request):
    delibery_window = DeliveryTiming.objects.all()
    windows = []
    for window in delibery_window:
        windows.append(window.window_name)
    return Response({"data":windows}, status=status.HTTP_200_OK)




# @authentication_classes([TokenAuthentication])
@api_view(['POST'])
def make_charge(request):
    try:
        amount = request.POST['amount']
        source = request.POST['source']
        description = request.POST['description']
        stripe.api_key = constant.STRIPE_SECRECT_KEY

        stripe.Charge.create(
          amount=amount,
          currency='sgd',
          source=source,
          description=description
        )
        return Response({"status": "success"}, status=status.HTTP_200_OK)
    except:
        return Response({"status": "fail"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)



@authentication_classes([TokenAuthentication])
@api_view(['POST'])
def add_card(request):
    try:
        token = request.POST['token']
        stripe.api_key = constant.STRIPE_SECRECT_KEY
        card = CreditCard()
        customer = Customer.objects.get(user=request.user)
        card.custmer = customer
        stripe_customer = stripe.Customer.create(
                    card= token,
                    description= customer.name,
                    email= customer.email
                    )
        card.strite_id = stripe_customer.id
        card.last_digits = str(stripe_customer.sources.data[0].last4)
        card.name = stripe_customer.sources.data[0].name
        card.expiry_date = str(stripe_customer.sources.data[0].exp_month)+"/"+str(stripe_customer.sources.data[0].exp_year)
        card.brand = str(stripe_customer.sources.data[0].brand)
        card.save()
        return Response({"status":"success"}, status=status.HTTP_200_OK)
    except Exception, e:
        return Response({"status":"fail"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

@authentication_classes([TokenAuthentication])
@api_view(['POST'])
def make_payment_with_card(request):
    try:
        card_id = request.POST['card_id']
        amount = request.POST['amount']
        card = CreditCard.objects.get(id=card_id)
        stripe.api_key = constant.STRIPE_SECRECT_KEY
        stripe.Charge.create(
            amount=int(amount),
            currency="sgd",
            customer=card.strite_id)
        return Response({"status":"success"}, status=status.HTTP_200_OK)
    except Exception, e:
        return Response({"status":"fail"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

@authentication_classes([TokenAuthentication])
@api_view(['POST'])
def remove_card(request):
    card_id = request.POST['card_id']
    try:
        card = CreditCard.objects.get(id=card_id)
        stripe.api_key = constant.STRIPE_SECRECT_KEY
        cu = stripe.Customer.retrieve(card.strite_id)
        cu.delete() 
        card.delete()
        return Response({"status":"success"}, status=status.HTTP_200_OK)
    except:
        return Response({"status":"fail"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


@authentication_classes([TokenAuthentication])
@api_view(['POST'])
def edit_card(request):
    card_id = request.POST['card_id']
    exp_month = request.POST['exp_month']
    exp_year = request.POST['exp_year']
    try:
        card = CreditCard.objects.get(id=card_id)
        stripe.api_key = constant.STRIPE_SECRECT_KEY
        stripe_customer = stripe.Customer.retrieve(card.strite_id)
        source = stripe_customer.sources.data[0].id
        strip_card = stripe_customer.sources.retrieve(source)
        strip_card.exp_year = int(exp_year)
        strip_card.exp_month = int(exp_month)
        strip_card.save()
        card.expiry_date = str(strip_card.exp_month)+"/"+str(strip_card.exp_year)
        card.save()
        return Response({"status":"success"}, status=status.HTTP_200_OK)
    except Exception, e:
        return Response({"status":"fail"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

@authentication_classes([TokenAuthentication])
@api_view(['POST'])
def get_nearest_drivers_location(request):
    """
        getting nearest dribers
    """
    # site_config = site_config = superadmin.views.get_site_configeartions()
    lat = request.POST['lat']
    lng = request.POST['lng']
    dispatch = Dispatch(request)
    # if site_config:
    #     distance = site_config.driver_radius
    # else:
    distance = 5
    nearest_drivers = dispatch.get_nearest_active_driver(lat, lng, distance)
    serializer = DriversLocationSerializer(nearest_drivers, many=True)
    return Response(serializer.data, status=status.HTTP_200_OK)

@authentication_classes([TokenAuthentication])
@api_view(['POST'])
def requer_for_pickup(request):
    """
        Create the pick up requesr and send dispatch 
        request request to neaar driver
    """
    try:
        pickupAddress = request.POST['pickupAddress']
        dropOffAddess = request.POST['dropOffAddess']
        noteForDriver = request.POST['noteForDriver']
        vehicleType = request.POST['vehicleType']
        customer = Customer.objects.get(user=request.user)
        dispatch = Dispatch(request)
        pickup_job = dispatch.create_pick_job(pickupAddress, dropOffAddess, noteForDriver, vehicleType, customer)
        tasks.dispatch_job_to_nearest_driver.delay(pickup_job.id)
        searilizer = PickupServiceSerializer(pickup_job, many=False)
        return Response(searilizer.data, status=status.HTTP_200_OK)
    except Exception, e:
        return Response({"status":e}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


@api_view(['GET'])
def driver_info(request, driver_id):
    """
        Create the pick up requesr and send dispatch
        request request to neaar driver
    """
    try:
        driver = Driver.objects.get(id=driver_id)
        driver_serializer = DriverSerializer(driver)
        return Response(driver_serializer.data, status=status.HTTP_200_OK)
    except Exception, e:
        return Response({"status":e}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


@api_view(['GET'])
def get_driver_location_by_id(request, driver_id):
    """
        Return the last location of a driver
    """
    dispatch = Dispatch(request)
    driver_location = dispatch.get_driver_location_id(driver_id)
    serializer = DriversLocationSerializer(driver_location, many=False)
    return Response(serializer.data, status=status.HTTP_200_OK)



@api_view(['GET'])
def get_driver(requests, driver_id):
    driver = Driver.objects.get(id=driver_id)
    weekly_earning = get_weekly_earnings(driver)
    acceptacce = get_acceptacce_rate(driver)
    cancellation = get_cencell_rate(driver)
    avg_rating = get_avarage_rattings(driver)
    serializer = DriverSerializer(driver)
    return Response({'data': serializer.data, 'rating': avg_rating,'weekly_earning': weekly_earning, 'acceptance': acceptacce,'cancellation': cancellation}, status=status.HTTP_200_OK)


@api_view(['GET'])
def get_cancel_reason(request):
    reason = PickupCancellationReasons.objects.all()
    reason_serializer = PickupCancellationReasonsSerializers(reason, many=True)
    return Response(reason_serializer.data, status=status.HTTP_200_OK)


@authentication_classes([TokenAuthentication])
@api_view(['POST'])
def cancell_pickup_by_customer(request):
    job_id = request.POST['job_id']
    reason = request.POST['reason']
    note = request.POST['note']
    try:
        pickup_job = PickupService.objects.get(id=job_id)
        if pickup_job.customer.user != request.user :
            return Response({'status': 'falure'}, status=status.HTTP_401_UNAUTHORIZED)
        dispatch = Dispatch(request)
        dispatch.cancell_job_by_customer(pickup_job, reason, note)
        return Response({'status': 'success'}, status=status.HTTP_200_OK)
    except:
        return Response({'status': 'falure'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


@api_view(['GET'])
def get_test_customer_push(request):
    dispatch = Dispatch(request)
    dispatch.confirm_customer_fo_pickup_taken(service.customer.device_token, "Your Pickup Taken", "Your Pickup Taken", 2)
    return Response({'status': 'falure'}, status=status.HTTP_401_UNAUTHORIZED)


@api_view(['GET'])
def get_faq(request):
    faq = Faq.objects.all()
    faq_serializer = FaqSerializers(faq, many=True)
    return Response(faq_serializer.data, status=status.HTTP_200_OK)


@api_view(['POST'])
def notification_test(request):
    url = "https://fcm.googleapis.com/fcm/send"
    noti = {}
    noti['content_available'] = True
    noti['notification'] = {}
    noti['notification']['body'] = request.POST['message']
    noti['notification']['title'] = request.POST['title']
    # noti['notification']['category'] = category
    # noti['notification']['driver_id'] = driver_id
    noti['data'] = {}
    noti['data']['message'] = request.POST['message']
    noti['data']['title'] = request.POST['title']
    noti['notification']['sound'] = 'default'
    noti['registration_ids'] = [request.POST['customer_token']]
    noti['priority'] = 'high'
    headers = {
        'authorization': "key=AIzaSyDYatjzv-xIZlTIVkCzWzFHOaIpLQFl-jQ",
        'content-type': "application/json",
        'cache-control': "no-cache",
    }
    requests.request("POST", url, data=json.dumps(noti), headers=headers)
    return Response({"message": "success"}, status=status.HTTP_200_OK)


@authentication_classes([TokenAuthentication])
@api_view(['POST'])
def customer_register_as_driver(request):
    customer = Customer.objects.get(user=request.user)
    try:
        verify = Driver.objects.get(email=customer.email)
        return Response({"message": "Information is not unique"}, status=status.HTTP_400_BAD_REQUEST)
    except:
        try:
            driver = Driver()
            driver.email = customer.email
            driver.name = customer.name
            driver.mobile_no = customer.contact_no
            driver.nric_photo = request.FILES['nric']
            driver.driving_licence_photo = request.FILES['license']
            driver.profile_image = customer.picture.url
            driver.driver_service_type = request.POST['service_type']
            driver.user = customer.user
            driver.save()
            serializer = DriverSerializer(driver)
            return Response(serializer.data, status=status.HTTP_200_OK)
        except Exception, e:
            return Response({"message": str(e)}, status=status.HTTP_400_BAD_REQUEST)


@authentication_classes([TokenAuthentication])
@api_view(['POST'])
def driver_register_as_customer(request):
    driver = Driver.objects.get(user=request.user)
    try:
        verify = Customer.objects.get(email=driver.email)
        return Response({"message": "Information is not unique"}, status=status.HTTP_400_BAD_REQUEST)
    except:
        try:
            customer = Customer()
            customer.email = driver.email
            customer.contact_no = driver.mobile_no
            customer.name = driver.name
            customer.picture = driver.profile_image.url
            customer.user = driver.user
            customer.save()
            serializer = CustomerSerializer(customer)
            return Response(serializer.data, status=status.HTTP_200_OK)
        except Exception, e:
            return Response({"message": str(e)}, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def driver_login(request):
    email_phone_number = request.POST['email_phone_number']
    device_token = request.POST.get("device_token", "")
    try:
        verify = User.objects.get(email=email_phone_number)
        valid = verify.check_password(request.POST['password'])
        if valid:
            try:
                token = Token.objects.get(user=verify.id)
                token.delete()
            except:
                pass
            driver = Driver.objects.get(email=verify.email)
            driver.device_token = device_token
            driver.save()
            weekly_earning = get_weekly_earnings(driver)
            # print 'after'
            acceptacce = get_acceptacce_rate(driver)
            cancellation = get_cencell_rate(driver)
            avg_rating = get_avarage_rattings(driver)
            message = AdminMessage.objects.filter(message_to_driver=driver)
            message_serializer = MessageSeralizer(message, many=True)
            token = Token.objects.create(user=User.objects.get(email=email_phone_number))
            serializer = DriverSerializer(driver)

            return Response({'token': token.key, 'type': 'Driver', 'user_data': serializer.data, 'rating': avg_rating,
                             'weekly_earning': weekly_earning, 'acceptance': acceptacce,
                             'cancellation': cancellation, 'message': message_serializer.data},
                            status=status.HTTP_200_OK)
        else:
            return Response({'message': 'Invalid User'}, status=status.HTTP_400_BAD_REQUEST)
    except Exception, e:
        return Response({'message': str(e)}, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def customer_login(request):
    email_phone_number = request.POST['email_phone_number']
    device_token = request.POST.get("device_token", "")
    try:
        verify = User.objects.get(email=email_phone_number)
        valid = verify.check_password(request.POST['password'])
        if valid:
            try:
                token = Token.objects.get(user=verify.id)
                token.delete()
            except:
                pass
            customer = Customer.objects.get(email=verify.email)
            customer.device_token = device_token
            customer.save()
            token = Token.objects.create(user=User.objects.get(email=email_phone_number))
            serializer = CustomerSerializer(customer, context={"request": request})
            return Response({'token': token.key, 'type': 'Customer', 'user_data': serializer.data},
                            status=status.HTTP_200_OK)
        else:
            return Response({'message': 'Invalid User'}, status=status.HTTP_400_BAD_REQUEST)
    except Exception, e:
        return Response({'message': str(e)}, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
def resend_sms(request, user_type, id):
    if user_type == 'driver':
        try:
            driver = Driver.objects.get(id=id)
            phone_number = driver.mobile_no
            code = driver.verification
            send_sms(code, phone_number)
            return Response({"status": "success"}, status=status.HTTP_200_OK)
        except Exception, e:
            return Response({'message': str(e)}, status=status.HTTP_400_BAD_REQUEST)

    else:
        try:
            customer = Customer.objects.get(id=id)
            phone_number = customer.contact_no
            code = customer.verification
            send_sms(code, phone_number)
            return Response({"status": "success"}, status=status.HTTP_200_OK)
        except Exception, e:
            return Response({'message': str(e)}, status=status.HTTP_400_BAD_REQUEST)


@authentication_classes([TokenAuthentication])
@api_view(['POST'])
def feedback(request):
    subject = request.POST['subject']
    details = request.POST['details']
    try:
        send_mail(
            subject,
            details,
            "unclefrank@gmail.com",
            ['feedback@unclefrank.com.sg'],
            fail_silently=True,
            )
        return Response({"status": "success"}, status=status.HTTP_200_OK)
    except Exception, e:
        return Response({'message': str(e)}, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
def about(request):
    about = AboutPage.objects.all()
    serializer = AboutSerializer(about, many=True)
    return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(['POST'])
def facebook_signup(request):
    email = request.POST['email']
    device_token = request.POST.get("device_token", "")
    name = request.POST['name']
    # picture = request.FILES['picture']
    password = '123456'
    # if user already exists then return token
    users = User.objects.filter(email=email)
    if users.count() > 0:   
        try:
            verify = User.objects.get(email=email)
            try:
                token = Token.objects.get(user=verify.id)
                token.delete()
            except:
                pass
            customer = Customer.objects.get(user=verify)
            # if customer.contact_no == "":
            # #     return Response({'status': 'no phone no found.'}, status=status.status.HTTP_200_OK)
            # else:
            customer.device_token = device_token
            customer.save()
            token = Token.objects.create(user=User.objects.get(email=email))
            serializer = CustomerSerializer(customer, context={"request": request})
            return Response({'token': token.key, 'type': 'Customer', 'user_data': serializer.data},
                            status=status.HTTP_200_OK)
        except Exception, e:
            return Response({'message': str(e)}, status=status.HTTP_400_BAD_REQUEST)
    else:
        try:
            user = User.objects.create_user(email, email, password)
            customer = Customer()
            customer.user = user
            customer.email = email
            customer.contact_no = ""
            customer.name = name
            customer.device_token = device_token
            customer.save()
            token = Token.objects.create(user=User.objects.get(email=email))
            serializer = CustomerSerializer(customer, context={"request": request})
            return Response({'token': token.key, 'type': 'Customer', 'user_data': serializer.data},
                            status=status.HTTP_200_OK)
            serializer = CustomerSerializer(customer)
            return Response(serializer.data, status=status.HTTP_200_OK)
        except Exception, e:
            return Response({"error": str(e)}, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def facebook_signin(request):
    email = request.POST['email']
    device_token = request.POST.get("device_token", "")
    try:
        verify = User.objects.get(email=email)
        try:
            token = Token.objects.get(user=verify.id)
            token.delete()
        except:
            pass
        customer = Customer.objects.get(email=verify.email)
        customer.device_token = device_token
        customer.save()
        token = Token.objects.create(user=User.objects.get(email=email))
        serializer = CustomerSerializer(customer, context={"request": request})
        return Response({'token': token.key, 'type': 'Customer', 'user_data': serializer.data},
                        status=status.HTTP_200_OK)
    except Exception, e:
        return Response({'message': str(e)}, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
def arrival(request, job_id):
    try:
        send_notification(job_id)
        return Response({"message": "success"}, status=status.HTTP_200_OK)
    except Exception, e:
        return Response({"message": str(e)}, status=status.HTTP_400_BAD_REQUEST)


