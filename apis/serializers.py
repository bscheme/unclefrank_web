from django.contrib.auth.models import User, Group
from rest_framework import serializers
from driver.models import Driver, Vechile, BankAccount
from service.models import AdminMessage, DeliveryService, Package, PackageSize, ActivityLog, VehicleType, PickupService, AdminMessage, Payment, Pricing, PickupCancellationReasons
from customer.models import Customer, UserProfile, CreditCard
from superadmin.models import ExpressWindows, Faq, AboutPage
from random import choice
from string import ascii_uppercase
from django.conf import settings
from base import constant
import stripe
#Geo Django
from geo.models import DriverLocation


################### CUSTOMER RELATED SERIALIZERS START ######################
class AboutSerializer(serializers.ModelSerializer):
    class Meta:
        model = AboutPage
        fields = ('id', 'title1', 'desc1', 'title2', 'desc2', 'title3', 'desc3')


class CreditCardSerializer(serializers.ModelSerializer):
    class Meta:
        model = CreditCard
        fields = ('id', 'custmer', 'name', 'card_no', 'contact_no', 'expiry_date', 'cvv', 'external_reff_no',
                  'strite_id', 'last_digits', 'brand')

class AdminMessageSerializer(serializers.ModelSerializer):
    class Meta:
        model= AdminMessage
        fields = ('id', 'message_body', 'created_at')


class CustomerSerializer(serializers.ModelSerializer):
    def create(self, validated_data):
        email = validated_data.get('email')
        contact_no = validated_data.get('contact_no')
        password = validated_data.get('password')
        name = validated_data.get('name')
        picture = validated_data.get('picture')
        verification = ''.join(choice(ascii_uppercase) for i in range(5))
        cutomer_rating = serializers.CharField(source='get_cutomer_rating', read_only=True)
        user = User.objects.create_user(email, email, password)
        phone_number = UserProfile.objects.create(phone_number=contact_no, user=user)
        if picture:
            customer = Customer.objects.create(verification=verification, user=user, email=email, contact_no=contact_no,
                                             name=name, picture=picture)
        else:
            customer = Customer.objects.create(verification=verification, user=user, email=email, contact_no=contact_no,
                                               name=name)
        return customer

    class Meta:
        model = Customer
        fields = ('id', 'name', 'email', 'address', 'postal_code', 'contact_no', 'location', 'picture', 'cutomer_rating')


################### CUSTOMER RELATED SERIALIZERS END ######################

def varificaton_code():
    return ''.join(choice(ascii_uppercase) for i in range(5))


class VechileTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = VehicleType
        fields = ('id', 'description', 'caption', 'pic', 'pic_active')


class VechileSerializer(serializers.ModelSerializer):
    vehicle_name = serializers.CharField(source='get_vehicle_name', read_only=True)
    class Meta:
        model = Vechile
        fields = ('plate', 'vechile_photo', 'vechile_type', 'vechile_number', 'vechile_photo', 'vehicle_name')


class BankAccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = BankAccount
        fields = ('name_of_bank', 'account_number', 'bank_code', 'branch_code')


####### CUSTOM DRIVER SERIALIZER FOR LOCATION ########

class CustomDriverSerializer(serializers.Serializer):
    name = serializers.CharField(required=True)
    last_lng = serializers.CharField(required=False)
    last_lat = serializers.CharField(required=False)
    driver_service_type = serializers.CharField(required=False)
    class Meta:
        model = Driver
        fields = ('name', 'last_lng', 'last_lat', 'driver_service_type')

####### CUSTOM DRIVER SERIALIZER FOR LOCATION ########


# Driver Serializer Class
class DriverSerializer(serializers.Serializer):
    # A field from the user's profile:
    id = serializers.ReadOnlyField(read_only=True)
    name = serializers.CharField(required=True)
    mobile_no = serializers.CharField(required=True)
    nric_photo = serializers.ImageField(required=False, allow_null=True)
    driving_licence_photo = serializers.ImageField(required=False, allow_null=True)
    email = serializers.CharField(required=True)
    company = serializers.CharField(allow_null=True, required=False)
    password = serializers.CharField(write_only=True, required=True, allow_null=False)
    last_lng = serializers.CharField(required=False)
    last_lat = serializers.CharField(required=False)
    driver_service_type = serializers.CharField(required=False)
    vechiles = VechileSerializer(source='get_vechiles', read_only=True, many=False)
    bank_ac = BankAccountSerializer(source='get_bank_ac', read_only=True, many=True)
    profile_image = serializers.ImageField(required=False, allow_null=True)
    stripe_id = serializers.CharField(required=False)
    stripe_status = serializers.CharField(required=False)
    is_active = serializers.BooleanField(read_only=True)
    driver_category = serializers.CharField(read_only=True)
    driver_no = serializers.CharField(read_only=True)
    # delivery_taken = DeleveryServiceSerializer(source='get_taken_delivery_service', read_only=True, many=True)
    # pickup_taken = PickupServiceSerializer(source='get_taken_pickup_service', read_only=True, many=True)

    def create(self, validated_data):
        """
        Create and return a new `Driver` instance, given the validated data.
        """
        email = validated_data.get('email')
        mobile_no = validated_data.get('mobile_no')
        password = validated_data.get('password')
        name = validated_data.get('name')
        company = validated_data.get('company')
        nric_photo = validated_data.get('nric_photo')
        driving_licence_photo = validated_data.get('driving_licence_photo')
        profile_image = validated_data.get('profile_image')
        driver_service_type = validated_data.get('driver_service_type')
        # stripe.api_key=constant.STRIPE_SECRECT_KEY
        #Create Stripe Account for driver
        # response = stripe.Account.create(country='SG', email=email)
        # stripe_id = response.id
        # stripe_status = response.managed
        user = User.objects.create_user(email, email, password)
        # This line will be uncommented them the phone varification is done
        user.is_active = False

        # phone = UserProfile.objects.create(user=user, phone_number=mobile_no)
        driver = Driver.objects.create(user=user, profile_image=profile_image, email=email, mobile_no=mobile_no,
                                       nric_photo=nric_photo, driving_licence_photo=driving_licence_photo,
                                       name=name, company=company, verification=varificaton_code(),
                                       driver_service_type=driver_service_type, stripe_id="", is_active=True,
                                       stripe_status=""
                                       )
        user.save()
        return driver




class VerificationSerializer(serializers.Serializer):
    verification = serializers.CharField(required=True)

    def verify(self, verification_id):
        driver = Driver.objects.get(id=verification_id)
        verification = self.validated_data['verification']
        code = driver.verification
        user = driver.user
        if verification == code:
            user.is_active = True
            user.save()
            # driver.verification.delete()
            return True
        else:
            return False

    def check(self, user_id):
        code = Driver.objects.get(id=user_id)
        return code.verification

class CustomerVerificationSerializer(serializers.Serializer):
    verification = serializers.CharField(required=True)

    def verify(self, verification_id):
        customer = Customer.objects.get(id=verification_id)
        verification = self.validated_data['verification']
        code = customer.verification
        user = customer.user
        if verification == code:
            user.is_active = True
            user.save()
            # driver.verification.delete()
            return True
        else:
            return False

    def check(self, user_id):
        code = Customer.objects.get(id=user_id)
        return code.verification


# Update Account Information serializer
class UpdateAccountSerializer(serializers.Serializer):
    driver_id = serializers.CharField(required=True)
    vechile_type = serializers.CharField(required=True)
    vechile_number = serializers.CharField(required=True)
    vechile_front_plate = serializers.ImageField(required=True)
    vechile_with_driver = serializers.ImageField(required=True)
    brand = serializers.CharField(required=False)
    bnak_name = serializers.CharField(required=True)
    bnak_ac_no = serializers.CharField(required=True)
    bank_code = serializers.CharField(required=False)
    bank_branch_code = serializers.CharField(required=False)

    def save(self):
        driver_id = self.validated_data['driver_id']
        vechile_type = self.validated_data['vechile_type']
        vechile_number = self.validated_data['vechile_number']
        brand = self.validated_data['brand']
        bnak_name = self.validated_data['bnak_name']
        bnak_ac_no = self.validated_data['bnak_ac_no']
        bank_code = self.validated_data['bank_code']
        bank_branch_code = self.validated_data['bank_branch_code']
        vechile_front_plate = self.validated_data['vechile_front_plate']
        vechile_with_driver = self.validated_data['vechile_with_driver']
        try:
            driver = Driver.objects.get(id=driver_id)
        except:
            return False
        try:
            vechile = Vechile.objects.get(driver=driver)
        except:
            vechile = Vechile()
            vechile.driver = driver
        vechile.vechile_type = vechile_type
        vechile.vechile_number = vechile_number
        vechile.brand = brand
        vechile.plate = vechile_front_plate
        vechile.vehicle_photo = vechile_with_driver
        vechile.save()
        try:
            bankaccount = BankAccount.objects.get(driver=driver)
        except:
            bankaccount = BankAccount()
            bankaccount.driver = driver
        bankaccount.name_of_bank = bnak_name
        bankaccount.account_number = bnak_ac_no
        bankaccount.bank_code = bank_code
        bankaccount.branch_code = bank_branch_code
        bankaccount.save()
        return True


class SizeSerializer(serializers.ModelSerializer):
    class Meta:
        model = PackageSize
        fields = ('name', 'start_weight', 'end_weight', 'length', 'width', 'height')


class PackageSerializer(serializers.ModelSerializer):
    size = SizeSerializer()

    class Meta:
        model = Package
        # fields = ('quantity', 'unit_price', 'image', 'weight', 'length', 'width', 'height', 'confirm_pickup', 'confirm_delevery')


class DeleveryServiceSerializer(serializers.ModelSerializer):
    packages = PackageSerializer(source='get_package', read_only=True, many=True)
    customer = serializers.CharField(source='get_customer_name', read_only=True)
    # taken_by = serializers.CharField(source='get_driver_name', read_only=True)

    class Meta:
        model = DeliveryService
        fields = ('id', 'signature', 'customer', 'created_at', 'job_start_time', 'job_complete_time', 'status', 'payment_status', 'eta', 'distance','tracking_code', 'collection_name', 'collection_address', 'collection_contact_no',
                  'collection_remark', 'delivery_name', 'delivery_address', 'delivery_contact_no',
                  'delevery_remarks', 'collection_date', 'collection_time', 'delevery_date', 'delevery_time',
                  'delevery_status', 'packages', 'taken_by', 'total_cost', 'auto_assigned')

class PickupDriverSerializer(serializers.ModelSerializer):
    class Meta:
        model = Driver
        fields = ('name', 'mobile_no', 'email', 'profile_image')


class PickupServiceSerializer(serializers.ModelSerializer):
    customer = serializers.CharField(source='get_customer_name', read_only=True)
    vehicle_caption = serializers.CharField(source='get_vehicle_caption', read_only=True)
    estmited_cost = serializers.DecimalField(max_digits=5, decimal_places=2, source='get_estmited_cost')
    customer_ratings = serializers.DecimalField(max_digits=5, decimal_places=2, source='get_customer_ratings')
    customer_phone = serializers.CharField(source='get_customer_phone', read_only=True)
    vehicle_pic = serializers.CharField(source='get_image_url', read_only=True)
    driver = PickupDriverSerializer(source='get_driver', read_only=True)
    message = AdminMessageSerializer(source='get_admin_message', read_only=True, many=True)

    # pickedup_by = serializers.CharField(source='get_driver_name', read_only=True)
    # image_url = serializers.SerializerMethodField()

    class Meta:
        model = PickupService
        fields = ('id', 'created_at', 'job_start_time', 'job_complete_time', 'customer', 'pickup_address',
        'dropoff_address', 'remarks', 'distance', 'pickup_status', 'tracking_code',
        'signature', 'vehicle_caption', 'pickedup_by', 'total_cost', 'estmited_cost', 'customer_ratings', 'customer_phone', 'vehicle_pic',
        'driver', 'message')

    # def get_image_url(self, pickup):
    #     request = self.context.get('request')
    #     image_url = pickup.vehicle_type.pic.url
    #     return request.build_absolute_uri(image_url)


class LogSerializer(serializers.ModelSerializer):
    class Meta:
        model = ActivityLog
        fields = ('date_time', 'delevery_status', 'log_message', 'logger_name', 'log_type')

        # class TakeDeleveryServiceSerializer(serializers.Serializer):
        #     delevery_service = serializers.CharField()
        #     driver_id = serializers.CharField()

class PickUpPriceSerializer(serializers.Serializer):
    min_fare = serializers.DecimalField(max_digits=5, decimal_places=2)
    base_fare = serializers.DecimalField(max_digits=5, decimal_places=2)
    rate = serializers.DecimalField(max_digits=5, decimal_places=2)
    waiting_charge = serializers.DecimalField(max_digits=5, decimal_places=2)
    peakMultiplier = serializers.DecimalField(max_digits=5, decimal_places=2)
    gst = serializers.DecimalField(max_digits=5, decimal_places=2)
    vehicle_name = serializers.CharField(source='get_vehicle_name', read_only=True)
    vehicle_description = serializers.CharField(source='get_vehicle_description', read_only=True)

class AddressSerializer(serializers.Serializer):
    post_code = serializers.CharField()
    bldg_no = serializers.CharField()
    street_name = serializers.CharField()
    bldg_name = serializers.CharField()
    floor = serializers.CharField()
    unit = serializers.CharField()

class MessageSeralizer(serializers.ModelSerializer):
    class Meta:
        model = AdminMessage
        fields = ('message_body', 'created_at')

class PaymentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Payment
        fields = ('total_cost', 'payment_status')

class PricingSerializer(serializers.ModelSerializer):
    # pricing_info = serializers.CharField(source='package_info', read_only=True, many=True)
    class Meta:
        model = Pricing
        fields = ('start_qty', 'end_qty', 'schedule_type', 'range_rate', 'corp_range_rate')

class ExpressWindowsSerializer(serializers.ModelSerializer):
    class Meta:
        model = ExpressWindows
        fields = ('window_name', 'price')

class PackageSizeInfoSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    name = serializers.CharField()
    start_weight = serializers.CharField()
    weight_index = serializers.IntegerField()
    pricing_info = PricingSerializer(source='get_package_detail', read_only=True, many=True)

################################ GEO LOCATION ###############################################

class DriversLocationSerializer(serializers.Serializer):
    """
        Documentation goes here
    """

    driver_id = serializers.IntegerField()
    lat = serializers.CharField(source='get_lat', read_only=True)
    lng = serializers.CharField(source='get_lng', read_only=True)
    driver_name = serializers.CharField(source='get_driver_name', read_only=True)
    driver_type = serializers.CharField(source='get_driver_type', read_only=True)
    vechile_type = serializers.CharField(source='def_driver_vechile_type', read_only=True)
    is_active = serializers.BooleanField(source='get_is_active', read_only=True)

    class Meta:
        model = DriverLocation
        fields = ('driver_id', 'lat', 'lng', 'driver_name', 'driver_type', 'vechile_type', 'is_active')

class PickupCancellationReasonsSerializers(serializers.ModelSerializer):
    class Meta:
        model = PickupCancellationReasons
        fields = ('id','image', 'image_active', 'reason')

class FaqSerializers(serializers.ModelSerializer):
    class Meta:
        model = Faq
        fields = ('id','title', 'description')

################# Trascking data for pickup ############################

# class Tracking(serializers.ModelSerializer):
