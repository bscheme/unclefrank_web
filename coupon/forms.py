from django import forms
from superadmin.models import ExpressWindows



COUPON_TYPE = (
    ('onetime', 'onetime'),
    ('multi', 'multi'),
)

DISCOUNT_TYPE = (
    ('amount', 'amount'),
    ('percentage', 'percentage'),
)

PERIOD = (
    ('MORNING', 'MORNING'),
    ('AFTERNOON', 'AFTERNOON'),
	('EVENING', 'EVENING'),
	('NIGHT', 'NIGHT'),
)



class CouponForm(forms.ModelForm):
	# code_type = forms.ChoiceField(choices=COUPON_TYPE, widget=forms.Select(attrs={'class': 'form-control select-styled site-field',
	# 																			  'required': 'required'}))
	code = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'id': 'code', 'required': 'required'}))
	delivery_window = forms.ModelChoiceField(queryset= ExpressWindows.objects.all(), required=False, empty_label="(All)", widget=forms.Select(attrs={'class': 'form-control select-styled site-field'}))
	start_date =  forms.DateField(widget=forms.widgets.DateInput(format='%m/%d/%y', attrs={'class': 'form-control select-styled site-field date',
																						   'required': 'required'}))
	start_time = forms.TimeField(required=True, input_formats=['%I:%M %p'], widget=forms.TimeInput(format='%I:%M %p', attrs={'class': 'form-control huge time-pick',
                                                               'id': 'start-time', 'required': 'required'}))
	end_date =  forms.DateField(widget=forms.widgets.DateInput(format='%m/%d/%y', attrs={'class': 'form-control select-styled site-field date',
																						 'required': 'required'}))
	discount_type = forms.ChoiceField(choices=DISCOUNT_TYPE, widget=forms.Select(attrs={'class': 'form-control select-styled site-field',
																						'required': 'required'}))
	end_time = forms.TimeField(required=True, input_formats=['%I:%M %p'], widget=forms.TimeInput(format='%I:%M %p', attrs={'class': 'form-control huge time-pick',
                                                               'id': 'start-time', 'required': 'required'}))
	discount_value = forms.DecimalField(widget=forms.NumberInput(attrs={'class': 'form-control huge', 'required': 'required'}))
	number_of_use = forms.IntegerField(widget=forms.NumberInput(attrs={'class': 'form-control huge', 'required': 'required'}))

	class Meta:
		model = ExpressWindows
		fields = ['code', 'delivery_window', 'start_date', 'start_time', 'end_date', 'end_time', 'discount_type', 'discount_value', 'number_of_use']