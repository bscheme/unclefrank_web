from django.db import models
from base.models import BaseModel
from superadmin.models import ExpressWindows
from customer.models import Customer

# Create your models here.

COUPON_TYPY = (
    ('onetime', 'onetime'),
    ('multi', 'multi'),
)

DISCOUNT_TYPE = (
    ('amount', 'amount'),
    ('percentage', 'percentage'),
)
JOB_TYPE = (
	('delivery', 'delivery'),
	('courier', 'courier')
	)

class Coupon(BaseModel):
	code = models.CharField(max_length=6, default=None, null=True)
	code_type = models.CharField(max_length=25, choices=COUPON_TYPY, default='multi', null=True)
	start_date = models.DateField(blank=True)
	end_date = models.DateField(blank=True)
	start_time = models.TimeField(null=True, default=None)
	end_time = models.TimeField(null=True, default=None)
	discount_type = models.CharField(max_length=25, choices=DISCOUNT_TYPE, default=None, null=True)
	discount_value = models.DecimalField(max_digits=10, decimal_places=2,default=0.00)
	number_of_use = models.IntegerField(default=0)
	is_user = models.BooleanField(default=False)
	period = models.CharField(max_length=50, null=True)
	delivery_window = models.ForeignKey(ExpressWindows, default=None, null=True)
	
	def __unicode__(self):
		return self.code

class CouponRedemptions(BaseModel):
	cupon_code = models.CharField(max_length=6, default=None, null=True)
	used_by = models.ForeignKey(Customer, default=None, null=True)
	job_id = models.IntegerField(default=0)
	job_type = models.CharField(max_length=6, default=None, null=True)
	discount_value = models.DecimalField(max_digits=10, decimal_places=2,default=0.00)
