from django.shortcuts import render
from random import choice
from django.http import HttpResponse, JsonResponse
from string import ascii_uppercase
from .models import Coupon
from .forms import CouponForm
from django.shortcuts import render, render_to_response, redirect
from django.template import RequestContext
import datetime
from superadmin.models import DeliveryTiming

# Create your views here.

def get_unique_code():
	code = ''.join(choice(ascii_uppercase) for i in range(8))
	data = Coupon.objects.filter(code=code)
	if data.count() > 0:
		get_unique_code()
	else:
		return code


def if_code_exists(code):
	data = Coupon.objects.filter(code=code)
	if data.count() > 0:
		return True
	else:
		return False


def coupon_code_generate(request):
	arg = {}
	form = CouponForm()
	arg['coupons'] = Coupon.objects.all()
	if request.method == 'POST':
		form = CouponForm(request.POST)
		if if_code_exists(request.POST['code']) :
			arg['error'] = "Coupone Code already exists"
			arg['form'] = form
			return render_to_response('coupon/add.html', arg, context_instance=RequestContext(request))
		if form.is_valid():
			coupon = Coupon()
			coupon.code = form.cleaned_data['code']
			# coupon.code_type =  form.cleaned_data['code_type']
			coupon.start_date = form.cleaned_data['start_date']
			coupon.end_date = form.cleaned_data['end_date']
			coupon.discount_type = form.cleaned_data['discount_type']
			coupon.discount_value = form.cleaned_data['discount_value']
			coupon.number_of_use = form.cleaned_data['number_of_use']
			coupon.is_user = False
			coupon.delivery_window = form.cleaned_data['delivery_window']
			coupon.start_time = form.cleaned_data['start_time']
			coupon.end_time = form.cleaned_data['end_time']
			coupon.save()
		# except Exception, e:
		# 	print e
		# 	form = CouponForm(request.POST)
	arg['form'] = form
	return render_to_response('coupon/add.html', arg, context_instance=RequestContext(request))

def coupon_code_delete(request, coupon_id):
	try:
		coupon = Coupon.objects.get(id=coupon_id)
		coupon.delete()
		return redirect('/sadmin/coupon_add/')
	except Exception, e:
		return HttpResponse(str(e))

def generate_code(request):
	return JsonResponse({'code': get_unique_code()})