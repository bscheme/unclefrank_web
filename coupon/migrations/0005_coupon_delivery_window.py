# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0042_auto_20170115_1536'),
        ('coupon', '0004_coupon_period'),
    ]

    operations = [
        migrations.AddField(
            model_name='coupon',
            name='delivery_window',
            field=models.ForeignKey(default=None, to='superadmin.ExpressWindows', null=True),
        ),
    ]
