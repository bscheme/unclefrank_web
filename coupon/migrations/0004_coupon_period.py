# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('coupon', '0003_coupon_number_of_use'),
    ]

    operations = [
        migrations.AddField(
            model_name='coupon',
            name='period',
            field=models.CharField(max_length=50, null=True),
        ),
    ]
