from django import forms
from django.db import models
from .models import RotatingBanner, SiteConfigeration, HomeCMS, AboutPage, Services, JoinUs, Careers, Corporate, ContactUsPage, SliderImage, RegistrationBackground, DriverStatus, Faq, Terms, Privacy, ExpressWindows, CommissionSetup, DriverCategory, CustomerFaqPage
from service.models import CourierGST, PickupGST, CourierTimeCharge, PickupTimeCharge, PackageSize, VehicleType, CartonType, ScheduleType, DayType, Pricing, HolidayList, PickupSurgeCharge, PickUpRate, CourierSurgeCharge, PickupCancellationReasons
from base import constant
from address.models import Postalcode




SCHEDULE_TYPE = (
    ('Same Day','Same Day'),
    ('Next Day','Next Day'),
    ('Two Working Days','Two Working Days'))

class HomeCMSForm(forms.ModelForm):

    logo = forms.ImageField(label='Logo (197x75px)', required=False)
    category = forms.CharField(label='Add category', max_length=30, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))


    banner = forms.ImageField(label='Banner Image(1920x650px)',required=False)
    banner_text = forms.CharField(label='Banner Heading', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    banner_desc = forms.CharField(label='Banner Description', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    under_banner_text = forms.CharField(label='App download text', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))

    service1_image = forms.ImageField(label='Image for service 1', required=False)
    service1_title = forms.CharField(label='Title for service 1', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    service1_desc = forms.CharField(label='Description for service 1', required=False, widget=forms.Textarea(attrs={'class': 'form-control huge'}))

    service2_image = forms.ImageField(label='Image for service 2', required=False)
    service2_title = forms.CharField(label='Title for service 2', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    service2_desc = forms.CharField(label='Description for service 1', required=False, widget=forms.Textarea(attrs={'class': 'form-control huge'}))

    features = forms.CharField(label='Features section headings', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    features_desc = forms.CharField(label='Features section description', required=False, widget=forms.Textarea(attrs={'class': 'form-control huge'}))

    feature1_image = forms.ImageField(label='Image for feature 1 (76x72px)', required=False)
    feature1_title = forms.CharField(label='Title for feature 1', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    feature1_desc = forms.CharField(label='Desctiption for feature 1', required=False, widget=forms.Textarea(attrs={'class': 'form-control huge'}))

    feature2_image = forms.ImageField(label='Image for feature 2 (76x72px)', required=False)
    feature2_title = forms.CharField(label='Title for feature 2', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    feature2_desc = forms.CharField(label='Desctiption for feature 2', required=False, widget=forms.Textarea(attrs={'class': 'form-control huge'}))

    feature3_image = forms.ImageField(label='Image for feature 3 (76x72px)', required=False)
    feature3_title = forms.CharField(label='Title for feature 3', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    feature3_desc = forms.CharField(label='Desctiption for feature 3', required=False, widget=forms.Textarea(attrs={'class': 'form-control huge'}))

    feature4_image = forms.ImageField(label='Image for feature 4 (76x72px)', required=False)
    feature4_title = forms.CharField(label='Title for feature 4', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    feature4_desc = forms.CharField(label='Desctiption for feature 4', required=False, widget=forms.Textarea(attrs={'class': 'form-control huge'}))

    footer1_text = forms.CharField(label='Footer text Title', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    footer1_desc = forms.CharField(label='Footer text Description', required=False, widget=forms.Textarea(attrs={'class': 'form-control huge'}))

    social1_icons = forms.URLField(label='Url for Social Icon 1', required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    social2_icons = forms.URLField(label='Url for Social Icon 2', required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    social3_icons = forms.URLField(label='Url for Social Icon 3', required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    social4_icons = forms.URLField(label='Url for Social Icon 4', required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    social5_icons = forms.URLField(label='Url for Social Icon 5', required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))

    # footer2_text = forms.CharField(label='Navigation Title', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    # footer2_url1 = forms.URLField(label='Navigation Link 1', required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    # footer2_url2 = forms.URLField(required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    # footer2_url3 = forms.URLField(required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    # footer2_url4 = forms.URLField(required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    # footer2_url5 = forms.URLField(required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))

    # footer3_url1 = forms.URLField(required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    # footer3_url2 = forms.URLField(required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    # footer3_url3 = forms.URLField(required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))

    footer4 = forms.CharField(label='Copyright text',max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))

    class Meta:
        model = HomeCMS
        fields =['logo', 'banner', 'banner_text', 'banner_desc', 'under_banner_text',
        'service1_image', 'service1_title', 'service1_desc',
        'service2_image', 'service2_title', 'service2_desc',
        'features', 'features_desc',
        'feature1_image', 'feature1_title', 'feature1_desc',
        'feature2_image', 'feature2_title', 'feature2_desc',
        'feature3_image', 'feature3_title', 'feature3_desc',
        'feature4_image', 'feature4_title', 'feature4_desc',
        'footer1_text', 'footer1_desc',
        'social1_icons', 'social2_icons', 'social3_icons', 'social4_icons', 'social5_icons',
        'footer4',
        ]

class AboutPageForm(forms.ModelForm):
    banner = forms.ImageField(label='Banner for About Page', required=False)

    title1 = forms.CharField(label='Page Title', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    desc1 = forms.CharField(label='Page Description', required=False, widget=forms.Textarea(attrs={'class': 'form-control huge'}))

    image1 = forms.ImageField(label='Content Image', required=False)
    title2 = forms.CharField(label='Content Title', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    desc2 = forms.CharField(label='Content Description', required=False, widget=forms.Textarea(attrs={'class': 'form-control huge'}))

    link1_text = forms.CharField(label='Link Button text', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))

    title3 = forms.CharField(label='Second Content Title', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    desc3 = forms.CharField(label='Second Content Description', required=False, widget=forms.Textarea(attrs={'class': 'form-control huge'}))

    #VIDEO FIELD MODEL
    video = forms.FileField(label = "Video File", required=False)
    image_title = forms.CharField(label='Footer Image Title', required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))

    image2 = forms.ImageField(label = "First Image", required=False)
    image3 = forms.ImageField(label = "Second Image", required=False)
    image4 = forms.ImageField(label = "Third Image", required=False)


    class Meta:
        model = AboutPage
        fields = [
        'banner',
        'title1', 'desc1',
        'image1', 'title2', 'desc2',
        'link1_text', 'title3', 'desc3', 'video', 'image_title',
        'image2', 'image3', 'image4'
        ]


class CustomerFaqPageForm(forms.ModelForm):
    question = forms.CharField(label='Question Title', max_length=120, required=True, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    answer = forms.CharField(label='Description', required=True, widget=forms.Textarea(attrs={'class': 'form-control huge'}))

    class Meta:
        model = CustomerFaqPage
        fields = ['question',
        'answer']

class SliderImageForm(forms.ModelForm):
    title = forms.CharField(label='Slider Image Title', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    desc = forms.CharField(label='Slider Image Description', required=False, widget=forms.Textarea(attrs={'class': 'form-control huge'}))
    images = forms.ImageField(label="Image", required=False)

    class Meta:
        model = SliderImage
        fields = [
            'title',
            'desc',
            'images'
        ]

class RegistrationBackgroundForm(forms.ModelForm):
    title = forms.CharField(label='Background Image Title', max_length=120, required=False,
                            widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    images = forms.ImageField(label="Image", required=False)

    class Meta:
        model = RegistrationBackground
        fields = [
            'title',
            'images'
        ]

class ServicesPageForm(forms.ModelForm):
    image1 = forms.ImageField(label='First Image (960x844)', required=False)

    title1 = forms.CharField(label='First Content Title', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    desc1 = forms.CharField(label='First Content Description', required=False, widget=forms.Textarea(attrs={'class': 'form-control huge'}))

    rewards1_image = forms.ImageField(label='Rewards Image 1(47x47)', required=False)
    rewards1_text = forms.CharField(label='Rewards Text 1', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    rewards1_desc = forms.CharField(label='Rewards Desc 1', required=False, widget=forms.Textarea(attrs={'class': 'form-control huge'}))

    rewards2_image = forms.ImageField(label='Rewards Image 2(47x47)', required=False)
    rewards2_text = forms.CharField(label='Rewards Text 2', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    rewards2_desc = forms.CharField(label='Rewards Desc 2', required=False, widget=forms.Textarea(attrs={'class': 'form-control huge'}))

    rewards3_image = forms.ImageField(label='Rewards Image 3(47x47)', required=False)
    rewards3_text = forms.CharField(label='Rewards Text 3', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    rewards3_desc = forms.CharField(label='Rewards Desc 3', required=False, widget=forms.Textarea(attrs={'class': 'form-control huge'}))

    rewards4_image = forms.ImageField(label='Rewards Image 4(47x47)', required=False)
    rewards4_text = forms.CharField(label='Rewards Text 4', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    rewards4_desc = forms.CharField(label='Rewards Desc 4', required=False, widget=forms.Textarea(attrs={'class': 'form-control huge'}))

    image2 = forms.ImageField(label='Second Image(960x681)', required=False)

    title2 = forms.CharField(label='Second Content Title', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    desc2 = forms.CharField(label='First Content Description', required=False, widget=forms.Textarea(attrs={'class': 'form-control huge'}))

    small_icon = forms.ImageField(label='small icon (65x65)', required=False)
    small_icon_desc = forms.CharField(label='Small icon description', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))

    title3 = forms.CharField(label='Third Content Title', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    image3 = forms.ImageField(label='Third Content Image(65x65)', required=False)
    desc3 = forms.CharField(label='Third Content Description', required=False, widget=forms.Textarea(attrs={'class': 'form-control huge'}))

    title4 = forms.CharField(label='Slider Title', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    desc4 = forms.CharField(label='Slider Description', required=False, widget=forms.Textarea(attrs={'class': 'form-control huge'}))

    tab1 = forms.CharField(label='Title of First Tab', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    tab1_desc = forms.CharField(label='Description of First Tab', required=False, widget=forms.Textarea(attrs={'class': 'form-control huge'}))

    tab2 = forms.CharField(label='Title of Second Tab', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    tab2_desc = forms.CharField(label='Description of Second Tab', required=False, widget=forms.Textarea(attrs={'class': 'form-control huge'}))

    tab3 = forms.CharField(label='Title of Third Tab', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    tab3_desc = forms.CharField(label='Description of Third Tab', required=False, widget=forms.Textarea(attrs={'class': 'form-control huge'}))

    slider_image1 = forms.ImageField(label='Slider Image 1', required=False)
    slider_image2 = forms.ImageField(label='Slider Image 2', required=False)
    slider_image3 = forms.ImageField(label='Slider Image 3', required=False)
    slider_image4 = forms.ImageField(label='Slider Image 4', required=False)
    slider_image5 = forms.ImageField(label='Slider Image 5', required=False)

    class Meta:
        model = Services
        fields = [
        'image1',
        'title1', 'desc1',
        'rewards1_image', 'rewards1_text', 'rewards1_desc',
        'rewards2_image', 'rewards2_text', 'rewards2_desc',
        'rewards3_image', 'rewards3_text', 'rewards3_desc',
        'rewards4_image', 'rewards4_text', 'rewards4_desc',
        'image2', 'title2', 'desc2', 'small_icon', 'small_icon_desc',
        'title3', 'image3', 'desc3',
        'tab1', 'tab1_desc',
        'tab2', 'tab2_desc',
        'tab3', 'tab3_desc',
        'title4', 'desc4',
        'slider_image1', 'slider_image2', 'slider_image3', 'slider_image4', 'slider_image5',
        ]

class JoinUsPageForm(forms.ModelForm):
#BE A DRIVER
    title1 = forms.CharField(label='First Title', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
#TOP IMAGE
    image1 = forms.ImageField(label='First Image (960x580)', required=False)
#BE A DRIVER DESCRIPTION
    desc1 = forms.CharField(label='First Description', required=False, widget=forms.Textarea(attrs={'class': 'form-control huge'}))

#BE A DELIVERY PARTNER
    title2 = forms.CharField(label='Second Title', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
#SECOND IMAGE
    image2 = forms.ImageField(label='Second Image (960x580)', required=False)
#BE A DELIVERY PARTNER DESCRIPTION
    desc2 = forms.CharField(label='Second Description', required=False, widget=forms.Textarea(attrs={'class': 'form-control huge'}))

#CAREERS
    title3 = forms.CharField(label='Third Title', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
#CAREER IMAGE
    image3 = forms.ImageField(label='Third Image(960x580)', required=False)
#BE A DELIVERY PARTNER DESCRIPTION
    desc3 = forms.CharField(label='Third Description', required=False, widget=forms.Textarea(attrs={'class': 'form-control huge'}))

#CAREER LINK
    link1_text = forms.CharField(label='Link Text', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))


###HOW IT WORKS IMAGES
    slider_image1 = forms.ImageField(label='slider image 1', required=False)
    slider_image2 = forms.ImageField(label='slider image 2', required=False)
    slider_image3 = forms.ImageField(label='slider image 3', required=False)
    slider_image4 = forms.ImageField(label='slider image 4', required=False)
    slider_image5 = forms.ImageField(label='slider imaeg 5', required=False)

#FEATURES
    features_text = forms.CharField(label='feature text', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    features_desc = forms.CharField(label='features desc', required=False, widget=forms.Textarea(attrs={'class': 'form-control huge'}))

    feature1_title = forms.CharField(label='Feature Title 1', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    feature1_image= forms.ImageField(label='Feature Image 1 (69x55)', required=False)
    feature1_desc = forms.CharField(label='Feature Description 1', required=False, widget=forms.Textarea(attrs={'class': 'form-control huge'}))

    feature2_title = forms.CharField(label='Feature Title 2', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    feature2_image= forms.ImageField(label='Feature Image 2 (69x55)', required=False)
    feature2_desc = forms.CharField(label='Feature Description 2', required=False, widget=forms.Textarea(attrs={'class': 'form-control huge'}))

    feature3_title = forms.CharField(label='Feature Title 3', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    feature3_image= forms.ImageField(label='Feature Image 3 (69x55)', required=False)
    feature3_desc = forms.CharField(label='Feature Description 3', required=False, widget=forms.Textarea(attrs={'class': 'form-control huge'}))

    feature4_title = forms.CharField(label='Feature Title 4', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    feature4_image= forms.ImageField(label='Feature Image 4 (69x55)', required=False)
    feature4_desc = forms.CharField(label='Feature Description 4', required=False, widget=forms.Textarea(attrs={'class': 'form-control huge'}))

    title4 = forms.CharField(label='Slider Title', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    desc4 = forms.CharField(label='Slider Description', required=False, widget=forms.Textarea(attrs={'class': 'form-control huge'}))


    footer_slider_image1 = forms.ImageField(label='Slider Image 1', required=False)
    footer_slider_image2 = forms.ImageField(label='Slider Image 2', required=False)
    footer_slider_image3 = forms.ImageField(label='Slider Image 3', required=False)
    footer_slider_image4 = forms.ImageField(label='Slider Image 4', required=False)
    footer_slider_image5 = forms.ImageField(label='Slider Image 5', required=False)


    class Meta:
        model = JoinUs
        fields = [
        'title1', 'image1', 'desc1',
        'title2', 'image2', 'desc2',
        'title3', 'image3', 'desc3',
        'link1_text',
        'slider_image1', 'slider_image2', 'slider_image3', 'slider_image4', 'slider_image5',
        'features_text','features_desc',
        'feature1_title', 'feature1_image', 'feature1_desc',
        'feature2_title', 'feature2_image', 'feature2_desc',
        'feature3_title', 'feature3_image', 'feature3_desc',
        'feature4_title', 'feature4_image', 'feature4_desc',
        'title4', 'desc4',
        'footer_slider_image1', 'footer_slider_image2',
        'footer_slider_image3', 'footer_slider_image4', 'footer_slider_image5',
        ]


class TermsForm(forms.ModelForm):
    title = forms.CharField(label='Title', max_length=120, required=False,
                            widget=forms.TextInput(attrs={'class': 'form-control huge', 'placeholder': 'Title'}))
    description = forms.CharField(label='Description', required=False,
                           widget=forms.Textarea(attrs={'class': 'form-control huge'}))
    class Meta:
        model = Terms
        fields = [
            'title', 'description',
        ]


class FaqForm(forms.ModelForm):
    title = forms.CharField(label='Title', max_length=120, required=False,
                            widget=forms.TextInput(attrs={'class': 'form-control huge', 'placeholder': 'Title'}))
    description = forms.CharField(label='Description', required=False,
                           widget=forms.Textarea(attrs={'class': 'form-control huge'}))
    class Meta:
        model = Faq
        fields = [
            'title', 'description',
        ]

class PrivacyForm(forms.ModelForm):
    title = forms.CharField(label='Title', max_length=120, required=False,
                            widget=forms.TextInput(attrs={'class': 'form-control huge', 'placeholder': 'Title'}))
    description = forms.CharField(label='Description', required=False,
                           widget=forms.Textarea(attrs={'class': 'form-control huge'}))
    class Meta:
        model = Privacy
        fields = [
            'title', 'description',
        ]


class CareersPageForm(forms.ModelForm):
    #banner image in career
    banner = forms.ImageField(label='Career banner image', required=False)
    #title of the page
    title = forms.CharField(label='Title', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    #description right beneath the title
    desc = forms.CharField(label='Description', required=False, widget=forms.Textarea(attrs={'class': 'form-control huge'}))
    #for each job, there will be title, description and list of responsibility
    job_title = forms.CharField(label='Job Title', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    job_desc = forms.CharField(label='Job description', required=False, widget=forms.Textarea(attrs={'class': 'form-control huge'}))
    job_responsibility = forms.CharField(label='Job responsibility', required=False, widget=forms.Textarea(attrs={'class': 'form-control huge'}))

    class Meta:
        model = Careers
        fields = [
        'banner',
        'title', 'desc',
        'job_title', 'job_desc', 'job_responsibility',
        ]
        
class CorporatePageForm(forms.ModelForm):
    banner = forms.ImageField(label='Corporate banner image', required=False)

    title1 = forms.CharField(label='Corporate Title', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    image1 = forms.ImageField(label='Corporate image 1', required=False)
    desc1 = forms.CharField(label='Description 1', required=False, widget=forms.Textarea(attrs={'class': 'form-control huge'}))
    link1_text = forms.CharField(label='Corporate link text 1', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))

    title2 = forms.CharField(label='Corporate Title 2', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    image2 = forms.ImageField(label='Corporate image 2', required=False)
    desc2 = forms.CharField(label='Description 2', required=False, widget=forms.Textarea(attrs={'class': 'form-control huge'}))
    link2_text = forms.CharField(label='Corporate link text 2', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))

    title3 = forms.CharField(label='Corporate Title 3', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    image3 = forms.ImageField(label='Corporate image 3', required=False)
    desc3 = forms.CharField(label='Description 3', required=False, widget=forms.Textarea(attrs={'class': 'form-control huge'}))
    link3_text = forms.CharField(label='Corporate link text 3', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))

    title4 = forms.CharField(label='Corporate Title 4', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    image4 = forms.ImageField(label='Corporate image 4', required=False)
    desc4 = forms.CharField(label='Description 4', required=False, widget=forms.Textarea(attrs={'class': 'form-control huge'}))
    link4_text = forms.CharField(label='Corporate link text 4', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))

    class Meta:
        model = Corporate
        fields = [
        'banner',
        'title1', 'image1', 'desc1', 'link1_text',
        'title2', 'image2', 'desc2', 'link2_text',
        'title3', 'image3', 'desc3', 'link3_text',
        'title4', 'image4', 'desc4', 'link4_text',
        ]

class ContactUsPageForm(forms.ModelForm):
    addr_title = forms.CharField(label='Address title', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    contact_location = forms.CharField(label='Location Address', max_length=120, required=False, widget=forms.TextInput(attrs={'id': 'location', 'class': 'form-control huge'}))

    # addr_addr = forms.CharField(label='Address', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    addr_tel = forms.CharField(label='Address telephone', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    addr_fax = forms.CharField(label='Address fax', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    addr_email = forms.EmailField(label='email field', required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))

    title = forms.CharField(label='Title', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    desc = forms.CharField(label='description', required=False, widget=forms.Textarea(attrs={'class': 'form-control huge'}))


    class Meta:
        model = ContactUsPage
        fields = [
        'addr_title', 'contact_location', 'addr_tel', 'addr_fax', 'addr_email',
        'title', 'desc'
        ]


#PACKAGE SETUP FORM START

class PackageForm(forms.ModelForm):
    name = forms.CharField(label='Package Name', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge', 'required':'required'}))
    # weight = forms.CharField(label='Weight', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    length = forms.CharField(label='Length', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    width = forms.CharField(label='Width', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    height = forms.CharField(label='Height', required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    start_weight = forms.CharField(label='Start Weight', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge', 'required': 'required'}))
    end_weight = forms.CharField(label='End Weight', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge', 'required': 'required'}))

    class Meta:
        model = PackageSize
        fields = [
        'name', 'start_weight', 'end_weight', 'length', 'width', 'height'
        ]

#PACKAGE SETUP FORM END

class pricingForm(forms.ModelForm):
    schedule_type = forms.ChoiceField(label='Schedule Type', choices=SCHEDULE_TYPE, widget=forms.Select(attrs={'class': 'form-control huge'}))
    package_size = forms.ModelChoiceField(label='Package Size', queryset=PackageSize.objects.all(), widget=forms.Select(attrs={'class': 'form-control huge'}))
    start_qty = forms.IntegerField(label='Start Quantity', widget=forms.NumberInput(attrs={'class': 'form-control huge'}))
    end_qty = forms.IntegerField(label='End Quantity', widget=forms.NumberInput(attrs={'class': 'form-control huge'}))
    range_rate = forms.DecimalField(label='Standard Rate', widget=forms.NumberInput(attrs={'class': 'form-control huge'}))
    corp_range_rate = forms.DecimalField(label='Corporate rate', widget=forms.NumberInput(attrs={'class': 'form-control huge'}))
    
    class Meta:
        model = Pricing
        fields = ['schedule_type', 'package_size', 'start_qty', 'end_qty',
                    'range_rate', 'corp_range_rate',
                 ]

#VEHICLE SETUP FORM START

class VehicleForm(forms.ModelForm):
    caption = forms.CharField(label="Vehicle Name", max_length=120, required=True, widget=forms.TextInput(attrs={'class':'form-control huge'}))
    description = forms.CharField(label="description", max_length=120, required=False, widget=forms.TextInput(attrs={'class':'form-control huge'}))
    pic = forms.ImageField(label="Upload a picture", required=True)
    pic_active = forms.ImageField(label="Upload a picture for Active Status", required=True)

    class Meta:
        model = VehicleType
        fields = [
        'caption','description', 'pic', 'pic_active',
        ]
#VEHICLE SETUP FORM END


#HOLIDAY SETUP FORM START

class HolidayForm(forms.ModelForm):
    date = forms.DateField(label='date', required=False, widget=forms.DateInput(attrs={'class':'form-control input-lg','data-plugin': 'bs-date', 'placeholder': 'Date', 'id':'date', 'required':'required'}))
    caption = forms.CharField(label='caption', max_length=500, required=False, widget=forms.TextInput(attrs={'class': 'form-control input-lg', 'placeholder': 'Description', 'required':'required'}))
    # input_formats = '%d-%c-%Y',
    class Meta:
        model = HolidayList
        fields = [
        'date', 'caption',
        ]

#HOLIDAY SETUP FORM END


#POSTALURCHARGE SETUP FORM START

class SurchargeForm(forms.ModelForm):
    postal_code = forms.CharField(label='Postal Code', required=True, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    charge = forms.DecimalField(label='Fee', required=True, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    corp_charge = forms.DecimalField(label='Corporate Fee', required=True,
                                     widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    description = forms.CharField(label='Description', required=True, widget=forms.TextInput(attrs={'class': 'form-control huge'}))

    class Meta:
        model = PickupSurgeCharge
        fields = [
        'postal_code', 'charge', 'corp_charge', 'description'
        ]

class CourierSurgeChargeForm(forms.ModelForm):
    postal_code = forms.CharField(label='Postal Code', required=True,
                                  widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    charge = forms.DecimalField(label='Fee', required=True,
                                widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    corp_charge = forms.DecimalField(label='Corporate Fee', required=True,
                                widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    description = forms.CharField(label='Description', required=True,
                                  widget=forms.TextInput(attrs={'class': 'form-control huge'}))

    class Meta:
        model = CourierSurgeCharge
        fields = [
            'postal_code', 'charge', 'corp_charge','description'
        ]
#POSTALSURCHARGE SETUP FORM END


#WEEKDAYPICKUPRATES SETUP FORM START

class PickupRateForm(forms.ModelForm):
    # query = VehicleType.objects.all()
    vehicle_type = forms.ModelChoiceField(label='Vehicle Type', queryset=VehicleType.objects.all(), widget=forms.Select(attrs={'class': 'form-control huge' ,'required':'required'}))
    min_fare = forms.DecimalField(label='Min Fare', widget=forms.NumberInput(attrs={'class': 'form-control huge','required': 'required'}))
    base_fare = forms.DecimalField(label='Base Fare', widget=forms.NumberInput(attrs={'class': 'form-control huge','required': 'required'}))
    rate = forms.DecimalField(label='Rate', widget=forms.NumberInput(attrs={'class': 'form-control huge','required': 'required'}))
    waiting_charge = forms.DecimalField(label='Hour\'s charge', widget=forms.NumberInput(attrs={'class': 'form-control huge','required': 'required'}))
    peakMultiplier = forms.DecimalField(label='Sur Charge', widget=forms.NumberInput(attrs={'class': 'form-control huge','required': 'required'}))
    gst = forms.DecimalField(label='GST %', widget=forms.NumberInput(attrs={'class': 'form-control huge','required': 'required'}))
    day_type = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control huge', 'required': 'required'}))

    class Meta:
        model = PickUpRate
        fields = [
        'vehicle_type', 'min_fare', 'base_fare', 'rate', 'waiting_charge', 'peakMultiplier'
        , 'gst', 'day_type',
        ]

#WEEKDAYPICKUPRATES SETUP FORM END
class DriverStatusForm(forms.ModelForm):
    driver_type = forms.CharField(label='Driver Type', widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    courier_commission = forms.DecimalField(label='Courier Commission', widget=forms.NumberInput(attrs={'class': 'form-control huge'}))
    pickup_commission = forms.DecimalField(label='Pickup Commission' ,widget=forms.NumberInput(attrs={'class': 'form-control huge'}))

    class Meta:
        model = DriverStatus
        fields = [
            'driver_type', 'courier_commission', 'pickup_commission',
        ]

class ExpressWindowsForm(forms.ModelForm):
    """
        Now this from is for delivery window only
    """
    window_name = forms.CharField(label='Window Name', widget=forms.TextInput(attrs={'class': 'form-control huge', 'required': 'required'}))
    # start_time = forms.TimeField(required=False, input_formats=['%I:%M %p'], widget=forms.TimeInput(format='%I:%M %p', attrs={'class': 'form-control huge time-pick',
    #                                                            'id': 'start-time'}))
    # end_time = forms.TimeField(required=False, input_formats=['%I:%M %p'], widget=forms.TimeInput(format='%I:%M %p', attrs={'class': 'form-control huge time-pick',
    #                                                            'id': 'start-time'}))
    price = forms.DecimalField(label='Price', widget=forms.NumberInput(attrs={'class': 'form-control huge', 'value': '0.00'}))
    corp_price = forms.DecimalField(label='Corporate Price', widget=forms.NumberInput(attrs={'class': 'form-control huge', 'value': '0.00'}))
    max_number_of_active_job = forms.IntegerField(label='Max Number of Active Job',  widget=forms.NumberInput(attrs={'class': 'form-control huge', 'value': '10000'}))
    class Meta:
        model = ExpressWindows
        fields = [
            'window_name',
            # 'start_time',
            # 'end_time',
            'price',
            'corp_price',
            'multiple',
            'max_number_of_active_job'
        ]

class PickupGSTForm(forms.ModelForm):
    """
        Now this from is for Pickup GST only
    """
    name = forms.CharField(label='Window Name', widget=forms.TextInput(attrs={'class': 'form-control huge', 'required': 'required'}))
    price = forms.DecimalField(label='Price', widget=forms.NumberInput(attrs={'class': 'form-control huge', 'value': '0.00'}))
    class Meta:
        model = PickupGST
        fields = [
            'name',
            'percentage',
            'price'
        ]


class CourierGSTForm(forms.ModelForm):
    """
        Now this from is for Pickup GST only
    """
    name = forms.CharField(label='Window Name', widget=forms.TextInput(attrs={'class': 'form-control huge', 'required': 'required'}))
    price = forms.DecimalField(label='Price', widget=forms.NumberInput(attrs={'class': 'form-control huge', 'value': '0.00'}))
    class Meta:
        model = CourierGST
        fields = [
            'name',
            'percentage',
            'price'
        ]

class PickupTimeChargeForm(forms.ModelForm):
    """
        Now this from is for delivery window only
    """
    postal_code = forms.ModelChoiceField(label='Postal Code', queryset=Postalcode.objects.all(), widget=forms.Select(attrs={'class': 'form-control huge', 'required': 'required'}))
    start_time = forms.TimeField(required=True, input_formats=['%I:%M %p'], widget=forms.TimeInput(format='%I:%M %p', attrs={'class': 'form-control huge time-pick','id': 'start-time'}))
    end_time = forms.TimeField(required=True, input_formats=['%I:%M %p'], widget=forms.TimeInput(format='%I:%M %p', attrs={'class': 'form-control huge time-pick',
                                                               'id': 'start-time'}))
    price = forms.DecimalField(label='Price', widget=forms.NumberInput(attrs={'class': 'form-control huge', 'value': '0.00'}))
    class Meta:
        model = PickupTimeCharge
        fields = [
            'postal_code',
            'start_time',
            'end_time',
            'price'
        ]


class CourierTimeChargeForm(forms.ModelForm):
    """
        Now this from is for delivery window only
    """
    postal_code = forms.ModelChoiceField(label='Postal Code', queryset=Postalcode.objects.all(), widget=forms.Select(attrs={'class': 'form-control huge', 'required': 'required'}))
    start_time = forms.TimeField(required=True, input_formats=['%I:%M %p'], widget=forms.TimeInput(format='%I:%M %p', attrs={'class': 'form-control huge time-pick',
                                                               'id': 'start-time'}))
    end_time = forms.TimeField(required=True, input_formats=['%I:%M %p'], widget=forms.TimeInput(format='%I:%M %p', attrs={'class': 'form-control huge time-pick',
                                                               'id': 'start-time'}))
    price = forms.DecimalField(label='Price', widget=forms.NumberInput(attrs={'class': 'form-control huge', 'value': '0.00'}))
    class Meta:
        model = CourierTimeCharge
        fields = [
            'postal_code',
            'start_time',
            'end_time',
            'price'
        ]

class CommissionSetupForm(forms.ModelForm):
    category = forms.CharField(label='Category', widget=forms.Select( choices=constant.DRIVER_TYPE,
        attrs={'class': 'form-control huge', 'required': 'required'}))
    commission_courier = forms.DecimalField(label='Courier Commission',
                                            widget=forms.NumberInput(attrs={'class': 'form-control huge',
                                                                            'required': 'required'}))
    commission_pickup = forms.DecimalField(label='Pickup Commission',
                                           widget=forms.NumberInput(attrs={'class': 'form-control huge',
                                                                           'required': 'required'}))
    class Meta:
        model = CommissionSetup
        fields = [
            'category', 'commission_pickup', 'commission_courier'
        ]
class SiteSettingsForm(forms.ModelForm):
    class Meta:
        model = SiteConfigeration
        fields = ['max_minuts_for_cencellations', 'office_address', 'office_phone_no', 'office_email', 'driver_radius', 'waiting_time_for_drivers']


class PickupCancellationReasonsForm(forms.ModelForm):
    reason = forms.CharField(label='Reason', max_length=120, required=True,
                             widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    image = forms.ImageField(label='Icon', required=True)
    image_active = forms.ImageField(label='Icon Active', required=True)
    class Meta:
        model = PickupCancellationReasons
        fields = ['image', 'image_active', 'reason']

class RotatingBannerForm(forms.ModelForm):
    image = forms.ImageField(label='Icon', required=True)
    title = forms.CharField(label='Title', max_length=120, required=True,
                            widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    class Meta:
        model=RotatingBanner
        fields = ['image', 'title']