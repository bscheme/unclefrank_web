from django import template

register = template.Library()

@register.filter(name='increment')
def increment():
    value = value + 1
    return value


@register.filter(name='decrement')
def decrement(value):
    value = value - 1
    return value