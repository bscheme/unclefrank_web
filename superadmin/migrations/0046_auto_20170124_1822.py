# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0045_auto_20170122_1409'),
    ]

    operations = [
        migrations.AddField(
            model_name='siteconfigeration',
            name='driver_radius',
            field=models.IntegerField(default=5000),
        ),
        migrations.AddField(
            model_name='siteconfigeration',
            name='waiting_time_for_drivers',
            field=models.IntegerField(default=15),
        ),
    ]
