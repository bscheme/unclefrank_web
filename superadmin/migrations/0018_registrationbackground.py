# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0017_auto_20161005_1550'),
    ]

    operations = [
        migrations.CreateModel(
            name='RegistrationBackground',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=120, null=True, blank=True)),
                ('images', models.ImageField(null=True, upload_to=b'login/images', blank=True)),
            ],
        ),
    ]
