# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0009_auto_20160808_1750'),
    ]

    operations = [
        migrations.CreateModel(
            name='Careers',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('banner', models.ImageField(null=True, upload_to=b'career', blank=True)),
                ('title', models.CharField(max_length=120, null=True, blank=True)),
                ('desc', models.TextField(null=True, blank=True)),
                ('job_title', models.CharField(max_length=120, null=True, blank=True)),
                ('job_desc', models.TextField(null=True, blank=True)),
                ('job_responsibility', models.TextField(null=True, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='ContactUsPage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('addr_title', models.CharField(max_length=120, null=True, blank=True)),
                ('addr_addr', models.CharField(max_length=120, null=True, blank=True)),
                ('addr_tel', models.CharField(max_length=120, null=True, blank=True)),
                ('addr_fax', models.CharField(max_length=120, null=True, blank=True)),
                ('addr_email', models.EmailField(max_length=254, null=True, blank=True)),
                ('title', models.CharField(max_length=120, null=True, blank=True)),
                ('desc', models.TextField(null=True, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Corporate',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('banner', models.ImageField(null=True, upload_to=b'corporate/banner', blank=True)),
                ('title1', models.CharField(max_length=120, null=True, blank=True)),
                ('image1', models.ImageField(null=True, upload_to=b'corporate', blank=True)),
                ('desc1', models.TextField(null=True, blank=True)),
                ('link1_text', models.CharField(max_length=120, null=True, blank=True)),
                ('title2', models.CharField(max_length=120, null=True, blank=True)),
                ('image2', models.ImageField(null=True, upload_to=b'corporate', blank=True)),
                ('desc2', models.TextField(null=True, blank=True)),
                ('link2_text', models.CharField(max_length=120, null=True, blank=True)),
                ('title3', models.CharField(max_length=120, null=True, blank=True)),
                ('image3', models.ImageField(null=True, upload_to=b'corporate', blank=True)),
                ('desc3', models.TextField(null=True, blank=True)),
                ('link3_text', models.CharField(max_length=120, null=True, blank=True)),
                ('title4', models.CharField(max_length=120, null=True, blank=True)),
                ('image4', models.ImageField(null=True, upload_to=b'corporate', blank=True)),
                ('desc4', models.TextField(null=True, blank=True)),
                ('link4_text', models.CharField(max_length=120, null=True, blank=True)),
            ],
        ),
    ]
