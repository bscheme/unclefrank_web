# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0018_registrationbackground'),
    ]

    operations = [
        migrations.CreateModel(
            name='DriverStatus',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('driver_type', models.CharField(max_length=120)),
                ('courier_commission', models.DecimalField(max_digits=5, decimal_places=2)),
                ('pickup_commission', models.DecimalField(max_digits=5, decimal_places=2)),
            ],
        ),
    ]
