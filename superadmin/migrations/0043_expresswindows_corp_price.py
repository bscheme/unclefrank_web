# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0042_auto_20170115_1536'),
    ]

    operations = [
        migrations.AddField(
            model_name='expresswindows',
            name='corp_price',
            field=models.DecimalField(default=0.0, max_digits=7, decimal_places=2),
        ),
    ]
