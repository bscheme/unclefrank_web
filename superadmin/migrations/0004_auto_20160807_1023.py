# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0003_aboutpage'),
    ]

    operations = [
        migrations.AlterField(
            model_name='aboutpage',
            name='desc1',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='aboutpage',
            name='desc2',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='aboutpage',
            name='desc3',
            field=models.TextField(null=True, blank=True),
        ),
    ]
