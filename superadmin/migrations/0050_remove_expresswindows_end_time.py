# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0049_auto_20170220_1914'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='expresswindows',
            name='end_time',
        ),
    ]
