# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0016_sliderimage'),
    ]

    operations = [
        migrations.RenameField(
            model_name='sliderimage',
            old_name='image_description',
            new_name='desc',
        ),
        migrations.RenameField(
            model_name='sliderimage',
            old_name='the_images',
            new_name='images',
        ),
        migrations.RenameField(
            model_name='sliderimage',
            old_name='image_title',
            new_name='title',
        ),
    ]
