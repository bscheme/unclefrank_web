# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0014_homecms_app_text'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='homecms',
            name='app_text',
        ),
    ]
