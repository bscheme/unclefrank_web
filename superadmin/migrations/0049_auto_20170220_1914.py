# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0048_auto_20170220_1539'),
    ]

    operations = [
        migrations.AddField(
            model_name='expresswindows',
            name='end_time',
            field=models.TimeField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='expresswindows',
            name='start_time',
            field=models.TimeField(null=True, blank=True),
        ),
    ]
