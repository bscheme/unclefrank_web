# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0034_customerfaqpage'),
    ]

    operations = [
        migrations.RenameField(
            model_name='customerfaqpage',
            old_name='desc',
            new_name='answer',
        ),
    ]
