# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0041_auto_20170110_1901'),
    ]

    operations = [
        migrations.AddField(
            model_name='expresswindows',
            name='end_time',
            field=models.TimeField(default=None, null=True),
        ),
        migrations.AddField(
            model_name='expresswindows',
            name='start_time',
            field=models.TimeField(default=None, null=True),
        ),
        migrations.AlterField(
            model_name='expresswindows',
            name='price',
            field=models.DecimalField(default=0.0, max_digits=7, decimal_places=2),
        ),
    ]
