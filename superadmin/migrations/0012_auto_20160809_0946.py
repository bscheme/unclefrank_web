# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0011_auto_20160809_0703'),
    ]

    operations = [
        migrations.AlterField(
            model_name='homecms',
            name='feature1_desc',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='homecms',
            name='feature2_desc',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='homecms',
            name='feature3_desc',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='homecms',
            name='feature4_desc',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='homecms',
            name='features_desc',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='homecms',
            name='service1_desc',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='homecms',
            name='service2_desc',
            field=models.TextField(null=True, blank=True),
        ),
    ]
