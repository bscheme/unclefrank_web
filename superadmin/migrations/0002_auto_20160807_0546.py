# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='homecms',
            name='footer1_desc',
            field=models.TextField(null=True, blank=True),
        ),
    ]
