# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0038_accesscontrol'),
    ]

    operations = [
        migrations.AddField(
            model_name='accesscontrol',
            name='is_address_setup',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='accesscontrol',
            name='is_window_setup',
            field=models.BooleanField(default=False),
        ),
    ]
