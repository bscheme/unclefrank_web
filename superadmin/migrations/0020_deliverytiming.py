# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0019_driverstatus'),
    ]

    operations = [
        migrations.CreateModel(
            name='DeliveryTiming',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('start_time', models.CharField(max_length=120, null=True)),
                ('end_time', models.CharField(max_length=120, null=True)),
            ],
        ),
    ]
