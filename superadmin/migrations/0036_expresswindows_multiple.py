# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0035_auto_20161222_1750'),
    ]

    operations = [
        migrations.AddField(
            model_name='expresswindows',
            name='multiple',
            field=models.BooleanField(default=False),
        ),
    ]
