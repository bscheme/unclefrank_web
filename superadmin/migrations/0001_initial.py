# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='HomeCMS',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('logo', models.ImageField(null=True, upload_to=b'', blank=True)),
                ('banner', models.ImageField(null=True, upload_to=b'', blank=True)),
                ('banner_text', models.CharField(max_length=120, null=True, blank=True)),
                ('banner_desc', models.CharField(max_length=120, null=True, blank=True)),
                ('under_banner_text', models.CharField(max_length=120, null=True, blank=True)),
                ('service1_image', models.ImageField(null=True, upload_to=b'', blank=True)),
                ('service1_title', models.CharField(max_length=120, null=True, blank=True)),
                ('service1_desc', models.CharField(max_length=120, null=True, blank=True)),
                ('service2_image', models.ImageField(null=True, upload_to=b'', blank=True)),
                ('service2_title', models.CharField(max_length=120, null=True, blank=True)),
                ('service2_desc', models.CharField(max_length=120, null=True, blank=True)),
                ('features', models.CharField(max_length=120, null=True, blank=True)),
                ('features_desc', models.CharField(max_length=120, null=True, blank=True)),
                ('feature1_image', models.ImageField(null=True, upload_to=b'', blank=True)),
                ('feature1_title', models.CharField(max_length=120, null=True, blank=True)),
                ('feature1_desc', models.CharField(max_length=120, null=True, blank=True)),
                ('feature2_image', models.ImageField(null=True, upload_to=b'', blank=True)),
                ('feature2_title', models.CharField(max_length=120, null=True, blank=True)),
                ('feature2_desc', models.CharField(max_length=120, null=True, blank=True)),
                ('feature3_image', models.ImageField(null=True, upload_to=b'', blank=True)),
                ('feature3_title', models.CharField(max_length=120, null=True, blank=True)),
                ('feature3_desc', models.CharField(max_length=120, null=True, blank=True)),
                ('feature4_image', models.ImageField(null=True, upload_to=b'', blank=True)),
                ('feature4_title', models.CharField(max_length=120, null=True, blank=True)),
                ('feature4_desc', models.CharField(max_length=120, null=True, blank=True)),
                ('footer1_text', models.CharField(max_length=120, null=True, blank=True)),
                ('footer1_desc', models.CharField(max_length=120, null=True, blank=True)),
                ('social1_icons', models.URLField(null=True, blank=True)),
                ('social2_icons', models.URLField(null=True, blank=True)),
                ('social3_icons', models.URLField(null=True, blank=True)),
                ('social4_icons', models.URLField(null=True, blank=True)),
                ('social5_icons', models.URLField(null=True, blank=True)),
                ('footer2_text', models.CharField(max_length=120, null=True, blank=True)),
                ('footer2_url1', models.URLField(null=True, blank=True)),
                ('footer2_url2', models.URLField(null=True, blank=True)),
                ('footer2_url3', models.URLField(null=True, blank=True)),
                ('footer2_url4', models.URLField(null=True, blank=True)),
                ('footer2_url5', models.URLField(null=True, blank=True)),
                ('footer3_url1', models.URLField(null=True, blank=True)),
                ('footer3_url2', models.URLField(null=True, blank=True)),
                ('footer3_url3', models.URLField(null=True, blank=True)),
                ('footer4', models.CharField(max_length=120, null=True, blank=True)),
            ],
        ),
    ]
