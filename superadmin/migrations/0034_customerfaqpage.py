# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0033_siteconfigeration_max_minuts_for_cencellations'),
    ]

    operations = [
        migrations.CreateModel(
            name='CustomerFaqPage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('question', models.CharField(max_length=120, null=True, blank=True)),
                ('desc', models.TextField(null=True, blank=True)),
            ],
        ),
    ]
