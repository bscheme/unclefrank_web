# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0032_siteconfigeration'),
    ]

    operations = [
        migrations.AddField(
            model_name='siteconfigeration',
            name='max_minuts_for_cencellations',
            field=models.IntegerField(default=100),
        ),
    ]
