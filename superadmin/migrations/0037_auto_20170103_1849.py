# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0036_expresswindows_multiple'),
    ]

    operations = [
        migrations.AddField(
            model_name='siteconfigeration',
            name='office_address',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='siteconfigeration',
            name='office_email',
            field=models.CharField(max_length=120, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='siteconfigeration',
            name='office_phone_no',
            field=models.CharField(max_length=120, null=True, blank=True),
        ),
    ]
