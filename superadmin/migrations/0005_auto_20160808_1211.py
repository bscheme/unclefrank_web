# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0004_auto_20160807_1023'),
    ]

    operations = [
        migrations.AlterField(
            model_name='aboutpage',
            name='banner',
            field=models.ImageField(null=True, upload_to=b'about', blank=True),
        ),
        migrations.AlterField(
            model_name='aboutpage',
            name='image1',
            field=models.ImageField(null=True, upload_to=b'about', blank=True),
        ),
        migrations.AlterField(
            model_name='aboutpage',
            name='image2',
            field=models.ImageField(null=True, upload_to=b'about/iteam', blank=True),
        ),
        migrations.AlterField(
            model_name='aboutpage',
            name='image3',
            field=models.ImageField(null=True, upload_to=b'about/iteam', blank=True),
        ),
        migrations.AlterField(
            model_name='aboutpage',
            name='image4',
            field=models.ImageField(null=True, upload_to=b'about/iteam', blank=True),
        ),
        migrations.AlterField(
            model_name='aboutpage',
            name='video',
            field=models.FileField(null=True, upload_to=b'about/video', blank=True),
        ),
        migrations.AlterField(
            model_name='homecms',
            name='banner',
            field=models.ImageField(null=True, upload_to=b'home', blank=True),
        ),
        migrations.AlterField(
            model_name='homecms',
            name='feature1_image',
            field=models.ImageField(null=True, upload_to=b'home', blank=True),
        ),
        migrations.AlterField(
            model_name='homecms',
            name='feature2_image',
            field=models.ImageField(null=True, upload_to=b'home', blank=True),
        ),
        migrations.AlterField(
            model_name='homecms',
            name='feature3_image',
            field=models.ImageField(null=True, upload_to=b'home', blank=True),
        ),
        migrations.AlterField(
            model_name='homecms',
            name='feature4_image',
            field=models.ImageField(null=True, upload_to=b'home', blank=True),
        ),
        migrations.AlterField(
            model_name='homecms',
            name='logo',
            field=models.ImageField(null=True, upload_to=b'home', blank=True),
        ),
        migrations.AlterField(
            model_name='homecms',
            name='service1_image',
            field=models.ImageField(null=True, upload_to=b'home', blank=True),
        ),
        migrations.AlterField(
            model_name='homecms',
            name='service2_image',
            field=models.ImageField(null=True, upload_to=b'home', blank=True),
        ),
    ]
