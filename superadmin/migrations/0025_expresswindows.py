# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0024_faq_privacy'),
    ]

    operations = [
        migrations.CreateModel(
            name='ExpressWindows',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('window_name', models.CharField(max_length=120, null=True)),
                ('price', models.DecimalField(max_digits=7, decimal_places=2)),
            ],
        ),
    ]
