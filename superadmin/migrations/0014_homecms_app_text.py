# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0013_homecms_category'),
    ]

    operations = [
        migrations.AddField(
            model_name='homecms',
            name='app_text',
            field=models.CharField(max_length=120, null=True, blank=True),
        ),
    ]
