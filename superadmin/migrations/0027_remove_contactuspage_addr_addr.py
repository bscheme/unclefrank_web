# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0026_contactuspage_contact_location'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='contactuspage',
            name='addr_addr',
        ),
    ]
