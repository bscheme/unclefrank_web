# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0037_auto_20170103_1849'),
    ]

    operations = [
        migrations.AddField(
            model_name='drivercategory',
            name='max_number_of_active_job',
            field=models.IntegerField(default=1000),
        ),
        migrations.AddField(
            model_name='expresswindows',
            name='max_number_of_active_job',
            field=models.IntegerField(default=1000),
        ),
    ]
