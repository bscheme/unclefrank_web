from __future__ import division
from datetime import date
import calendar
from django.shortcuts import render, render_to_response, redirect
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404
from .forms import RotatingBannerForm, PickupCancellationReasonsForm,CourierGSTForm, PickupGSTForm,SiteSettingsForm, HomeCMSForm, CourierTimeChargeForm, PickupRateForm, CourierSurgeChargeForm, PickupTimeChargeForm, HolidayForm, SurchargeForm, AboutPageForm, ServicesPageForm, JoinUsPageForm, CareersPageForm, CorporatePageForm, ContactUsPageForm, PackageForm, VehicleForm, pricingForm, SliderImageForm, RegistrationBackgroundForm, DriverStatusForm, TermsForm, FaqForm, PrivacyForm, ExpressWindowsForm, CommissionSetupForm, CustomerFaqPageForm
from django.core.context_processors import csrf
from .models import RotatingBanner, SiteConfigeration, HomeCMS, AboutPage, Services, JoinUs, Careers, Corporate, ContactUsPage, SliderImage, RegistrationBackground, DriverStatus, DeliveryTiming, DriverCategory, Terms, Faq, Privacy, ExpressWindows, CommissionSetup, CustomerFaqPage, AccessControl
from django.template import RequestContext
from django.contrib.auth import authenticate, login
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from service.models import PickupCancellationReasons,CorporateDriverCustomer, CourierGST,PickupGST,PackageSize, VehicleType, CourierTimeCharge, CartonType, ScheduleType, DayType, Pricing, HolidayList, PickupTimeCharge, PickupSurgeCharge, PickUpRate, DeliveryService, CourierSurgeCharge,PickupService, CancellationInfo, ActivityLog, AdminMessage, DriverRating, Payment, Package
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect
from customer.models import Customer
from driver.models import Driver, Vechile
import datetime
from django.db.models import Q
import itertools
import functools
from base import constant
from django.views.decorators.csrf import csrf_exempt
from django.db.models import Sum
from apis.views import get_acceptacce_rate, get_cencell_rate
import csv
from easy_pdf.views import PDFTemplateView
from django.template import RequestContext
from django.contrib.auth import logout



# Create your views here.
def logout_view(request):
    logout(request)
    return redirect('/sadmin/login/')


def customer_faq_form(request):
    arg = RequestContext(request)
    if not request.user.is_active:
        return redirect ('/sadmin/login/')

    instance = CustomerFaqPage.objects.all()
    form = CustomerFaqPageForm()



    if request.method == "POST":
        form = CustomerFaqPageForm(request.POST)
        if form.is_valid():
            form.save()
            redirect ('/sadmin/customer_faq_form/')
    context = {
        'form': form,
        'instance': instance,
    }
    context.update(csrf(request))
    return render_to_response('superadmin/customer_faq_page.html',context, arg)

def customer_faq_form_update(request, question_id):
    arg = RequestContext(request)
    question = CustomerFaqPage.objects.filter(id=question_id)
    form = CustomerFaqPageForm(instance=question[0])
    if request.method == 'POST':
        form = CustomerFaqPageForm(request.POST, instance=question[0])
        if form.is_valid():
            # print "ok"
            form.save()
            return redirect('/sadmin/customer_faq_form/')
    context = {
        'form': form,
        'instance': question,
    }
    context.update(csrf(request))
    return render_to_response('superadmin/customer_faq_page.html', context, arg)


def customer_faq_form_delete(request, question_id):
    question = CustomerFaqPage.objects.get(id=question_id)
    question.delete()
    return redirect('/sadmin/customer_faq_form/')


def pricing_form(request):
    arg = RequestContext(request)
    if not request.user.is_active:
        return redirect ('/sadmin/login/')
    instance = Pricing.objects.all()
    form = pricingForm()
    context= {
        'form': form,
        'instance': instance,
    }

    if request.method == "POST":
        form = pricingForm(request.POST)
        if form.is_valid():
            form.save()
            redirect ('/sadmin/pricing_form/')
            
    context.update(csrf(request))
    return render_to_response('superadmin/pricingform.html',context,arg)


def pricing_delete(request, pricing_id):
    selectedPrice= Pricing.objects.get(id=pricing_id)
    selectedPrice.delete(False)
    return redirect('/sadmin/pricing_form/')

def first_day_of_month(d):
    return date(d.year, d.month, 1)

def last_day_of_month(d):
    first = first_day_of_month(d)
    last_day = calendar.monthrange(d.year, d.month)[1]
    last = first + datetime.timedelta(days=last_day-1)
    return last
#@login_required
def home(request):
    context = RequestContext(request)
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    delivery = DeliveryService.objects.all().order_by('-id')[:5]
    sum = Payment.objects.aggregate(Sum('paid_amount'))
    if Payment.objects.all().count() > 0:
        avg = sum['paid_amount__sum']/Payment.objects.all().count()
    else:
        avg = 0
    total_deliveries =DeliveryService.objects.all().count()
    total_pickup = PickupService.objects.all().count()
    total_jobs = total_deliveries + total_pickup
    if total_jobs > 0:
        percent_deliveries = (total_deliveries/total_jobs)*100
        percent_pickup = (total_pickup / total_jobs) * 100
    else:
        percent_deliveries = 0
        percent_pickup = 0


    pending_courier = DeliveryService.objects.filter(delevery_status='pending').count()
    assigned_courier = DeliveryService.objects.filter(delevery_status='assigned').count()
    taken_courier = DeliveryService.objects.filter(delevery_status='taken').count()
    completed_courier = DeliveryService.objects.filter(delevery_status='completed').count()
    cancelled_courier = DeliveryService.objects.filter(delevery_status='cancelled').count()



    if DeliveryService.objects.all().count() > 0:
        percent_pending_del = (pending_courier / DeliveryService.objects.all().count())
    else:
        percent_pending_del = 0
    if DeliveryService.objects.all().count() > 0:
        percent_assigned_del = (assigned_courier / DeliveryService.objects.all().count())
    else:
        percent_assigned_del = 0
    if DeliveryService.objects.all().count() > 0:
        percent_taken_del = (taken_courier / DeliveryService.objects.all().count())
    else:
        percent_taken_del = 0
    if DeliveryService.objects.all().count() > 0:
        percent_completed_del = (completed_courier / DeliveryService.objects.all().count())
    else:
        percent_completed_del = 0
    if DeliveryService.objects.all().count() > 0:
        percent_cancelled_del = (cancelled_courier / DeliveryService.objects.all().count())
    else:
        percent_cancelled_del = 0
    # print first_day_of_month(datetime.datetime.today())
    # print last_day_of_month(datetime.datetime.today())

    pending_pickup = PickupService.objects.filter(pickup_status='pending').count()
    assigned_pickup = PickupService.objects.filter(pickup_status='assigned').count()
    taken_pickup = PickupService.objects.filter(pickup_status='taken').count()
    completed_pickup = PickupService.objects.filter(pickup_status='completed').count()
    cancelled_pickup = PickupService.objects.filter(pickup_status='cancelled').count()

    if PickupService.objects.all().count() > 0:
        percent_pending_pick = (pending_pickup / PickupService.objects.all().count())
    else:
        percent_pending_pick = 0
    if PickupService.objects.all().count() > 0:
        percent_assigned_pick = (assigned_pickup / PickupService.objects.all().count())
    else:
        percent_assigned_pick = 0
    if PickupService.objects.all().count() > 0:
        percent_taken_pick = (taken_pickup / PickupService.objects.all().count())
    else:
        percent_taken_pick = 0
    if PickupService.objects.all().count() > 0:
        percent_completed_pick = (completed_pickup / PickupService.objects.all().count())
    else:
        percent_completed_pick = 0
    if PickupService.objects.all().count() > 0:
        percent_cancelled_pick = (cancelled_pickup / PickupService.objects.all().count())
    else:
        percent_cancelled_pick = 0





    arg={}
    arg['per_del'] = percent_deliveries
    arg['per_pick'] = percent_pickup
    arg['delivery'] = delivery

    arg['percent_pending_del'] = percent_pending_del
    arg['percent_assigned_del'] = percent_assigned_del
    arg['percent_taken_del'] = percent_taken_del
    arg['percent_completed_del'] = percent_completed_del
    arg['percent_cancelled_del'] = percent_cancelled_del

    arg['percent_pending_pick'] = percent_pending_pick
    arg['percent_assigned_pick'] = percent_assigned_pick
    arg['percent_taken_pick'] = percent_taken_pick
    arg['percent_completed_pick'] = percent_completed_pick
    arg['percent_cancelled_pick'] = percent_cancelled_pick

    arg['delivery'] = delivery
    arg['sum'] = sum
    arg['avg'] = avg
    return render_to_response('superadmin/home.html', arg, context)

def super_admin_login(request):
    # context = RequestContext(request)
    arg = {}
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user:
            if user.is_active and user.is_superuser:
                login(request, user)
                return redirect ('/sadmin/')
        else:
            arg['error'] = "Invalid username or password"
    arg.update(csrf(request))
    return render_to_response('superadmin/login.html', arg)


#     return render_to_response('base/pages/index_superadmin.html', {})
#@login_required
def homepage_edit(request):
    arg = RequestContext(request)
    if not request.user.is_active:
        return redirect ('/sadmin/login/')
    try:
        instance = HomeCMS.objects.all()
        instance = instance[0]
        form = HomeCMSForm(instance=instance)
    except:
        form = HomeCMSForm()
    if request.method == "POST":
        if instance:
            form = HomeCMSForm(request.POST, request.FILES, instance=instance)
        else:
            form = HomeCMSForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('/sadmin/homepage_edit/')
    context= {
        'form': form,
        'instance': instance,
    }
    context.update(csrf(request))
    return render_to_response('superadmin/homepage_edit.html', context, arg)



#@login_required
def aboutpage_edit(request):
    arg = RequestContext(request)
    sliderinstance = SliderImage.objects.all()
    backgroundimageinstance = RegistrationBackground.objects.all()
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    try:
        instance = AboutPage.objects.all()
        instance = instance[0]
        form = AboutPageForm(instance=instance)
        sliderinstance = SliderImage.objects.all()
        sliderimageform = SliderImageForm()
        backgroundimageinstance = RegistrationBackground.objects.all()
        backgroundimageform = RegistrationBackgroundForm()
    except:
        form = AboutPageForm()
        sliderimageform = SliderImageForm()
        backgroundimageform = RegistrationBackgroundForm()
    if request.method == "POST" and 'btn-form-1' in request.POST:
        if instance:
            form = AboutPageForm(request.POST, request.FILES, instance=instance)
        else:
            form = AboutPageForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            redirect('/sadmin/aboutpage_edit/')
    elif request.method == "POST" and 'btn-form-2' in request.POST:
        sliderimageform = SliderImageForm(request.POST, request.FILES)
        if sliderimageform.is_valid():
            sliderimageform.save()
            redirect('/sadmin/aboutpage_edit/')
    elif request.method == "POST" and 'btn-form-3' in request.POST:
        backgroundimageform = RegistrationBackgroundForm(request.POST, request.FILES)
        if backgroundimageform.is_valid():
            if RegistrationBackground.objects.all():
                RegistrationBackground.objects.all().delete()
            backgroundimageform.save()
            redirect('/sadmin/aboutpage_edit/')

    context = {
        'form': form,
        'instance': instance,
        'sliderimageform': sliderimageform,
        'sliderinstance': sliderinstance,
        'backgroundimageform': backgroundimageform,
        'backgroundimageinstance': backgroundimageinstance
    }
    context.update(csrf(request))
    return render_to_response('superadmin/index_page_form.html', context, arg)





def slider_delete(request, image_id):
    selectedImage = SliderImage.objects.get(id=image_id)
    selectedImage.delete(False)
    instance = AboutPage.objects.all()
    instance = instance[0]
    form = AboutPageForm()
    sliderimageform = SliderImageForm(request.POST, request.FILES)
    sliderinstance = SliderImage.objects.all()
    backgroundimageinstance = RegistrationBackground.objects.all()
    backgroundimageform = RegistrationBackgroundForm(request.POST, request.FILES)

    context = {
        'form': form,
        'instance': instance,
        'sliderimageform': sliderimageform,
        'sliderinstance': sliderinstance,
        'backgroundimageform': backgroundimageform,
        'backgroundimageinstance': backgroundimageinstance
    }
    context.update(csrf(request))
    return redirect('/sadmin/aboutpage_edit/', context)


def regback_delete(request, image_id):
    selectedImage = RegistrationBackground.objects.get(id=image_id)
    selectedImage.delete(False)
    instance = AboutPage.objects.all()
    instance = instance[0]
    form = AboutPageForm()
    sliderimageform = SliderImageForm(request.POST, request.FILES)
    sliderinstance = SliderImage.objects.all()
    backgroundimageinstance = RegistrationBackground.objects.all()
    backgroundimageform = RegistrationBackgroundForm(request.POST, request.FILES)

    context = {
        'form': form,
        'instance': instance,
        'sliderimageform': sliderimageform,
        'sliderinstance': sliderinstance,
        'backgroundimageform': backgroundimageform,
        'backgroundimageinstance': backgroundimageinstance
    }
    context.update(csrf(request))
    return redirect('/sadmin/aboutpage_edit/', context)

def services_edit(request):
    arg = RequestContext(request)
    if not request.user.is_active:
        return redirect ('/sadmin/login/')
    try:
        instance = Services.objects.all()
        instance = instance[0]
        form = ServicesPageForm(instance=instance)
    except:
        form = ServicesPageForm()
    if request.method == "POST":
        if instance:
            form = ServicesPageForm(request.POST, request.FILES, instance=instance)
        else:
            form = ServicesPageForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect ('/sadmin/services_edit/')
    context= {
        'form': form,
        'instance': instance,
    }
    context.update(csrf(request))
    return render_to_response('superadmin/servicepage_edit.html',context,arg)

def joinus_edit(request):
    arg = RequestContext(request)
    if not request.user.is_active:
        return redirect ('/sadmin/login/')
    try:
        instance = JoinUs.objects.all()
        instance = instance[0]
        form = JoinUsPageForm(instance=instance)
    except:
        form = JoinUsPageForm()
    if request.method == "POST":
        if instance:
            form = JoinUsPageForm(request.POST, request.FILES, instance=instance)
        else:
            form = JoinUsPageForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect ('/sadmin/joinus_edit/')
    context= {
        'form': form,
        'instance': instance,
    }
    context.update(csrf(request))
    return render_to_response('superadmin/joinus_edit.html',context,arg)


def careers_edit(request):
    arg = RequestContext(request)
    if not request.user.is_active:
        return redirect ('/sadmin/login/')
    try:
        instance = Careers.objects.all()
        instance = instance[0]
        form = CareersPageForm(instance=instance)
    except:
        form = CareersPageForm()
    if request.method == "POST":
        if instance:
            form = CareersPageForm(request.POST, request.FILES, instance=instance)
        else:
            form = CareersPageForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect ('/sadmin/careers_edit/')
    context= {
        'form': form,
        'instance': instance,
    }
    context.update(csrf(request))
    return render_to_response('superadmin/careers_edit.html',context, arg)

def corporate_edit(request):
    arg = RequestContext(request)
    if not request.user.is_active:
        return redirect ('/sadmin/login/')
    try:
        instance = Corporate.objects.all()
        instance = instance[0]
        form = CorporatePageForm(instance=instance)
    except:
        form = CorporatePageForm()
    if request.method == "POST":
        if instance:
            form = CorporatePageForm(request.POST, request.FILES, instance=instance)
        else:
            form = CorporatePageForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect ('/sadmin/corporate_edit/')
    context= {
        'form': form,
        'instance': instance,
    }
    context.update(csrf(request))
    return render_to_response('superadmin/corporate_edit.html',context, arg)

def contactus_edit(request):
    arg = RequestContext(request)
    if not request.user.is_active:
        return redirect ('/sadmin/login/')
    try:
        instance = ContactUsPage.objects.all()
        instance = instance[0]
        form = ContactUsPageForm(instance=instance)
    except:
        form = ContactUsPageForm()
    if request.method == "POST":
        if instance:
            form = ContactUsPageForm(request.POST, instance=instance)
        else:
            form = ContactUsPageForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect ('/sadmin/contactus_edit/')
    context= {
        'form': form,
        'instance': instance,
    }
    context.update(csrf(request))
    return render_to_response('superadmin/contactuspage_edit.html', context, arg)

def vehicle_edit(request):
    arg = RequestContext(request)
    if not request.user.is_active:
        return redirect('/sadmin/login')
    instance = VehicleType.objects.all()
    form = VehicleForm()
    if request.method == 'POST':
        form = VehicleForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('/sadmin/vehicle_edit/')
    context = {
        'form': form,
        'instance': instance,
    }

    context.update(csrf(request))
    return render_to_response('superadmin/vehicle_edit.html', context, arg)

def vehicle_delete(request, vehichle_id):
    if not request.user.is_active:
        return redirect('/sadmin/login')
    selectedVehicle = VehicleType.objects.get(id=vehichle_id)
    selectedVehicle.delete(False)
    form = VehicleForm()
    instance = VehicleType.objects.all()
    context = {
        'form': form,
        'instance': instance,
    }
    context.update(csrf(request))
    return redirect('/sadmin/vehicle_edit/', context)

def package_edit(request):
    arg = RequestContext(request)
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    instance = PackageSize.objects.all()
    #instance = instance[0]
    form = PackageForm()
    if request.method == "POST":
        form = PackageForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('/sadmin/package_edit/')
    context= {
        'form': form,
        'instance': instance,
    }
    context.update(csrf(request))
    return render_to_response('superadmin/package_edit.html',context,arg)

def package_delete(request, package_id):
    selectedPackage = PackageSize.objects.get(id=package_id)
    selectedPackage.delete(False)
    form = PackageForm()
    instance = PackageSize.objects.all()
    context= {
        'form': form,
        'instance': instance,
    }
    context.update(csrf(request))
    return redirect('/sadmin/package_edit/')


def package_update(request, package_id):
    arg = RequestContext(request)
    package = PackageSize.objects.get(id=package_id)
    instance = [package]
    # print(instance[0].name)
    form = PackageForm(instance=instance[0])
    if request.method == 'POST':
        form = PackageForm(request.POST, request.FILES, instance=instance[0])
        if form.is_valid():
            # print "ok"
            form.save()
            form = PackageForm()
            instance = PackageSize.objects.all()
            context= {
                'form': form,
                'instance': instance,
            }
            context.update(csrf(request))
            return redirect('/sadmin/package_edit/')
    context = {
        'form': form,
        'instance': instance,
    }
    context.update(csrf(request))
    return render_to_response('superadmin/package_edit.html',context,arg)


def holiday_list(request):
    arg = RequestContext(request)
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    form = HolidayForm()
    holidays = HolidayList.objects.all()
    if request.method == "POST":
        form = HolidayForm(request.POST)
        # form.date = datetime.strptime(request.POST['date'], '%d-%b-%Y').date()
        # print form.date
        # form.caption = request.POST['caption']
        # print form.caption
        # form = HolidayForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/sadmin/holiday_list/')
        else:
            # print "error"
            pass
    context = {
        'form': form,
        'holidays' : holidays,
    }
    context.update(csrf(request))
    return render_to_response('superadmin/holiday_list.html', context, arg)

def holiday_list_update(request, holiday_id):
    arg = RequestContext(request)
    holiday = HolidayList.objects.get(id=holiday_id)
    instance = [holiday]
    form = HolidayForm(instance=instance[0])
    if request.method == 'POST':
        form = HolidayForm(request.POST, instance=instance[0])
        if form.is_valid():
            # print "ok"
            form.save()
            form = HolidayForm()
            instance = HolidayList.objects.all()
            context= {
                'form': form,
                'holidays': instance,
            }
            context.update(csrf(request))
            return redirect('/sadmin/holiday_list/',context)
    context = {
        'form': form,
        'holidays': instance,
    }
    context.update(csrf(request))
    return render_to_response('superadmin/holiday_list.html',context, arg)

def holiday_list_delete(request, holiday_id):
    holiday = HolidayList.objects.get(id=holiday_id)
    holiday.delete(False)
    form = HolidayForm()
    instance = HolidayList.objects.all()
    return redirect('/sadmin/holiday_list/')


def courier_surcharge(request):
    arg = RequestContext(request)
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    form = CourierSurgeChargeForm()
    charges = CourierSurgeCharge.objects.all()
    if request.method == "POST":
        form = CourierSurgeChargeForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('/sadmin/courier_surcharge/')
    context = {
        'form': form,
        'charges' : charges,
    }
    context.update(csrf(request))
    return render_to_response('superadmin/courier_postal_surcharge.html', context, arg)

def courier_surcharge_update(request, surcharge_id):
    arg = RequestContext(request)
    charge = CourierSurgeCharge.objects.get(id=surcharge_id)
    instance = [charge]
    form = CourierSurgeChargeForm(instance=instance[0])
    if request.method == 'POST':
        form = CourierSurgeChargeForm(request.POST, request.FILES, instance=instance[0])
        if form.is_valid():
            # print "ok"
            form.save()
            form = CourierSurgeChargeForm()
            instance = CourierSurgeCharge.objects.all()
            context= {
                'form': form,
                'charges': instance,
            }
            context.update(csrf(request))
            return redirect('/sadmin/courier_surcharge/')
    context = {
        'form': form,
        'charges': instance,
    }
    context.update(csrf(request))
    return render_to_response('superadmin/courier_postal_surcharge.html',context,arg)

def courier_surcharge_delete(request, surcharge_id):
    charge = CourierSurgeCharge.objects.get(id=surcharge_id)
    charge.delete(False)
    form = CourierSurgeChargeForm()
    instance = CourierSurgeCharge.objects.all()
    return redirect('/sadmin/courier_surcharge/')

def pickup_surcharge(request):
    arg = RequestContext(request)
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    form = SurchargeForm()
    charges = PickupSurgeCharge.objects.all()
    if request.method == "POST":
        form = SurchargeForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('/sadmin/pickup_surcharge/')
    context = {
        'form': form,
        'charges' : charges,
    }
    context.update(csrf(request))
    return render_to_response('superadmin/postal_surcharge.html', context, arg)

def pickup_surcharge_update(request, surcharge_id):
    arg = RequestContext(request)
    charge = PickupSurgeCharge.objects.get(id=surcharge_id)
    instance = [charge]
    form = SurchargeForm(instance=instance[0])
    if request.method == 'POST':
        form = SurchargeForm(request.POST, request.FILES, instance=instance[0])
        if form.is_valid():
            # print "ok"
            form.save()
            form = SurchargeForm()
            instance = PickupSurgeCharge.objects.all()
            context= {
                'form': form,
                'charges': instance,
            }
            context.update(csrf(request))
            return redirect('/sadmin/pickup_surcharge/',context)
    context = {
        'form': form,
        'charges': instance,
    }
    context.update(csrf(request))
    return render_to_response('superadmin/postal_surcharge.html',context,arg)

def pickup_surcharge_delete(request, surcharge_id):
    charge = PickupSurgeCharge.objects.get(id=surcharge_id)
    charge.delete(False)
    form = SurchargeForm()
    instance = PickupSurgeCharge.objects.all()
    return redirect('/sadmin/pickup_surcharge/')

def passenger_pickup_rates(request):
    arg = RequestContext(request)
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    form = PickupRateForm(initial={'day_type': 'Weekday'})
    form_two = PickupRateForm(initial={'day_type': 'Weekend'})
    charges = PickUpRate.objects.all().filter(day_type = 'Weekday')
    charges_two = PickUpRate.objects.all().filter(day_type = 'Weekend')
    if request.method == "POST":
        form = PickupRateForm(request.POST)
        # print "form object created"
        if form.is_valid():
            form.save()
            return redirect('/sadmin/passenger_pickup_rates/')
        else:
            return HttpResponse("Form is invalid")
    context = {
        'form': form,
        'form_two' : form_two,
        'charges' : charges,
        'charges_two' : charges_two,
    }
    context.update(csrf(request))
    return render_to_response('superadmin/passenger_pickup_rates.html', context, arg)


def pricing_update(request, pricing_id):
    arg = RequestContext(request)
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    pricing = Pricing.objects.get(id=pricing_id)
    instance = [pricing]
    # print(instance[0].name)
    form = pricingForm(instance=instance[0])
    if request.method == 'POST':
        form = pricingForm(request.POST, instance=instance[0])
        if form.is_valid():
            # print "ok"
            form.save()
            form = pricingForm()
            instance = Pricing.objects.all()
            context = {
                'form': form,
                'instance': instance,
            }
            context.update(csrf(request))
            return redirect('/sadmin/pricing_form/', context)
    context = {
        'form': form,
        'instance': instance,
    }
    context.update(csrf(request))
    return render_to_response('superadmin/pricingform.html', context, arg)

def vehicle_update(request, vehicle_id):
    arg = RequestContext(request)
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    package = VehicleType.objects.get(id=vehicle_id)
    instance = [package]
    # print(instance[0].caption)
    form = VehicleForm(instance=instance[0])
    if request.method == 'POST':
        form = VehicleForm(request.POST, request.FILES, instance=instance[0])
        if form.is_valid():
            # print "ok"
            form.save()
            form = VehicleForm()
            instance = VehicleType.objects.all()
            return redirect('/sadmin/vehicle_edit/')
    context = {
        'form': form,
        'instance': instance,
    }
    context.update(csrf(request))
    return render_to_response('superadmin/vehicle_edit.html', context, arg)

def passenger_pickup_rates_update(request, rate_id):
    arg = RequestContext(request)
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    rate = PickUpRate.objects.get(id=rate_id)
    instance = [rate]
    # print(instance[0].caption)
    form = PickupRateForm(instance=instance[0])
    if request.method == 'POST':
        form = PickupRateForm(request.POST, instance=instance[0])
        if form.is_valid():
            # print "ok"
            form.save()
            form = PickupRateForm()
            instance = PickUpRate.objects.all()
            context = {
                'form': form,
                'instance': instance,
            }
            context.update(csrf(request))
            return redirect('/sadmin/passenger_pickup_rates/', context)
    context = {
        'form': form,
        'instance': instance,
    }
    context.update(csrf(request))
    return render_to_response('superadmin/passenger_pickup_rates.html', context, arg)

def passenger_pickup_rates_delete(request, rate_id):
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    charge = PickUpRate.objects.get(id=rate_id)
    charge.delete(False)
    form = PickupRateForm()
    instance = PickUpRate.objects.all()
    context = {
        'form': form,
        'charges': instance,
    }
    context.update(csrf(request))
    return redirect('/sadmin/passenger_pickup_rates/', context)

def customer_list(request):
    context = RequestContext(request)
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    customers = Customer.objects.all()
    arg={}
    arg['customers'] = customers
    return render_to_response('superadmin/customer_list.html', arg, context)

def customer_delivery_order_list(request, user_id):
    context = RequestContext(request)
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    try:
        customers = Customer.objects.get(id=user_id)
        try:
            delivery = DeliveryService.objects.filter(customer=customers.id).order_by('-id')
        except:
            delivery = None
        arg={}
        arg['customers'] = customers
        arg['delivery'] = delivery
        return render_to_response('superadmin/customer_delivery_details.html', arg, context)
    except Exception, e:
        return HttpResponse(e)

def customer_pickup_order_list(request, user_id):
    context = RequestContext(request)
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    try:
        customers = Customer.objects.get(id=user_id)
        try:
            pickup = PickupService.objects.filter(customer=customers.id).order_by('-id')
        except:
            pickup = None
        arg={}
        arg['customers'] = customers
        arg['pickup'] = pickup
        return render_to_response('superadmin/customer_pickup_details.html', arg, context)
    except Exception, e:
        return HttpResponse(e)

def activate_customer(request, user_id):
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    user = User.objects.get(id=user_id)
    user.is_active = True
    user.save()
    customers = Customer.objects.all()
    return redirect('/sadmin/customer_list/')

def deactivate_customer(request, user_id):
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    user = User.objects.get(id=user_id)
    user.is_active = False
    user.save()
    customers = Customer.objects.all()
    return redirect('/sadmin/customer_list/')


def driver_list(request):
    context = RequestContext(request)
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    drivers = Driver.objects.all()
    arg = {}
    arg['drivers'] = drivers
    return render_to_response('superadmin/driver_list.html', arg, context)

def activate_driver(request, user_id):
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    user = User.objects.get(id=user_id)
    user.is_active = True
    user.save()
    drivers = Driver.objects.all()
    return redirect('/sadmin/driver_list/')

def deactivate_driver(request, user_id):
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    user = User.objects.get(id=user_id)
    user.is_active = False
    user.save()
    drivers = Driver.objects.all()
    return redirect('/sadmin/driver_list/')

def driver_status_edit(request):
    context = RequestContext(request)
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    driver_status = DriverStatus.objects.all()
    form = DriverStatusForm()
    arg={}
    if request.method == 'POST':
        form = DriverStatusForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/sadmin/driver_status_edit/')
    arg = {
        'driver_status': driver_status,
        'form': form
    }
    arg.update(csrf(request))
    return render_to_response('superadmin/driverstatus_edit.html', arg, context)

def driver_status_update(request, status_id):
    context = RequestContext(request)
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    driver_status = DriverStatus.objects.get(id=status_id)
    instance = [driver_status]
    arg = {}
    if request.method == 'POST':
        form = DriverStatusForm(request.POST, instance=instance[0])
        if form.is_valid():
            form.save()
            return redirect('/sadmin/driver_status_edit/')
    form = DriverStatusForm(instance=instance[0])
    driver_status = DriverStatus.objects.all()
    arg = {
        'driver_status': driver_status,
        'form': form
    }
    arg.update(csrf(request))
    return render_to_response('superadmin/driverstatus_edit.html', arg, context)

def driver_status_delete(request, status_id):
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    driver_status = DriverStatus.objects.get(id=status_id)
    driver_status.delete(False)
    return redirect('/sadmin/driver_status_edit/')


def pickup_orderlist(request):
    context = RequestContext(request)
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    pickup_infos = PickupService.objects.filter(is_archived=False).order_by('-id')
    if request.method == 'POST':
        try:
            job = PickupService.objects.get(id=request.POST['job_id'])
            # print job.customer
            # print job.pickedup_by
            if request.POST['message-to'] == 'customer':
                message = AdminMessage()
                message.message_to_customer = Customer.objects.get(id=job.customer.id)
                message.message_body = request.POST['message']
                message.pickup = job
                message.save()
                return redirect('/sadmin/pickup_order_list/')
            else:
                message = AdminMessage()
                message.message_to_driver = Driver.objects.get(id=job.pickedup_by.id)
                message.message_body = request.POST['message']
                message.pickup = job
                message.save()
                return redirect('/sadmin/pickup_order_list/')
        except Exception, e:
            return HttpResponse(e)
    arg={}
    arg['informations'] = pickup_infos
    arg.update(csrf(request))
    return render_to_response('superadmin/pickup_orderlist.html', arg, context_instance=RequestContext(request))


def archived_pickup_order_list(request):
    context = RequestContext(request)
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    pickup_infos = PickupService.objects.filter(is_archived=True)
    if request.method == 'POST':
        try:
            job = PickupService.objects.get(id=request.POST['job_id'])
            # print job.customer
            # print job.pickedup_by
            if request.POST['message-to'] == 'customer':
                message = AdminMessage()
                message.message_to_customer = Customer.objects.get(id=job.customer.id)
                message.message_body = request.POST['message']
                message.pickup = job
                message.save()
                return redirect('/sadmin/archived_pickup_order_list/')
            else:
                message = AdminMessage()
                message.message_to_driver = Driver.objects.get(id=job.pickedup_by.id)
                message.message_body = request.POST['message']
                message.pickup = job
                message.save()
                return redirect('/sadmin/archived_pickup_order_list/')
        except Exception, e:
            return HttpResponse(e)
    arg={}
    arg['informations'] = pickup_infos
    arg.update(csrf(request))
    return render_to_response('superadmin/archived_pickup_orderlist.html', arg, context_instance=RequestContext(request))


def pickup_orderlist_unarchive(request, order_id):
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    try:
        pickup_infos = PickupService.objects.get(id=order_id)
        pickup_infos.is_archived = False
        pickup_infos.save()
        return redirect('/sadmin/archived_pickup_order_list/')
    except Exception, e:
        return HttpResponse(e)

def pickup_orderlist_archive(request, order_id):
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    try:
        pickup_infos = PickupService.objects.get(id=order_id)
        pickup_infos.is_archived = True
        pickup_infos.save()
        return redirect('/sadmin/pickup_order_list/')
    except Exception, e:
        return HttpResponse(e)

def pickup_remarks(request):
    pickup = PickupService.objects.get(id=request.POST['job_id'])
    pickup.admin_remarks = request.POST['remark']
    pickup.save()
    return redirect('/sadmin/pickup_order_list/')


def pickup_orderlist_delete(request, order_id):
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    pickup_info = PickupService.objects.get(id=order_id)
    pickup_info.delete()
    return redirect('/sadmin/pickup_order_list/')

def pickup_orderlist_cancel(request, order_id):
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    cancel_info = CancellationInfo()
    log = ActivityLog()
    pickup_info = PickupService.objects.get(id=order_id)
    cancel_info.pickup_job = pickup_info
    cancel_info.driver = pickup_info.pickedup_by
    cancel_info.customer = pickup_info.customer
    cancel_info.cancelled_by = 'admin'
    log.pickup_service = pickup_info
    log.pickup_status = "cancelled"
    log.log_message = constant.DELEVERY_DERVICE_CANCELL_MESSAGE
    log.logger_name = pickup_info.pickedup_by.name
    log.log_type = constant.CANCELLED_JOB
    log.save()
    pickup_info.pickup_status = 'pending'
    pickup_info.pickedup_by = None
    cancel_info.save()
    pickup_info.save()
    messages.success(request, 'Job has been successfully cancelled.')
    return redirect('/sadmin/pickup_order_list/')

def corporate_delivery_orderlist(request):
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    delivery_infos = DeliveryService.objects.filter(is_archived=False, is_corporate=True)
    if request.method == 'POST':
        try:
            job = DeliveryService.objects.get(id=request.POST['job_id'])
            # print job.customer
            # print job.taken_by
            if request.POST['message-to'] == 'customer':
                message = AdminMessage()
                message.message_to_customer = Customer.objects.get(id=job.customer.id)
                message.message_body = request.POST['message']
                message.delivery = job
                message.save()
                return redirect('/sadmin/delivery_order_list/')
            else:
                message = AdminMessage()
                message.message_to_driver = Driver.objects.get(id=job.taken_by.id)
                message.message_body = request.POST['message']
                message.delivery = job
                message.save()
                return redirect('/sadmin/corporate_delivery_order_list/')
        except Exception, e:
            return HttpResponse(e)
    arg = {}
    arg['informations'] = delivery_infos
    arg.update(csrf(request))
    return render_to_response('superadmin/corporate_delivery_orderlist.html', arg, context_instance=RequestContext(request))


def corporate_delivery_orderlist_public(request,job_id):
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    try:
        cancel_info = CancellationInfo()
        delivery_info = DeliveryService.objects.get(id=job_id)
        delivery_info.is_corporate = False
        delivery_info.save()
        messages.success(request, 'Job has been successfully made public.')
        return redirect('/sadmin/corporate_delivery_order_list/')
    except:
        # print 'error'
        pass
    return redirect('/sadmin/corporate_delivery_order_list/')


def corporate_assign_delivery(request, job_id):
    context = RequestContext(request)
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    try:
        job = DeliveryService.objects.get(id=job_id)
        driver = Driver.objects.filter(is_corporate=True)
        if request.method == 'POST':
            assigned_job = DeliveryService.objects.get(id=request.POST['id'])
            assigned_job.taken_by = Driver.objects.get(id=request.POST['drivers'])
            assigned_job.delevery_status = 'assigned'
            log = ActivityLog()
            log.delevery_service = assigned_job
            log.delevery_status = "assigned"
            log.log_message = constant.ASSIGNED_JOB
            log.logger_name = assigned_job.taken_by.name
            log.log_type = constant.ASSIGNED_JOB
            log.save()
            assigned_job.save()
            messages.success(request, 'Job has been successfully assigned.')
            return redirect('/sadmin/corporate_delivery_order_list/')
        arg={}
        arg['driver'] = driver
        arg['job'] = job
        arg.update(csrf(request))
        return render_to_response('superadmin/assign_delivery.html', arg, context)
    except Exception, e:
        return HttpResponse(e)







def corporate_delivery_orderlist_delete(request, order_id):
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    try:
        delivery_info = DeliveryService.objects.get(id=order_id)
        delivery_info.delete()
    except Exception, e:
        # print 'error'
        return HttpResponse(e)
    return redirect('/sadmin/corporate_delivery_order_list/')

def corporate_delivery_orderlist_cancel(request, order_id):
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    try:
        cancel_info = CancellationInfo()
        delivery_info = DeliveryService.objects.get(id=order_id)
        log = ActivityLog()
        cancel_info.delivery_job = delivery_info
        cancel_info.driver = delivery_info.taken_by
        cancel_info.customer = delivery_info.customer
        cancel_info.cancelled_by = 'admin'
        log.delevery_service = delivery_info
        log.delevery_status = "cancelled"
        log.log_message = constant.DELEVERY_DERVICE_CANCELL_MESSAGE
        log.logger_name = delivery_info.taken_by.name
        log.log_type = constant.CANCELLED_JOB
        log.save()
        delivery_info.delevery_status = 'pending'
        delivery_info.taken_by = None
        delivery_info.save()
        cancel_info.save()
        messages.success(request, 'Job has been successfully cancelled.')
        return redirect('/sadmin/corporate_delivery_order_list/')
    except:
        # print 'error'
        pass
    return redirect('/sadmin/corporate_delivery_order_list/')

def corporate_delivery_remarks(request):
    delivery = DeliveryService.objects.get(id=request.POST['job_id'])
    delivery.admin_remarks = request.POST['remark']
    delivery.save()
    return redirect('/sadmin/corporate_delivery_order_list/')





















def delivery_orderlist(request):
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    delivery_infos = DeliveryService.objects.filter(is_archived=False).order_by('-id')
    if request.method == 'POST':
        try:
            job = DeliveryService.objects.get(id=request.POST['job_id'])
            # print job.customer
            # print job.taken_by
            if request.POST['message-to'] == 'customer':
                message = AdminMessage()
                message.message_to_customer = Customer.objects.get(id=job.customer.id)
                message.message_body = request.POST['message']
                message.delivery = job
                message.save()
                return redirect('/sadmin/delivery_order_list/')
            else:
                message = AdminMessage()
                message.message_to_driver = Driver.objects.get(id=job.taken_by.id)
                message.message_body = request.POST['message']
                message.delivery = job
                message.save()
                return redirect('/sadmin/delivery_order_list/')
        except Exception, e:
            return HttpResponse(e)
    arg = {}
    arg['informations'] = delivery_infos
    arg.update(csrf(request))
    return render_to_response('superadmin/delivery_orderlist.html', arg, context_instance=RequestContext(request))

def archived_delivery_order_list(request):
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    delivery_infos = DeliveryService.objects.filter(is_archived=True).order_by('-id')
    if request.method == 'POST':
        try:
            job = DeliveryService.objects.get(id=request.POST['job_id'])
            # print job.customer
            # print job.taken_by
            if request.POST['message-to'] == 'customer':
                message = AdminMessage()
                message.message_to_customer = Customer.objects.get(id=job.customer.id)
                message.message_body = request.POST['message']
                message.delivery = job
                message.save()
                return redirect('/sadmin/archived_delivery_order_list/')
            else:
                message = AdminMessage()
                message.message_to_driver = Driver.objects.get(id=job.taken_by.id)
                message.message_body = request.POST['message']
                message.delivery = job
                message.save()
                return redirect('/sadmin/archived_delivery_order_list/')
        except Exception, e:
            return HttpResponse(e)
    arg = {}
    arg['informations'] = delivery_infos
    arg.update(csrf(request))
    return render_to_response('superadmin/archived_delivery_orderlist.html', arg, context_instance=RequestContext(request))

def delivery_orderlist_unarchive(request, order_id):
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    try:
        delivery_infos = DeliveryService.objects.get(id=order_id)
        delivery_infos.is_archived = False
        delivery_infos.save()
        return redirect('/sadmin/archived_delivery_order_list/')
    except Exception, e:
        return HttpResponse(e)

def delivery_orderlist_archive(request, order_id):
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    try:
        delivery_infos = DeliveryService.objects.get(id=order_id)
        delivery_infos.is_archived=True
        delivery_infos.save()
        return redirect('/sadmin/delivery_order_list/')
    except Exception, e:
        return HttpResponse(e)

def delivery_orderlist_delete(request, order_id):
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    try:
        delivery_info = DeliveryService.objects.get(id=order_id)
        delivery_info.delete()
    except Exception, e:
        # print 'error'
        return HttpResponse(e)
    return redirect('/sadmin/delivery_order_list/')

def corporate_delivery_orderlist_archive(request, job_id):
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    try:
        delivery_infos = DeliveryService.objects.get(id=job_id)
        delivery_infos.is_archived=True
        delivery_infos.save()
        return redirect('/sadmin/corporate_delivery_order_list/')
    except Exception, e:
        return HttpResponse(e)

def delivery_orderlist_delete(request, order_id):
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    try:
        delivery_info = DeliveryService.objects.get(id=order_id)
        delivery_info.delete()
    except Exception, e:
        # print 'error'
        return HttpResponse(e)
    return redirect('/sadmin/delivery_order_list/')

def delivery_orderlist_cancel(request, order_id):
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    try:
        cancel_info = CancellationInfo()
        delivery_info = DeliveryService.objects.get(id=order_id)
        log = ActivityLog()
        cancel_info.delivery_job = delivery_info
        cancel_info.driver = delivery_info.taken_by
        cancel_info.customer = delivery_info.customer
        cancel_info.cancelled_by = 'admin'
        log.delevery_service = delivery_info
        log.delevery_status = "cancelled"
        log.log_message = constant.DELEVERY_DERVICE_CANCELL_MESSAGE
        log.logger_name = delivery_info.taken_by.name
        log.log_type = constant.CANCELLED_JOB
        log.save()
        delivery_info.delevery_status = 'pending'
        delivery_info.taken_by = None
        delivery_info.save()
        cancel_info.save()
        messages.success(request, 'Job has been successfully cancelled.')
        return redirect('/sadmin/delivery_order_list/')
    except:
        # print 'error'
        pass
    return redirect('/sadmin/delivery_order_list/')

def delivery_remarks(request):
    delivery = DeliveryService.objects.get(id=request.POST['job_id'])
    delivery.admin_remarks = request.POST['remark']
    delivery.save()
    return redirect('/sadmin/delivery_order_list/')


def active_driver_list(request):
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    try:
        drivers = Driver.objects.filter(is_active=True)
        arg = {}
        arg['drivers'] = drivers
        return render_to_response('superadmin/active_driver_list.html', arg)
    except Exception, e:
        # print 'error'
        return HttpResponse('error')

def active_driver_location(request, user_id):
    driver = Driver.objects.get(id=user_id)
    arg={}
    arg['driver'] = driver
    return render_to_response('superadmin/active_driver_location.html', arg)

############ MAP SAMPLE ##############


def show_map(request):
    context = RequestContext(request)
    return render(request, 'superadmin/map_sample.html', context)


############ Courier Driver ################

def courier_driver_list(request):
    context = RequestContext(request)
    driver = Driver.objects.filter(Q(driver_service_type='courier')|Q(driver_service_type='both'))
    vehicle = Vechile.objects.all()
    drivers = []
    for d in driver:
        d.taken_job = DeliveryService.objects.filter(delevery_status='taken', taken_by=d).count()
        d.completed_job = DeliveryService.objects.filter(delevery_status='completed', taken_by=d).count()
        drivers.append(d)
    arg = {
        'driver': drivers,
        'vehicle': vehicle,
    }
    return render_to_response('superadmin/courier_driver_list.html', arg, context)



def activate_all_driver(request, user_id):
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    user = User.objects.get(id=user_id)
    user.is_active = True
    user.save()
    return redirect('/sadmin/all_driver_list/')

def deactivate_all_driver(request, user_id):
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    user = User.objects.get(id=user_id)
    user.is_active = False
    user.save()
    return redirect('/sadmin/all_driver_list/')

def pickup_driver_list(request):
    context = RequestContext(request)
    driver = Driver.objects.filter(Q(driver_service_type='pickup')|Q(driver_service_type='both'))
    vehicle = Vechile.objects.all()
    drivers = []
    for d in driver:
        d.taken_job = PickupService.objects.filter(pickup_status='taken', pickedup_by=d).count()
        d.completed_job = PickupService.objects.filter(pickup_status='completed', pickedup_by=d).count()
        drivers.append(d)
    arg = {
        'driver': drivers,
        'vehicle': vehicle,
    }
    return render_to_response('superadmin/pickup_driver_list.html', arg, context)



def activate_pickup_driver(request, user_id):
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    user = User.objects.get(id=user_id)
    user.is_active = True
    user.save()
    return redirect('/sadmin/pickup_driver_list/')

def deactivate_pickup_driver(request, user_id):
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    user = User.objects.get(id=user_id)
    user.is_active = False
    user.save()
    return redirect('/sadmin/pickup_driver_list/')

def assign_pickup(request, job_id):
    context = RequestContext(request)
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    try:
        job = PickupService.objects.get(id=job_id)
        # print job.vehicle_type
        driver = Vechile.objects.filter(vechile_type=job.vehicle_type.id)
        if request.method == 'POST':
            assigned_job = PickupService.objects.get(id=request.POST['id'])
            assigned_job.pickedup_by = Driver.objects.get(id=request.POST['drivers'])
            assigned_job.pickup_status = 'assigned'
            log = ActivityLog()
            log.pickup_service = assigned_job
            log.pickup_status = "assigned"
            log.log_message = constant.ASSIGNED_JOB
            log.logger_name = assigned_job.pickedup_by.name
            log.log_type = constant.ASSIGNED_JOB
            log.save()
            assigned_job.save()
            messages.success(request, 'Job has been successfully assigned.')
            return redirect('/sadmin/pickup_order_list/')
        arg={}
        arg['driver'] = driver
        arg['job'] = job
        arg.update(csrf(request))
        return render_to_response('superadmin/assign_pickup.html', arg, context)
    except Exception, e:
        return HttpResponse(e)

def assign_delivery(request, job_id):
    context = RequestContext(request)
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    try:
        job = DeliveryService.objects.get(id=job_id)
        driver = Driver.objects.filter(Q(driver_service_type='courier')|Q(driver_service_type='both'))
        if request.method == 'POST':
            assigned_job = DeliveryService.objects.get(id=request.POST['id'])
            assigned_job.taken_by = Driver.objects.get(id=request.POST['drivers'])
            assigned_job.delevery_status = 'assigned'
            assigned_job.auto_assigned = True
            log = ActivityLog()
            log.delevery_service = assigned_job
            log.delevery_status = "assigned"
            log.log_message = constant.ASSIGNED_JOB
            log.logger_name = assigned_job.taken_by.name
            log.log_type = constant.ASSIGNED_JOB
            log.save()
            assigned_job.save()
            messages.success(request, 'Job has been successfully assigned.')
            send_notification(job_id)
            return redirect('/sadmin/delivery_order_list/')
        arg={}
        arg['driver'] = driver
        arg['job'] = job
        arg.update(csrf(request))
        return render_to_response('superadmin/assign_delivery.html', arg, context)
    except Exception, e:
        return HttpResponse(e)

def send_notification(job_id):
    import requests
    import json
    try:
        job = DeliveryService.objects.get(id=job_id)
        url = "https://fcm.googleapis.com/fcm/send"
        noti = {}
        noti['content_available'] = True
        noti['notification'] = {}
        noti['notification']['body'] = 'You have been assigned a courier job.'
        noti['notification']['title'] = 'Job assigned'
        noti['notification']['category'] = 6
        noti['notification']['driver_id'] = job.taken_by.id
        noti['data'] = {}
        noti['data']['message'] = 'I have arrived'
        noti['data']['title'] = 'Courier# ' + str(job.id)
        noti['data']['category'] = 6
        noti['notification']['sound'] = 'default'
        noti['registration_ids'] = [job.taken_by.device_token]
        print job.taken_by.device_token
        noti['priority'] = 'high'
        headers = {
            'authorization': "key=AIzaSyDZWqkNJXm11ilon9e8j2F7tecYQd12flU",
            'content-type': "application/json",
            'cache-control': "no-cache",
        }
        return requests.request("POST", url, data=json.dumps(noti), headers=headers)
    except Exception, e:
        print str(e)
        return False

def delivery_timing(request):
    context = RequestContext(request)
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    arg={}
    del_time = DeliveryTiming.objects.all()
    if request.method=='POST':
        try:
            start_ap = request.POST['start-ap']
            start = request.POST['start']
            if start_ap == 'PM' and int(start) == 12:
                start = 00
                start = str(start) + ':00:00'
            elif start_ap == 'PM':
                start = int(start) + 12
                start = str(start) + ':00:00'
            else:
                start = start + ':00:00'
            end_ap = request.POST['end-ap']
            end = request.POST['end']
            if end_ap == 'PM' and int(end) == 12:
                end = 00
                end = str(end) + ':00:00'
            elif end_ap == 'PM':
                end = int(end) + 12
                end = str(end) + ':00:00'
            else:
                end = end + ':00:00'
            window_name = request.POST['delivery_window']
            del_time = DeliveryTiming()
            del_time.start_time = start
            del_time.end_time = end
            del_time.window_name = window_name
            del_time.max_number_of_active_job = request.POST['max_number_of_active_job']
            del_time.save()
            return redirect('/sadmin/delivery_timing/')
        except Exception, e:
            return HttpResponse(e)
    arg['del_time'] = del_time
    arg.update(csrf(request))
    return render_to_response('superadmin/timing_list.html', arg, context)

def delivery_timing_delete(request, time_id):
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    try:
        del_time = DeliveryTiming.objects.get(id=time_id)
        del_time.delete()
        return redirect('/sadmin/delivery_timing/')
    except Exception, e:
        return HttpResponse(e)


def delivery_timing_update(request, time_id):
    context = RequestContext(request)
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    arg={}
    del_time = DeliveryTiming.objects.get(id=time_id)
    if request.method=='POST':
        try:
            start_ap = request.POST['start-ap']
            start = request.POST['start']
            if start_ap == 'PM' and int(start) == 12:
                start = 00
                start = str(start) + ':00:00'
            elif start_ap == 'PM':
                start = int(start) + 12
                start = str(start) + ':00:00'
            else:
                start = start + ':00:00'
            end_ap = request.POST['end-ap']
            end = request.POST['end']
            if end_ap == 'PM' and int(end) == 12:
                end = 00
                end = str(end) + ':00:00'
            elif end_ap == 'PM':
                end = int(end) + 12
                end = str(end) + ':00:00'
            else:
                end = end + ':00:00'
            window_name = request.POST['delivery_window']
            del_time = DeliveryTiming.objects.get(id=time_id)
            del_time.start_time = start
            del_time.end_time = end
            del_time.window_name = window_name
            del_time.max_number_of_active_job = request.POST['max_number_of_active_job']
            del_time.save()
            return redirect('/sadmin/delivery_timing/')
        except Exception, e:
            return HttpResponse(e)
    arg['del_time'] = del_time
    arg.update(csrf(request))
    return render_to_response('superadmin/timing_list_update.html', arg, context)

def get_vehicle_number(driver):
    try:
        driver_vehicle = Vechile.objects.get(driver=driver)
        # print driver_vehicle.vechile_number
        if driver_vehicle.vechile_number:
            return driver_vehicle.vechile_number
        else:
            return None
    except:
        return None

def get_vehicle_plate(driver):
    try:
        driver_vehicle = Vechile.objects.get(driver=d)
        if driver_vehicle.plate:
            return driver_vehicle.plate
        else:
            return None
    except:
        return None


def all_driver_list(request):
    context = RequestContext(request)
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    driver = Driver.objects.all()
    drivers = []
    arg = {}
    for d in driver:
        try:
            ratings = DriverRating.objects.filter(driver=d.id)
            total_rate = 0
            if ratings.count() > 0:
                for rating in ratings:
                    total_rate = total_rate+int(rating.rating)
            avg_rate = total_rate/ratings.count()
            d.rate = avg_rate
            d.acceptance_rate = get_acceptacce_rate(d)
            d.cancellation_rate = get_cencell_rate(d)
            d.vehicle_number = get_vehicle_number(d)
            d.plate = get_vehicle_plate(d)
            drivers.append(d)
        except:
            avg_rate = 0
            d.rate = avg_rate
            d.acceptance_rate = get_acceptacce_rate(d)
            d.cancellation_rate = get_cencell_rate(d)
            d.vehicle_number = get_vehicle_number(d)
            d.plate = get_vehicle_plate(d)
            drivers.append(d)
    arg['driver'] = drivers
    return render_to_response('superadmin/all_driver_list.html', arg, context)

@csrf_exempt
def drivers_from_date(request):
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    from_date = request.POST['from_date']
    from_date = datetime.datetime.strptime(from_date, '%d %b %Y').date()
    to_date = request.POST['to_date']
    to_date = datetime.datetime.strptime(to_date, '%d %b %Y').date()
    try:
        drivers = Driver.objects.filter(created_at__range=[from_date, to_date])
    except Exception, e:
        return HttpResponse(e)
    ready_data = '<thead>'
    ready_data = ready_data + '<tr>'
    ready_data = ready_data + '<th class="col_search">ID</th>'
    ready_data = ready_data + '<th class="col_search">Name</th>'
    ready_data = ready_data + '<th class="col_search">Email</th>'
    ready_data = ready_data + '<th class="col_search">Driver Category</th>'
    ready_data = ready_data + '<th class="col_search">Contact No.</th>'
    ready_data = ready_data + '<th class="col_search">Vehicle Number</th>'
    ready_data = ready_data + '<th class="col_search">Company</th>'
    ready_data = ready_data + '<th class="col_search">Driver Type</th>'
    ready_data = ready_data + '<th class="col_search">Acceptance Rate</th>'
    ready_data = ready_data + '<th class="col_search">Cancellation Rate</th>'
    ready_data = ready_data + '<th>Driver Since</th>'
    ready_data = ready_data + '<th>Rating</th>'
    ready_data = ready_data + '<th>Action</th></tr></thead><tbody>'
    if not drivers:
        ready_data = ready_data + '</tbody>'
    else:
        driver = []
        for d in drivers:
            try:
                ratings = DriverRating.objects.filter(driver=d.id)
                total_rate = 0
                if ratings.count() > 0:
                    for rating in ratings:
                        total_rate = total_rate + int(rating.rating)
                avg_rate = total_rate / ratings.count()
                d.rate = avg_rate
                d.acceptance_rate = get_acceptacce_rate(d)
                d.cancellation_rate = get_cencell_rate(d)
                d.vehicle_number = get_vehicle_number(d)
                d.plate = get_vehicle_plate(d)
                driver.append(d)
            except:
                avg_rate = 0
                d.rate = avg_rate
                d.acceptance_rate = get_acceptacce_rate(d)
                d.cancellation_rate = get_cencell_rate(d)
                d.vehicle_number = get_vehicle_number(d)
                d.plate = get_vehicle_plate(d)
                driver.append(d)

        for d in driver:
            ready_data = ready_data + '<tr class="forRemove"><td>0100'+str(d.id)+'</td>'
            ready_data = ready_data + '<td><a href="/sadmin/driver_profile_detail/' + str(d.id) + '/">' + d.name + '</a></td>'
            if d.email:
                ready_data = ready_data + '<td>'+d.email+'</td>'
            else:
                ready_data = ready_data + '<td>'+d.user.email+'</td>'
            if d.driver_category == 'NORMAL':
                ready_data = ready_data + '<td style="color:#00a8c6">'+ d.driver_category +'</td>'
            elif d.driver_category == 'BRONZE':
                ready_data = ready_data + '<td style="color:#C9AE5D">' + d.driver_category + '</td>'
            elif d.driver_category == 'SILVER':
                ready_data = ready_data + '<td style="color:#A9A9A9">' + d.driver_category + '</td>'
            elif d.driver_category == 'GOLD':
                ready_data = ready_data + '<td style="color:#FFD700">' + d.driver_category + '</td>'
            else:
                ready_data = ready_data + '<td>No category yet.</td>'

            ready_data = ready_data + '<td>'+d.mobile_no+'</td>'
            if d.vehicle_number:
                # print d.vehicle_number
                ready_data = ready_data + '<td>' + d.vehicle_number + '</td>'
            else:
                ready_data = ready_data + '<td>No number provided</td>'
            if d.company:
                ready_data = ready_data + '<td>' + d.company + '</td>'
            else:
                ready_data = ready_data + '<td>No company name</td>'
            if d.driver_service_type:
                ready_data = ready_data + '<td>'+ d.driver_service_type +'</td>'
            else:
                ready_data = ready_data + '<td>Not chosen service type</td>'
            ready_data = ready_data + '<td>' + str(d.acceptance_rate) + '</td>'
            ready_data = ready_data + '<td>' + str(d.cancellation_rate) + '</td>'
            ready_data = ready_data + '<td>' + d.created_at.strftime('Since %d, %b %Y') + '</td>'
            ready_data = ready_data + '<td><input class="'+ 'stars' +'" value="'+ str(d.rate) +'"></td>'
            if d.user.is_active == 0:
                ready_data = ready_data + '<td><div class="actions-orders btn-color-white nowrap"><a class="btn btn-sm btn-primary unstroke" href="/sadmin/all_driver_list/activate/'+ str(d.user.id) +'">Activate</a></div></td></tr>'
            else:
                ready_data = ready_data + '<td><div class="actions-orders btn-color-white nowrap"><a class="btn btn-sm btn-warning unstroke" href="/sadmin/all_driver_list/deactivate/' + str(d.user.id) + '">Deactivate</a></div></td></tr>'
    ready_data = ready_data + '</tbody>'
    return HttpResponse(ready_data)

@csrf_exempt
def customers_from_date(request):
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    from_date = request.POST['from_date']
    from_date = datetime.datetime.strptime(from_date, '%d %b %Y').date()
    to_date = request.POST['to_date']
    to_date = datetime.datetime.strptime(to_date, '%d %b %Y').date()
    try:
        customers = Customer.objects.filter(created_at__range=[from_date, to_date])
    except Exception, e:
        return HttpResponse(e)
    ready_data = '<thead>'
    ready_data = ready_data + '<tr>'
    ready_data = ready_data + '<th></th>'
    ready_data = ready_data + '<th class="col_search">ID</th>'
    ready_data = ready_data + '<th class="col_search">Name</th>'
    ready_data = ready_data + '<th class="col_search">Email</th>'
    ready_data = ready_data + '<th class="col_search">Customer Since</th>'
    ready_data = ready_data + '<th class="col_search">Contact No.</th>'
    # ready_data = ready_data + '<th class="col_search">Verification</th>'
    ready_data = ready_data + '<th>Profile Picture</th>'
    ready_data = ready_data + '<th>Action</th></tr></thead><tbody>'
    if not customers:
        ready_data = ready_data + '</tbody>'
    else:
        for customer in customers:
            ready_data = ready_data + '<tr class="forRemove"><td><input type="checkbox" id="check" value="'+ str(customer.user.id) +'" class="checkbox"></td></td>'
            ready_data = ready_data + '<td>0100'+str(customer.id)+'</td>'
            ready_data = ready_data + '<td><a href="/sadmin/customer_profile_detail/' + str(customer.id) + '/">' + customer.name + '</a></td>'
            if customer.email:
                ready_data = ready_data + '<td>'+ customer.email +'</td>'
            else:
                ready_data = ready_data + '<td>'+ customer.user.email +'</td>'
            ready_data = ready_data + '<td>' + customer.created_at.strftime('Since %d, %b %Y') + '</td>'
            ready_data = ready_data + '<td>' + customer.contact_no + '</td>'
            # ready_data = ready_data + '<td>' + customer.verification + '</td>'
            if customer.picture:
                ready_data = ready_data + '<td><img src="' + str(customer.picture.url) + '" width="100px" height="50px"/></td>'
            else:
                ready_data = ready_data + '<td>No Image</td>'
            if customer.user.is_active == 0:
                ready_data = ready_data + '<td><div class="actions-orders btn-color-white nowrap"><a class="btn btn-sm btn-primary unstroke" href="/sadmin/customer/activate/'+ str(customer.user.id) +'">Activate</a></div></td></tr>'
            else:
                ready_data = ready_data + '<td><div class="actions-orders btn-color-white nowrap"><a class="btn btn-sm btn-warning unstroke" href="/sadmin/customer/deactivate/' + str(customer.user.id) + '">Deactivate</a></div></td></tr>'
    ready_data = ready_data + '</tbody>'
    return HttpResponse(ready_data)

def customer_profile_detail(request, customer_id):
    context = RequestContext(request)
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    try:
        customer = Customer.objects.get(id=customer_id)
    except Exception, e:
        return HttpResponse(e)
    try:
        pickup_orders_first = PickupService.objects.filter(customer=customer.id).order_by('id')[0]
    except:
        pickup_orders_first = None
    try:
        delivery_orders_first = DeliveryService.objects.filter(customer=customer.id).order_by('id')[0]
    except:
        delivery_orders_first = None
    arg={
        "pickup": pickup_orders_first,
        "delivery": delivery_orders_first,
        "customer": customer
    }
    return render_to_response('superadmin/customer_profile_detail.html', arg, context)

def driver_profile_detail(request, driver_id):
    context = RequestContext(request)
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    # ratings = DriverRating.objects.filter(driver=driver_id)
    avg_rate = ''
    try:
        rates = DriverRating.objects.filter(driver=driver_id)
        total_rate = 0
        for rate in rates:
            total_rate = total_rate + int(rate.rating)
        if total_rate > 0:
            avg_rate = total_rate/rates.count()
        # print avg_rate
    except:
        avg_rate = 0
    try:
        driver = Driver.objects.get(id=driver_id)
    except Exception, e:
        return HttpResponse(e)
    try:
        pickup_orders_first = PickupService.objects.filter(pickedup_by=driver.id).order_by('id')[0]
    except:
        pickup_orders_first = None
    try:
        delivery_orders_first = DeliveryService.objects.filter(taken_by=driver.id).order_by('id')[0]
    except:
        delivery_orders_first = None
    categories = DriverCategory.objects.all()
    if not get_earnings(driver.id, driver.driver_service_type):
        earnings = None
    else:
        earnings = get_earnings(driver.id, driver.driver_service_type)
    arg={
        "pickup": pickup_orders_first,
        "delivery": delivery_orders_first,
        "driver": driver,
        "categories": categories,
        "rate": avg_rate,
        "acceptance_rate": get_acceptacce_rate(driver),
        "cancellation_rate": get_cencell_rate(driver),
        "earnings": earnings,
        "ratings": DriverRating.objects.filter(driver=driver_id)
    }
    return render_to_response('superadmin/driver_profile_detail.html', arg, context)


def driver_delivery_order_list(request, user_id):
    context = RequestContext(request)
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    try:
        driver = Driver.objects.get(id=user_id)
        try:
            delivery = DeliveryService.objects.filter(taken_by=driver.id)
        except:
            delivery = None
        arg={}
        arg['drivers'] = driver
        arg['delivery'] = delivery
        return render_to_response('superadmin/driver_delivery_details.html', arg, context)
    except Exception, e:
        return HttpResponse(e)

def driver_pickup_order_list(request, user_id):
    context = RequestContext(request)
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    try:
        driver = Driver.objects.get(id=user_id)
        try:
            pickup = PickupService.objects.filter(pickedup_by=driver.id)
        except:
            pickup = None
        arg={}
        arg['drivers'] = driver
        arg['pickup'] = pickup
        return render_to_response('superadmin/driver_pickup_details.html', arg, context)
    except Exception, e:
        return HttpResponse(e)

@csrf_exempt
def driver_profile_edit(request):
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    id = request.POST['id']
    name = request.POST['name']
    try:
        driver = Driver.objects.get(id=id)
        driver.name = name
        driver.driver_category = request.POST['category']
        driver.save()
        return JsonResponse({"message": "success"})
    except Exception, e:
        return JsonResponse({"message": e})

def faq_edit(request):
    context = RequestContext(request)
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    instance = Faq.objects.all()
    if not instance:
        form = FaqForm()
    else:
        form = FaqForm(instance=instance[0])
    if request.method == 'POST':
        if not instance:
            form = FaqForm(request.POST)
        else:
            form = FaqForm(request.POST, instance=instance[0])
        if form.is_valid():
            # print "ok"
            form.save()
            return redirect('/sadmin/faq_edit/')
    arg={
        'form':form,
        'faq':instance
    }
    arg.update(csrf(request))
    return render_to_response('superadmin/faq_edit.html', arg, context)

def privacy_edit(request):
    context = RequestContext(request)
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    instance = Privacy.objects.all()
    if not instance:
        form = PrivacyForm()
    else:
        form = PrivacyForm(instance=instance[0])
    if request.method == 'POST':
        if not instance:
            form = PrivacyForm(request.POST)
        else:
            form = PrivacyForm(request.POST, instance=instance[0])
        if form.is_valid():
            # print "ok"
            form.save()
            return redirect('/sadmin/privacy_edit/')
    arg={
        'form':form,
        'privacy':instance
    }
    arg.update(csrf(request))
    return render_to_response('superadmin/privacy_edit.html', arg, context)

def tnc_edit(request):
    context = RequestContext(request)
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    instance = Terms.objects.all()
    if not instance:
        form = TermsForm()
    else:
        form = TermsForm(instance=instance[0])
    if request.method == 'POST':
        if not instance:
            form = TermsForm(request.POST)
        else:
            form = TermsForm(request.POST, instance=instance[0])
        if form.is_valid():
            # print "ok"
            form.save()
            return redirect('/sadmin/tnc_edit/')
    arg={
        'form':form,
        'terms':instance
    }
    arg.update(csrf(request))
    return render_to_response('superadmin/tnc_edit.html', arg, context)

def express_pricing(request):
    context = RequestContext(request)
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    arg={}
    express = ExpressWindows.objects.all()
    if request.method == 'POST':
        form = ExpressWindowsForm(request.POST)
        form.save()
        return redirect('/sadmin/express_pricing/')
    form = ExpressWindowsForm()
    arg = {
        'express': express,
        'form':form
    }
    arg.update(csrf(request))
    return render_to_response('superadmin/express_edit.html', arg, context)

def express_pricing_update(request, win_id):
    context = RequestContext(request)
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    express = ExpressWindows.objects.filter(id=win_id)
    form = ExpressWindowsForm(instance=express[0])
    if request.method == 'POST':
        form = ExpressWindowsForm(request.POST, instance=express[0])
        try:
            form.save()
        except Exception, e:
            print e
        return redirect('/sadmin/express_pricing/')
    arg = {
        'express': express,
        'form': form
    }
    arg.update(csrf(request))
    return render_to_response('superadmin/express_edit.html', arg, context)

def express_pricing_delete(request, win_id):
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    express = ExpressWindows.objects.get(id=win_id)
    express.delete()
    return redirect('/sadmin/express_pricing/')










def pickup_time_charge(request):
    context = RequestContext(request)
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    arg={}
    timecharge = PickupTimeCharge.objects.all()
    if request.method == 'POST':
        form = PickupTimeChargeForm(request.POST)
        form.save()
        return redirect('/sadmin/pickup_time_charge/')
    form = PickupTimeChargeForm()
    arg = {
        'timecharge': timecharge,
        'form':form
    }
    arg.update(csrf(request))
    return render_to_response('superadmin/pickup_time_charge.html', arg, context)

def pickup_time_charge_update(request, win_id):
    context = RequestContext(request)
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    timecharge = PickupTimeCharge.objects.filter(id=win_id)
    form = PickupTimeChargeForm(instance=timecharge[0])
    if request.method == 'POST':
        form = PickupTimeChargeForm(request.POST, instance=timecharge[0])
        form.save()
        return redirect('/sadmin/pickup_time_charge/')
    arg = {
        'timecharge': timecharge,
        'form': form
    }
    arg.update(csrf(request))
    return render_to_response('superadmin/pickup_time_charge.html', arg, context)

def pickup_time_charge_delete(request, win_id):
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    timecharge = PickupTimeCharge.objects.get(id=win_id)
    timecharge.delete()
    return redirect('/sadmin/pickup_time_charge/')










def pickup_gst(request):
    context = RequestContext(request)
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    arg={}
    timecharge = PickupGST.objects.all()
    if request.method == 'POST':
        form = PickupGSTForm(request.POST)
        form.save()
        return redirect('/sadmin/pickup_gst/')
    form = PickupGSTForm()
    arg = {
        'timecharge': timecharge,
        'form':form
    }
    arg.update(csrf(request))
    return render_to_response('superadmin/pickup_gst.html', arg, context)

def pickup_gst_update(request, win_id):
    context = RequestContext(request)
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    timecharge = PickupGST.objects.filter(id=win_id)
    form = PickupGSTForm(instance=timecharge[0])
    if request.method == 'POST':
        form = PickupGSTForm(request.POST, instance=timecharge[0])
        form.save()
        return redirect('/sadmin/pickup_gst/')
    arg = {
        'timecharge': timecharge,
        'form': form
    }
    arg.update(csrf(request))
    return render_to_response('superadmin/pickup_gst.html', arg, context)

def pickup_gst_delete(request, win_id):
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    timecharge = PickupGST.objects.get(id=win_id)
    timecharge.delete()
    return redirect('/sadmin/pickup_gst/')



def courier_gst(request):
    context = RequestContext(request)
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    arg={}
    timecharge = CourierGST.objects.all()
    if request.method == 'POST':
        form = CourierGSTForm(request.POST)
        form.save()
        return redirect('/sadmin/courier_gst/')
    form = PickupGSTForm()
    arg = {
        'timecharge': timecharge,
        'form':form
    }
    arg.update(csrf(request))
    return render_to_response('superadmin/courier_gst.html', arg, context)

def courier_gst_update(request, win_id):
    context = RequestContext(request)
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    timecharge = CourierGST.objects.filter(id=win_id)
    form = CourierGSTForm(instance=timecharge[0])
    if request.method == 'POST':
        form = CourierGSTForm(request.POST, instance=timecharge[0])
        form.save()
        return redirect('/sadmin/courier_gst/')
    arg = {
        'timecharge': timecharge,
        'form': form
    }
    arg.update(csrf(request))
    return render_to_response('superadmin/courier_gst.html', arg, context)

def courier_gst_delete(request, win_id):
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    timecharge = CourierGST.objects.get(id=win_id)
    timecharge.delete()
    return redirect('/sadmin/courier_gst/')










def courier_time_charge(request):
    context = RequestContext(request)
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    arg={}
    timecharge = CourierTimeCharge.objects.all()
    if request.method == 'POST':
        form = CourierTimeChargeForm(request.POST)
        form.save()
        return redirect('/sadmin/courier_time_charge/')
    form = CourierTimeChargeForm()
    arg = {
        'timecharge': timecharge,
        'form':form
    }
    arg.update(csrf(request))
    return render_to_response('superadmin/courier_time_charge.html', arg, context)

def courier_time_charge_update(request, win_id):
    context = RequestContext(request)
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    timecharge = CourierTimeCharge.objects.filter(id=win_id)
    form = CourierTimeChargeForm(instance=timecharge[0])
    if request.method == 'POST':
        form = CourierTimeChargeForm(request.POST, instance=timecharge[0])
        form.save()
        return redirect('/sadmin/courier_time_charge/')
    arg = {
        'timecharge': timecharge,
        'form': form
    }
    arg.update(csrf(request))
    return render_to_response('superadmin/courier_time_charge.html', arg, context)

def courier_time_charge_delete(request, win_id):
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    timecharge = CourierTimeCharge.objects.get(id=win_id)
    timecharge.delete()
    return redirect('/sadmin/courier_time_charge/')











def commission_setup(request):
    context = RequestContext(request)
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    commissions = CommissionSetup.objects.all()
    form = CommissionSetupForm()
    if request.method == 'POST':
        form = CommissionSetupForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/sadmin/commission_setup/')
    arg = {
        'commissions': commissions,
        'form': form
    }
    arg.update(csrf(request))
    return render_to_response('superadmin/commission_setup.html', arg, context)

def commission_setup_update(request, commission_id):
    context = RequestContext(request)
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    commission = CommissionSetup.objects.filter(id=commission_id)
    form = CommissionSetupForm(instance=commission[0])
    if request.method == 'POST':
        form = CommissionSetupForm(request.POST, instance=commission[0])
        if form.is_valid():
            form.save()
            return redirect('/sadmin/commission_setup/')
    arg = {
        'commissions': commission,
        'form': form
    }
    arg.update(csrf(request))
    return render_to_response('superadmin/commission_setup.html', arg, context)

def commission_setup_delete(request, commission_id):
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    commission = CommissionSetup.objects.get(id=commission_id)
    commission.delete()
    return redirect('/sadmin/commission_setup/')




#
#
#   Finding earnings
#
#


def get_driver_type(id):
    driver = Driver.objects.get(id=id)
    return driver.driver_service_type

def get_earnings(id, type):
    if type == 'courier':
        delivery_earnings = get_delivery_earnings(id)
        if not delivery_earnings:
            delivery_earnings = []
        return delivery_earnings

    elif type == 'pickup':
        pickup_earnings = get_pickup_earnings(id)
        if not pickup_earnings:
            pickup_earnings = []
        return pickup_earnings

    else:
        delivery_earnings = get_delivery_earnings(id)
        if not delivery_earnings:
            delivery_earnings = None
            # print 'no delivery'
        #  delivery_earnings
        pickup_earnings = get_pickup_earnings(id)
        if not pickup_earnings:
            pickup_earnings = None
            # print 'no pickup'
        if not pickup_earnings and not delivery_earnings:
            pickup_earnings = None
            delivery_earnings = None
            return False
        return delivery_earnings + pickup_earnings


def get_delivery_earnings(id):
    earnings = []
    delivery = DeliveryService.objects.filter(taken_by=id, delevery_status='completed')
    if delivery.count() > 0:
        for obj in delivery:
            try:
                payment = Payment.objects.get(delevery_service=obj)
                obj.payment = payment
                obj.type = 'DELIVERY'
            except:
                obj.type = 'DELIVERY'
                obj.payment = None
            earnings.append(obj)
            # print earnings
        return earnings
    else:
        earnings = None
        return earnings

def get_pickup_earnings(id):
    earnings = []
    pickup = PickupService.objects.filter(pickedup_by=id, pickup_status='completed')
    if pickup.count() > 0:
        for obj in pickup:
            try:
                payment = Payment.objects.get(pickup_service=obj)
                obj.type = 'PICKUP'
                obj.payment = payment
            except:
                obj.type = 'PICKUP'
                obj.payment = None
            earnings.append(obj)
        return earnings
    else:
        earnings = None
        return earnings





#
#
#   Finding earnings by date
#
#

def process_html(earning):
    ready_data = ''
    ready_data = ready_data + '<thead>'
    ready_data = ready_data + '<tr>'
    ready_data = ready_data + '<th>ID</th>'
    ready_data = ready_data + '<th>Total Cost</th>'
    ready_data = ready_data + '<th>Paid Amount</th>'
    ready_data = ready_data + '<th>Admin Fee</th>'
    ready_data = ready_data + '<th>Car Fee</th>'
    ready_data = ready_data + '<th>Toll Fee</th>'
    ready_data = ready_data + '<th>Online Fee</th>'
    ready_data = ready_data + '<th>Payment Status</th>'
    ready_data = ready_data + '</tr>'
    ready_data = ready_data + '</thead>'

    if not earning:
        return HttpResponse('<tr><td>' + 'No Records' + '</td></tr>')
    else:
        # print 'found'
        for e in earning:
            ready_data = ready_data + '<tr>'
            if e.payment:
                if e.payment.payment_status == 'UNPAID':
                    ready_data = ready_data + '<td><input type="checkbox" id="check_' + str(
                        e.payment.id) + '"value="' + str(e.payment.id) + '">' + e.type + '#' + str(e.id) + '</td>'
                else:
                    ready_data = ready_data + '<td><input type="checkbox" disabled>' + e.type + '#' + str(
                        e.id) + '</td>'
            else:
                ready_data = ready_data + '<td><input type="checkbox" disabled>' + e.type + '#' + str(e.id) + '</td>'
            if e.payment:
                ready_data = ready_data + '<td>' + str(e.payment.total_cost) + '</td>'
            else:
                ready_data = ready_data + '<td></td>'
            if e.payment:
                ready_data = ready_data + '<td>' + str(e.payment.paid_amount) + '</td>'
            else:
                ready_data = ready_data + '<td></td>'
            if e.payment:
                ready_data = ready_data + '<td>' + str(e.payment.admin_fee) + '</td>'
            else:
                ready_data = ready_data + '<td></td>'
            if e.payment:
                ready_data = ready_data + '<td>' + str(e.payment.car_fee) + '</td>'
            else:
                ready_data = ready_data + '<td></td>'
            if e.payment:
                ready_data = ready_data + '<td>' + str(e.payment.toll_fee) + '</td>'
            else:
                ready_data = ready_data + '<td></td>'
            if e.payment:
                ready_data = ready_data + '<td>' + str(e.payment.online_fee) + '</td>'
            else:
                ready_data = ready_data + '<td></td>'
            if e.payment:
                ready_data = ready_data + '<td>' + e.payment.payment_status + '</td>'
            else:
                ready_data = ready_data + '<td></td>'
            ready_data = ready_data + '</tr>'
    return ready_data

def date_difference(start, end):
    difference = date(start.year, start.month, start.day) - date(end.year, end.month, end.day)
    return difference.days

def driver_earnings_last_month(request, driver_id):
    datetoday = datetime.datetime.today()
    first_day = first_day_of_month(datetoday)
    diff = date_difference(datetoday, first_day) + 1
    first_day = first_day_of_month(datetime.datetime.today() - datetime.timedelta(days=diff))
    # print first_day
    last_day = last_day_of_month(first_day)
    # print last_day
    earning = earning_from_date_range(driver_id, first_day, last_day)
    ready_data = process_html(earning)
    return HttpResponse(ready_data)

def driver_earnings_last_week(request, driver_id):
    datetoday = datetime.datetime.today() - datetime.timedelta(days=7)
    first_day = date(week_magic(datetoday)[0].year, week_magic(datetoday)[0].month, week_magic(datetoday)[0].day)
    last_day = date(week_magic(datetoday)[1].year, week_magic(datetoday)[1].month, week_magic(datetoday)[1].day)
    earning = earning_from_date_range(driver_id, first_day, last_day)
    ready_data = process_html(earning)
    return HttpResponse(ready_data)


def driver_earnings_last_year(request, driver_id):
    datetoday = datetime.datetime.today()
    first_day = date(datetoday.year, 1, 1)
    last_day = date(datetoday.year, 12, 31)
    earning = earning_from_date_range(driver_id, first_day, last_day)
    ready_data = process_html(earning)
    return HttpResponse(ready_data)

def earning_from_date_range(id, first, last):
    type = get_driver_type(id)
    if type == 'courier':
        earnings = delivery_earnings_from_date(id, first, last)
        return earnings
    elif type == 'pickup':
        earnings = pickup_earnings_from_date(id, first, last)
        return earnings
    elif type == 'both':
        delivery_earnings = delivery_earnings_from_date(id, first, last)
        pickup_earnings = pickup_earnings_from_date(id, first, last)
        if delivery_earnings and not pickup_earnings:
            # print 'No delivery'
            return delivery_earnings
        if pickup_earnings and not delivery_earnings:
            # print 'No pickup'
            return pickup_earnings
        if not delivery_earnings and not pickup_earnings:
            value = None
            return value
        else:
            return delivery_earnings + pickup_earnings


def delivery_earnings_from_date(id, start, end):
    start_date = datetime.datetime.strptime(start.strftime('%d-%m-%Y'), '%d-%m-%Y')
    # print start_date
    end_date = datetime.datetime.strptime(end.strftime('%d-%m-%Y'), '%d-%m-%Y')
    earnings = []
    delivery = DeliveryService.objects.filter(taken_by=id, delevery_status='completed', job_complete_time__range=[start_date, end_date])
    if delivery.count() > 0:
        for obj in delivery:
            try:
                payment = Payment.objects.get(delevery_service=obj)
                obj.payment = payment
                obj.type = 'DELIVERY'
            except:
                obj.type = 'DELIVERY'
                obj.payment = None
            earnings.append(obj)
            # print earnings
        return earnings
    else:
        earnings = None
        return earnings

def pickup_earnings_from_date(id, start, end):
    start_date = datetime.datetime.strptime(start.strftime('%d-%m-%Y'), '%d-%m-%Y')
    end_date = datetime.datetime.strptime(end.strftime('%d-%m-%Y'), '%d-%m-%Y')
    earnings = []
    pickup = PickupService.objects.filter(pickedup_by=id, pickup_status='completed', job_complete_time__range=[start_date, end_date])
    if pickup.count() > 0:
        for obj in pickup:
            try:
                payment = Payment.objects.get(pickup_service=obj)
                obj.type = 'PICKUP'
                obj.payment = payment
            except:
                obj.type = 'PICKUP'
                obj.payment = None
            earnings.append(obj)
        return earnings
    else:
        earnings = None
        return earnings

def week_magic(day):
    day_of_week = day.weekday()

    to_beginning_of_week = datetime.timedelta(days=day_of_week)
    beginning_of_week = day - to_beginning_of_week

    to_end_of_week = datetime.timedelta(days=6 - day_of_week)
    end_of_week = day + to_end_of_week

    return (beginning_of_week, end_of_week)

#
#
#
#
#   END OF EARNINGS
#
#
#
#

def delivery_summary(request, delivery_id):
    context = RequestContext(request)
    arg = {}
    arg['booking_active'] = True
    try:
        booking = DeliveryService.objects.get(id=delivery_id)
        try:
            driver = Driver.objects.get(id=booking.taken_by.id)
            ratings = DriverRating.objects.filter(driver=driver.id)
            if ratings.count() > 0:
                total_rate = 0
                for rating in ratings:
                    total_rate = total_rate + int(rating.rating)
                avg_rating = total_rate / ratings.count()
            else:
                avg_rating = 0
            # print avg_rating
            driver.rating = avg_rating
            arg['driver'] = driver
        except:
            pass
        arg['booking'] = booking
        arg['packages'] = Package.objects.filter(service=booking)
        arg['logs'] = ActivityLog.objects.filter(delevery_service=booking)
        messages = AdminMessage.objects.filter(delivery=delivery_id)
        arg['messages'] = messages
        try:
            arg['payment'] = Payment.objects.filter(delevery_service=booking)
        except:
            arg['payment'] = None
    except Exception, e:
        # print e
        return redirect('/sadmin/delivery_order_list/')
    return render_to_response('superadmin/delivery_summary.html', arg, context)

def pickup_summary(request, pick_id):
    context = RequestContext(request)
    arg = {}
    arg['booking_active'] = True
    try:
        booking = PickupService.objects.get(id=pick_id)
        arg['booking'] = booking
        arg['logs'] = ActivityLog.objects.filter(pickup_service=booking)
        messages = AdminMessage.objects.filter(pickup=pick_id)
        arg['messages'] = messages
    except Exception, e:
        # print e
        return redirect('/sadmin/pickup_order_list/')
    return render_to_response('superadmin/pickup_summary.html', arg, context)


@csrf_exempt
def pay_driver(request):
    ids = request.POST.getlist('pay_id[]')
    for id in ids:
        try:
            payment = Payment.objects.get(id=id)
            payment.payment_status = 'PAID'
            payment.save()
        except Exception, e:
            return JsonResponse({"message": e})
    return JsonResponse({"message": "success"})


# def driver_reports(request):
#     if not request.user.is_active:
#         return redirect('/sadmin/login/')
#     driver = Driver.objects.all()
#     drivers = []
#     arg = {}
#
#     for d in driver:
#         acceptance_rate = get_acceptacce_rate(d)
#         cancellation_rate = get_cencell_rate(d)
#         d.acceptance_rate = acceptance_rate
#         d.cancellation_rate = cancellation_rate
#         print d.acceptance_rate
#         print d.cancellation_rate
#         drivers.append(d)
#     arg['driver'] = drivers
#     return render_to_response('superadmin/courier_report.html', arg)

def courier_sales_report(request):
    context = RequestContext(request)
    arg={}
    payment = Payment.objects.all()
    for p in payment:
        print p.id
    arg['earnings'] = payment
    return render_to_response('superadmin/delivery_sales_reports.html', arg, context)




def earnings_last_year(request):
    context = RequestContext(request)
    arg={}
    payment = Payment.objects.all()
    for p in payment:
        print p.id
    arg['earnings'] = payment
    return render_to_response('superadmin/delivery_sales_reports.html', arg, context)




def process_html_earnings(earning, type):
    if type == 'courier':
        ready_data = ''
        ready_data = ready_data + '<thead>'
        ready_data = ready_data + '<tr>'
        ready_data = ready_data + '<th>ID</th>'
        ready_data = ready_data + '<th>Driver ID</th>'
        ready_data = ready_data + '<th>Posting Time</th>'
        ready_data = ready_data + '<th>Total Cost</th>'
        ready_data = ready_data + '<th>Job Status</th>'
        ready_data = ready_data + '<th>Paid Amount</th>'
        ready_data = ready_data + '<th>Admin Fee</th>'
        ready_data = ready_data + '<th>Car Fee</th>'
        ready_data = ready_data + '<th>Toll Fee</th>'
        ready_data = ready_data + '<th>Online Fee</th>'
        ready_data = ready_data + '<th>Job Completion Time</th>'
        ready_data = ready_data + '<th>Transaction ID</th>'
        ready_data = ready_data + '<th>Payment Status</th>'
        ready_data = ready_data + '</tr>'
        ready_data = ready_data + '</thead><tbody>'
        if not earning:
            ready_data = ready_data + '<tr><td>' + 'No Records' + '</td>'
            ready_data = ready_data + '<td>' + 'No Records' + '</td>'
            ready_data = ready_data + '<td>' + 'No Records' + '</td>'
            ready_data = ready_data + '<td>' + 'No Records' + '</td>'
            ready_data = ready_data + '<td>' + 'No Records' + '</td>'
            ready_data = ready_data + '<td>' + 'No Records' + '</td>'
            ready_data = ready_data + '<td>' + 'No Records' + '</td>'
            ready_data = ready_data + '<td>' + 'No Records' + '</td>'
            ready_data = ready_data + '<td>' + 'No Records' + '</td>'
            ready_data = ready_data + '<td>' + 'No Records' + '</td>'
            ready_data = ready_data + '<td>' + 'No Records' + '</td>'
            ready_data = ready_data + '<td>' + 'No Records' + '</td>'
            ready_data = ready_data + '<td>' + 'No Records' + '</td></tr></tbody>'
            return HttpResponse(ready_data)
        else:
            for e in earning:
                # print str(e.delevery_service.id)

                if e.delevery_service:
                    ready_data = ready_data + '<tr>'
                    if e.payment_status == 'UNPAID':

                        ready_data = ready_data + '<td><div class="nowrap checkbox-vmiddle"><div class="checkbox-custom inline"><input type="checkbox" id="check_' + str(e.id) + '"value="' + str(e.id) + '"><label><a href="/sadmin/delivery_summary/'+ str(
                            e.delevery_service.id) +'/">DELIVERY#' + str(e.delevery_service.id) + '</label></div> </a></td>'
                    else:
                        ready_data = ready_data + '<td><div class="nowrap checkbox-vmiddle"><div class="checkbox-custom inline"><input type="checkbox" disabled><label>DELIVERY#' + str(
                            e.delevery_service.id) + '</label></div></div></td>'
                    if e.delevery_service.taken_by:
                        ready_data = ready_data + '<td>' + str(
                            e.delevery_service.taken_by.id) + '</td>'
                    else:
                        ready_data = ready_data + '<td>Job not taken yet.</td>'
                    ready_data = ready_data + '<td>' + str(e.delevery_service.created_at.strftime('%d %b %Y-%H:%M %P')) + '</td>'
                    ready_data = ready_data + '<td>' + str(e.total_cost) + '</td>'
                    ready_data = ready_data + '<td>' + e.delevery_service.delevery_status + '</td>'
                    ready_data = ready_data + '<td>' + str(e.paid_amount) + '</td>'
                    ready_data = ready_data + '<td>' + str(e.admin_fee) + '</td>'
                    ready_data = ready_data + '<td>' + str(e.car_fee) + '</td>'
                    ready_data = ready_data + '<td>' + str(e.toll_fee) + '</td>'
                    ready_data = ready_data + '<td>' + str(e.online_fee) + '</td>'
                    if e.delevery_service.job_complete_time:
                        ready_data = ready_data + '<td>' + str(e.delevery_service.job_complete_time.strftime('%d %b %Y-%H:%M %P')) + '</td>'
                    else:
                        ready_data = ready_data + '<td>None</td>'
                    ready_data = ready_data + '<td>Transaction# ' + str(e.id) + '</td>'
                    ready_data = ready_data + '<td>' + e.payment_status + '</td>'
                    ready_data = ready_data + '</tr>'
                else:
                    ready_data = ready_data + '</tr>'
            ready_data = ready_data + '</tbody>'
            return HttpResponse(ready_data)
    if type == 'pickup':
        ready_data = ''
        ready_data = ready_data + '<thead>'
        ready_data = ready_data + '<tr>'
        ready_data = ready_data + '<th>ID</th>'
        ready_data = ready_data + '<th>Total Cost</th>'
        ready_data = ready_data + '<th>Driver ID</th>'
        ready_data = ready_data + '<th>Posting Time</th>'
        ready_data = ready_data + '<th>Paid Amount</th>'
        ready_data = ready_data + '<th>Admin Fee</th>'
        ready_data = ready_data + '<th>Car Fee</th>'
        ready_data = ready_data + '<th>Toll Fee</th>'
        ready_data = ready_data + '<th>Online Fee</th>'
        ready_data = ready_data + '<th>Job Completion Time</th>'
        ready_data = ready_data + '<th>Transaction ID</th>'
        ready_data = ready_data + '<th>Payment Status</th>'
        # ready_data = ready_data + '<th>Action</th>'
        ready_data = ready_data + '</tr>'
        ready_data = ready_data + '</thead><tbody>'
        if not earning:
            ready_data = ready_data + '<tr><td>' + 'No Records' + '</td>'
            ready_data = ready_data + '<td>' + 'No Records' + '</td>'
            ready_data = ready_data + '<td>' + 'No Records' + '</td>'
            ready_data = ready_data + '<td>' + 'No Records' + '</td>'
            ready_data = ready_data + '<td>' + 'No Records' + '</td>'
            ready_data = ready_data + '<td>' + 'No Records' + '</td>'
            ready_data = ready_data + '<td>' + 'No Records' + '</td>'
            ready_data = ready_data + '<td>' + 'No Records' + '</td>'
            ready_data = ready_data + '<td>' + 'No Records' + '</td>'
            ready_data = ready_data + '<td>' + 'No Records' + '</td>'
            ready_data = ready_data + '<td>' + 'No Records' + '</td>'
            ready_data = ready_data + '<td>' + 'No Records' + '</td>'
            ready_data = ready_data + '<td>' + 'No Records' + '</td></tr></tbody>'
            return HttpResponse(ready_data)
        else:
            for e in earning:
                # print str(e.delevery_service.id)

                if e.pickup_service:
                    ready_data = ready_data + '<tr>'
                    if e.payment_status == 'UNPAID':
                        ready_data = ready_data + '<td><div class="nowrap checkbox-vmiddle"><div class="checkbox-custom inline"><input type="checkbox" id="check_' + str(e.id) + '"value="' + str(e.id) + '"><label><a href="/sadmin/pickup_summary/' + str(e.pickup_service.id) + '/">PICKUP#' + str(e.pickup_service.id) + '</a></label></div></div></td>'
                    else:
                        ready_data = ready_data + '<td><div class="nowrap checkbox-vmiddle"><div class="checkbox-custom inline"><input type="checkbox" disabled>PICKUP#<label>' + str(
                            e.pickup_service.id) + '</label></div></div></td>'
                    # if e.payment_status == 'UNPAID':
                    #     ready_data = ready_data + '<td><input type="checkbox" id="check_' + str(
                    #         e.id) + '"value="' + str(e.id) + '">PICKUP#' + str(e.pickup_service.id) + '</td>'
                    # else:
                    #     ready_data = ready_data + '<td><input type="checkbox" disabled>PICKUP#' + str(
                    #         e.pickup_service.id) + '</td>'
                    ready_data = ready_data + '<td>' + str(e.total_cost) + '</td>'
                    if e.pickup_service.pickedup_by:
                        ready_data = ready_data + '<td>Driver# ' + str(e.pickup_service.pickedup_by.id) + '</td>'
                    else:
                        ready_data = ready_data + '<td>Job has not been taken yet.</td>'
                    ready_data = ready_data + '<td>' + e.pickup_service.created_at.strftime('%d %b %Y-%H:%M %P') + '</td>'
                    ready_data = ready_data + '<td>' + str(e.paid_amount) + '</td>'
                    ready_data = ready_data + '<td>' + str(e.admin_fee) + '</td>'
                    ready_data = ready_data + '<td>' + str(e.car_fee) + '</td>'
                    ready_data = ready_data + '<td>' + str(e.toll_fee) + '</td>'
                    ready_data = ready_data + '<td>' + str(e.online_fee) + '</td>'
                    ready_data = ready_data + '<td>' + e.pickup_service.pickup_status + '</td>'
                    ready_data = ready_data + '<td>' + e.pickup_service.job_complete_time.strftime('%d %b %Y-%H:%M %P') + '</td>'
                    ready_data = ready_data + '<td>Transaction# ' + str(e.id) + '</td>'
                    ready_data = ready_data + '<td>' + e.payment_status + '</td>'
                    ready_data = ready_data + '</tr>'
                else:
                    ready_data = ready_data + '</tr>'
            ready_data = ready_data + '</tbody>'
            return HttpResponse(ready_data)

@csrf_exempt
def del_earnings_from_date(request):
    datetoday = datetime.datetime.today()
    first_day = datetime.datetime.strptime(request.POST['from_date'], '%d %b %Y').date()
    # diff = date_difference(datetoday, first_day) + 1
    last_day = datetime.datetime.strptime(request.POST['to_date'], '%d %b %Y').date()
    earning = delivery_earning_from_date_range(first_day, last_day)
    ready_data = process_html_earnings(earning, 'courier')
    return HttpResponse(ready_data)


def del_earnings_last_month(request):
    datetoday = datetime.datetime.today()
    first_day = first_day_of_month(datetoday)
    diff = date_difference(datetoday, first_day) + 1
    last_day = last_day_of_month(first_day)
    earning = delivery_earning_from_date_range(first_day, last_day)
    ready_data = process_html_earnings(earning, 'courier')
    return HttpResponse(ready_data)




def del_earnings_last_week(request):
    datetoday = datetime.datetime.today() - datetime.timedelta(days=7)
    first_day = date(week_magic(datetoday)[0].year, week_magic(datetoday)[0].month, week_magic(datetoday)[0].day)
    last_day = date(week_magic(datetoday)[1].year, week_magic(datetoday)[1].month, week_magic(datetoday)[1].day)
    earning = delivery_earning_from_date_range(first_day, last_day)
    ready_data = process_html_earnings(earning, 'courier')
    return HttpResponse(ready_data)




def del_earnings_last_year(request):
    datetoday = datetime.datetime.today()
    first_day = date(datetoday.year, 1, 1)
    last_day = date(datetoday.year, 12, 31)
    earning = delivery_earning_from_date_range(first_day, last_day)
    ready_data = process_html_earnings(earning, 'courier')
    return HttpResponse(ready_data)

@csrf_exempt
def pickup_earnings_from_date(request):
    datetoday = datetime.datetime.today()
    first_day = datetime.datetime.strptime(request.POST['from_date'], '%d %b %Y').date()
    # diff = date_difference(datetoday, first_day) + 1
    last_day = datetime.datetime.strptime(request.POST['to_date'], '%d %b %Y').date()
    earning = pickup_earning_from_date_range(first_day, last_day)
    ready_data = process_html_earnings(earning, 'pickup')
    return HttpResponse(ready_data)


def pickup_earnings_last_month(request):
    datetoday = datetime.datetime.today()
    first_day = first_day_of_month(datetoday)
    diff = date_difference(datetoday, first_day) + 1
    last_day = last_day_of_month(first_day)
    earning = pickup_earning_from_date_range(first_day, last_day)
    ready_data = process_html_earnings(earning, 'pickup')
    return HttpResponse(ready_data)




def pickup_earnings_last_week(request):
    datetoday = datetime.datetime.today() - datetime.timedelta(days=7)
    first_day = date(week_magic(datetoday)[0].year, week_magic(datetoday)[0].month, week_magic(datetoday)[0].day)
    last_day = date(week_magic(datetoday)[1].year, week_magic(datetoday)[1].month, week_magic(datetoday)[1].day)
    earning = pickup_earning_from_date_range(first_day, last_day)
    ready_data = process_html_earnings(earning, 'pickup')
    return HttpResponse(ready_data)




def pickup_earnings_last_year(request):
    datetoday = datetime.datetime.today()
    first_day = date(datetoday.year, 1, 1)
    last_day = date(datetoday.year, 12, 31)
    earning = pickup_earning_from_date_range(first_day, last_day)
    ready_data = process_html_earnings(earning, 'pickup')
    return HttpResponse(ready_data)

def pickup_earning_from_date_range(first_day, last_day):
    # type = get_driver_type(id)
    earnings = only_pickup_earnings_from_date(first_day, last_day)
    # print earnings[0].delevery_service
    if earnings:
        return earnings
    return None




def delivery_earning_from_date_range(first_day, last_day):
    # type = get_driver_type(id)
    earnings = only_delivery_earnings_from_date(first_day, last_day)
    # print earnings[0].delevery_service
    if earnings:
        return earnings
    return None





def only_delivery_earnings_from_date(start, end):
    start_date = datetime.datetime.strptime(start.strftime('%d-%m-%Y'), '%d-%m-%Y')
    # print start_date
    end_date = datetime.datetime.strptime(end.strftime('%d-%m-%Y'), '%d-%m-%Y')
    payment = Payment.objects.filter(date_time__range=[start_date, end_date])
    if payment.count() > 0:
        return payment
    else:
        return None

def only_pickup_earnings_from_date(start, end):
    start_date = datetime.datetime.strptime(start.strftime('%d-%m-%Y'), '%d-%m-%Y')
    # print start_date
    end_date = datetime.datetime.strptime(end.strftime('%d-%m-%Y'), '%d-%m-%Y')
    payment = Payment.objects.filter(date_time__range=[start_date, end_date])
    if payment.count() > 0:
        return payment
    else:
        return None


def pickup_sales_report(request):
    context = RequestContext(request)
    arg = {}
    payment = Payment.objects.filter(pickup_service__isnull=False)
    arg['earnings'] = payment
    return render_to_response('superadmin/pickup_sales_report.html', arg, context)

def pickup_sales_delete(request, payment_id):
    payment = Payment.objects.get(id=payment_id)
    payment.delete()
    return redirect('/sadmin/pickup_sales_report/')

def delivery_sales_delete(request, payment_id):
    payment = Payment.objects.get(id=payment_id)
    payment.delete()
    return redirect('/sadmin/courier_sales_report/')

def download_driver_csv(request):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="drivers.csv"'
    writer = csv.writer(response)
    driver = Driver.objects.all()
    drivers = []
    for d in driver:
        d.acceptance_rate = get_acceptacce_rate(d)
        d.cancellation_rate = get_cencell_rate(d)
        try:
            ratings = DriverRating.objects.filter(driver=d.id)
            total_rate = 0
            if ratings.count() > 0:
                for rating in ratings:
                    total_rate = total_rate + int(rating.rating)
            avg_rate = total_rate / ratings.count()
            d.rate = avg_rate
        except:
            d.rate = 0
        drivers.append(d)
    writer.writerow(
        ['Name', 'Contact No', 'Service Type', 'Email', 'Driver Category', 'Acceptance Rate', 'Cancellation Rate', 'Driver Rate'])
    for d in drivers:
        writer.writerow([d.name, d.mobile_no, d.driver_service_type, d.email, d.driver_category, d.acceptance_rate, d.cancellation_rate, d.rate])
    return response

def download_customer_csv(request):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="customers.csv"'
    writer = csv.writer(response)
    customer = Customer.objects.all()
    writer.writerow(
        ['Name', 'Contact No', 'Email', 'Created At', 'Address', 'Location'])
    for c in customer:
        writer.writerow([c.name, c.contact_no, c.email, c.created_at, c.address, c.location])
    return response

def download_courier_report_csv(request):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="courier_report.csv"'
    writer = csv.writer(response)
    payment = Payment.objects.all()
    writer.writerow(
        ['ID', 'Driver ID', 'Total Cost', 'Job status', 'Paid amount', 'Admin Fee', 'Car Fee', 'Toll Fee', 'Online Fee', 'Job Completion Time', 'Payment Status'])
    for p in payment:
        writer.writerow([p.delevery_service.id, p.delevery_service.taken_by, p.total_cost, p.delevery_service.delevery_status,
                         p.paid_amount, p.admin_fee, p.car_fee, p.toll_fee, p.online_fee, p.delevery_service.job_complete_time, p.payment_status])
    return response


def download_pickup_report_csv(request):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="pickup_report.csv"'
    writer = csv.writer(response)
    payment = Payment.objects.all()
    writer.writerow(
        ['ID', 'Driver ID', 'Total Cost', 'Job status', 'Paid amount', 'Admin Fee', 'Car Fee', 'Toll Fee', 'Online Fee', 'Job Completion Time', 'Payment Status'])
    for p in payment:
        writer.writerow([p.pickup_service.id, p.pickup_service.pickedup_by, p.total_cost, p.pickup_service.pickup_status,
                         p.paid_amount, p.admin_fee, p.car_fee, p.toll_fee, p.online_fee, p.pickup_service.job_complete_time, p.payment_status])
    return response


class DriverPDF(PDFTemplateView):
    template_name = "superadmin/driver_pdf.html"
    bill = None

    def get_context_data(self, **kwargs):
        context = super(DriverPDF, self).get_context_data(**kwargs)
        driver = Driver.objects.all()
        drivers = []
        for d in driver:
            d.acceptance_rate = get_acceptacce_rate(d)
            d.cancellation_rate = get_cencell_rate(d)
            try:
                ratings = DriverRating.objects.filter(driver=d.id)
                total_rate = 0
                if ratings.count() > 0:
                    for rating in ratings:
                        total_rate = total_rate + int(rating.rating)
                avg_rate = total_rate / ratings.count()
                d.rate = avg_rate
            except:
                d.rate = 0
            drivers.append(d)
        context['drivers'] = drivers
        return context

class CourierReportPDF(PDFTemplateView):
    template_name = "superadmin/courier_report_pdf.html"
    bill = None

    def get_context_data(self, **kwargs):
        context = super(CourierReportPDF, self).get_context_data(**kwargs)
        payment = Payment.objects.all()
        context['earnings'] = payment
        return context

class PickupReportPDF(PDFTemplateView):
    template_name = "superadmin/pickup_report_pdf.html"
    bill = None

    def get_context_data(self, **kwargs):
        context = super(PickupReportPDF, self).get_context_data(**kwargs)
        payment = Payment.objects.all()
        context['earnings'] = payment
        return context

class CustomerPDF(PDFTemplateView):
    template_name = "superadmin/customer_pdf.html"
    bill = None

    def get_context_data(self, **kwargs):
        context = super(CustomerPDF, self).get_context_data(**kwargs)
        customers = Customer.objects.all()
        context['customers'] = customers
        return context

def refund_reports(request):
    context = RequestContext(request)
    arg={}
    payment = Payment.objects.filter(Q(payment_status = 'REFUND')| Q(payment_status='REFUNDED'))
    arg['earnings'] = payment
    return render_to_response('superadmin/refund_report.html', arg, context)

@csrf_exempt
def refund(request):
    ids = request.POST.getlist('pay_id[]')
    for id in ids:
        try:
            payment = Payment.objects.get(id=id)
            payment.payment_status = 'REFUNDED'
            payment.save()
        except Exception, e:
            return JsonResponse({"message": e})
    messages.error(request, 'Refund Successful')
    return JsonResponse({"message": "success"})

def edit_cnfigurations(request):
    arg = RequestContext(request)
    if not request.user.is_active:
        return redirect ('/sadmin/login/')
    try:
        instance = SiteConfigeration.objects.all()
        instance = instance[0]
        form = SiteSettingsForm(instance=instance)
    except:
        form = SiteSettingsForm()
    if request.method == "POST":
        if instance:
            form = SiteSettingsForm(request.POST, instance=instance)
        else:
            form = SiteSettingsForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('/sadmin/configurations/')
    context= {
        'form': form,
        'instance': instance,
    }
    context.update(csrf(request))
    return render_to_response('superadmin/configrations.html', context, arg)

def create_superadmin(request):
    if not request.user.is_active:
        return redirect('/sadmin/login/')
    if request.method == "POST":
        try:
            user = User.objects.create_user(request.POST['username'], request.POST['email'], request.POST['password'])
            user.first_name = request.POST['first_name']
            user.last_name = request.POST['last_name']
            user.is_active = True
            user.is_staff = True
            user.is_superuser = True
            access_control = AccessControl()
            try:
                access_control.is_cms = request.POST['cms']
            except:
                access_control.is_cms = False
            try:
                access_control.is_settings = request.POST['settings']
            except:
                access_control.is_settings = False
            try:
                access_control.is_window_setup = request.POST['delivery_window']
            except:
                access_control.is_window_setup = False
            try:
                access_control.is_address_setup = request.POST['address']
            except:
                access_control.is_address_setup = False
            try:
                access_control.is_courier = request.POST['courier']
            except:
                access_control.is_courier = False
            try:
                access_control.is_pickup = request.POST['pickup']
            except:
                access_control.is_pickup = False
            try:
                access_control.is_driver_info = request.POST['driver_info']
            except:
                access_control.is_driver_info = False
            try:
                access_control.is_sales_report = request.POST['sales_report']
            except:
                access_control.is_sales_report = False
            try:
                access_control.is_customer_list = request.POST['customer_list']
            except:
                access_control.is_customer_list = False
            try:
                access_control.is_account = request.POST['account']
            except:
                access_control.is_account = False
            try:
                access_control.is_add_coupon = request.POST['coupon']
            except:
                access_control.is_add_coupon = False
            access_control.user = user
            access_control.save()
            user.save()
            messages.success(request, 'Superadmin user has been created')
            return redirect('/sadmin/create_superadmin/')
        except Exception, e:
            print e
            return redirect('/sadmin/create_superadmin/')
    context={}
    superadmins = User.objects.filter(is_superuser=1)
    # print str(superadmins)
    suad = []
    for sadmin in superadmins:
        # print str(sadmin.id)
        access = AccessControl.objects.get(user=sadmin)
        sadmin.access = access
        suad.append(sadmin)
    context['superadmin'] = suad
    # print str(suad[0].access.is_pickup)
    context.update(csrf(request))
    return render_to_response('superadmin/superadmin_register.html', context, RequestContext(request))

def superadmin_delete(request, super_id):
    user = User.objects.get(id=super_id)
    access = AccessControl.objects.get(user=user)
    access.delete()
    user.delete()
    return redirect('/sadmin/create_superadmin/')

@csrf_exempt
def activate_all(request):
    ids = request.POST.getlist('customer_id[]')
    for id in ids:
        try:
            user = User.objects.get(id=id)
            if not user.is_active:
                user.is_active = True
                user.save()
                print "changed"
        except Exception, e:
            return JsonResponse({"message": e})
    # messages.error(request, 'Success')
    return JsonResponse({"message": "success"})


@csrf_exempt
def deactivate_all(request):
    # print "before"
    ids = request.POST.getlist('customer_id[]')
    # print "after"
    for id in ids:
        try:
            user = User.objects.get(id=id)
            print str(user.is_active)
            if user.is_active:
                user.is_active = False
                user.save()
        except Exception, e:
            print e
            return JsonResponse({"message": e})
    # messages.error(request, 'Success')
    return JsonResponse({"message": "success"})


def assign_driver_for_corporate(request):
    context = RequestContext(request)
    corporates = CorporateDriverCustomer.objects.all()
    driver = Driver.objects.filter(is_corporate=True)
    customer = Customer.objects.filter(is_corporate=True)
    arg = {}
    arg['driver'] = driver
    arg['customer'] = customer
    arg['corporates'] = corporates
    if request.method == 'POST':
        corporates = CorporateDriverCustomer()
        corporates.driver = Driver.objects.get(id=request.POST['drivers'])
        corporates.customer = Customer.objects.get(id=request.POST['customers'])
        corporates.save()
        return redirect('/sadmin/assign_driver_for_corporate/')
    return render_to_response('superadmin/assign_driver_for_corporate.html', arg, context)

def assign_driver_for_corporate_delete(request, corporate_id):
    context = RequestContext(request)
    corporates = CorporateDriverCustomer.objects.get(id=corporate_id)
    corporates.delete()
    return redirect('/sadmin/assign_driver_for_corporate/')


def make_customer_corporate(request, customer_id):
    customer = Customer.objects.get(id=customer_id)
    if customer.is_corporate:
        customer.is_corporate = False
        customer.save()
        try:
            corporate = CorporateDriverCustomer.objects.get(customer=customer)
            corporate.delete()
        except:
            pass
    else:
        customer.is_corporate = True
        customer.save()
    return redirect('/sadmin/customer_profile_detail/'+customer_id+'/')


def make_driver_corporate(request, driver_id):
    driver = Driver.objects.get(id=driver_id)
    if driver.is_corporate:
        driver.is_corporate = False
        driver.save()
        try:
            corporate = CorporateDriverCustomer.objects.get(driver=driver)
            corporate.delete()
        except:
            pass
    else:
        driver.is_corporate = True
        driver.save()
    return redirect('/sadmin/driver_profile_detail/'+driver_id+'/')


def pickup_cancellation_reason(request):
    context = RequestContext(request)
    reason = PickupCancellationReasons.objects.all()
    form = PickupCancellationReasonsForm()
    if request.method == 'POST':
        form = PickupCancellationReasonsForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('/sadmin/pickup_cancellation_reason/')
    arg = {}
    arg['reason'] = reason
    arg['form'] = form
    arg.update(csrf(request))
    return render_to_response('superadmin/cancellation_reasons.html', arg, context)

def pickup_cancellation_reason_update(request, reason_id):
    context = RequestContext(request)
    reason = PickupCancellationReasons.objects.filter(id=reason_id)
    form = PickupCancellationReasonsForm(instance=reason[0])
    if request.method == 'POST':
        form = PickupCancellationReasonsForm(request.POST, request.FILES, instance=reason[0])
        if form.is_valid():
            form.save()
            return redirect('/sadmin/pickup_cancellation_reason/')
    arg = {}
    arg['reason'] = reason
    arg['form'] = form
    arg.update(csrf(request))
    return render_to_response('superadmin/cancellation_reasons.html', arg, context)


def pickup_cancellation_reason_delete(request, reason_id):
    context = RequestContext(request)
    reason = PickupCancellationReasons.objects.get(id=reason_id)
    reason.delete()
    return redirect('/sadmin/pickup_cancellation_reason/')


def get_site_configeartions():
    try:
        objects = SiteConfigeration.objects.all()
        return objects[0]
    except:
        return None


def rotating_banner(request):
    context = RequestContext(request)
    arg={}
    rotating = RotatingBanner.objects.all()
    form = RotatingBannerForm()
    if request.method == 'POST':
        form = RotatingBannerForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('/sadmin/rotating_banner/')
    arg['rotating'] = rotating
    arg['form'] = form
    arg.update(csrf(request))
    return render_to_response('superadmin/rotating_banner.html', arg, context)

def rotating_banner_update(request, banner_id):
    context = RequestContext(request)
    arg = {}
    rotating = RotatingBanner.objects.filter(id=banner_id)
    instance = rotating
    form = RotatingBannerForm(instance=instance[0])
    if request.method == 'POST':
        form = RotatingBannerForm(request.POST, request.FILES, instance=instance[0])
        if form.is_valid():
            form.save()
            return redirect('/sadmin/rotating_banner/')
    arg['rotating'] = rotating
    arg['form'] = form
    arg.update(csrf(request))
    return render_to_response('superadmin/rotating_banner.html', arg, context)

def rotating_banner_delete(request, banner_id):
    rotating = RotatingBanner.objects.get(id=banner_id)
    rotating.delete()
    return redirect('/sadmin/rotating_banner/')

def audite_trile(request):
    context = RequestContext(request)
    arg = {}
    from base.models import AuditTrile
    data = AuditTrile.objects.all()
    arg['data'] = data
    return render_to_response('superadmin/audite_trile.html', arg, context)

@csrf_exempt
def get_drivers_search(request):
    try:
        driver = Driver.objects.filter(name__contains=request.POST['driver'], is_corporate=True)
        ready_data = ''
        for d in driver:
            ready_data = ready_data + '<input type="radio" name="drivers" value="'+ str(d.id) +'">' + d.name + '</input><br/>'
        return HttpResponse(ready_data)
    except Exception, e:
        return HttpResponse('<p>' + str(e) + '</p>')
