from django.db import models
from base import constant
from django.contrib.auth.models import User


# Create your models here.





class HomeCMS(models.Model):
    category = models.CharField(max_length=120, blank=True, null=True)

    logo = models.ImageField(blank=True, null=True, upload_to='home')

    banner = models.ImageField(blank=True, null=True, upload_to='home')
    banner_text = models.CharField(max_length=120, blank=True, null=True)
    banner_desc = models.CharField(max_length=120, blank=True, null=True)

    under_banner_text = models.CharField(max_length=120, blank=True, null=True)

    service1_image = models.ImageField(blank=True, null=True, upload_to='home')
    service1_title = models.CharField(max_length=120, blank=True, null=True)
    service1_desc = models.TextField(blank=True, null=True)

    service2_image = models.ImageField(blank=True, null=True, upload_to='home')
    service2_title = models.CharField(max_length=120, blank=True, null=True)
    service2_desc = models.TextField(blank=True, null=True)

    features = models.CharField(max_length=120, blank=True, null=True)
    features_desc = models.TextField(blank=True, null=True)

    feature1_image = models.ImageField(blank=True, null=True, upload_to='home/feature')
    feature1_title = models.CharField(max_length=120, blank=True, null=True)
    feature1_desc = models.TextField(blank=True, null=True)

    feature2_image = models.ImageField(blank=True, null=True, upload_to='home/feature')
    feature2_title = models.CharField(max_length=120, blank=True, null=True)
    feature2_desc = models.TextField(blank=True, null=True)

    feature3_image = models.ImageField(blank=True, null=True, upload_to='home/feature')
    feature3_title = models.CharField(max_length=120, blank=True, null=True)
    feature3_desc = models.TextField(blank=True, null=True)

    feature4_image = models.ImageField(blank=True, null=True, upload_to='home/feature')
    feature4_title = models.CharField(max_length=120, blank=True, null=True)
    feature4_desc = models.TextField(blank=True, null=True)

    footer1_text = models.CharField(max_length=120, blank=True, null=True)
    footer1_desc = models.TextField(blank=True, null=True)

    social1_icons = models.URLField(blank=True, null=True)
    social2_icons = models.URLField(blank=True, null=True)
    social3_icons = models.URLField(blank=True, null=True)
    social4_icons = models.URLField(blank=True, null=True)
    social5_icons = models.URLField(blank=True, null=True)

    footer2_text = models.CharField(max_length=120, blank=True, null=True)
    footer2_url1 = models.URLField(blank=True, null=True)
    footer2_url2 = models.URLField(blank=True, null=True)
    footer2_url3 = models.URLField(blank=True, null=True)
    footer2_url4 = models.URLField(blank=True, null=True)
    footer2_url5 = models.URLField(blank=True, null=True)

    footer3_url1 = models.URLField(blank=True, null=True)
    footer3_url2 = models.URLField(blank=True, null=True)
    footer3_url3 = models.URLField(blank=True, null=True)

    footer4 = models.CharField(max_length=120, blank=True, null=True)

    def __unicode__(self):
        return "Index page edits"

class Terms(models.Model):
    title = models.CharField(max_length=120, blank=True, null=True)
    description = models.TextField(blank=True, null=True)

class Faq(models.Model):
    title = models.CharField(max_length=120, blank=True, null=True)
    description = models.TextField(blank=True, null=True)

class Privacy(models.Model):
    title = models.CharField(max_length=120, blank=True, null=True)
    description = models.TextField(blank=True, null=True)

class AboutPage(models.Model):
    banner = models.ImageField(blank=True, null=True, upload_to='about/banner')

    title1 = models.CharField(max_length=120, blank=True, null=True)
    desc1 = models.TextField(blank=True, null=True)

    image1 = models.ImageField(blank=True, null=True, upload_to='about')
    title2 = models.CharField(max_length=120, blank=True, null=True)
    desc2 = models.TextField(blank=True, null=True)

    link1_text = models.CharField(max_length=120, blank=True, null=True)

    title3 = models.CharField(max_length=120, blank=True, null=True)
    desc3 = models.TextField(blank=True, null=True)

    #VIDEO FIELD MODEL
    video = models.FileField(blank=True, null=True, upload_to='about/video')
    image_title = models.CharField(max_length=120, blank=True, null=True)

    image2 = models.ImageField(blank=True, null=True, upload_to='about/iteam')
    image3 = models.ImageField(blank=True, null=True, upload_to='about/iteam')
    image4 = models.ImageField(blank=True, null=True, upload_to='about/iteam')

class CustomerFaqPage(models.Model):
    # banner = models.ImageField(blank=True, null=True, upload_to='about/banner')

    question = models.CharField(max_length=120, blank=True, null=True)
    answer = models.TextField(blank=True, null=True)

    def __unicode__(self):
        return "Customer Faq"

class SliderImage(models.Model):
    title = models.CharField(max_length=120, blank=True, null=True)
    desc = models.TextField(blank=True, null=True)
    images = models.ImageField(blank=True, null=True, upload_to='about/iteam')

    def __unicode__(self):
        return "About page edits"

class RegistrationBackground(models.Model):
    title = models.CharField(max_length=120, blank=True, null=True)
    images = models.ImageField(blank=True, null=True, upload_to='login/images')

    def __unicode__(self):
        return "About page edits"

class Services(models.Model):
    image1 = models.ImageField(blank=True, null=True, upload_to='services')

    title1 = models.CharField(max_length=500, blank=True, null=True)
    desc1 = models.TextField(blank=True, null=True)

    rewards1_image = models.ImageField(blank=True, null=True, upload_to='services/reward')
    rewards1_text = models.CharField(max_length=120, blank=True, null=True)
    rewards1_desc = models.TextField(blank=True, null=True)

    rewards2_image = models.ImageField(blank=True, null=True, upload_to='services/reward')
    rewards2_text = models.CharField(max_length=120, blank=True, null=True)
    rewards2_desc = models.TextField(blank=True, null=True)

    rewards3_image = models.ImageField(blank=True, null=True, upload_to='services/reward')
    rewards3_text = models.CharField(max_length=120, blank=True, null=True)
    rewards3_desc = models.TextField(blank=True, null=True)

    rewards4_image = models.ImageField(blank=True, null=True, upload_to='services/reward')
    rewards4_text = models.CharField(max_length=120, blank=True, null=True)
    rewards4_desc = models.TextField(blank=True, null=True)

    image2 = models.ImageField(blank=True, null=True, upload_to='services')

    title2 = models.CharField(max_length=120, blank=True, null=True)
    desc2 = models.TextField(blank=True, null=True)

    small_icon = models.ImageField(blank=True, null=True, upload_to='services/icon')
    small_icon_desc = models.TextField(blank=True, null=True)
    title3 = models.CharField(max_length=120, blank=True, null=True)
    image3 = models.ImageField(blank=True, null=True, upload_to='services')
    desc3 = models.TextField(blank=True, null=True)

    title4 = models.CharField(max_length=120, blank=True, null=True)
    desc4 = models.TextField(blank=True, null=True)

    tab1 = models.CharField(max_length=120, blank=True, null=True)
    tab1_desc = models.TextField(blank=True, null=True)

    tab2 = models.CharField(max_length=120, blank=True, null=True)
    tab2_desc = models.TextField(blank=True, null=True)

    tab3 = models.CharField(max_length=120, blank=True, null=True)
    tab3_desc = models.TextField(blank=True, null=True)

    slider_image1 = models.ImageField(blank=True, null=True, upload_to='services/slider')
    slider_image2 = models.ImageField(blank=True, null=True, upload_to='services/slider')
    slider_image3 = models.ImageField(blank=True, null=True, upload_to='services/slider')
    slider_image4 = models.ImageField(blank=True, null=True, upload_to='services/slider')
    slider_image5 = models.ImageField(blank=True, null=True, upload_to='services/slider')
    def __unicode__(self):
        return "Services"


class JoinUs(models.Model):
    #BE A DRIVER
    title1 = models.CharField(max_length=120, blank=True, null=True)
    #TOP IMAGE
    image1 = models.ImageField(blank=True, null=True, upload_to='joinus')
    #BE A DRIVER DESCRIPTION
    desc1 = models.TextField(blank=True, null=True)

    #BE A DELIVERY PARTNER
    title2 = models.CharField(max_length=120, blank=True, null=True)
    #SECOND IMAGE
    image2 = models.ImageField(blank=True, null=True, upload_to='joinus')
    #BE A DELIVERY PARTNER DESCRIPTION
    desc2 = models.TextField(blank=True, null=True)

    #CAREERS
    title3 = models.CharField(max_length=120, blank=True, null=True)
    #CAREER IMAGE
    image3 = models.ImageField(blank=True, null=True, upload_to='joinus')
    #BE A DELIVERY PARTNER DESCRIPTION
    desc3 = models.TextField(blank=True, null=True)

    #CAREER LINK
    link1_text = models.CharField(max_length=120, blank=True, null=True)


    ###HOW IT WORKS IMAGES
    slider_image1 = models.ImageField(blank=True, null=True, upload_to='joinus/slider')
    slider_image2 = models.ImageField(blank=True, null=True, upload_to='joinus/slider')
    slider_image3 = models.ImageField(blank=True, null=True, upload_to='joinus/slider')
    slider_image4 = models.ImageField(blank=True, null=True, upload_to='joinus/slider')
    slider_image5 = models.ImageField(blank=True, null=True, upload_to='joinus/slider')

    #FEATURES
    features_text = models.CharField(max_length=120, blank=True, null=True)
    features_desc = models.TextField(blank=True, null=True)

    feature1_title = models.CharField(max_length=120, blank=True, null=True)
    feature1_image= models.ImageField(blank=True, null=True, upload_to='joinus/feature')
    feature1_desc = models.TextField(blank=True, null=True, )

    feature2_title = models.CharField(max_length=120, blank=True, null=True)
    feature2_image= models.ImageField(blank=True, null=True, upload_to='joinus/feature')
    feature2_desc = models.TextField(blank=True, null=True)

    feature3_title = models.CharField(max_length=120, blank=True, null=True)
    feature3_image= models.ImageField(blank=True, null=True, upload_to='joinus/feature')
    feature3_desc = models.TextField(blank=True, null=True)

    feature4_title = models.CharField(max_length=120, blank=True, null=True)
    feature4_image= models.ImageField(blank=True, null=True, upload_to='joinus/feature')
    feature4_desc = models.TextField(blank=True, null=True)

    title4 = models.CharField(max_length=120, blank=True, null=True)
    desc4 = models.TextField(blank=True, null=True)

    footer_slider_image1 = models.ImageField(blank=True, null=True, upload_to='services/slider')
    footer_slider_image2 = models.ImageField(blank=True, null=True, upload_to='services/slider')
    footer_slider_image3 = models.ImageField(blank=True, null=True, upload_to='services/slider')
    footer_slider_image4 = models.ImageField(blank=True, null=True, upload_to='services/slider')
    footer_slider_image5 = models.ImageField(blank=True, null=True, upload_to='services/slider')

    def __unicode__(self):
        return "Join Us"

class Careers(models.Model):

    banner = models.ImageField(blank=True, null=True, upload_to='career')

    title = models.CharField(max_length=120, blank=True, null=True)
    desc = models.TextField(blank=True, null=True)

    job_title = models.CharField(max_length=120, blank=True, null=True)
    job_desc = models.TextField(blank=True, null=True)
    job_responsibility = models.TextField(blank=True, null=True)

    def __unicode__(self):
        return "Careers"

class Corporate(models.Model):
    banner = models.ImageField(blank=True, null=True, upload_to='corporate/banner')

    title1 = models.CharField(max_length=120, blank=True, null=True)
    image1= models.ImageField(blank=True, null=True, upload_to='corporate')
    desc1 = models.TextField(blank=True, null=True)
    link1_text = models.CharField(max_length=120, blank=True, null=True)

    title2 = models.CharField(max_length=120, blank=True, null=True)
    image2= models.ImageField(blank=True, null=True, upload_to='corporate')
    desc2 = models.TextField(blank=True, null=True)
    link2_text = models.CharField(max_length=120, blank=True, null=True)

    title3 = models.CharField(max_length=120, blank=True, null=True)
    image3= models.ImageField(blank=True, null=True, upload_to='corporate')
    desc3 = models.TextField(blank=True, null=True)
    link3_text = models.CharField(max_length=120, blank=True, null=True)

    title4 = models.CharField(max_length=120, blank=True, null=True)
    image4= models.ImageField(blank=True, null=True, upload_to='corporate')
    desc4 = models.TextField(blank=True, null=True)
    link4_text = models.CharField(max_length=120, blank=True, null=True)

    def __unicode__(self):
        return "Corporate"

class ContactUsPage(models.Model):
    addr_title = models.CharField(max_length=120, blank=True, null=True)
    # addr_addr = models.CharField(max_length=120, blank=True, null=True)
    addr_tel = models.CharField(max_length=120,blank=True, null=True)
    addr_fax = models.CharField(max_length=120,blank=True, null=True)
    addr_email = models.EmailField(blank=True, null=True)

    title = models.CharField(max_length=120, blank=True, null=True)
    desc = models.TextField(blank=True, null=True)
    contact_location = models.CharField(max_length=120, null=True)

    def __unicode__(self):
        return "ContactUs"

class DriverStatus(models.Model):
    driver_type = models.CharField(max_length=120)
    courier_commission = models.DecimalField(max_digits=5, decimal_places=2)
    pickup_commission = models.DecimalField(max_digits=5, decimal_places=2)
    def __unicode__(self):
        return self.driver_type

class DeliveryTiming(models.Model):
    """
        Now this model is for collection window only
    """
    start_time = models.TimeField(null=True)
    end_time = models.TimeField(null=True)
    window_name = models.CharField(max_length=120, null=True)
    max_number_of_active_job = models.IntegerField(default=1000)

class DriverCategory(models.Model):
    category_name = models.CharField(max_length=120, null=True)
    category_details = models.CharField(max_length=120, null=True)
    def __unicode__(self):
        return self.category_name

class ExpressWindows(models.Model):
    """
        Now this model is for delivery window only
    """
    window_name = models.CharField(max_length=120, null=True)
    start_time = models.TimeField(null=True, blank=True)
    end_time = models.TimeField(null=True, blank=True)
    price = models.DecimalField(max_digits=7, decimal_places=2, default=0.00)
    corp_price = models.DecimalField(max_digits=7, decimal_places=2, default=0.00)
    multiple = models.BooleanField(default=False)
    max_number_of_active_job = models.IntegerField(default=1000)

    def __unicode__(self):
        return self.window_name


class CommissionSetup(models.Model):
    category = models.CharField(max_length=100, choices=constant.DRIVER_TYPE, default='NORMAL', null=True)
    commission_pickup = models.DecimalField(max_digits=5, decimal_places=2)
    commission_courier = models.DecimalField(max_digits=5, decimal_places=2)

class SiteConfigeration(models.Model):
    max_delivery_ojb = models.IntegerField()
    max_minuts_for_cencellations = models.IntegerField(default=100)
    office_address = models.TextField(blank=True, null=True)
    office_phone_no = models.CharField(max_length=120,blank=True, null=True)
    office_email = models.CharField(max_length=120,blank=True, null=True)
    driver_radius = models.IntegerField(default=5000)
    waiting_time_for_drivers = models.IntegerField(default=15)
    # job_expire_days_courier = models.IntegerField(default=15)

class AccessControl(models.Model):
    user = models.ForeignKey(User, null=True)
    is_cms = models.BooleanField(default=False)
    is_settings = models.BooleanField(default=False)
    is_courier = models.BooleanField(default=False)
    is_pickup = models.BooleanField(default=False)
    is_driver_info = models.BooleanField(default=False)
    is_sales_report = models.BooleanField(default=False)
    is_customer_list = models.BooleanField(default=False)
    is_account = models.BooleanField(default=False)
    is_add_coupon = models.BooleanField(default=False)
    is_window_setup = models.BooleanField(default=False)
    is_address_setup = models.BooleanField(default=False)

class RotatingBanner(models.Model):
    image = models.ImageField(blank=True, null=True, upload_to='home/banner')
    title = models.CharField(max_length=120, blank=True, null=True)