from django.db import models
from base.models import BaseModel
from django.contrib.auth.models import User
from django.template.defaultfilters import slugify
from superadmin.models import DriverStatus, DriverCategory
from base import constant
from datetime import date
# Create your models here.

SERVICE_TYPE = (
    ('courier', 'courier'),
    ('pickup', 'pickup'),
    ('both', 'both'),
)


def content_file_name(instance, filename):
    return '/'.join(['content', instance.user.username, filename])

class Driver(BaseModel):
	user = models.OneToOneField(User)
	name = models.CharField(max_length=255, default=None)
	profile_image = models.ImageField(default=None, blank=True, null=True, upload_to='driver/profile')
	email = models.CharField(max_length=255, unique=True)
	verification = models.CharField(max_length=5, default=None, null=True)
	# Need to use this part later #
	# mobile_no = models.CharField(max_length=255, unique=True, null=True, default=None)
	# Need to use this part later #
	mobile_no = models.CharField(max_length=255, null=True, default=None)
	nric_photo = models.ImageField(default=None, blank=True, null=True, upload_to='driver/nrc')
	driving_licence_photo = models.ImageField(default=None, blank=True, null=True, upload_to='driver/licence')
	company = models.CharField(max_length=255, default=None, null=True)
	last_lat = models.CharField(max_length=255, default=None, null=True)
	last_lng = models.CharField(max_length=255, default=None, null=True)
	driver_type = models.OneToOneField(DriverStatus, default=None, null=True)
	driver_service_type = models.CharField(max_length=10, choices=SERVICE_TYPE, default='both', null=True)
	is_active = models.BooleanField(default=False)
	stripe_id = models.CharField(max_length=225, null=True, default=None)
	stripe_status = models.CharField(max_length=225, null=True, default=None)
	driver_category = models.CharField(max_length=100, choices=constant.DRIVER_TYPE, default='NORMAL')
	#device token for push
	device_token = models.CharField(max_length=225, null=True, default=None)
	device_id = models.CharField(max_length=225, null=True, default=None)
	driver_no = models.CharField(max_length=20, default=None, null=True)
	is_corporate = models.BooleanField(default=False)
	def save(self, *args, **kwargs):
		tatal = Driver.objects.all()
		d = date.today()
		month = str(d.month)
		pid = str(tatal.count()+1)
		if self.driver_service_type == 'courier':
			prefix = 'C'
		elif self.driver_service_type == 'pickup':
			prefix = 'P'
		else:
			prefix = 'B'
		self.driver_no = prefix+month.zfill(2)+str(d.year)+pid.zfill(4)
		super(Driver, self).save(*args, **kwargs)

	def __unicode__(self):
		return self.name

	def get_vechiles(self):
		try:
			return Vechile.objects.filter(driver=self)[0]
		except:
			return None
	def get_bank_ac(self):
		return BankAccount.objects.filter(driver=self)

class Vechile(BaseModel):
	driver = models.ForeignKey(Driver)
	plate = models.ImageField(default=None, blank=True, null=True, upload_to='driver/plate')
	vechile_photo = models.ImageField(default=None, blank=True, null=True, upload_to='driver/plate')
	vechile_type = models.CharField(max_length=255)
	vechile_number = models.CharField(max_length=255, default=None, null=True)
	brand = models.CharField(max_length=255, default=None, null=True)

	def get_vehicle_name(self):
		try:
			from service.models import VehicleType
			vechile = VehicleType.objects.get(id=int(self.vechile_type))
			return vechile.caption
		except:
			return None

class BankAccount(BaseModel):
	driver = models.ForeignKey(Driver)
	name_of_bank = models.CharField(max_length=255)
	account_number = models.CharField(max_length=255)
	cvv = models.CharField(max_length=225, null=True, default=None)
	bank_code = models.CharField(max_length=255, default=None, null=True)
	branch_code = models.CharField(max_length=255, default=None, null=True)
