# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('driver', '0014_auto_20161026_1956'),
    ]

    operations = [
        migrations.AddField(
            model_name='bankaccount',
            name='cvv',
            field=models.CharField(default=None, max_length=225, null=True),
        ),
        migrations.AddField(
            model_name='bankaccount',
            name='stripe_id',
            field=models.CharField(default=None, max_length=225, null=True),
        ),
        migrations.AddField(
            model_name='bankaccount',
            name='stripe_status',
            field=models.CharField(default=None, max_length=225, null=True),
        ),
    ]
