# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('driver', '0002_auto_20160821_0806'),
    ]

    operations = [
        migrations.CreateModel(
            name='BankAccount',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_at', models.DateTimeField(auto_now=True, null=True)),
                ('status', models.BooleanField(default=True)),
                ('name_of_bank', models.CharField(max_length=255)),
                ('account_number', models.CharField(max_length=255)),
                ('bank_code', models.CharField(default=None, max_length=255, null=True)),
                ('branch_code', models.CharField(default=None, max_length=255, null=True)),
                ('created_by', models.ForeignKey(related_name='+', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Vechile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_at', models.DateTimeField(auto_now=True, null=True)),
                ('status', models.BooleanField(default=True)),
                ('plate', models.ImageField(default=None, null=True, upload_to=b'driver/plate', blank=True)),
                ('vechile_photo', models.ImageField(default=None, null=True, upload_to=b'driver/plate', blank=True)),
                ('vechile_type', models.CharField(max_length=255)),
                ('vechile_number', models.CharField(default=None, max_length=255, null=True)),
                ('brand', models.CharField(default=None, max_length=255, null=True)),
                ('created_by', models.ForeignKey(related_name='+', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='driver',
            name='company',
            field=models.CharField(default=None, max_length=255, null=True),
        ),
        migrations.AddField(
            model_name='vechile',
            name='deriver',
            field=models.ForeignKey(to='driver.Driver'),
        ),
        migrations.AddField(
            model_name='vechile',
            name='updated_by',
            field=models.ForeignKey(related_name='+', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='bankaccount',
            name='deriver',
            field=models.ForeignKey(to='driver.Driver'),
        ),
        migrations.AddField(
            model_name='bankaccount',
            name='updated_by',
            field=models.ForeignKey(related_name='+', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
    ]
