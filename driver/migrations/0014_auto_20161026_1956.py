# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('driver', '0013_auto_20161026_1954'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='bankaccount',
            name='cvv',
        ),
        migrations.RemoveField(
            model_name='bankaccount',
            name='expiry_date',
        ),
        migrations.RemoveField(
            model_name='bankaccount',
            name='external_reff_no',
        ),
        migrations.RemoveField(
            model_name='bankaccount',
            name='last_digits',
        ),
        migrations.RemoveField(
            model_name='bankaccount',
            name='strite_id',
        ),
    ]
