# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('driver', '0022_auto_20161220_1355'),
    ]

    operations = [
        migrations.AddField(
            model_name='driver',
            name='driver_no',
            field=models.CharField(default=None, max_length=20, null=True),
        ),
    ]
