# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('driver', '0006_driver_mobile_no'),
    ]

    operations = [
        migrations.AddField(
            model_name='driver',
            name='last_lat',
            field=models.CharField(default=None, max_length=255, null=True),
        ),
        migrations.AddField(
            model_name='driver',
            name='last_lng',
            field=models.CharField(default=None, max_length=255, null=True),
        ),
    ]
