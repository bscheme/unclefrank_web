# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('driver', '0003_auto_20160908_1648'),
    ]

    operations = [
        migrations.RenameField(
            model_name='bankaccount',
            old_name='deriver',
            new_name='driver',
        ),
        migrations.RenameField(
            model_name='vechile',
            old_name='deriver',
            new_name='driver',
        ),
    ]
