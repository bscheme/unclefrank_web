# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import driver.models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Driver',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_at', models.DateTimeField(auto_now=True, null=True)),
                ('status', models.BooleanField(default=True)),
                ('first_name', models.CharField(default=None, max_length=255)),
                ('last_name', models.CharField(default=None, max_length=255)),
                ('email', models.CharField(max_length=255)),
                ('address', models.TextField()),
                ('contact_no', models.CharField(max_length=20)),
                ('licence_no', models.CharField(max_length=50)),
                ('national_id', models.CharField(max_length=50)),
                ('vehicle_type', models.CharField(max_length=20)),
                ('active', models.BooleanField(default=False)),
                ('driving_licence', models.FileField(upload_to=driver.models.content_file_name)),
                ('slug', models.SlugField(default=None)),
                ('created_by', models.ForeignKey(related_name='+', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
                ('updated_by', models.ForeignKey(related_name='+', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
