# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('driver', '0023_driver_driver_no'),
    ]

    operations = [
        migrations.AlterField(
            model_name='driver',
            name='driver_category',
            field=models.CharField(default=b'NORMAL', max_length=100, choices=[(b'NORMAL', b'NORMAL'), (b'BRONZE', b'BRONZE'), (b'SILVER', b'SILVER'), (b'GOLD', b'GOLD'), (b'CORPORATE', b'CORPORATE')]),
        ),
    ]
