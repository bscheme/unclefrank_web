# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('driver', '0007_auto_20161003_1250'),
    ]

    operations = [
        migrations.AddField(
            model_name='driver',
            name='profile_image',
            field=models.ImageField(default=None, null=True, upload_to=b'driver/profile', blank=True),
        ),
    ]
