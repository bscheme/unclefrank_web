# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0022_drivercategory'),
        ('driver', '0016_auto_20161026_2020'),
    ]

    operations = [
        migrations.AddField(
            model_name='driver',
            name='driver_cateogory',
            field=models.ForeignKey(to='superadmin.DriverCategory', null=True),
        ),
    ]
