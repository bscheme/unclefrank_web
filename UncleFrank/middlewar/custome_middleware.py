from customer.models import Customer

class CustomerMiddleware(object):

    def __call__(self, request):
        # Code to be executed for each request before
        # the view (and later middleware) are called.

        response = self.get_response(request)

        # Code to be executed for each request/response after
        # the view is called.

        return response

    def process_request(self, request):
        try:
            customer = Customer.objects.get(user=request.user)
        except:
            customer = None
        request.customer = customer
        return 