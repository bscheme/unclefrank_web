from django.db import models
from base.models import BaseModel
from customer.models import Customer
from driver.models import Driver
import uuid
import datetime
from datetime import date

# Create your models here.

DELEVERY_STATUS = (
    ('pending', 'pending'),
    ('assigned', 'assigned'),
    ('taken', 'taken'),
    ('pickedup', 'pickedup'),
    ('completed', 'completed'),
    ('paid', 'paid'),
    ('cancelled', 'cancelled'))

PICKUP_STATUS = (
    ('pending', 'pending'),
    ('assigned', 'assigned'),
    ('taken', 'taken'),
    ('pickedup', 'pickedup'),
    ('completed', 'completed'),
    ('paid', 'paid'),
    ('cancelled', 'cancelled'))

CANCELLED_BY = (
    ('admin', 'admin'),
    ('customer', 'customer'),
    ('driver', 'driver'))


def get_customer_avarage_rainngs(customer):
    customer_ratings = CustomerRating.objects.filter(customer=customer)
    total_rating = 0
    if customer_ratings.count() > 0:
        for rate in customer_ratings:
            total_rating = total_rating + rate.rating
        return float(total_rating)/float(customer_ratings.count())
    else:
        return 0.0

def content_file_name(instance, filename):
    return '/'.join(['content', instance.user.username, filename])


class CartonType(models.Model):
    caption = models.CharField(max_length=255, default=None, null=True)

    def __unicode__(self):
        return self.caption


class ScheduleType(models.Model):
    caption = models.CharField(max_length=255, default=None, null=True)

    def __unicode__(self):
        return self.caption


class DayType(models.Model):
    caption = models.CharField(max_length=255, default=None, null=True)

    def __unicode__(self):
        return self.caption


class DeliveryService(BaseModel):
    tracking_code = models.UUIDField(default=uuid.uuid4, editable=False)
    customer = models.ForeignKey(Customer)
    collection_name = models.CharField(max_length=255, default=None)
    collection_address = models.TextField(null=True)
    collection_building = models.CharField(max_length=20, null=True)
    collection_contact_no = models.CharField(max_length=255, default=None)
    collection_remark = models.TextField(null=True)
    # collection_reff_no = models.CharField(max_length=255, default=None)
    delivery_name = models.CharField(max_length=255, default=None)
    delivery_address = models.TextField()
    delivery_building = models.CharField(max_length=20, null=True)
    delivery_contact_no = models.CharField(max_length=255, default=None)
    delevery_remarks = models.TextField(null=True)
    collection_date = models.DateField(blank=True)
    collection_time =  models.CharField(max_length=255, default=None)
    delevery_date = models.DateField(blank=True)
    delevery_time = models.CharField(max_length=255, default=None)
    delevery_status = models.CharField(choices=DELEVERY_STATUS, max_length=255, default="pending")
    signature = models.ImageField(upload_to='delevery/signature', blank=True, default=None, null=True)
    taken_by = models.ForeignKey(Driver, default=None, null=True)
    total_cost = models.DecimalField(max_digits=10, decimal_places=2,default=0.00)
    drivers_fee = models.DecimalField(max_digits=10, decimal_places=2, default=0.00)
    completion_note = models.TextField(default=None, null=True)
    cancell_note = models.TextField(default=None, null=True)
    job_start_time = models.DateTimeField(auto_now=False, default=None, null=True)
    job_complete_time = models.DateTimeField(auto_now=False, default=None, null=True)
    job_cancelled_time = models.DateTimeField(auto_now=False, default=None, null=True)
    distance = models.CharField(max_length=255, default=None, null=True)
    eta = models.CharField(max_length=255, default=None, null=True)
    rated = models.CharField(max_length=20, default=None)
    payment_status = models.BooleanField(default=False)
    admin_remarks = models.CharField(max_length=120, null=True)
    collection_unit_number = models.CharField(max_length=120, null=True)
    delivery_unit_number = models.CharField(max_length=120, null=True)
    collection_postal_code = models.CharField(max_length=120, null=True)
    delivery_postal_code = models.CharField(max_length=120, null=True)
    is_archived = models.BooleanField(default=False)
    order_no = models.CharField(max_length=20, default=None, null=True)
    is_corporate = models.BooleanField(default=False)
    auto_assigned = models.BooleanField(default=False)

    def save(self, *args, **kwargs):
        tatal = DeliveryService.objects.all()
        d = date.today()
        month = str(d.month)
        pid = str(tatal.count()+1)
        if self.customer.is_corporate:
            self.order_no = 'CCO'+month.zfill(2)+str(d.year)+pid.zfill(4)
        else:
            self.order_no = 'CO'+month.zfill(2)+str(d.year)+pid.zfill(4)
        super(DeliveryService, self).save(*args, **kwargs)

    def get_package(self):
        pagckes = Package.objects.filter(service=self)
        return pagckes
    def get_customer_name(self):
        customer = self.customer
        return customer.name
    def get_driver_name(self):
        driver = self.taken_by
        return driver.name

class PackageSize(models.Model):
    name = models.CharField(max_length=255, default=None)
    weight = models.IntegerField(default=0)
    length = models.IntegerField(default=0)
    width = models.IntegerField(default=0)
    height = models.IntegerField(default=0)
    start_weight = models.IntegerField(null=True)
    end_weight = models.IntegerField(null=True)

    def __unicode__(self):
        return self.name

    def get_package_detail(self):
        package_info = Pricing.objects.filter(package_size=self)
        return package_info




class Pricing(models.Model):
    schedule_type = models.CharField(max_length=255, default=None, null=True)
    package_size = models.ForeignKey(PackageSize, null=True)
    start_qty = models.IntegerField(null=True)
    end_qty = models.IntegerField(null=True)
    range_rate = models.DecimalField(max_digits=5, decimal_places=2, default=None, null=True)
    corp_range_rate = models.DecimalField(max_digits=5, decimal_places=2, default=None, null=True)
    base_rate = models.DecimalField(max_digits=5, decimal_places=2, default=None, null=True)
    corp_base_rate = models.DecimalField(max_digits=5, decimal_places=2, default=None, null=True)

    def __unicode__(self):
        return self.base_rate or u''




class VehicleType(BaseModel):
    caption = models.CharField(max_length=250)
    description = models.TextField(default=None, null=True)
    pic = models.ImageField(upload_to='sadmin/vehicle_edit/', blank=True, default=None, null=True)
    pic_active = models.ImageField(upload_to='sadmin/vehicle_edit/', blank=True, default=None, null=True)

    def __unicode__(self):
        return self.caption


class PickupService(BaseModel):
    customer = models.ForeignKey(Customer)
    pickup_address = models.TextField()
    dropoff_address = models.TextField()
    remarks = models.TextField()
    distance = models.TextField(null=True)
    pickedup_by = models.ForeignKey(Driver, default=None, null=True)
    vehicle_type = models.ForeignKey(VehicleType, default=None)
    pickup_status = models.CharField(choices=PICKUP_STATUS, max_length=255, default="pending")
    tracking_code = models.UUIDField(default=uuid.uuid4, editable=False)
    signature = models.ImageField(upload_to='pickup/signature', blank=True, default=None, null=True)
    total_cost = models.DecimalField(max_digits=10, decimal_places=2,default=0.00)
    completion_note = models.TextField(default=None, null=True)
    cancell_note = models.TextField(default=None, null=True)
    cancel_reason = models.TextField(default=None, null=True)
    job_start_time = models.DateTimeField(auto_now=False, default=None, null=True)
    job_complete_time = models.DateTimeField(auto_now=False, default=None, null=True)
    job_cancelled_time = models.DateTimeField(auto_now=False, default=None, null=True)
    distance = models.CharField(max_length=255, default=None, null=True)
    pickup_address_lat = models.CharField(max_length=255, default=None, null=True)
    pickup_address_lng = models.CharField(max_length=255, default=None, null=True)
    dropoff_address_lat = models.CharField(max_length=255, default=None, null=True)
    dropoff_address_lng = models.CharField(max_length=255, default=None, null=True)
    rated = models.CharField(max_length=20, default=None)
    admin_remarks = models.CharField(max_length=120, null=True)
    is_archived = models.BooleanField(default=False)
    order_no = models.CharField(max_length=20, default=None, null=True)

    def save(self, *args, **kwargs):
        tatal = PickupService.objects.all()
        d = date.today()
        month = str(d.month)
        pid = str(tatal.count()+1)
        self.order_no = 'PO'+month.zfill(2)+str(d.year)+pid.zfill(4)
        super(PickupService, self).save(*args, **kwargs)

    def get_admin_message(self):
        message = AdminMessage.objects.filter(message_to_customer=self.customer, pickup=self)
        if message is not None:
            return message
        else:
            return None

    def get_driver(self):
        if self.pickedup_by is not None:
            driver = Driver.objects.get(id=self.pickedup_by.id)
            return driver
        else:
            return None


    def get_customer_name(self):
        customer = self.customer
        return customer.name

    def get_driver_name(self):
        driver = self.pickedup_by
        return driver.name

    def get_image_url(self):
        # return "http://localhost:8000/apis/vehicle/image/"+str(self.vehicle_type.id)+"/"
        return "/media/"+str(self.vehicle_type.pic)

    def get_vehicle_caption(self):
        return self.vehicle_type.caption

    def get_estmited_cost(self):
        try:
            distance_array = self.distance.split()
            distance = float(distance_array[0])
            datype = "Weekday"
            today = datetime.datetime.today()
            if today.weekday() == 6:
                datype = "Weekend"
            elif HolidayList.objects.filter(date=today).count() > 0:
                datype = "Weekend"
            price = PickUpRate.objects.filter(vehicle_type=self.vehicle_type, day_type=datype)
            return float(price[0].base_fare) + distance * float(price[0].rate)
        except:
            return 0

    def get_customer_ratings(self):
        return get_customer_avarage_rainngs(self.customer)

    def get_customer_phone(self):
        return self.customer.contact_no

class ActivityLog(BaseModel):
    date_time = models.DateTimeField(auto_now=True)
    delevery_status = models.CharField(choices=DELEVERY_STATUS, max_length=255, default="pending")
    pickup_status = models.CharField(choices=PICKUP_STATUS, max_length=255, default="pending")
    delevery_service = models.ForeignKey(DeliveryService, default=None, null=True)
    pickup_service = models.ForeignKey(PickupService, default=None, null=True)
    log_message = models.TextField()
    logger_name = models.CharField(max_length=255, default=None, null=True)
    log_type = models.CharField(max_length=120, default=None, null=True)
    lat = models.CharField(max_length=255, default=None, null=True)
    lng = models.CharField(max_length=255, default=None, null=True)


class Package(BaseModel):
    service = models.ForeignKey(DeliveryService)
    size = models.ForeignKey(PackageSize, null=True, default=None)
    quantity = models.IntegerField()
    unit_price = models.DecimalField(max_digits=20, decimal_places=2, default=None, null=True)
    image = models.FileField(upload_to=content_file_name)
    weight = models.IntegerField()
    length = models.IntegerField()
    width = models.IntegerField()
    height = models.IntegerField()
    confirm_pickup = models.BooleanField(default=False)
    confirm_delevery = models.BooleanField(default=False)


class Tracking(BaseModel):
    trip = models.ForeignKey(PickupService)
    data_id = models.IntegerField(default=0)
    lat = models.CharField(max_length=255, default=None)
    lng = models.CharField(max_length=255, default=None)
    datetime = models.DateTimeField(null=True)
    zipcode = models.CharField(max_length=6, null=True)


class HolidayList(models.Model):
    date = models.DateField(null=True)
    caption = models.CharField(max_length=500, null=True)

    def __unicode__(self):
        return self.caption


class PickupSurgeCharge(models.Model):
    postal_code = models.CharField(max_length=500, null=True)
    charge = models.DecimalField(max_digits=10, decimal_places=2, default=0.00)
    corp_charge = models.DecimalField(max_digits=10, decimal_places=2, default=0.00)
    description = models.CharField(max_length=500, null=True, default=None)

    def __unicode__(self):
        return self.postal_code

class CourierSurgeCharge(models.Model):
    postal_code = models.CharField(max_length=500, null=True)
    charge = models.DecimalField(max_digits=10, decimal_places=2, default=0.00)
    corp_charge = models.DecimalField(max_digits=10, decimal_places=2, default=0.00)
    description = models.CharField(max_length=500, null=True, default=None)

    def __unicode__(self):
        return self.postal_code

class PickupTimeCharge(models.Model):
    postal_code = models.CharField(max_length=120, null=True)
    start_time = models.TimeField(null=True, default=None)
    end_time = models.TimeField(null=True, default=None)
    price = models.DecimalField(max_digits=7, decimal_places=2, default=0.00)

class CourierTimeCharge(models.Model):
    postal_code = models.CharField(max_length=120, null=True)
    start_time = models.TimeField(null=True, default=None)
    end_time = models.TimeField(null=True, default=None)
    price = models.DecimalField(max_digits=7, decimal_places=2, default=0.00)


class PickUpRate(models.Model):
    vehicle_type = models.ForeignKey(VehicleType)
    min_fare = models.DecimalField(max_digits=10, decimal_places=2, default=0.00)
    base_fare = models.DecimalField(max_digits=10, decimal_places=2, default=0.00)
    rate = models.DecimalField(max_digits=10, decimal_places=2, default=0.00)
    waiting_charge = models.DecimalField(max_digits=5, decimal_places=2, default=0.00)
    peakMultiplier = models.DecimalField(max_digits=5, decimal_places=2, default=0.00)
    gst = models.DecimalField(max_digits=5, decimal_places=2, default=0.00)
    # Weekday or Weekend
    day_type = models.CharField(max_length=50, null=True)

    def get_vehicle_name(self):
        return self.vehicle_type.caption

    def get_vehicle_description(self):
        return self.vehicle_type.description


class CustomerRating(models.Model):
    customer = models.ForeignKey(Customer)
    rated_by = models.ForeignKey(Driver)
    rating = models.IntegerField()
    comment = models.CharField(max_length=220, null=True)
    pickup = models.ForeignKey(PickupService, null=True)
    delivery = models.ForeignKey(DeliveryService, null=True)


class DriverRating(models.Model):
    driver = models.ForeignKey(Driver)
    rated_by = models.ForeignKey(Customer)
    rating = models.DecimalField(decimal_places=2, max_digits=3, null=True)
    comment = models.CharField(max_length=220, null=True)
    pickup = models.ForeignKey(PickupService, null=True)
    delivery = models.ForeignKey(DeliveryService, null=True)


class CancellationInfo(models.Model):
    pickup_job = models.ForeignKey(PickupService, null=True)
    delivery_job = models.ForeignKey(DeliveryService, null=True)
    driver = models.ForeignKey(Driver)
    customer = models.ForeignKey(Customer)
    cancelled_by = models.CharField(max_length=120, null=True, choices=CANCELLED_BY)


class AdminMessage(models.Model):
    message_to_customer = models.ForeignKey(Customer, null=True)
    message_to_driver = models.ForeignKey(Driver, null=True)
    message_body = models.CharField(max_length=600, null=True)
    delivery = models.ForeignKey(DeliveryService, null=True)
    pickup = models.ForeignKey(PickupService, null=True)
    created_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)


JOB_TYPE = (
    ('pickup', 'pickup'),
    ('delevery', 'delevery'))

class Payment(BaseModel):
    customer = models.ForeignKey(Customer)
    delevery_service = models.ForeignKey(DeliveryService, default=None, null=True)
    pickup_service = models.ForeignKey(PickupService, default=None, null=True)
    date_time = models.DateTimeField(auto_now=True)
    total_cost = models.DecimalField(max_digits=10, decimal_places=2, default=0.00)
    coupon_amount = models.DecimalField(max_digits=10, decimal_places=2, default=0.00)
    coupon_code = models.CharField(max_length=10, null=True, default=None)
    paid_amount = models.DecimalField(max_digits=10, decimal_places=2, default=0.00)
    online_fee = models.DecimalField(max_digits=10, decimal_places=2, default=0.00)
    toll_fee = models.DecimalField(max_digits=10, decimal_places=2, default=0.00)
    admin_fee = models.DecimalField(max_digits=10, decimal_places=2, default=0.00)
    car_fee = models.DecimalField(max_digits=10, decimal_places=2, default=0.00)
    payment_status = models.CharField(max_length=10, default='UNPAID')
    job_type = models.CharField(max_length=100, null=True, choices=JOB_TYPE)

JOB_ACTION = (
    ('view', 'view'),
    ('accept', 'accept'),
    ('cancell', 'cancell'))

class PickupJobLogDriver(models.Model):
    pickup = models.ForeignKey(PickupService)
    driver = models.ForeignKey(Driver)
    action = models.CharField(max_length=100, null=True, choices=JOB_ACTION)
    date = models.DateField(auto_now=True)

class PickupGST(models.Model):
    name = models.CharField(max_length=120, null=True)
    percentage = models.BooleanField(default=False)
    price = models.DecimalField(max_digits=10, decimal_places=2, default=0.00)

class CourierGST(models.Model):
    name = models.CharField(max_length=120, null=True)
    percentage = models.BooleanField(default=False)
    price = models.DecimalField(max_digits=10, decimal_places=2, default=0.00)

class CorporateDriverCustomer(models.Model):
    driver = models.ForeignKey(Driver)
    customer = models.ForeignKey(Customer)

class FavoriteDriverCustomer(models.Model):
    driver = models.ForeignKey(Driver)
    customer = models.ForeignKey(Customer)

class PickupCancellationReasons(models.Model):
    image = models.ImageField(upload_to='pickup/cancellation', blank=True, default=None, null=True)
    image_active = models.ImageField(upload_to='pickup/cancellation', blank=True, default=None, null=True)
    reason = models.CharField(max_length=120, null=True)