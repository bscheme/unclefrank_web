from __future__ import division
from django.shortcuts import render, render_to_response
from django.core.context_processors import csrf
from django.http import HttpResponse, HttpResponseRedirect
from models import Customer
from django.contrib import messages
from django.template import RequestContext
from django.contrib.auth.models import User
from django.views.generic.detail import DetailView
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from datetime import datetime, timedelta
from cart.cart import Cart
from .models import Payment, DeliveryService, Package, PackageSize, PickupService, VehicleType, ActivityLog, CartonType, \
    ScheduleType, DayType, Pricing, PickUpRate, HolidayList, DriverRating, AdminMessage, Tracking, CorporateDriverCustomer, FavoriteDriverCustomer, \
    PickupCancellationReasons
from superadmin.models import ExpressWindows, DeliveryTiming, SiteConfigeration
import googlemaps
import csv
from driver.models import Driver, Vechile
from operator import attrgetter
from django.contrib.auth.decorators import login_required
from base.custome_decorator import customer_only
from customer.models import CreditCard
from base import constant
import stripe
import os
from easy_pdf.views import PDFTemplateView
from django.conf import settings
from base.utils import send_email_notification
from apis.views import send_push_message, send_push_message_for_pickeup_job_post
from django.views.generic import TemplateView
from payment.payment import Payment as Pay
from payment.pricecalculation import PriceCalculation
from address.models import Street, Postalcode
from dispatch.dispatch import Dispatch
# from dispatch import dispatch as dis
from dispatch import tasks
from cart.models import Item

from django.db.models import Q

from django.utils import timezone

# Create your views here.
# Check if the courier booking avaiable for a perticular slote


@csrf_exempt
@login_required
@customer_only
def passengerPickup(request):
    vehicles = VehicleType.objects.all()
    arg = {
        'vehicles': vehicles,
    }
    cards = CreditCard.objects.filter(custmer=request.customer)
    if cards.count() < 1:
            # send_push_message(None, "New Job", "New courier Job Posted", 1)
            return HttpResponseRedirect('/customer/payment/')
    if request.method == 'POST':
        try:
            pickupAddress = request.POST['pickupAddress']
            dropOffAddess = request.POST['dropOffAddess']
            noteForDriver = request.POST['noteForDriver']
            vehicleType = request.POST['vehicleType']
            customer = request.customer
            d = Dispatch(request)
            pickup = d.create_pick_job(pickupAddress, dropOffAddess, noteForDriver, vehicleType, customer)
            tasks.dispatch_job_to_nearest_driver.delay(pickup.id)
            # 9dis.dispatch_job_to_nearest_driver(request, pickup)
            arg['pickup_id'] = pickup.id
            arg['pickup'] = pickup
            arg['pickupAddress'] = request.POST['pickupAddress']
            arg['dropOffAddess'] = request.POST['dropOffAddess']
            arg['base_fare'] = request.POST['base_fare']
            arg['rate'] = request.POST['rate']
            arg['reasons'] = PickupCancellationReasons.objects.all()
            arg['est_cost'] = pickup.get_estmited_cost()
            return render(request, 'passengerPickup/pickupService2.html', arg)
        except:
            arg['error'] = "Invalid address input"
            arg['pickupAddress'] = request.POST['pickupAddress']
            arg['dropOffAddess'] = request.POST['dropOffAddess']
    arg['book_service_active'] = True
    return render(request, 'passengerPickup/pickupService.html', arg)


def passengerPickupFinish(request, book_id):
    pickup = PickupService.objects.get(id=book_id)
    est_cost = pickup.get_estmited_cost()
    try:
        ratings = DriverRating.objects.filter(driver=pickup.pickedup_by.id)
        driver_vehicle = Vechile.objects.get(driver=pickup.pickedup_by.id)
        vehicle = VehicleType.objects.get(id=driver_vehicle.vechile_type)
        total_rating = 0
        if ratings.count() > 0:
            for rating in ratings:
                total_rating = total_rating + int(rating.rating)
            avg_rate = total_rating / ratings.count()
        else:
            avg_rate = 0
    except:
        rating = None
        driver_vehicle = None
        vehicle = None
        avg_rate = None

    if pickup.pickup_status != 'cancelled':
        daytype = "Weekday"
        today = datetime.today()
        if today.weekday() == 6:
            daytype = "Weekend"
        elif HolidayList.objects.filter(date=today).count() > 0:
            daytype = "Weekend"
        price = PickUpRate.objects.filter(vehicle_type=pickup.vehicle_type, day_type=daytype)
        arg = {'pickup': pickup,
               'price': price[0],
               'rating': avg_rate,
               'vehicle': vehicle,
               'est_cost': est_cost
               }
        arg['book_service_active'] = True
        if request.method == 'POST':
            reason = request.POST['cancel-radio']
            note = request.POST['comments']
            id = request.POST['pickup_id']
            pickup = PickupService.objects.get(id=id)
            pickup.pickup_status = 'cancelled'
            pickup.cancell_note = note
            pickup.cancel_reason = reason
            pickup.job_cancelled_time = timezone.now()
            pickup.save()
            messages.error(request, 'Your pickup request has been cancelled.')
            canuclation  = PriceCalculation(request)
            cancellation_fees = canuclation.get_cancelation_fees_for_pickup(pickup)
            if cancellation_fees > 0 :
                return HttpResponseRedirect('/service/payment/pickup/' + str(pickup.id) + '/' + "None" + '/')
            return redirect('/service/myBookings/?tab=pickup')
        return render(request, 'passengerPickup/pickupService3.html', arg)
    else:
        return redirect('/customer/profile/')


# Create your views here.
@login_required
@customer_only
def courier_booking_one(request):
    arg = {}
    arg['book_service_active'] = True
    context = RequestContext(request)
    arg.update(csrf(request))
    try:
        arg['collectionAddress'] = request.session['collection-address']
        arg['collectionName'] = request.session['collection-name']
        arg['collectionContactNumber'] = request.session['collection-contact-number']
        arg['collectionRemarks'] = request.session['collection-remarks']
        arg['collectionUnitNumber'] = request.session['collection-unit-number']
        arg['collectionPostalCode'] = request.session['collection-postal-code']
        arg['deliveryAddress'] = request.session['delivery-address']
        arg['deliveryName'] = request.session['delivery-name']
        arg['deliveryContactNumber'] = request.session['delivery-contact-number']
        arg['deliveryRemarks'] = request.session['delivery-remarks']
        arg['deliveryUnitNumber'] = request.session['delivery-unit-number']
        arg['deliveryPostalCode'] = request.session['delivery-postal-code']
    except:
        pass
    if request.method == 'POST':
        request.session['collection-address'] = request.POST['collection_address']
        request.session['collection-name'] = request.POST['collection_name']
        if request.POST['collection_building']:
            request.session['collection-building'] = request.POST['collection_building']
        else:
            request.session['collection-building'] = None
        if request.POST['delivery_building']:
            request.session['delivery-building'] = request.POST['delivery_building']
        else:
            request.session['delivery-building'] = None
        request.session['collection-contact-number'] = request.POST['collection_contact_number']
        request.session['collection-remarks'] = request.POST['collection-remarks']
        request.session['delivery-address'] = request.POST['delivery_address']
        request.session['delivery-name'] = request.POST['delivery_name']
        request.session['delivery-contact-number'] = request.POST['delivery_contact_number']
        request.session['delivery-remarks'] = request.POST['delivery-remarks']
        request.session['completed-form'] = '1'
        request.session['collection-unit-number'] = request.POST['collection_unit_number']
        request.session['collection-postal-code'] = request.POST['collection_postal_code']
        request.session['delivery-unit-number'] = request.POST['delivery_unit_number']
        request.session['delivery-postal-code'] = request.POST['delivery_postal_code']
        try:
            gmaps = googlemaps.Client(key='AIzaSyD4qpAFSCX_E_hTOkpw0t53Dv3ULOQ2lw4')

            directions_results = gmaps.distance_matrix(
                request.POST['collection_address'],
                request.POST['delivery_address'],
                mode=None,
                language=None,
                avoid=None,
                units="metric",
                departure_time=None,
                arrival_time=None,
                transit_mode=None,
                transit_routing_preference=None,
                traffic_model=None
            )

            approx_distance = str(directions_results['rows'][0]['elements'][0]['distance']['text'])
            approx_time = str(directions_results['rows'][0]['elements'][0]['duration']['text'])
            request.session['eta'] = approx_time
            request.session['distance'] = approx_distance
        except Exception, e:
            arg['error'] = "Unable to calculate distance based on your provided address."
            return render_to_response('service/courier-service-step-1.html', arg, context)

        return HttpResponseRedirect('/service/courier_booking_two/')

    return render_to_response('service/courier-service-step-1.html', arg, context)


@login_required
@customer_only
def courier_booking_two(request):
    try:
        if request.session['completed-form'] != '1' and request.session['completed-form'] != '2' and request.session[
            'completed-form'] != '3' and request.session['completed-form'] != '4':
            return HttpResponseRedirect('/service/courier_booking_one/')
    except:
        return HttpResponseRedirect('/service/courier_booking_one/')
    arg = {}
    arg['book_service_active'] = True
    arg.update(csrf(request))
    context = RequestContext(request)
    cart = Cart(request)
    items = cart.get_packages()
    arg['items'] = items
    sizes = PackageSize.objects.all()
    arg['sizes'] = sizes
    arg['counter'] = 0
    arg['counter_two'] = 1
    if request.method == 'POST':
        request.session['completed-form'] = '2'
        return HttpResponseRedirect('/service/courier_booking_three/')
    return render_to_response('service/courier-service-step-2.html', arg, context)


@login_required
@customer_only
def courier_booking_three(request):
    arg = {}
    express = ExpressWindows.objects.all()
    timing = DeliveryTiming.objects.all()
    arg['express'] = express
    arg['timing'] = timing
    arg['book_service_active'] = True
    try:
        arg['cellectionDate'] = request.session['cellection_date']
        arg['cellectionTime'] = request.session['cellection-time']
        arg['deliveryDate'] = request.session['delivery_date']
        arg['deliveryTime'] = request.session['delivery-time']
    except:
        pass
    try:
        if request.session['completed-form'] != '2' and request.session['completed-form'] != '3' and request.session['completed-form'] != '4':
            return HttpResponseRedirect('/service/courier_booking_two/')
    except:
        return HttpResponseRedirect('/service/courier_booking_two/')
    arg['collectionAddress'] = request.session['collection-address']
    arg['collectionName'] = request.session['collection-name']
    arg['collectionContactNumber'] = request.session['collection-contact-number']
    arg['collectionRemarks'] = request.session['collection-remarks']
    arg['collectionBuilding'] = request.session['collection-building']
    arg['deliveryBuilding'] = request.session['delivery-building']
    arg['deliveryAddress'] = request.session['delivery-address']
    arg['deliveryName'] = request.session['delivery-name']
    arg['deliveryContactNumber'] = request.session['delivery-contact-number']
    arg['deliveryRemarks'] = request.session['delivery-remarks']
    arg['collectionUnitNumber'] = request.session['collection-unit-number']
    arg['collectionPostalCode'] = request.session['collection-postal-code']
    arg['deliveryUnitNumber'] = request.session['delivery-unit-number']
    arg['deliveryPostalCode'] = request.session['delivery-postal-code']
    arg.update(csrf(request))
    context = RequestContext(request)
    if request.method == 'POST':
        pricing = PriceCalculation(request)
        if not pricing.if_job_available_for_delivery_winodw(datetime.strptime(request.POST['delivery_date'], '%d %b %Y').date(), request.POST['delivery-time']) :
            messages.error(request, 'Maximum order limit for this window excideed')
            return HttpResponseRedirect('/service/courier_booking_three/')
        #check if the collection window is same
        if request.POST['cellection_date'] == request.POST['delivery_date'] and request.POST['cellection-time'] == request.POST['delivery-time']:
            messages.error(request, 'Delivery order date and window cannot be same as collection')
            return HttpResponseRedirect('/service/courier_booking_three/')
        request.session['completed-form'] = '3'
        request.session['cellection_date'] = request.POST['cellection_date']
        request.session['cellection-time'] = request.POST['cellection-time']
        request.session['delivery_date'] = request.POST['delivery_date']
        request.session['delivery-time'] = request.POST['delivery-time']
        request.session['promo_code'] = request.POST['promo_code']
        return HttpResponseRedirect('/service/courier_booking_four/')
    return render_to_response('service/courier-service-step-3.html', arg, context)


@login_required
@customer_only
def courier_booking_four(request):
    try:
        if request.session['completed-form'] != '3' and request.session['completed-form'] != '4':
            return HttpResponseRedirect('/service/courier_booking_three/')
    except:
        return HttpResponseRedirect('/service/courier_booking_three/')
    arg = {}
    arg['book_service_active'] = True
    arg['collectionAddress'] = request.session['collection-address']
    arg['collectionName'] = request.session['collection-name']
    arg['collectionContactNumber'] = request.session['collection-contact-number']
    arg['collectionRemarks'] = request.session['collection-remarks']
    arg['collectionBuilding'] = request.session['collection-building']
    arg['deliveryBuilding'] = request.session['delivery-building']
    arg['deliveryAddress'] = request.session['delivery-address']
    arg['deliveryName'] = request.session['delivery-name']
    arg['deliveryContactNumber'] = request.session['delivery-contact-number']
    arg['deliveryRemarks'] = request.session['delivery-remarks']
    arg['cellectionDate'] = request.session['cellection_date']
    arg['cellectionTime'] = request.session['cellection-time']
    arg['deliveryDate'] = request.session['delivery_date']
    arg['deliveryTime'] = request.session['delivery-time']
    arg['collectionUnitNumber'] = request.session['collection-unit-number']
    arg['collectionPostalCode'] = request.session['collection-postal-code']
    arg['deliveryUnitNumber'] = request.session['delivery-unit-number']
    arg['deliveryPostalCode'] = request.session['delivery-postal-code']
    # Get discount for promocode
    promo_code = request.session['promo_code']
    arg.update(csrf(request))
    context = RequestContext(request)
    cart = Cart(request)
    items = cart.get_packages()

    ############# PRICING STARTS HERE ###############

    scheType = ""
    totalPrice = 0
    period = cart.get_time_difference(request.session['delivery_date'], request.session['cellection_date'])

    if period == 0:
        scheType = "Same Day"
    elif period == 1:
        scheType = "Next Day"
    else:
        scheType = "Two Working Days"


    sortedItems = sorted(items, key=attrgetter('size.start_weight'), reverse=True)

    itemCounter = 1
    for myItem in sortedItems:
        for pkg in xrange(0, myItem.quantity):
            try:
                if str.lower(str(myItem.size.name)) == 'custom':
                    package_cheat = PackageSize.objects.get(start_weight__lte=myItem.weight , end_weight__gte=myItem.weight)
                    relevantPrices = Pricing.objects.filter(schedule_type=scheType,
                                                              package_size=package_cheat,
                                                              start_qty__lte=itemCounter,
                                                              end_qty__gte=itemCounter)
                    myItem.unit_price = myItem.unit_price + relevantPrices[0].range_rate
                    itemCounter = itemCounter + 1
                else:
                    relevantPrices = Pricing.objects.filter(schedule_type=scheType,
                                                            package_size=myItem.size,
                                                            start_qty__lte=itemCounter,
                                                            end_qty__gte=itemCounter)
                    myItem.unit_price = myItem.unit_price + relevantPrices[0].range_rate
                    itemCounter = itemCounter + 1
            except:
                myItem.unit_price = myItem.unit_price + 10
                itemCounter = itemCounter + 1
        totalPrice = totalPrice + myItem.unit_price
    sortedItems = sorted(items, key=attrgetter('weight'), reverse=True)
    
    try:
        express = ExpressWindows.objects.get(window_name=request.session['delivery-time'])
        arg['express'] = express
        if express.multiple:
            arg['totalPrice'] = totalPrice * int(express.price)
            arg['express_price'] = totalPrice * int(express.price) - totalPrice
        else:
            arg['totalPrice'] = totalPrice + int(express.price)
            arg['express_price'] = int(express.price)
    except:
        arg['totalPrice'] = totalPrice
    payment = Pay(request)
    arg['anount_after_discount'] = arg['totalPrice']
    if payment.is_coupne_available_for_delivery_window(promo_code, request.session['delivery-time']):
        arg['anount_after_discount'] = payment.promo_code_discount(promo_code, arg['totalPrice'])
    arg['discount'] = arg['totalPrice'] - arg['anount_after_discount']
    # arg['error'] = "Invalid promo code"
    if arg['discount'] == 0 and not not promo_code:
        arg['error'] = "Invalid promo code"



    totalPriceWithExtra = int(totalPrice * 100)

    arg['items'] = sortedItems
    # request.session['items'] = arg['items']
    # print arg['items']
    arg['totalPriceWithExtra'] = totalPriceWithExtra
    arg['distance'] = request.session['distance']
    request.session['total_price'] = str(arg['totalPrice'])

    if request.method == 'POST':
        cards = CreditCard.objects.filter(custmer=request.customer)
        request.session['completed-form'] = '4'
        if request.customer.is_corporate:
            corporate = CorporateDriverCustomer.objects.filter(customer=request.customer)
            if corporate.count() > 0:
                # print corporate[0].driver.name
                dispatch = Dispatch(request)
                delivery_id = dispatch.dispatch_courier_job_corporate_with_driver(arg['items'])
                session_clear(request)
                if promo_code == '':
                    promo_code = 'False'
                return HttpResponseRedirect('/service/payment/courier/' + str(delivery_id) + '/' + promo_code + '/')
            else:
                dispatch = Dispatch(request)
                delivery_id = dispatch.dispatch_courier_job_corporate_without_driver(arg['items'])
                session_clear(request)
                if promo_code == '':
                    promo_code = 'False'
                return HttpResponseRedirect('/service/payment/courier/' + str(delivery_id) + '/' + promo_code + '/')
        else:
            dispatch = Dispatch(request)
            delivery_id = dispatch.dispatch_courier_job_normal(arg['items'])
            session_clear(request)
            if promo_code == '':
                promo_code = 'False'
            return HttpResponseRedirect('/service/payment/courier/' + str(delivery_id) + '/' + promo_code + '/')
    return render(request, 'service/courier-service-step-4.html', arg, context)


def test(request):
    arg = {}
    context = RequestContext(request)
    cart = Cart(request)
    items = cart.get_packages
    arg['items'] = items
    return render_to_response('service/courier-service.html', arg, context)


def add_package(request):
    cart = Cart(request)
    try:
        size = PackageSize.objects.all()
        item_id = cart.add_package(size[0])
        return JsonResponse({'status': 'success', 'id': str(item_id)})
    except:
        return JsonResponse({'status': 'failure'})


@csrf_exempt
def remove_item(request):
    arg = {}
    arg['status'] = 'falure'
    if request.method == 'POST':
        item_id = request.POST['id']
        cart = Cart(request)
        if cart.remove_package(item_id):
            arg['status'] = 'success'
    return JsonResponse(arg)


@csrf_exempt
def change_image(request):
    if request.method == 'POST':
        item_id = request.POST['id'];
        image = request.FILES['file']
        cart = Cart(request)
        cart.change_image(item_id, image)
        return JsonResponse({'status': 'success'})
    else:
        return JsonResponse({'status': 'failure'})


@csrf_exempt
def change_quantity(request):
    if request.method == 'POST':
        item_id = request.POST['id'];
        quantity = request.POST['quantity']
        cart = Cart(request)
        cart.change_quantity(item_id, quantity)
        return JsonResponse({'status': 'success'})
    else:
        return JsonResponse({'status': 'failure'})


@csrf_exempt
def change_size(request):
    arg = {}
    arg['status'] = 'failure'
    if request.method == 'POST':
        item_id = request.POST['id']
        size_id = request.POST['size_id']
        size = PackageSize.objects.get(id=size_id)
        if size.name == 'Custom':
            cart = Cart(request)
            cart.change_size_dimension(item_id, size)
            arg['status'] = 'success'
        else:
            cart = Cart(request)
            cart.change_size(item_id, size)
            arg['status'] = 'success'
    return JsonResponse(arg)


@csrf_exempt
def change_dimention(request):
    arg = {}
    arg['status'] = 'failure'
    if request.method == 'POST':
        item_id = request.POST['id']
        dimention_type = request.POST['type']
        dimention_value = request.POST['value']
        print dimention_value

        cart = Cart(request)
        if dimention_value:
            if dimention_type == 'height':
                try:
                    item = Item.objects.get(id=item_id)
                    item.height = dimention_value
                    result = round((int(item.length) * int(item.width) * int(dimention_value)) / 5000)
                    print result
                    if result == 0:
                        item.weight = 1
                    else:
                        item.weight = result
                    item.save()
                    # if cart.change_height(item_id, dimention_value):
                    #     arg['status'] = 'success'
                    #     print 'height ' + dimention_value
                    # else:
                    #     pass
                except Exception, e:
                    print e

            if dimention_type == 'weight':
                try:
                    item = Item.objects.get(id=item_id)
                    item.weight = dimention_value
                    item.save()
                    # if cart.change_weight(item_id, dimention_value):
                    #     arg['status'] = 'success'
                    #     print 'weight ' + dimention_value
                    # else:
                    #     pass
                except Exception, e:
                    print e

            if dimention_type == 'length':
                try:
                    # item = Item.objects.get(id=item_id)
                    # item.length = dimention_value
                    # item.save()
                    item = Item.objects.get(id=item_id)
                    item.length = dimention_value
                    result = round((int(item.height) * int(item.width) * int(dimention_value)) / 5000)
                    print result
                    if result == 0:
                        item.weight = 1
                    else:
                        item.weight = result
                    # item.weight = int(round(int((dimention_value) * int(item.width) * int(item.length)) / 5000))
                    item.save()
                    # if cart.change_length(item_id, dimention_value):
                    #     arg['status'] = 'success'
                    #     print 'length ' + dimention_value
                    # else:
                    #     pass
                except Exception, e:
                    print e

            if dimention_type == 'width':
                try:
                    # item = Item.objects.get(id=item_id)
                    # item.width = dimention_value
                    # item.save()
                    item = Item.objects.get(id=item_id)
                    item.width = dimention_value
                    result = round((int(item.height) * int(item.length) * int(dimention_value)) / 5000)
                    print result
                    if result == 0:
                        item.weight = 1
                    else:
                        item.weight = result
                    # item.weight = int(round(int((item.height) * int(dimention_value) * int(item.length)) / 5000))
                    item.save()
                    # if cart.change_weidth(item_id, dimention_value):
                    #     arg['status'] = 'success'
                    #     print 'width ' + dimention_value
                    # else:
                    #     pass

                except Exception, e:
                    print e

        else:
            if dimention_type == 'height':
                if cart.change_height(item_id, 1):
                    arg['status'] = 'success'
            elif dimention_type == 'weight':
                if cart.change_weight(item_id, 1):
                    arg['status'] = 'success'
            elif dimention_type == 'length':
                if cart.change_length(item_id, 1):
                    arg['status'] = 'success'
            elif dimention_type == 'width':
                if cart.change_weidth(item_id, 1):
                    arg['status'] = 'success'
    return JsonResponse(arg)


@login_required
@customer_only
def my_bookings(request):
    all_pickups = PickupService.objects.filter(customer=request.customer, status=True).order_by('-id')
    # all_deliveries = DeliveryService.objects.filter(Q(delevery_date__gte=datetime.now()-timedelta(days=1)) & Q(customer=request.customer) & Q(status=True)).order_by('-id')
    all_deliveries = DeliveryService.objects.filter(customer=request.customer).order_by('-id')
    closed_deliveries = all_deliveries.filter(delevery_status="completed", status=True).order_by('-id')
    current_deliveries = all_deliveries.filter(delevery_status="pending", status=True, delevery_date__gte=datetime.now()-timedelta(days=0)).order_by('-id')
    closed_pickups = all_pickups.filter(pickup_status="completed", status=True).order_by('-id')
    current_pickups = all_pickups.filter(pickup_status="pending", status=True).order_by('-id')
    ratings = DriverRating.objects.all()
    context = {
        "all_pickups": all_pickups,
        "all_deliveries": all_deliveries,
        'booking_active': True,
        "closed_deliveries": closed_deliveries,
        "current_deliveries": current_deliveries,
        "closed_pickups": closed_pickups,
        "current_pickups": current_pickups,
        "ratings": ratings,
    }
    return render(request, 'service/my-bookings.html', context)


@login_required
@customer_only
def my_booking_details_courier(request, book_id):
    arg = {}
    arg['booking_active'] = True
    try:
        booking = DeliveryService.objects.get(id=book_id, customer=request.customer)
        try:
            driver = Driver.objects.get(id=booking.taken_by.id)
            ratings = DriverRating.objects.filter(driver=driver.id)
            if ratings.count() > 0:
                total_rate = 0
                for rating in ratings:
                    total_rate = total_rate + int(rating.rating)
                avg_rating = total_rate / ratings.count()
            else:
                avg_rating = 0
            driver.rating = avg_rating
            arg['driver'] = driver
            if not driver:
                # print "There is a driver"
                print "no driver"
                arg['favorite'] = False
            else:
                if request.customer.is_corporate:
                    try:
                        favorite = CorporateDriverCustomer.objects.filter(driver=driver, customer=request.customer)
                        if favorite.count() == 0:
                            arg['favorite'] = False
                        else:
                            arg['favorite'] = True
                    except:
                        arg['favorite'] = False
                else:
                    try:
                        favorite = FavoriteDriverCustomer.objects.filter(driver=driver, customer=request.customer)
                        if favorite.count() == 0:
                            arg['favorite'] = False
                        else:
                            arg['favorite'] = True
                    except:
                        arg['favorite'] = False
        except:
            arg['favorite'] = False

        arg['booking'] = booking
        arg['packages'] = Package.objects.filter(service=booking)
        arg['logs'] = ActivityLog.objects.filter(delevery_service=booking)
        messages = AdminMessage.objects.filter(delivery=book_id, message_to_customer=booking.customer.id)
        arg['messages'] = messages
        try:
            arg['payment'] = Payment.objects.filter(delevery_service=booking)
        except:
            arg['payment'] = None
    except Exception, e:
        return HttpResponseRedirect('/service/myBookings/')
    return render(request, 'service/order-summary.html', arg)


@login_required
@customer_only
def my_booking_details_pickup(request, book_id):
    arg = {}
    arg['booking_active'] = True
    try:
        booking = PickupService.objects.get(id=book_id, customer=request.customer)
        arg['booking'] = booking
        arg['logs'] = ActivityLog.objects.filter(pickup_service=booking)
        messages = AdminMessage.objects.filter(pickup=book_id, message_to_customer=booking.customer.id)
        arg['messages'] = messages
        arg['tracking'] = Tracking.objects.filter(trip=booking)
        try:
            arg['payment'] = Payment.objects.filter(pickup_service=booking)
        except:
            arg['payment'] = None
    except:
        return HttpResponseRedirect('/service/myBookings/')

    return render(request, 'service/order-summary-pickup.html', arg)

@login_required
@customer_only
def courier_csv(request):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="somefilename.csv"'
    writer = csv.writer(response)
    booking = DeliveryService.objects.filter(customer=request.customer)
    writer.writerow(
        ['Collection Name', 'Collection Address', 'Collection Contact No', 'Delivery Name', 'Delivery Address',
         'Delivery Contact No', 'Collection Date', 'Collection Time', 'Delevery Date', 'Delevery Time',
         'Delevery Status'])
    for book in booking:
        writer.writerow([book.collection_name, book.collection_address, book.collection_contact_no, book.delivery_name,
                         book.delivery_address, book.delivery_contact_no, book.collection_date, book.collection_time,
                         book.delevery_date, book.delevery_time, book.delevery_status])
    return response


@login_required
@customer_only
def pickup_csv(request):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="somefilename.csv"'
    writer = csv.writer(response)
    booking = PickupService.objects.filter(customer=request.customer)
    writer.writerow(['Pickup Address', 'Dropoff Address', 'Distance'])
    for book in booking:
        writer.writerow([book.pickup_address, book.dropoff_address, book.distance])
    return response


def session_clear(request):
    del request.session['collection-address']
    del request.session['collection-name']
    del request.session['collection-contact-number']
    del request.session['collection-remarks']
    del request.session['delivery-address']
    del request.session['delivery-name']
    del request.session['delivery-contact-number']
    del request.session['delivery-remarks']
    del request.session['eta']
    del request.session['distance']
    del request.session['completed-form']
    del request.session['cellection-time']
    del request.session['cellection_date']
    del request.session['delivery-time']
    del request.session['delivery_date']


@login_required
@customer_only
def make_payment(request, job_type, job_id, promo_code):
    arg = {}
    if job_type == 'courier':
        arg['booking_type'] = "Courrier"
        courier = DeliveryService.objects.get(id=job_id)
        payment = Pay(request)
        arg['discount'] = payment.promo_code_discount(promo_code, courier.total_cost)
        arg['cards'] = payment.get_avaiable_cards(request.customer)
        arg['courier'] = courier
        if request.method == 'POST':
            if request.POST['addedCard'] == '':
                name = request.POST['name']
                card_no = request.POST['card_no']
                expiry_date = request.POST['expiry_date']
                date_list = expiry_date.split("/")
                cvv = request.POST['cvv']
                if promo_code == "" :
                    if payment.pay_for_courier_with_card(courier, courier.total_cost, card_no, cvv, date_list[0], date_list[1]):
                        courier.payment_status = True
                        courier.status = True
                        courier.save()
                        send_email_notification(request.user, "Your payment has been received.")
                    messages.success(request, 'Payment successful')
                    return redirect('/service/myBookings/')
                else:
                    if payment.pay_for_courier_with_card_promo(courier, courier.total_cost, promo_code, card_no, cvv, date_list[0], date_list[1]):
                        courier.payment_status = True
                        courier.status = True
                        courier.save()
                        send_email_notification(request.user, "Your payment has been received.")
                    messages.success(request, 'Payment successful')
                    return redirect('/service/myBookings/')
            else:
                card = CreditCard.objects.get(id=request.POST['addedCard'])
                if promo_code == "" :
                    if payment.pay_for_courier_for_saved_caed(card, courier.total_cost, courier):
                        courier.payment_status = True
                        courier.status = True
                        courier.save()
                        messages.success(request, 'Payment successful')
                        return redirect('/service/myBookings/')
                else:
                    if payment.pay_for_courier_for_saved_card_promo(card, promo_code, courier.total_cost, courier):
                        courier.payment_status = True
                        courier.status = True
                        courier.save()
                        messages.success(request, 'Payment successful')
                        return redirect('/service/myBookings/')
    elif job_type == 'pickup':
        arg['booking_type'] = "Pickup"
        pickup = PickupService.objects.get(id=job_id)
        payment = Pay(request)
        calculation =  PriceCalculation(request)
        arg['cancellation_fees'] = calculation.get_cancelation_fees_for_pickup(pickup)
        arg['cards'] = payment.get_avaiable_cards(request.customer)
        arg['job'] = pickup
        if request.method == 'POST':
            if request.POST['addedCard'] == '':
                name = request.POST['name']
                card_no = request.POST['card_no']
                expiry_date = request.POST['expiry_date']
                date_list = expiry_date.split("/")
                cvv = request.POST['cvv']
                if payment.pay_for_pickup_cancellation_fees_with_card(pickup, arg['cancellation_fees'], card_no, cvv, date_list[0], date_list[1]):
                    send_email_notification(request.user, "Your cancellation fees has been charged.")
                    messages.success(request, 'Payment successful')
                    return redirect('/service/myBookings/')
            else:
                card = CreditCard.objects.get(id=request.POST['addedCard'])
                if payment.pay_for_pickup_cancellation_fees_with_saved_card(pickup, card, arg['cancellation_fees']):
                    send_email_notification(request.user, "Your cancellation fees has been charged.")
                    messages.success(request, 'Payment successful')
                    return redirect('/service/myBookings/')
    return render(request, 'service/payment.html', arg)


@csrf_exempt
def rate_delivery(request):
    if request.method == 'POST':
        delivery = DeliveryService.objects.get(id=request.POST['id'])
        delivery.rated = 'rated'
        rating = DriverRating()
        rating.rating = request.POST['rating']
        rating.delivery = delivery
        rating.rated_by = delivery.customer
        rating.driver = delivery.taken_by
        rating.save()
        delivery.save()
        return JsonResponse({"message": "success"})
    return JsonResponse({"message": "failure"})


class WayBill(PDFTemplateView):
    template_name = "pdf/pdf.html"
    bill = None

    def get_context_data(self, **kwargs):
        context = super(WayBill, self).get_context_data(**kwargs)
        context['bill'] = self.request.GET['bill_id']
        context['order'] = DeliveryService.objects.get(id=self.request.GET['bill_id'])
        import mybarcode
        d = mybarcode.MyBarcodeDrawing(str(context['order'].order_no)).save(formats=['png'],
                                                                                        outDir=settings.MEDIA_ROOT + '/barcode',
                                                                                        fnRoot=context['bill'])
        context['image'] = self.request.GET['bill_id'] + '.png'
        context['way_bill_logo'] = "way_bill_logo.jpg"
        context['packages'] = Package.objects.filter(service=context['order'])
        #affress 
        conf = SiteConfigeration.objects.all()
        context['address'] = conf[0].office_address
        context['phone_no'] = conf[0].office_phone_no
        context['office_email'] = conf[0].office_email
        return context


class Consegment(PDFTemplateView):
    template_name = "pdf/consegment.html"
    bill = None

    def get_context_data(self, **kwargs):
        context = super(Consegment, self).get_context_data(**kwargs)
        context['bill'] = self.request.GET['bill_id']
        context['order'] = DeliveryService.objects.get(id=self.request.GET['bill_id'])
        logs = ActivityLog.objects.filter(delevery_service=context['order'], delevery_status='pickedup')
        if logs.count() > 0:
            context['collection_time'] = logs[0].date_time
        else:
            context['collection_time'] = None
        percells = Package.objects.filter(service=context['order'], confirm_pickup=True)
        context['percell_count'] = percells.count()
        context['way_bill_logo'] = "way_bill_logo.jpg"
        return context

#method for testing barcode generation
#this method will be removed on the
def barcode(request):
    # instantiate a drawing object
    import mybarcode
    d = mybarcode.MyBarcodeDrawing("HELLO WORLD")
    binaryStuff = d.asString('gif')
    return HttpResponse(binaryStuff)


def get_ratings(delivery):
    try:
        rating = DriverRating.objects.get(delivery=delivery)
    except:
        rating = None
    return rating


def all_deliveries(request):
    customer = Customer.objects.get(user=request.user.id)
    deliveries = DeliveryService.objects.filter(customer=customer, status=True).order_by('-id')
    ready_data = '<table id="tableCourier" class="table table-hover dataTable table-striped width-full th-nowrap">'
    ready_data = ready_data + '<thead>'
    ready_data = ready_data + '<tr>'
    ready_data = ready_data + '<th>#</th>'
    ready_data = ready_data + '<th>Order Date </th>'
    ready_data = ready_data + '<th>Detail</th>'
    ready_data = ready_data + '<th>Quick Ref #</th>'
    ready_data = ready_data + '<th>Price</th>'
    ready_data = ready_data + '<th>Collection Address</th>'
    ready_data = ready_data + '<th>Delivery Address</th>'
    ready_data = ready_data + '<th>Status</th>'
    ready_data = ready_data + '<th>Rating</th>'
    ready_data = ready_data + '<th>Action</th>'
    ready_data = ready_data + '</tr>'
    ready_data = ready_data + '</thead>'
    ready_data = ready_data + '<tbody id="tableCourierBody">'

    if deliveries.count() > 0:
        counter = 1
        for delivery in deliveries:
            if delivery.delevery_status == 'pending':
                color_tag = 'state-pending'
            if delivery.delevery_status == 'assigned':
                color_tag =  'state-assigned'
            if delivery.delevery_status == 'taken':
                color_tag =  'state-accepted'
            if delivery.delevery_status == 'pickedup':
                color_tag =  'state-pickup'
            if delivery.delevery_status == 'completed':
                color_tag =  'state-completed'
            if delivery.delevery_status == 'cancelled':
                color_tag =  'state-cancelled'
            ready_data = ready_data + '<tr>'
            delivery_date = delivery.created_at.strftime('%d-%m-%Y')
            ready_data = ready_data + '<td>'+str(counter)+'</td>'
            ready_data = ready_data + '<td><span class="o-date">' + delivery_date + '</span></td>'
            ready_data = ready_data + '<td><a href="/service/my_booking/courier/' + str(delivery.id) + '">View</a></td>'
            ready_data = ready_data + '<td>' + str(delivery.order_no) + '</td>'
            ready_data = ready_data + '<td><span class="price">' + str(delivery.total_cost) + '</span></td>'
            ready_data = ready_data + '<td><span class="cl-address">' + delivery.collection_address + '</span></td>'
            ready_data = ready_data + '<td><span class="cl-address">' + delivery.delivery_address + '</span></td>'
            ready_data = ready_data + '<td><span class="state '+ color_tag +'">' + delivery.delevery_status + '</span></td>'
            if delivery.delevery_status == 'completed':
                if delivery.rated == 'not_rated':
                    ready_data = ready_data + '<td>Not Rated</td>'
                else:
                    rating = get_ratings(delivery)
                    if rating:
                        ready_data = ready_data + '<td><input class="rated-star" value="' + str(
                            rating.rating) + '"></td>'
            else:
                ready_data = ready_data + '<td>Incomplete Job</td>'
            if delivery.delevery_status == 'completed' or delivery.delevery_status == 'cancelled' or delivery.delevery_status == 'pickedup':
                ready_data = ready_data + '<td><a href="#" disabled class="btn btn-gray">No Actions</a></td>'
            else:
                ready_data = ready_data + '<td><a href="/service/del_cancel/' + str(
                    delivery.id) + '/" class="btn btn-orange unstroke btn-sm">Cancel</a></td>'
            ready_data = ready_data + '</tr>'
            counter = counter + 1
        ready_data = ready_data + '</tbody></table>'
    else:
        ready_data = ready_data + '</tbody></table>'
    return HttpResponse(ready_data)


def current_deliveries(request):
    customer = Customer.objects.get(user=request.user.id)
    deliveries = DeliveryService.objects.filter(customer=customer, delevery_status='pending', delevery_date__gte=datetime.now()-timedelta(days=0)).order_by('-id')
    ready_data = '<table id="tableCourier" class="table table-hover dataTable table-striped width-full th-nowrap">'
    ready_data = ready_data + '<thead>'
    ready_data = ready_data + '<tr>'
    ready_data = ready_data + '<th>#</th>'
    ready_data = ready_data + '<th>Order Date </th>'
    ready_data = ready_data + '<th>Detail</th>'
    ready_data = ready_data + '<th>Quick Ref #</th>'
    ready_data = ready_data + '<th>Price</th>'
    ready_data = ready_data + '<th>Collection Address</th>'
    ready_data = ready_data + '<th>Delivery Address</th>'
    ready_data = ready_data + '<th>Status</th>'
    ready_data = ready_data + '<th>Rating</th>'
    ready_data = ready_data + '<th>Action</th>'
    ready_data = ready_data + '</tr>'
    ready_data = ready_data + '</thead>'
    ready_data = ready_data + '<tbody id="tableCourierBody">'
    if deliveries.count() > 0:
        counter = 1
        for delivery in deliveries:
            if delivery.delevery_status == 'pending':
                color_tag = 'state-pending'
            if delivery.delevery_status == 'assigned':
                color_tag =  'state-assigned'
            if delivery.delevery_status == 'taken':
                color_tag =  'state-accepted'
            if delivery.delevery_status == 'pickedup':
                color_tag =  'state-pickup'
            if delivery.delevery_status == 'completed':
                color_tag =  'state-completed'
            if delivery.delevery_status == 'cancelled':
                color_tag =  'state-cancelled'
            ready_data = ready_data + '<tr>'
            delivery_date = delivery.created_at.strftime('%d-%m-%Y')
            ready_data = ready_data + '<td>'+str(counter)+'</td>'
            ready_data = ready_data + '<td><span class="o-date">' + delivery_date + '</span></td>'
            ready_data = ready_data + '<td><a href="/service/my_booking/courier/' + str(delivery.id) + '">View</a></td>'
            ready_data = ready_data + '<td>' + str(delivery.order_no) + '</td>'
            ready_data = ready_data + '<td><span class="price">' + str(delivery.total_cost) + '</span></td>'
            ready_data = ready_data + '<td><span class="cl-address">' + delivery.collection_address + '</span></td>'
            ready_data = ready_data + '<td><span class="cl-address">' + delivery.delivery_address + '</span></td>'
            ready_data = ready_data + '<td><span class="state '+ color_tag +'">' + delivery.delevery_status + '</span></td>'
            if delivery.delevery_status == 'completed':
                if delivery.rated == 'not_rated':
                    ready_data = ready_data + '<td>Not Rated</td>'
                else:
                    rating = get_ratings(delivery)
                    if rating:
                        ready_data = ready_data + '<td><input class="rated-star" value="' + str(
                            rating.rating) + '"></td>'
            else:
                ready_data = ready_data + '<td>Incomplete Job</td>'
            if delivery.delevery_status == 'completed' or delivery.delevery_status == 'cancelled' or delivery.delevery_status == 'pickedup':
                ready_data = ready_data + '<td><a href="#" disabled class="btn btn-gray">No Actions</a></td>'
            else:
                ready_data = ready_data + '<td><a href="/service/del_cancel/' + str(
                    delivery.id) + '/" class="btn btn-orange unstroke btn-sm">Cancel</a></td>'
            ready_data = ready_data + '</tr>'
            counter = counter + 1
        ready_data = ready_data + '</tbody></table>'
    else:
        ready_data = ready_data + '</tbody></table>'
    return HttpResponse(ready_data)


def closed_deliveries(request):
    customer = Customer.objects.get(user=request.user.id)
    deliveries = DeliveryService.objects.filter(customer=customer, delevery_status='completed', status=True).order_by('-id')
    ready_data = '<table id="tableCourier" class="table table-hover dataTable table-striped width-full th-nowrap">'
    ready_data = ready_data + '<thead>'
    ready_data = ready_data + '<tr>'
    ready_data = ready_data + '<th>#</th>'
    ready_data = ready_data + '<th>Order Date </th>'
    ready_data = ready_data + '<th>Detail</th>'
    ready_data = ready_data + '<th>Quick Ref #</th>'
    ready_data = ready_data + '<th>Price</th>'
    ready_data = ready_data + '<th>Collection Address</th>'
    ready_data = ready_data + '<th>Delivery Address</th>'
    ready_data = ready_data + '<th>Status</th>'
    ready_data = ready_data + '<th>Rating</th>'
    ready_data = ready_data + '<th>Action</th>'
    ready_data = ready_data + '</tr>'
    ready_data = ready_data + '</thead>'
    ready_data = ready_data + '<tbody id="tableCourierBody">'
    if deliveries.count() > 0:
        counter = 1
        for delivery in deliveries:
            if delivery.delevery_status == 'pending':
                color_tag = 'state-pending'
            if delivery.delevery_status == 'assigned':
                color_tag =  'state-assigned'
            if delivery.delevery_status == 'taken':
                color_tag =  'state-accepted'
            if delivery.delevery_status == 'pickedup':
                color_tag =  'state-pickup'
            if delivery.delevery_status == 'completed':
                color_tag =  'state-completed'
            if delivery.delevery_status == 'cancelled':
                color_tag =  'state-cancelled'
            ready_data = ready_data + '<tr>'
            delivery_date = delivery.created_at.strftime('%d-%m-%Y')
            ready_data = ready_data + '<td>'+str(counter)+'</td>'
            ready_data = ready_data + '<td><span class="o-date">' + delivery_date + '</span></td>'
            ready_data = ready_data + '<td><a href="/service/my_booking/courier/' + str(delivery.id) + '">View</a></td>'
            ready_data = ready_data + '<td>' + str(delivery.order_no) + '</td>'
            ready_data = ready_data + '<td><span class="price">' + str(delivery.total_cost) + '</span></td>'
            ready_data = ready_data + '<td><span class="cl-address">' + delivery.collection_address + '</span></td>'
            ready_data = ready_data + '<td><span class="cl-address">' + delivery.delivery_address + '</span></td>'
            ready_data = ready_data + '<td><span class="state '+ color_tag +'">' + delivery.delevery_status + '</span></td>'
            if delivery.delevery_status == 'completed':
                if delivery.rated == 'not_rated':
                    ready_data = ready_data + '<td>Not Rated</td>'
                else:
                    rating = get_ratings(delivery)
                    if rating:
                        ready_data = ready_data + '<td><input class="rated-star" value="' + str(
                            rating.rating) + '"></td>'
            else:
                ready_data = ready_data + '<td>Incomplete Job</td>'
            if delivery.delevery_status == 'completed' or delivery.delevery_status == 'cancelled' or delivery.delevery_status == 'pickedup':
                ready_data = ready_data + '<td><a href="#" disabled class="btn btn-gray">No Actions</a></td>'
            else:
                ready_data = ready_data + '<td><a href="/service/del_cancel/' + str(
                    delivery.id) + '/" class="btn btn-orange unstroke btn-sm">Cancel</a></td>'
            ready_data = ready_data + '</tr>'
            counter = counter + 1
        ready_data = ready_data + '</tbody></table>'
    else:
        ready_data = ready_data + '</tbody></table>'
    return HttpResponse(ready_data)

def del_cancel(request, book_id):
    delivery = DeliveryService.objects.get(id=book_id)
    delivery.delevery_status = 'cancelled'
    payment = Payment.objects.get(delevery_service = book_id)
    payment.payment_status = 'REFUND'
    payment.save()
    delivery.save()
    messages.error(request, 'Your job has been cancelled.')
    return redirect('/service/myBookings/')


# @login_required
# @customer_only
def make_payment_for_batch(request, job_list, order_cost):
    cards = CreditCard.objects.filter(custmer=request.customer)
    if cards.count() < 1:
        return False
    else:
        stripe.api_key = constant.STRIPE_SECRECT_KEY
        try:
            stripe.Charge.create(
                amount=order_cost,
                currency="sgd",
                customer=cards[0].strite_id)
        except Exception, e:
            return False

    for job in job_list:
        courier = DeliveryService.objects.get(id=job.id)
        try:
            payment = Payment()
            payment.customer = request.customer
            payment.delevery_service = courier
            payment.total_cost = job.total_cost
            payment.paid_amount = job.total_cost
            payment.save()
            log = ActivityLog()
            log.log_message = "Your payment has been received."
            log.delevery_service = courier
            log.log_type = "The payment is done."
            log.delevery_status = "paid"
            log.save()
            # print 'ok'
        except Exception, e:
            return False
    return True



def calculate_price_packages(request, items, delevery_date, collection_date, cart):
    scheType = ""
    totalPrice = 0
    period = cart.get_time_difference(delevery_date, collection_date)
    # period = cart.get_time_difference(datetime.strptime(delevery_date, '%d-%b%Y').date(), datetime.strptime(collection_date, '%d %b %Y').date())

    if period == 0:
        scheType = "Same Day"
    elif period == 1:
        scheType = "Next Day"
    else:
        scheType = "Two Working Days"

    # for myItem in items:
    #   itemCounter = 1

    sortedItems = sorted(items, key=attrgetter('size.start_weight'), reverse=True)


    itemCounter = 1
    for myItem in sortedItems:
        for pkg in xrange(0, myItem.quantity):
            relevantPrices = Pricing.objects.filter(schedule_type=scheType,
                                                    package_size=myItem.size,
                                                    start_qty__lte=itemCounter,
                                                    end_qty__gte=itemCounter)
            myItem.unit_price = myItem.unit_price + relevantPrices[0].range_rate
            itemCounter = itemCounter + 1
        totalPrice = totalPrice + myItem.unit_price
    return totalPrice

def batch_courrier_order(request):
    arg = {}
    arg.update(csrf(request))
    if request.method == 'POST':
        csvFile = request.FILES['file']
        reader = csv.DictReader(csvFile)
        orders = []
        packages = []
        delivery_name = ''
        order_cost = 0
        created_delevery_list = []
        for row in reader:
            try:
                gmaps = googlemaps.Client(key='AIzaSyD4qpAFSCX_E_hTOkpw0t53Dv3ULOQ2lw4')

                directions_results = gmaps.distance_matrix(
                    row['collection_address'],
                    row['delivery_address'],
                    mode=None,
                    language=None,
                    avoid=None,
                    units="metric",
                    departure_time=None,
                    arrival_time=None,
                    transit_mode=None,
                    transit_routing_preference=None,
                    traffic_model=None
                )

                approx_distance = str(directions_results['rows'][0]['elements'][0]['distance']['text'])
                approx_time = str(directions_results['rows'][0]['elements'][0]['duration']['text'])
                request.session['eta'] = approx_time
                request.session['distance'] = approx_distance
            except Exception, e:
                arg['error'] = "Unable to calculate distance based on your provided address."
                return render_to_response('service/batch_order/')
            package_size = row['package_size']
            delevery_service = DeliveryService()
            delevery_service.customer = request.customer
            delevery_service.collection_name = row['collection_name']
            delevery_service.collection_address = row['collection_address']
            delevery_service.collection_contact_no = row['collection_contract']
            delevery_service.collection_remark = row['collection_remarks']
            delevery_service.delivery_name = row['delivery_name']
            delevery_service.delivery_address = row['delivery_address']
            delevery_service.delivery_contact_no = row['delivery_contact']
            delevery_service.delevery_remarks = row['delivery_remarks']
            delevery_service.collection_date = datetime.strptime(row['collection_date'], '%d %b %Y').date()
            delevery_service.collection_time = row['collection_window']
            delevery_service.delevery_date = datetime.strptime(row['delivery_date'], '%d %b %Y').date()
            delevery_service.delevery_time = row['delivery_window']
            delevery_service.collection_postal_code = row['collection_postal_code']
            delevery_service.delivery_postal_code = row['delivery_postal_code']
            delevery_service.collection_unit_number = row['collection_unit_number']
            delevery_service.delivery_unit_number = row['delivery_unit_number']
            delevery_service.distance = request.session['distance']
            delevery_service.eta = request.session['eta']
            delevery_service.rated = 'not_rated'
            delevery_service.delevery_status = 'pending'
            # delevery_service.total_cost = totalPrice
            delevery_service.payment_status = False
            delevery_service.save()
            packages = package_size.split(",")
            cart = Cart(request)
            cart.clear_items()
            size = PackageSize.objects.all()
            for item in packages:
                try:
                    s = item.rstrip()
                    s = item.lstrip()
                    package_data = s.split(":")
                    package_size = PackageSize.objects.get(name=package_data[0])
                    item_id = cart.add_package(package_size)
                    cart.change_quantity(item_id, int(package_data[1]))
                    package = Package()
                    package.service = delevery_service
                    package.quantity = int(package_data[1])
                    package.weight = package_size.weight
                    package.length = package_size.length
                    package.width = package_size.width
                    package.height = package_size.height
                    package.size = package_size
                    package.save()
                except:
                    pass
            order_price = calculate_price_packages(request, cart.get_item(), row['delivery_date'], row['collection_date'], cart)
            delevery_service.total_cost = order_price
            order_cost = order_cost + order_price
            delevery_service.save()
            created_delevery_list.append(delevery_service)
        #makepayment
        make_payment_for_batch(request, created_delevery_list, order_cost)
        messages.success(request, 'Order successful')
        return redirect('/service/myBookings/')
    return render_to_response('service/batch_orders.html', arg)


@csrf_exempt
def courier_from_date(request):
    # print request.user
    customer = Customer.objects.get(user=request.user.id)
    # print customer.id
    from_date = request.POST['from_date']
    from_date = datetime.strptime(from_date, '%d %b %Y').date()
    print from_date
    to_date = request.POST['to_date']
    to_date = datetime.strptime(to_date, '%d %b %Y').date()
    print to_date
    deliveries = DeliveryService.objects.filter(customer=customer, created_at__range=[from_date, to_date])
    # print str(deliveries[0].created_at)

    ready_data = '<table id="tableCourier" class="table table-hover dataTable table-striped width-full th-nowrap">'
    ready_data = ready_data + '<thead>'
    ready_data = ready_data + '<tr>'
    ready_data = ready_data + '<th>#</th>'
    ready_data = ready_data + '<th>Order Date </th>'
    ready_data = ready_data + '<th>Tracking URL</th>'
    ready_data = ready_data + '<th>Quick Ref #</th>'
    ready_data = ready_data + '<th>Price</th>'
    ready_data = ready_data + '<th>Collection Address</th>'
    ready_data = ready_data + '<th>Delivery Address</th>'
    ready_data = ready_data + '<th>Status</th>'
    ready_data = ready_data + '<th>Rating</th>'
    ready_data = ready_data + '<th>Action</th>'
    ready_data = ready_data + '</tr>'
    ready_data = ready_data + '</thead>'
    ready_data = ready_data + '<tbody id="tableCourierBody">'
    if deliveries.count() > 0:
        counter = 1
        for delivery in deliveries:
            ready_data = ready_data + '<tr>'
            delivery_date = delivery.created_at.strftime('%d-%m-%Y')
            ready_data = ready_data + '<td>' + str(counter) + '</td>'
            ready_data = ready_data + '<td><span class="o-date">' + delivery_date + '</span></td>'
            ready_data = ready_data + '<td><a href="/service/my_booking/courier/' + str(delivery.id) + '">View</a></td>'
            ready_data = ready_data + '<td>UF' + str(delivery.id) + '</td>'
            ready_data = ready_data + '<td><span class="price">' + str(delivery.total_cost) + '</span></td>'
            ready_data = ready_data + '<td><span class="cl-address">' + delivery.collection_address + '</span></td>'
            ready_data = ready_data + '<td><span class="cl-address">' + delivery.delivery_address + '</span></td>'
            ready_data = ready_data + '<td><span class="cl-address">' + delivery.delevery_status + '</span></td>'
            if delivery.delevery_status == 'completed':
                if delivery.rated == 'not_rated':
                    ready_data = ready_data + '<td>Not Rated</td>'
                else:
                    rating = get_ratings(delivery)
                    if rating:
                        ready_data = ready_data + '<td><input class="rated-star" value="' + str(
                            rating.rating) + '"></td>'
            else:
                ready_data = ready_data + '<td>Incomplete Job</td>'
            if delivery.delevery_status == 'completed' or delivery.delevery_status == 'cancelled' or delivery.delevery_status == 'pickedup':
                ready_data = ready_data + '<td></td>'
            else:
                ready_data = ready_data + '<td><a href="/service/del_cancel/' + str(
                    delivery.id) + '/" class="btn btn-gray">Cancel</a></td>'
            ready_data = ready_data + '</tr>'
            counter = counter + 1
        ready_data = ready_data + '</tbody></table>'
    else:
        ready_data = ready_data + '<tbody id="tableCourierBody"></tbody></table>'
    return HttpResponse(ready_data)

@csrf_exempt
def pickup_from_date(request):
    # print request.user
    customer = Customer.objects.get(user=request.user.id)
    # print customer.id
    from_date = request.POST['from_date']
    from_date = datetime.strptime(from_date, '%d %b %Y').date()
    print from_date
    to_date = request.POST['to_date']
    to_date = datetime.strptime(to_date, '%d %b %Y').date()
    print to_date
    pickups = PickupService.objects.filter(customer=customer, created_at__range=[from_date, to_date])

    ready_data = '<table id="tablePickup" class="table table-hover dataTable table-striped width-full">'
    ready_data = ready_data + '<thead>'
    ready_data = ready_data + '<tr>'
    ready_data = ready_data + '<th>#</th>'
    ready_data = ready_data + '<th>Order Date </th>'
    ready_data = ready_data + '<th>Pickup Address</th>'
    ready_data = ready_data + '<th>Drop off Address</th>'
    ready_data = ready_data + '<th>Distance</th>'
    ready_data = ready_data + '<th>Status</th>'
    ready_data = ready_data + '</tr>'
    ready_data = ready_data + '</thead>'
    ready_data = ready_data + '<tbody id="tableCourierBody">'

    if pickups.count() > 0:
        counter = 1
        for pickup in pickups:
            ready_data = ready_data + '<tr>'
            pickup_date = pickup.created_at.strftime('%d-%m-%Y')
            ready_data = ready_data + '<td>' + str(counter) + '</td>'
            ready_data = ready_data + '<td><span class="o-date">' + pickup_date + '</span></td>'
            ready_data = ready_data + '<td><span class="cl-address">' + pickup.pickup_address + '</span></td>'
            ready_data = ready_data + '<td><span class="cl-address">' + pickup.dropoff_address + '</span></td>'
            ready_data = ready_data + '<td><span class="cl-address">' + pickup.distance + '</span></td>'
            ready_data = ready_data + '<td><span class="state state-completed">' + pickup.pickup_status + '</span></td>'
            ready_data = ready_data + '</tr>'
            counter = counter + 1
        ready_data = ready_data + '</tbody></table>'
    else:
        ready_data = ready_data + '<tbody id="tablePickup"></tbody></table>'
    return HttpResponse(ready_data)

@csrf_exempt
def postal_code(request):
    s = request.POST['address']
    s = s.replace(' Singapore', '')
    s = s.upper()
    street = Street.objects.get(street_name=s)
    try:
        building = request.POST['building']
        # print building
        postal = Postalcode.objects.get(street_key=street.street_key, buliding_no=building)
        return HttpResponse(postal.post_code)
    except:
        try:
            postal = Postalcode.objects.get(street_key=street.street_key)
            return HttpResponse(postal.post_code)
        except Exception, e:
            print e
            return None

@csrf_exempt
def building_address(request):
    s = request.POST['postal']
    postal = Postalcode.objects.get(post_code=s)
    try:
        # print building
        street = Street.objects.get(street_key=postal.street_key)
        return JsonResponse({"street": street.street_name + ' SINGAPORE', "building": postal.buliding_no})
    except Exception, e:
        print e
        return None

@csrf_exempt
def toggle_favorite(request):
    driver = Driver.objects.get(id=request.POST['id'])
    customer = Customer.objects.get(id=request.customer.id)
    try:
        if customer.is_corporate:
            try:
                favorite = CorporateDriverCustomer.objects.get(driver=driver, customer=request.customer)
                favorite.delete()
                return JsonResponse({"message": "Made unfavorite"})
            except:
                favorite = CorporateDriverCustomer()
                favorite.customer = request.customer
                favorite.driver = driver
                favorite.save()
                return JsonResponse({"message": "Made favorite"})
        else:
            try:
                favorite = FavoriteDriverCustomer.objects.get(driver=driver, customer=request.customer)
                favorite.delete()
                return JsonResponse({"message": "Made unfavorite"})
            except:
                favorite = FavoriteDriverCustomer()
                favorite.customer = request.customer
                favorite.driver = driver
                favorite.save()
                return JsonResponse({"message": "Made favorite"})
    except Exception, e:
        return JsonResponse({"message": str(e)})

@csrf_exempt
def enable_collection_time(request):
    today = datetime.today().date()
    posted_date = datetime.strptime(request.POST['from_date'], '%d %b %Y').date()
    print datetime.now().time()
    ready_data = ''
    if today == posted_date:
        # try:
        window = DeliveryTiming.objects.filter(start_time__gt = datetime.now().time())
        if window:
            for w in window:
                ready_data = ready_data + '<option value="' + w.window_name + '">' + w.window_name + ' ' + str(w.start_time.strftime(
                    "%I:%M %p")) + '-' + str(w.end_time.strftime("%I:%M %p")) + '</option>'
        else:
            ready_data = 'failed'
        return HttpResponse(ready_data)
    else:
        window = DeliveryTiming.objects.all()
        if window:
            for w in window:
                ready_data = ready_data + '<option value="' + w.window_name + '">' + w.window_name + ' ' + str(
                    w.start_time.strftime(
                        "%I:%M %p")) + '-' + str(w.end_time.strftime("%I:%M %p")) + '</option>'
        return HttpResponse(ready_data)

@csrf_exempt
def enable_delivery_time(request):
    # print request.POST['from_date']
    # print request.POST['to_date']
    delivery_window = DeliveryTiming.objects.get(window_name = request.POST['from_time'])
    # print type(delivery_window.start_time)
    # print type(delivery_window.start_time)
    # today = datetime.today().date()
    posted_date = datetime.strptime(request.POST['from_date'], '%d %b %Y').date()
    delivery_date = datetime.strptime(request.POST['to_date'], '%d %b %Y').date()
    # print delivery_date
    # print datetime.now().time()
    ready_data = ''
    if delivery_date == posted_date:
        # try:
        # print 'ok'
        window = DeliveryTiming.objects.filter(start_time__gt = delivery_window.start_time)
        express = ExpressWindows.objects.all()
        if window:
            print 'ok'
            for w in window:
                ready_data = ready_data + '<option value="' + w.window_name + '">' + w.window_name + ' ' + str(w.start_time.strftime("%I:%M %p")) + '-' + str(w.end_time.strftime("%I:%M %p")) + '</option>'
        if express:
            for w in express:
                ready_data = ready_data + '<option value="' + w.window_name + '">' + w.window_name + '</option>'
        else:
            ready_data = 'failed'
        return HttpResponse(ready_data)
        # except Exception, e:
        #     return HttpResponse('failed')
    else:
        window = DeliveryTiming.objects.all()
        if window:
            for w in window:
                ready_data = ready_data + '<option value="' + w.window_name + '">' + w.window_name + ' ' + str(w.start_time.strftime("%I:%M %p")) + '-' + str(w.end_time.strftime("%I:%M %p")) + '</option>'
        return HttpResponse(ready_data)


@csrf_exempt
def verify_date(request):
    collection_date = datetime.strptime(request.POST['from_date'], '%d %b %Y').date()
    print collection_date
    delivery_date = datetime.strptime(request.POST['to_date'], '%d %b %Y').date()
    print delivery_date
    # try:
    if collection_date > delivery_date:
        return JsonResponse({"message": 'False'})
    elif collection_date == delivery_date:
        return JsonResponse({"message": "True"})
    else:
        return JsonResponse({"message": "True"})
    # else:
    #     return JsonResponse({"message": str(collection_date < delivery_date)})
    # except Exception, e:
    #     return JsonResponse({"message": str(e)})
    # except Exception, e:
    #     return JsonResponse(e)