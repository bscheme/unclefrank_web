from django.contrib import admin
from models import PackageSize, VehicleType, DeliveryService

# Register your models here.
admin.site.register(PackageSize)
admin.site.register(VehicleType)
admin.site.register(DeliveryService)

