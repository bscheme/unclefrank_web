# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0075_auto_20170105_1838'),
    ]

    operations = [
        migrations.AlterField(
            model_name='deliveryservice',
            name='collection_address',
            field=models.TextField(null=True),
        ),
        migrations.AlterField(
            model_name='deliveryservice',
            name='collection_remark',
            field=models.TextField(null=True),
        ),
    ]
