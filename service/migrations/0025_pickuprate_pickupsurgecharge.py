# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0024_holidaylist'),
    ]

    operations = [
        migrations.CreateModel(
            name='PickUpRate',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('base_fare', models.DecimalField(default=0.0, max_digits=10, decimal_places=2)),
                ('rate', models.DecimalField(default=0.0, max_digits=10, decimal_places=2)),
                ('waiting_charge', models.DecimalField(default=0.0, max_digits=5, decimal_places=2)),
                ('peakMultiplier', models.DecimalField(default=0.0, max_digits=5, decimal_places=2)),
                ('gst', models.DecimalField(default=0.0, max_digits=5, decimal_places=2)),
                ('day_type', models.CharField(max_length=50, null=True)),
                ('vehicle_type', models.ForeignKey(to='service.VehicleType')),
            ],
        ),
        migrations.CreateModel(
            name='PickupSurgeCharge',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('postal_code', models.CharField(max_length=500, null=True)),
                ('charge', models.DecimalField(default=0.0, max_digits=10, decimal_places=2)),
            ],
        ),
    ]
