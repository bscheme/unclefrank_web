# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0063_merge'),
    ]

    operations = [
        migrations.AddField(
            model_name='payment',
            name='job_type',
            field=models.CharField(max_length=100, null=True, choices=[(b'pickup', b'pickup'), (b'delevery', b'delevery')]),
        ),
    ]
