# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0010_auto_20160901_0808'),
    ]

    operations = [
        migrations.AlterField(
            model_name='deliveryservice',
            name='delevery_status',
            field=models.CharField(default=b'current', max_length=255),
        ),
    ]
