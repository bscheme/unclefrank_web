# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0043_auto_20161103_0125'),
    ]

    operations = [
        migrations.AlterField(
            model_name='activitylog',
            name='delevery_status',
            field=models.CharField(default=b'pending', max_length=255, choices=[(b'pending', b'pending'), (b'taken', b'taken'), (b'completed', b'completed'), (b'cancelled', b'cancelled')]),
        ),
    ]
