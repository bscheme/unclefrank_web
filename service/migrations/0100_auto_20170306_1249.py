# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0099_auto_20170305_1427'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='deliveryservice',
            name='assigned_by',
        ),
        migrations.AddField(
            model_name='deliveryservice',
            name='auto_assigned',
            field=models.BooleanField(default=False),
        ),
    ]
