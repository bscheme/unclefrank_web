# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0055_merge'),
    ]

    operations = [
        migrations.AddField(
            model_name='vehicletype',
            name='description',
            field=models.TextField(default=None, null=True),
        ),
    ]
