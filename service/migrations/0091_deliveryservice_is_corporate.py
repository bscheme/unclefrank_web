# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0090_auto_20170122_1335'),
    ]

    operations = [
        migrations.AddField(
            model_name='deliveryservice',
            name='is_corporate',
            field=models.BooleanField(default=False),
        ),
    ]
