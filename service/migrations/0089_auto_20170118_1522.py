# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0088_auto_20170117_1714'),
    ]

    operations = [
        migrations.AddField(
            model_name='deliveryservice',
            name='collection_building',
            field=models.CharField(max_length=20, null=True),
        ),
        migrations.AddField(
            model_name='deliveryservice',
            name='delivery_building',
            field=models.CharField(max_length=20, null=True),
        ),
    ]
