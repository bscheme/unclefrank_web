# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0036_auto_20161030_1555'),
        ('service', '0038_pickupservice_cancel_reason'),
    ]

    operations = [
    ]
