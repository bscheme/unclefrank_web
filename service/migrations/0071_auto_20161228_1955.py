# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0070_auto_20161227_1545'),
    ]

    operations = [
        migrations.AddField(
            model_name='payment',
            name='coupon_amount',
            field=models.DecimalField(default=0.0, max_digits=10, decimal_places=2),
        ),
        migrations.AddField(
            model_name='payment',
            name='coupon_code',
            field=models.CharField(default=None, max_length=10, null=True),
        ),
    ]
