# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0030_pickupservice_pickedup_by'),
    ]

    operations = [
        migrations.AlterField(
            model_name='activitylog',
            name='delevery_status',
            field=models.CharField(default=b'current', max_length=255, choices=[(b'pending', b'pending'), (b'taken', b'taken'), (b'completed', b'completed'), (b'cancelled', b'cancelled')]),
        ),
        migrations.AlterField(
            model_name='deliveryservice',
            name='delevery_status',
            field=models.CharField(default=b'current', max_length=255, choices=[(b'pending', b'pending'), (b'taken', b'taken'), (b'completed', b'completed'), (b'cancelled', b'cancelled')]),
        ),
    ]
