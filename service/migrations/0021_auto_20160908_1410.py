# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0020_activitylog_logger_name'),
    ]

    operations = [
        migrations.AddField(
            model_name='package',
            name='confirm_delevery',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='package',
            name='confirm_pickup',
            field=models.BooleanField(default=False),
        ),
    ]
