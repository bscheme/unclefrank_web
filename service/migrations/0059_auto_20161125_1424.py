# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0058_tracking_data_id'),
    ]

    operations = [
        migrations.AddField(
            model_name='deliveryservice',
            name='admin_remarks',
            field=models.CharField(max_length=120, null=True),
        ),
        migrations.AddField(
            model_name='pickupservice',
            name='admin_remarks',
            field=models.CharField(max_length=120, null=True),
        ),
    ]
