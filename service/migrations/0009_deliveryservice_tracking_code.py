# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0008_deliveryservice_signature'),
    ]

    operations = [
        migrations.AddField(
            model_name='deliveryservice',
            name='tracking_code',
            field=models.UUIDField(default=uuid.uuid4, editable=False),
        ),
    ]
