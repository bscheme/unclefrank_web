# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0057_pickuprate_min_fare'),
    ]

    operations = [
        migrations.AddField(
            model_name='tracking',
            name='data_id',
            field=models.IntegerField(default=0),
        ),
    ]
