# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0051_auto_20161107_1625'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='deliveryservice',
            name='collection_reff_no',
        ),
    ]
