# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0073_auto_20170103_1801'),
    ]

    operations = [
        migrations.AddField(
            model_name='pickupservice',
            name='order_no',
            field=models.CharField(default=None, max_length=20, null=True),
        ),
    ]
