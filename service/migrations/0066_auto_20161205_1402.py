# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0065_auto_20161205_1400'),
    ]

    operations = [
        migrations.AlterField(
            model_name='tracking',
            name='zipcode',
            field=models.CharField(max_length=6, null=True),
        ),
    ]
