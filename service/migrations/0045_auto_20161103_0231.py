# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0044_auto_20161103_0125'),
    ]

    operations = [
        migrations.AlterField(
            model_name='deliveryservice',
            name='rated',
            field=models.CharField(default=None, max_length=20),
        ),
        migrations.AlterField(
            model_name='pickupservice',
            name='rated',
            field=models.CharField(default=None, max_length=20),
        ),
    ]
