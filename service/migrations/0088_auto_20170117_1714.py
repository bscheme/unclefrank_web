# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0087_auto_20170117_1714'),
    ]

    operations = [
        migrations.RenameField(
            model_name='couriertimecharge',
            old_name='postal_codes',
            new_name='postal_code',
        ),
    ]
