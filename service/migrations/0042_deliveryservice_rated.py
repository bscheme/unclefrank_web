# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0041_pickupservice_rated'),
    ]

    operations = [
        migrations.AddField(
            model_name='deliveryservice',
            name='rated',
            field=models.BooleanField(default=False),
        ),
    ]
