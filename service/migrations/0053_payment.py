# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0016_remove_customer_nric'),
        ('service', '0052_remove_deliveryservice_collection_reff_no'),
    ]

    operations = [
        migrations.CreateModel(
            name='Payment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_time', models.DateTimeField(auto_now=True)),
                ('total_cost', models.DecimalField(default=0.0, max_digits=10, decimal_places=2)),
                ('paid_amount', models.DecimalField(default=0.0, max_digits=10, decimal_places=2)),
                ('customer', models.ForeignKey(to='customer.Customer')),
                ('delevery_service', models.ForeignKey(default=None, to='service.DeliveryService', null=True)),
                ('pickup_service', models.ForeignKey(default=None, to='service.PickupService', null=True)),
            ],
        ),
    ]
