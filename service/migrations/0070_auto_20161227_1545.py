# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import service.models


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0069_auto_20161227_1544'),
    ]

    operations = [
        migrations.AlterField(
            model_name='package',
            name='image',
            field=models.FileField(upload_to=service.models.content_file_name),
        ),
    ]
