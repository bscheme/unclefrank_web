# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('driver', '0002_auto_20160821_0806'),
        ('service', '0011_auto_20160904_1128'),
    ]

    operations = [
        migrations.AddField(
            model_name='deliveryservice',
            name='taken_by',
            field=models.ForeignKey(default=None, to='driver.Driver', null=True),
        ),
    ]
