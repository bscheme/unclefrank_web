# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0009_deliveryservice_tracking_code'),
    ]

    operations = [
        migrations.AddField(
            model_name='pickupservice',
            name='pickup_status',
            field=models.CharField(default=b'pending', max_length=255),
        ),
        migrations.AddField(
            model_name='pickupservice',
            name='signature',
            field=models.ImageField(default=None, null=True, upload_to=b'pickup/signature', blank=True),
        ),
        migrations.AddField(
            model_name='pickupservice',
            name='tracking_code',
            field=models.UUIDField(default=uuid.uuid4, editable=False),
        ),
    ]
