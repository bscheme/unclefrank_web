# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0033_vehicletype_pic_active'),
    ]

    operations = [
        migrations.AddField(
            model_name='pickupservice',
            name='cancell_note',
            field=models.TextField(default=None, null=True),
        ),
        migrations.AddField(
            model_name='pickupservice',
            name='completion_note',
            field=models.TextField(default=None, null=True),
        ),
        migrations.AddField(
            model_name='pickupservice',
            name='job_cancelled_time',
            field=models.DateTimeField(default=None, null=True),
        ),
        migrations.AddField(
            model_name='pickupservice',
            name='job_complete_time',
            field=models.DateTimeField(default=None, null=True),
        ),
        migrations.AddField(
            model_name='pickupservice',
            name='job_start_time',
            field=models.DateTimeField(default=None, null=True),
        ),
        migrations.AddField(
            model_name='pickupservice',
            name='total_cost',
            field=models.DecimalField(default=0.0, max_digits=10, decimal_places=2),
        ),
        migrations.AlterField(
            model_name='pickupservice',
            name='distance',
            field=models.CharField(default=None, max_length=255, null=True),
        ),
    ]
