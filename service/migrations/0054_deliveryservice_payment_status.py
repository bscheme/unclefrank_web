# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0053_payment'),
    ]

    operations = [
        migrations.AddField(
            model_name='deliveryservice',
            name='payment_status',
            field=models.BooleanField(default=False),
        ),
    ]
