# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0086_auto_20170117_1711'),
    ]

    operations = [
        migrations.RenameField(
            model_name='couriertimecharge',
            old_name='name',
            new_name='postal_codes',
        ),
        migrations.RemoveField(
            model_name='couriertimecharge',
            name='postal_code',
        ),
        migrations.RemoveField(
            model_name='pickuptimecharge',
            name='name',
        ),
        migrations.AlterField(
            model_name='pickuptimecharge',
            name='postal_code',
            field=models.CharField(max_length=120, null=True),
        ),
    ]
