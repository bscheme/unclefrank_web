# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0023_auto_20160925_1535'),
    ]

    operations = [
        migrations.CreateModel(
            name='HolidayList',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date', models.DateField(null=True)),
                ('caption', models.CharField(max_length=500, null=True)),
            ],
        ),
    ]
