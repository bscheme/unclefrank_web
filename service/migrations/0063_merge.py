# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0061_pickupjoblogdriver'),
        ('service', '0062_payment_payment_status'),
    ]

    operations = [
    ]
