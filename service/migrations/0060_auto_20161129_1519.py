# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0059_auto_20161125_1424'),
    ]

    operations = [
        migrations.AlterField(
            model_name='activitylog',
            name='delevery_status',
            field=models.CharField(default=b'pending', max_length=255, choices=[(b'pending', b'pending'), (b'assigned', b'assigned'), (b'taken', b'taken'), (b'pickedup', b'pickedup'), (b'completed', b'completed'), (b'paid', b'paid'), (b'cancelled', b'cancelled')]),
        ),
        migrations.AlterField(
            model_name='activitylog',
            name='pickup_status',
            field=models.CharField(default=b'pending', max_length=255, choices=[(b'pending', b'pending'), (b'assigned', b'assigned'), (b'taken', b'taken'), (b'pickedup', b'pickedup'), (b'completed', b'completed'), (b'paid', b'paid'), (b'cancelled', b'cancelled')]),
        ),
        migrations.AlterField(
            model_name='deliveryservice',
            name='delevery_status',
            field=models.CharField(default=b'pending', max_length=255, choices=[(b'pending', b'pending'), (b'assigned', b'assigned'), (b'taken', b'taken'), (b'pickedup', b'pickedup'), (b'completed', b'completed'), (b'paid', b'paid'), (b'cancelled', b'cancelled')]),
        ),
        migrations.AlterField(
            model_name='pickupservice',
            name='pickup_status',
            field=models.CharField(default=b'pending', max_length=255, choices=[(b'pending', b'pending'), (b'assigned', b'assigned'), (b'taken', b'taken'), (b'pickedup', b'pickedup'), (b'completed', b'completed'), (b'paid', b'paid'), (b'cancelled', b'cancelled')]),
        ),
    ]
