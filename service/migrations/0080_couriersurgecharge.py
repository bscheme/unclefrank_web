# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0079_timecharge'),
    ]

    operations = [
        migrations.CreateModel(
            name='CourierSurgeCharge',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('postal_code', models.CharField(max_length=500, null=True)),
                ('charge', models.DecimalField(default=0.0, max_digits=10, decimal_places=2)),
                ('description', models.CharField(default=None, max_length=500, null=True)),
            ],
        ),
    ]
