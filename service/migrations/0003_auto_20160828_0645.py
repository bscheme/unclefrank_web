# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0002_packagesize'),
    ]

    operations = [
        migrations.AlterField(
            model_name='deliveryservice',
            name='collection_time',
            field=models.CharField(default=None, max_length=255),
        ),
        migrations.AlterField(
            model_name='deliveryservice',
            name='delevery_time',
            field=models.CharField(default=None, max_length=255),
        ),
    ]
