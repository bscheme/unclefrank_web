# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('driver', '0021_auto_20161128_1840'),
        ('service', '0060_auto_20161129_1519'),
    ]

    operations = [
        migrations.CreateModel(
            name='PickupJobLogDriver',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('action', models.CharField(max_length=100, null=True, choices=[(b'view', b'view'), (b'accept', b'accept'), (b'cancell', b'cancell')])),
                ('date', models.DateField(auto_now=True)),
                ('driver', models.ForeignKey(to='driver.Driver')),
                ('pickup', models.ForeignKey(to='service.PickupService')),
            ],
        ),
    ]
