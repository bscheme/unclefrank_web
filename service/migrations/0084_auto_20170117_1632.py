# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0083_gst'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='gst',
            new_name='PickupGST',
        ),
    ]
