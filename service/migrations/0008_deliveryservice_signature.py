# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0007_pickupservice_distance'),
    ]

    operations = [
        migrations.AddField(
            model_name='deliveryservice',
            name='signature',
            field=models.ImageField(default=None, upload_to=b'delevery/signature', blank=True, null=True),
        ),
    ]
