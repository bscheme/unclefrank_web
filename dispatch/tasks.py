from UncleFrank import celery_app
import time
from service.models import PickupService
from dispatch import Dispatch
from driver.models import Driver
import requests
import json
from django.utils import timezone

def inform_customer_no_job_found(device_token_list, job_id):
	"""
		Category 5 for job cancellation
	"""
	url = "https://fcm.googleapis.com/fcm/send"
	noti = {}
	noti['content_available'] = True
	noti['notification'] = {}
	noti['notification']['body'] = "No driver found for your job. Your job jas been cancelled."
	noti['notification']['title'] = "No driver found for your job."
	noti['notification']['category'] = 5
	noti['notification']['job_id'] = job_id
	noti['data'] = {}
	noti['data']['message'] = "No driver found for your job. Your job jas been cancelled."
	noti['data']['title'] = "No driver found for your job."
	noti['notification']['sound'] = 'default'
	noti['registration_ids'] = device_token_list
	noti['priority'] = 'high'
	headers = {
		'authorization': "key=AIzaSyDYatjzv-xIZlTIVkCzWzFHOaIpLQFl-jQ",
		'content-type': "application/json",
		'cache-control': "no-cache",
		}
	return requests.request("POST", url, data=json.dumps(noti), headers=headers)


def send_push_message_for_pickeup_job_post(device_token_list, message, title, category=0, job_id=0):
	url = "https://fcm.googleapis.com/fcm/send"
	noti = {}
	noti['content_available'] = True
	noti['notification'] = {}
	noti['notification']['body'] = message
	noti['notification']['title'] = title
	noti['notification']['category'] = category
	noti['notification']['job_id'] = job_id
	noti['data'] = {}
	noti['data']['message'] = message
	noti['data']['title'] = title
	noti['notification']['sound'] = 'default'
	noti['registration_ids'] = device_token_list
	noti['priority'] = 'high'
	headers = {
		'authorization': "key=AIzaSyDZWqkNJXm11ilon9e8j2F7tecYQd12flU",
		'content-type': "application/json",
		'cache-control': "no-cache",
		}
	return requests.request("POST", url, data=json.dumps(noti), headers=headers)

def cancell_job_by_system(job):
	job.pickup_status = 'cancelled'
	job.cancell_note = "No Driver Availbale"
	job.cancel_reason = "No Driver Availbale"
	job.job_cancelled_time = timezone.now()
	job.save()

@celery_app.task()
def dispatch_job_to_nearest_driver(pickup_job_id):
	"""
			Request drivers for accepting the job one by one
			from nearest to ferthest
		"""
	# site_config = superadmin.views.get_site_configeartions()
	pickup_job = PickupService.objects.get(id=pickup_job_id)
	puck_up_lat = pickup_job.pickup_address_lat
	puck_up_lng = pickup_job.pickup_address_lng
	dispatch = Dispatch(None)
	# if site_config:
	# 	distance = site_config.driver_radius
	# 	waiting_time = site_config.waiting_time_for_drivers
	# else:
	distance = 5
	waiting_time = 18
	near_nearest_driver_list = dispatch.get_nearest_active_driver( puck_up_lat, puck_up_lng, distance)
	# print("ok")
	# print("Driver No"+str(near_nearest_driver_list.count()))

	#the following code is temporary
	# and will remove shortly
	check_job = PickupService.objects.get(id=pickup_job_id)
	for driver in near_nearest_driver_list:
		try:
			d = Driver.objects.get(id=driver.driver_id)
			if driver.get_is_active() and d.driver_service_type != "courier" : # also driver vechile type will matched
				token = d.device_token
				send_push_message_for_pickeup_job_post([token], "New Job", "New Pickup Job Posted", 2, pickup_job.id)
			else:
				continue
		except Exception, e:
			print(e)
			continue
		time.sleep(20)
		check_job = PickupService.objects.get(id=pickup_job_id)
		if check_job.pickup_status != "pending":
			break
	if check_job.pickup_status == "pending":
		"""
		IF THE JOB IS STILL NOT TAKEN, JOB WILL BE CANCELLED
		"""
		# inform_customer_no_job_found([check_job.customer.device_token], check_job.id)
		cancell_job_by_system(check_job)

	return True