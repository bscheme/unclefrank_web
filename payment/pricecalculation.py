from superadmin.models import ExpressWindows, DeliveryTiming
from service.models import DeliveryService, Pricing, CourierSurgeCharge, PickupSurgeCharge
from cart.cart import Cart
from django.db.models import Q
from datetime import datetime


class PriceCalculation:
    def __init__(self, request):
    	self.request = request

    def get_max_job_limit(self, delivery_window):
        """
			get if the delivery window is Express or normal and get the maximum job
			Limit for this window.
		"""
        window = None
        max_job_limit = 0
        try:
            window = ExpressWindows.objects.get(window_name=delivery_window)
        except:
            window = DeliveryTiming.objects.get(window_name=delivery_window)
        if window is not None:
            max_job_limit = window.max_number_of_active_job
        return max_job_limit

    def if_job_available_for_delivery_winodw(self, delivery_date, delivery_window):
        """
			check if the maximum limit for the job for a delivery window
			for a particular job.
		"""
        order = DeliveryService.objects.filter(delevery_date=delivery_date, delevery_time=delivery_window,
                                               delevery_status="pending")
        max_no = self.get_max_job_limit(delivery_window)
        if order.count() < max_no:
            return True
        else:
            return False

    def day_defference(self):
        pass

    def get_delivery_type(self, collect_delivery_day_difference):
        if collect_delivery_day_difference == 0:
            scheType = "Same Day"
        elif collect_delivery_day_difference == 1:
            scheType = "Next Day"
        else:
            scheType = "Two Working Days"
        return scheType

    def get_delivery_window(self):
        pass

    def aditional_charger_for_postalcode(self):
        pass

    def get_additional_charger_for_delivery_window(self, delivery_window):
        try:
            express = ExpressWindows.objects.get(window_name=delivery_window)
            return float(express.price)
        except:
            return 0

    def get_additional_charger_for_delivery_window_corporate(self, delivery_window):
        try:
            express = ExpressWindows.objects.get(window_name=delivery_window)
            return float(express.price)
        except:
            return 0

    def calculate_charge_delivery(self, request_type, package_id, quantity, collection_date, delivery_date, collection_window, delivery_window,
                                  collection_postal_code, delivery_postalcode):
        """
			Calculates the price of the objects given
		"""
        if request_type == 'corporate':
            return self.get_package_corporate_base_price(collection_date, delivery_date, quantity, package_id) \
                   + self.get_corporate_postal_code_charge(collection_postal_code) + \
                   self.get_additional_charger_for_delivery_window_corporate(delivery_window)
        else:
            return self.get_package_base_price(collection_date, delivery_date, quantity, package_id) + \
                   self.get_postal_code_charge(collection_postal_code) + \
                   self.get_additional_charger_for_delivery_window(delivery_window)

    # def get_total_pricing_after_discount(self):
    #     pass
    def get_corporate_postal_code_charge(self, collection_postal_code):
        try:
            fee = CourierSurgeCharge.objects.get(postal_code=collection_postal_code)
            return float(fee.corp_charge)
        except:
            return 0

    def get_postal_code_charge(self, collection_postal_code):
        try:
            fee = CourierSurgeCharge.objects.get(postal_code=collection_postal_code)
            return float(fee.charge)
        except:
            return 0
    # def get_price_for_corporate_order(self):
    #     pass

    # def get_price_batch_order(self):
    #     pass

    # def get_price_for_corporate_batch_order(self):
    #     pass

    def get_schedule_type(self, start_date, end_date):
        """
            get the type of schedule from two dates
        """
        # cart = Cart()
        time_difference = self.get_time_difference(start_date, end_date)
        # print time_difference
        # print self.get_delivery_type(time_difference)
        return self.get_delivery_type(time_difference)

    def get_package_base_price(self, collection_date, delivery_date, quantity, package_id):
        """
            get the base price for the normal order
        """
        schedule_type = self.get_schedule_type(collection_date, delivery_date)
        pricing = Pricing.objects.get(schedule_type=schedule_type, package_size=package_id, start_qty__lte=quantity, end_qty__gte=quantity)
        return float(pricing.range_rate)

    def get_package_corporate_base_price(self, collection_date, delivery_date, quantity, package_id):
        """
            get the base price for the corporate order
        """
        schedule_type = self.get_schedule_type(collection_date, delivery_date)
        pricing = Pricing.objects.get(schedule_type=schedule_type, package_size=package_id, start_qty__lte=quantity,
                                      end_qty__gte=quantity)
        return float(pricing.corp_range_rate)

    def get_time_difference(self, del_date, pick_date):
        del_date = datetime.strptime(del_date, '%d %b %Y')
        pick_date = datetime.strptime(pick_date, '%d %b %Y')
        return (del_date - pick_date).days

    # def extimated_price_forpickup()

    def get_cancelation_fees_for_pickup(self, pickup_job):
        """
            The Value will be repleased with
            new data
        """
        created_time = pickup_job.created_at
        cancellation_time = pickup_job.job_cancelled_time
        c = cancellation_time - created_time
        if int(c.total_seconds()) > 300 :
            return 5
        else:
            return 0
