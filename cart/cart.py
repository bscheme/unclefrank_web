import datetime
import models
from operator import attrgetter

CART_ID = 'CART-ID'

class ItemAlreadyExists(Exception):
    pass

class ItemDoesNotExist(Exception):
    pass

class Cart:
    def __init__(self, request):
        cart_id = request.session.get(CART_ID)
        if cart_id:
            try:
                cart = models.Cart.objects.get(id=cart_id, checked_out=False)
            except models.Cart.DoesNotExist:
                cart = self.new(request)
        else:
            cart = self.new(request)
        self.cart = cart

    def __iter__(self):
        for item in self.cart.item_set.all():
            yield item

    def new(self, request):
        cart = models.Cart(creation_date=datetime.datetime.now())
        cart.save()
        request.session[CART_ID] = cart.id
        return cart

    def add(self, product, unit_price, quantity=1):
        try:
            item = models.Item.objects.get(
                cart=self.cart,
                product=product,
            )
        except models.Item.DoesNotExist:
            item = models.Item()
            item.cart = self.cart
            item.product = product
            item.unit_price = unit_price
            item.quantity = quantity
            item.save()
        else: #ItemAlreadyExists
            item.unit_price = unit_price
            item.quantity = item.quantity + int(quantity)
            item.save()

    def add_package(self, size):
        item = models.Item()
        item.cart = self.cart
        item.quantity = 1
        item.size = size
        item.unit_price = 0
        item.save()
        return item.pk

    def change_image(self, id, image):
        item = models.Item.objects.get(
                cart=self.cart,
                id=id,
            )
        item.image = image
        item.save()

    def change_quantity(self, id, quantity):
        item = models.Item.objects.get(
                cart=self.cart,
                id=id,
            )
        if str(quantity) == '0':
            item.quantity = 1
            item.save()
        else:
            item.quantity = quantity
            item.save()


    def change_size(self, id, size):
        # try:
            item = models.Item.objects.get(
                cart=self.cart,
                id=id,
            )
            item.size = size
            # item.height = size.height
            # item.width = size.width
            # item.lenght = size.lenght
            # item.weight = size.weight
            item.save()
        #     return True
        # except:
        #     return False

    def change_size_dimension(self, id, size):
        # try:
        item = models.Item.objects.get(
            cart=self.cart,
            id=id,
        )
        item.size = size

        item.height = 1
        item.width = 1
        item.length = 1
        item.weight = 1
        item.save()
        #     return True
        # except:
        #     return False

    def change_height(self, id, height):
        try:
            item = models.Item.objects.get(
                cart=self.cart,
                id=id,
            )
            if str(height) == '0':
                item.height = 1
                item.save()
            else:
                item.height = height
                item.save()
            return True
        except Exception, e:
            print e
            return False

    def change_weight(self, id, weight):

        try:
            item = models.Item.objects.get(
                cart=self.cart,
                id=id,
            )
            if str(weight) == '0':
                item.weight = 1
                item.save()
            else:
                item.weight = weight
                item.save()
            return True
        except Exception, e:
            print e
            return False

    def change_length(self, id, lenght):
        try:
            item = models.Item.objects.get(
                cart=self.cart,
                id=id,
            )
            if str(lenght) == '0':
                item.length = 1
                item.save()
            else:
                item.length = lenght
                item.save()

            return True
        except Exception, e:
            print e
            return False

    def change_weidth(self, id, width):
        try:
            item = models.Item.objects.get(
                cart=self.cart,
                id=id,
            )
            if str(width) == '0':
                item.width = 1
                item.save()
            else:
                item.width = width
                item.save()

            return True
        except Exception, e:
            print e
            return False


    def get_packages(self):
        items = models.Item.objects.filter(cart=self.cart)
        return items

    def get_weekend(self, test_date):
        weekendOrNot = datetime.datetime.strptime(test_date, '%d %b %Y')
        weekendOrNot = weekendOrNot.strftime('%a')
        if weekendOrNot == 'Sat' or weekendOrNot == 'Sun':
            return "Weekend"
        else:
            return "Weekday"

    def get_time_difference(self, del_date, pick_date):
        del_date = datetime.datetime.strptime(del_date, '%d %b %Y')
        pick_date = datetime.datetime.strptime(pick_date, '%d %b %Y')
        return (del_date - pick_date).days

    def get_distance(self, distance):
        return distance

    def get_sorted_weight(self, items):
        item_list = []
        for item in items:
            if item.size:
                packageWeight = item.size.weight
                item_list.append(packageWeight)
            else:
                packageWeight = item.weight
                item_list.append(packageWeight)
        item_list.sort(reverse=True)
        return item_list

    def get_item(self):
        items = self.get_packages()
        return items

    def remove_package(self, id):
        try:
            item = models.Item.objects.get(
                cart=self.cart,
                id=id,
            )
            item.delete()
            return True
        except:
            return False

    def clear_items(self):
        try:
            items = models.Item.objects.filter(
                cart=self.cart
            )
            for item in items:
                item.delete()
            return True
        except:
            return False


    def remove(self, product):
        try:
            item = models.Item.objects.get(
                cart=self.cart,
                product=product,
            )
        except models.Item.DoesNotExist:
            raise ItemDoesNotExist
        else:
            item.delete()

    def update(self, product, quantity, unit_price=None):
        try:
            item = models.Item.objects.get(
                cart=self.cart,
                product=product,
            )
        except models.Item.DoesNotExist:
            raise ItemDoesNotExist
        else: #ItemAlreadyExists
            if quantity == 0:
                item.delete()
            else:
                item.unit_price = unit_price
                item.quantity = int(quantity)
                item.save()

    def count(self):
        result = 0
        for item in self.cart.item_set.all():
            result += 1 * item.quantity
        return result
        
    def summary(self):
        result = 0
        for item in self.cart.item_set.all():
            result += item.total_price
        return result

    def clear(self):
        for item in self.cart.item_set.all():
            item.delete()

