# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cart', '0004_remove_item_object_id'),
    ]

    operations = [
        migrations.AlterField(
            model_name='item',
            name='unit_price',
            field=models.DecimalField(default=None, null=True, max_digits=20, decimal_places=2),
        ),
    ]
