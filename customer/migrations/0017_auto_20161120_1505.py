# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0016_remove_customer_nric'),
    ]

    operations = [
        migrations.AddField(
            model_name='customer',
            name='device_id',
            field=models.CharField(default=None, max_length=225, null=True),
        ),
        migrations.AddField(
            model_name='customer',
            name='device_token',
            field=models.CharField(default=None, max_length=225, null=True),
        ),
    ]
