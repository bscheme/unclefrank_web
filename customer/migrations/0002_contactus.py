# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='ContactUs',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('first_name', models.CharField(max_length=120)),
                ('last_name', models.CharField(max_length=120)),
                ('email_address', models.EmailField(max_length=254)),
                ('company', models.CharField(max_length=120)),
                ('country', models.CharField(max_length=120)),
                ('phone', models.IntegerField()),
                ('subjects', models.CharField(max_length=3, null=True, choices=[(b'Subject-1', b'Subject-1'), (b'Subject-2', b'Subject-2'), (b'Subject-3', b'Subject-3'), (b'Subject-4', b'Subject-4'), (b'Subject-5', b'Subject-5')])),
                ('message', models.TextField()),
                ('track_id', models.CharField(max_length=120)),
            ],
        ),
    ]
