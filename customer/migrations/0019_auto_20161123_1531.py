# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0018_customer_fav_driver'),
    ]

    operations = [
        migrations.AlterField(
            model_name='customer',
            name='fav_driver',
            field=models.ManyToManyField(to='driver.Driver'),
        ),
    ]
