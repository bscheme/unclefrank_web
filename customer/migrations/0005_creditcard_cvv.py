# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0004_creditcard'),
    ]

    operations = [
        migrations.AddField(
            model_name='creditcard',
            name='cvv',
            field=models.CharField(default=None, max_length=225),
        ),
    ]
