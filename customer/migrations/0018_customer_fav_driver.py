# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('driver', '0019_auto_20161120_1505'),
        ('customer', '0017_auto_20161120_1505'),
    ]

    operations = [
        migrations.AddField(
            model_name='customer',
            name='fav_driver',
            field=models.ManyToManyField(to='driver.Driver', null=True),
        ),
    ]
