# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0015_auto_20161109_1337'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='customer',
            name='nric',
        ),
    ]
