# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0011_auto_20161018_1404'),
    ]

    operations = [
        migrations.AlterField(
            model_name='creditcard',
            name='card_no',
            field=models.CharField(default=None, max_length=225, null=True),
        ),
        migrations.AlterField(
            model_name='creditcard',
            name='contact_no',
            field=models.CharField(default=None, max_length=225, null=True),
        ),
        migrations.AlterField(
            model_name='creditcard',
            name='cvv',
            field=models.CharField(default=None, max_length=225, null=True),
        ),
        migrations.AlterField(
            model_name='creditcard',
            name='expiry_date',
            field=models.CharField(default=None, max_length=225, null=True),
        ),
        migrations.AlterField(
            model_name='creditcard',
            name='external_reff_no',
            field=models.CharField(default=None, max_length=225, null=True),
        ),
    ]
