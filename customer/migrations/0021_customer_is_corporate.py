# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0020_creditcard_brand'),
    ]

    operations = [
        migrations.AddField(
            model_name='customer',
            name='is_corporate',
            field=models.BooleanField(default=False),
        ),
    ]
