from django.contrib import admin
from .models import Customer
from superadmin.models import HomeCMS
# Register your models here.


admin.site.register(Customer)
admin.site.register(HomeCMS)
