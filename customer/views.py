from django.shortcuts import render, render_to_response
from forms import CustomerRegistrationForm
from forms import CustomerEditProfileForm
from django.core.context_processors import csrf
from django.http import HttpResponse, JsonResponse
from models import Customer, CreditCard, UserProfile
from django.contrib import messages
from django.template import RequestContext
from django.contrib.auth.models import User
from django.views.generic.detail import DetailView
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from service.models import DeliveryService, PickupService, PackageSize, Pricing, VehicleType, DriverRating
from superadmin.models import RegistrationBackground, ContactUsPage, AboutPage, CustomerFaqPage
from datetime import datetime
from django.core.mail import send_mail
from random import choice
from string import ascii_uppercase
from base.custome_decorator import customer_only
from twilio.rest import TwilioRestClient
from django.conf import settings
from base import constant
import stripe
from base.utils import send_email_notification



# Create your views here.

def home(request):
	arg = {}
	package = PackageSize.objects.all()
	vehicle = VehicleType.objects.all()
	arg['package'] = package
	arg['vehicle'] = vehicle
	return render_to_response('base/index.html', arg, context_instance=RequestContext(request))

@login_required
@customer_only
def load_profile(request):
	arg = {}
	arg['customer'] = Customer.objects.get(user=request.user)
	arg['dashboard_active']= True
	try:
		arg['delivery_service'] = DeliveryService.objects.filter(customer = arg['customer'] ).order_by('-id')[0]
	except:
		arg['delivery_service'] = None
	try:
		arg['pickup_service'] = PickupService.objects.filter(customer = arg['customer'] ).order_by('-id')[0]
	except:
		arg['pickup_service'] = None
	try:
		arg['completed_delivery'] = DeliveryService.objects.filter(customer=arg['customer'], delevery_status='completed', rated='not_rated').order_by('-id')[0]
	except:
		arg['completed_delivery'] = None
	try:
		arg['completed_pickup'] = PickupService.objects.filter(customer=arg['customer'], pickup_status='completed', rated='not_rated').order_by('-id')[0]
	except:
		arg['completed_pickup'] = None
	arg['email'] = request.user.email
	return render(request, "customer/dashboard_home.html", arg)

def register_customer(request):
	arg = {}
	arg.update(csrf(request))
	arg['form'] = CustomerRegistrationForm()
	backgroundimage = RegistrationBackground.objects.all()
	if backgroundimage:
		arg['backgroundimage'] = backgroundimage[0]
	if request.method == 'POST':
		form = CustomerRegistrationForm(request.POST)
		arg['form'] = form
		if form.is_valid():
			try:
				check_phone = UserProfile.objects.filter(phone_number=request.POST['contact_no'])
				if check_phone.count() == 0:
					user, created = User.objects.get_or_create(username=form.cleaned_data['email'], email=form.cleaned_data['email'])
					if not created and user.is_active:
						arg['error'] = 'User already exists'
					user.set_password(form.cleaned_data['password'])
					user.is_active = False
					user.save()
					customer = Customer()
					customer.email = form.cleaned_data['email']
					customer.user = user
					customer.name = form.cleaned_data['name']
					customer.contact_no = form.cleaned_data['contact_no']
					customer.verification = ''.join(choice(ascii_uppercase) for i in range(5))
					if not send_sms(customer.verification, customer.contact_no):
						arg['error'] = 'Unable to send Text message'
						return render_to_response('customer/register.html', arg, context_instance=RequestContext(request))
					customer.email = user.email
					customer.save()
					request.session['customer_id'] = customer.id
					# messages.success(request, "Registration Successful")
					user_profile = UserProfile()
					user_profile.user = customer.user
					user_profile.phone_number = customer.contact_no
					user_profile.save()
					return redirect('/customer/verify/')
				else:
					arg['error'] = 'Phone no already exists.'
			except Exception, e:
				arg['error'] =  e
	return render_to_response('customer/register.html', arg, context_instance=RequestContext(request))

# We will need this later

def send_sms(message, send_to):
	try:
		send_from = '+12027937265'
		client = TwilioRestClient(settings.TWILIO_ACCOUNT_SID, settings.TWILIO_AUTH_TOKEN)
		response = client.messages.create(body='Your UF verfication code is: '+message, to=send_to, from_=send_from)
		return True
	except:
		return False

def resend_sms(request):
	try:
		verification = Customer.objects.get(id=request.session['customer_id'])
		send_sms(verification.verification, verification.contact_no)
		return HttpResponse("Success!!")
	except Exception, e:
		pass
	# send_from = '+65 9773 6937'
	# client = TwilioRestClient(settings.TWILIO_ACCOUNT_SID, settings.TWILIO_AUTH_TOKEN)
	# response = client.messages.create(body=verification.verification, to=send_to, from_=send_from)



def customer_virificarion(request):
	arg = {}
	customer = Customer.objects.get(id=request.session['customer_id'])
	arg['code'] = customer.verification
	background = RegistrationBackground.objects.all()
	if background:
		arg['background'] = background[0]
	arg.update(csrf(request))
	if request.method == 'POST':
		user = customer.user
		code = request.POST['verification']
		if Customer.objects.filter(verification = code):
			user.is_active = True
			user.save()
			send_email_notification(user, "Your registration has been successfully completed")
			return redirect('/customer/details/')
	return render_to_response('customer/virification.html', arg, context_instance=RequestContext(request))


@csrf_exempt
@login_required
@customer_only
def customer_details(request):
	context_dict = {}
	context_dict['customer_details_active'] = True
	try:
		customer = Customer.objects.get(user=request.user)
		customer_rating = customer.cutomer_rating()
		print customer_rating
	except Exception, e:
		print e
		customer = None
	if customer is None:
		return redirect('/customer/accounts/login')
	if request.method == 'POST':
		if request.POST['updatePass'] == 'true':
			user = request.user
			user.set_password(request.POST['newPass'])
			user.save()
			return JsonResponse({'redirect':'/customer/accounts/login'})
		else:
			customer.name = request.POST['name']
			customer.email = request.POST['email']
			customer.address = request.POST['address']
			customer.postal_code = request.POST['pcode']
			customer.contact_no = request.POST['contact']
			customer.nric = request.POST['nric']
			customer.save()
		return JsonResponse({'foo':'bar'})
	
	context_dict['customer_profile'] = customer
	context_dict['user'] = request.user
	context_dict['rating'] = customer_rating
	return render(request, 'customer/account_information.html', context_dict)

class CustomerProfileView(DetailView):
	template_name = 'customer/customer_profile.html'
	model = Customer

@login_required
@customer_only
def edit_profile(request, slug):
	arg = {}
	arg.update(csrf(request))
	customer = Customer.objects.get(slug=slug)
	# print customer_rating
	if request.method == 'POST':
		customer = CustomerEditProfileForm(data= request.POST, instance=customer)
		arg['form'] = customer
		if customer.is_valid():
			customer.save()
			messages.success(request, "Updated Successfully")
			#redirect after save
			return redirect('/customer/edit/'+customer.instance.slug)

	return render_to_response('customer/edit.html', arg, context_instance=RequestContext(request))

@login_required
@customer_only
def payment(request):
	arg = {}
	arg.update(csrf(request))
	if request.method == 'POST':
		card = CreditCard()
		card.custmer = request.customer
		name = request.POST['name']
		card_no = request.POST['card_no']
		expiry_date = request.POST['expiry_date']
		date_list = expiry_date.split("/")
		cvv = request.POST['cvv']
		try:
			stripe.api_key = constant.STRIPE_SECRECT_KEY
			token = stripe.Token.create(card={
	        "number": card_no,
	        "exp_month": date_list[0],
	        "exp_year": date_list[1],
	        "cvc": cvv},)
			stripe_customer = stripe.Customer.create(
				card= token,
				description= name,
				email= request.customer.email
				)
			card.strite_id = stripe_customer.id
			card.last_digits = str(stripe_customer.sources.data[0].last4)
			card.name = request.POST['name']
			card.expiry_date = request.POST['expiry_date']
			card.brand = str(stripe_customer.sources.data[0].brand)
			card.save()
		except Exception, e:
			pass
	cards = CreditCard.objects.filter(custmer= request.customer)
	arg = {'cards': cards, 'payment_active': True}
	return render_to_response('customer/payment.html', arg, context_instance=RequestContext(request))

def payment_delete(request, payment_id):
	arg = {}
	try:
		card = CreditCard.objects.get(id=payment_id)
		stripe.api_key = constant.STRIPE_SECRECT_KEY
		cu = stripe.Customer.retrieve(card.strite_id)
		cu.delete()	
		card.delete()
	except:
		#hrrow a error message 
		pass
	return redirect('/customer/payment/')



def change_password(request, slug):
	return render_to_response('customer/change_pass.html', arg, context_instance=RequestContext(request))

@csrf_exempt
@customer_only
def bookCourier(request):
	if request.method == 'POST':
		try:
			customer = Customer.objects.get(user=request.user)
			booking = DeliveryService()

			booking.customer = customer
			booking.collection_name = request.POST['collName']
			booking.collection_address = request.POST['collAdd']
			booking.collection_contact_no = request.POST['collConNum']
			booking.collection_remark = request.POST['colRemk']
			booking.collection_reff_no = request.POST['refNum']
			booking.delivery_name = request.POST['delName']
			booking.delivery_address = request.POST['delAdd']
			booking.delivery_contact_no = request.POST['delConNum']
			booking.delevery_remarks = request.POST['delRemk']
			booking.collection_date = datetime.strptime(request.POST['collDate'], "%d-%m-%Y").date()
			booking.collection_time = request.POST['collTime']
			booking.delevery_date = datetime.strptime(request.POST['delDate'], "%d-%m-%Y").date()
			booking.delevery_time = request.POST['delTime']

			booking.save()
			return JsonResponse({'foo':'Courier booking saved'})

		except Exception, e:
			pass
	else:	
		return render(request, 'booking/booking1.html')


@csrf_exempt
def uploadCustomerImage(request):
	if request.method == 'POST':
		try:
			image = request.FILES['file']
			customer = Customer.objects.get(user=request.user)
			if customer.picture is not None:
				customer.picture.delete(False)
			customer.picture = image
			customer.save()
			return JsonResponse({'result':'Image Updated'})
		except Exception, e:
			pass
			# print(e)

#Set your Location
def location(request):
	arg = {}
	arg['location_active'] = True
	return render_to_response('customer/location.html', arg, context_instance=RequestContext(request))

@csrf_exempt
def change_location(request):
	arg = {}
	arg['status'] = 'falure'
	if request.method == 'POST':
		location = request.POST['location']
		customer = request.customer
		customer.location = location
		customer.save()
		arg['status'] = 'success'
	return JsonResponse(arg)

@login_required
@customer_only
def dashboard_about(request):
	arg = {}
	arg['about'] = AboutPage.objects.all()[0]
	address = ContactUsPage.objects.all()
	arg['address'] = address[0]
	arg['about_active'] = True
	return render_to_response('pages/about.html', arg, context_instance=RequestContext(request))

@login_required
@customer_only
def dashboard_faq(request):
	arg = {}
	arg['questions'] = CustomerFaqPage.objects.all()
	arg['faq_active'] = True
	return render_to_response('pages/faq.html', arg, context_instance=RequestContext(request))

@login_required
@customer_only
def feedback(request):
	arg = {}
	arg.update(csrf(request))
	if request.method == 'POST':
		subject = request.POST['subject']
		details = request.POST['details']
		try:
			send_mail(
				subject,
				details,
				"unclefrank@gmail.com",
				['feedback@unclefrank.com.sg',],
				fail_silently=True,
				)
			arg['message'] = 'Feedback sent successfully'
		except:
			arg['message'] = 'Sorry'
	return render_to_response('pages/feedback.html', arg, context_instance=RequestContext(request))



def session_clear(request):
	del request.session['collection-address']
	del request.session['collection-name']
	del request.session['collection-contact-number']
	del request.session['collection-remarks']
	del request.session['external-reff-number']
	del request.session['delivery-address']
	del request.session['delivery-name']
	del request.session['delivery-contact-number']
	del request.session['delivery-remarks']
	del request.session['eta']
	del request.session['distance']
	del request.session['completed-form']
	del request.session['cellection-time']
	del request.session['cellection-date']
	del request.session['delivery-time']
	del request.session['delivery-date']

@csrf_exempt
def primary_courier_calc(request):
	try:
		if request.method == 'POST':
			if request.POST['quantity'] > 0:
				pricing = Pricing.objects.filter(package_size=request.POST['package'],
												 start_qty__lte=request.POST['quantity'],
												 end_qty__gte=request.POST['quantity'])
				same_day = pricing[0].range_rate*int(request.POST['quantity'])
				next_day = pricing[1].range_rate*int(request.POST['quantity'])
				two_working_day = pricing[2].range_rate*int(request.POST['quantity'])
				return JsonResponse(
					{'package_name': pricing[0].package_size.name, 'same_day': same_day, 'next_day': next_day,
					 'two_day': two_working_day, 'quantity':request.POST['quantity']})
	except Exception,e:
		return HttpResponse(e)

@csrf_exempt
def rate(request):
	try:
		delivery_job = DeliveryService.objects.get(id=request.POST['id'])
		delivery_job.rated = 'rated'
		rating = DriverRating()
		rating.delivery = delivery_job
		rating.driver = delivery_job.taken_by
		rating.rated_by = delivery_job.customer
		rating.rating = request.POST['rating']
		rating.comment = request.POST['comment']
		rating.save()
		delivery_job.save()
		return JsonResponse({"job_id": request.POST['id'], "rating": request.POST['rating']})
	except Exception, e:
		return JsonResponse({"error": e})

@csrf_exempt
def rate_pickup(request):
	try:
		pickup_job = PickupService.objects.get(id=request.POST['id'])
		pickup_job.rated = 'rated'
		rating = DriverRating()
		rating.pickup = pickup_job
		rating.driver = pickup_job.pickedup_by
		rating.rated_by = pickup_job.customer
		rating.rating = request.POST['rating']
		rating.comment = request.POST['comment']
		rating.save()
		pickup_job.save()
		return JsonResponse({"job_id": request.POST['id'], "rating": request.POST['rating']})
	except Exception, e:
		return JsonResponse({"error": e})

def test_email(request):
	send_email_notification(request.user, "hello")
	return HttpResponse("Email sent")