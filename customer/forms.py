from django import forms
from models import Customer, ContactUs

COUNTRIES = (
	('Singapore', 'Singapore'),
    ('Bangladesh', 'Bangladesh'),
    ('India', 'India'),
    ('England', 'England'),
    ('Brazil', 'Brazil'),
    ('Argentina', 'Argentina'),
)


class CustomerRegistrationForm(forms.Form):
	# first_name = forms.CharField(label='First Name', widget=forms.TextInput(attrs={'class': 'form-control huge', 'placeholder': 'First Name *'}))
	# last_name = forms.CharField(label='Last Name', widget=forms.TextInput(attrs={'class': 'form-control huge', 'placeholder': 'Last Name *'}))
	name = forms.CharField(label='Name', widget=forms.TextInput(attrs={'class': 'form-control huge', 'placeholder': 'Name or Company Name *'}))
	email = forms.CharField(label='Email', widget=forms.TextInput(attrs={'class': 'form-control huge', 'placeholder': 'Email *'}))
	password = forms.CharField(label='Password', widget=forms.PasswordInput(attrs={'class': 'form-control huge', 'placeholder': 'Password *'}))
	re_password = forms.CharField(label='Re-enter Password', widget=forms.PasswordInput(attrs={'class': 'form-control huge', 'placeholder': 'Re-enter Password *'}))
	# address = forms.CharField(label='Address', widget=forms.TextInput(attrs={'class': 'form-control huge', 'placeholder': 'Address *'}))
	contact_no = forms.CharField(label='Contcat No', widget=forms.TextInput(attrs={'class': 'form-control huge', 'placeholder': 'Mobile or Office Number (for verification) *'}))



class CustomerEditProfileForm(forms.ModelForm):
	first_name = forms.CharField(label='First Name', widget=forms.TextInput(attrs={}))
	last_name = forms.CharField(label='Last Name', widget=forms.TextInput(attrs={}))
	email = forms.CharField(label='Email', widget=forms.TextInput(attrs={}))
	address = forms.CharField(label='Address', widget=forms.TextInput(attrs={}))
	contact_no = forms.CharField(label='Contcat No', widget=forms.TextInput(attrs={}))
	class Meta:
		model = Customer
		fields = ('first_name','last_name', 'email', 'address','contact_no')

class SignupForm(forms.Form):
	contact_no = forms.CharField(label='Contcat No', widget=forms.TextInput(attrs={'class': 'form-control huge', 'placeholder': 'Mobile or Office Number (for verification) *', 'required': 'true'}))

	def signup(self, request, user):
		customer = Customer()
		customer.user = user
		customer.name = user.first_name + " " + user.last_name
		customer.contact_no = self.cleaned_data['contact_no']
		customer.email = user.email
		customer.save()

class ContactUsForm(forms.ModelForm):
	first_name = forms.CharField(label='first_name', widget=forms.TextInput(attrs={'class': 'form-control site-field'}))
	last_name = forms.CharField(label='last_name', widget=forms.TextInput(attrs={'class': 'form-control site-field'}))
	email_address = forms.CharField(label='email_address', widget=forms.TextInput(attrs={'class': 'form-control site-field'}))
	company = forms.CharField(label='company', widget=forms.TextInput(attrs={'class': 'form-control site-field'}))
	country = forms.ChoiceField(label='country', choices=COUNTRIES, widget=forms.Select(attrs={'class': 'form-control select-styled site-field'}))
	phone = forms.CharField(label='phone', widget=forms.TextInput(attrs={'class': 'form-control site-field'}))
	subjects = forms.CharField(label='subject', widget=forms.TextInput(attrs={'class':'form-control site-field'}))
	message = forms.CharField(label='message', widget=forms.Textarea(attrs={'class': 'form-control site-field'}))
	track_id = forms.CharField(label='track_id', widget=forms.TextInput(attrs={'class': 'form-control site-field'}))
	class Meta:
		model = ContactUs
		fields=['first_name', 'last_name', 'email_address', 'company', 'country', 'phone', 'message', 'track_id']
