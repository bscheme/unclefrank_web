from django.db import models
from django.core import serializers
from django.db.models.signals import pre_save
from django.dispatch import receiver
from gadjo.requestprovider.signals import get_request
from service.models import PickUpRate
from models import AuditTrile


#Here the The Tuse Audite trile data will be saved by using signals

#
@receiver(models.signals.pre_save)
def _user_audite(sender, instance, **kwargs):
    if sender == PickUpRate:
        trac = AuditTrile()
        http_request = get_request()
        user = http_request.user
        trac.table = instance._meta.db_table
        if instance.pk is not None:
            old = objname.objects.get(pk= instance.pk)
            trac.old_data = serializers.serialize('json', [old])
            trac.new_data = serializers.serialize('json', [instance])
            trac.operation = "edit"
            trac.save()

#
#
# @receiver(models.signals.pre_delete)
# def _user_audite_delete(sender, instance, **kwargs):
#     if sender != UserAuditTrail and sender != Session and sender != User:
#         #user = get_current_user()
#         trac = UserAuditTrail()
#         http_request = get_request()
#         user = http_request.user
#         trac.user_id = user.id
#         trac.old_record = serializers.serialize('json', [instance])
#         trac.action = "delete"
#         trac.reference =  instance.id
#         trac.save()
#
# @receiver(models.signals.post_save)
# def _user_audite_add(sender, instance, created, **kwargs):
#     if sender != UserAuditTrail and sender != Session and sender != User:
#         #user = get_current_user()
#         trac = UserAuditTrail()
#         http_request = get_request()
#         user = http_request.user
#         trac.user_id = user.id
#         trac.controller = instance._meta.db_table
#         objname = instance.__class__
#         trac.controller = instance._meta.db_table
#         if created:
#             trac.action = "add"
#             trac.reference =  instance.id
#             trac.new_record = serializers.serialize('json', [instance])
#             trac.save()
#