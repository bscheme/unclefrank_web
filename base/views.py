from django.shortcuts import render, render_to_response
from django.template import RequestContext
from customer.forms import ContactUsForm
from django.http import HttpResponse
from django.core.mail import send_mail
from django.contrib.auth.decorators import login_required
from superadmin.models import SliderImage



def veri(request):
	return HttpResponse("f562ab3264ec8f3502571c8cfb56f5b0c673bdf17819790d5ba5bbeb2063c90c")

# Create your views here.
def about(request):
	arg = {}
	arg['about'] = True
	return render_to_response('base/pages/about.html', arg, context_instance=RequestContext(request))	

def service(request):
	arg = {}
	arg['service'] = True
	return render_to_response('base/pages/services.html', arg, context_instance=RequestContext(request))
def join_us(request):
	arg = {}
	arg['join_us'] = True
	return render_to_response('base/pages/join-us.html', arg, context_instance=RequestContext(request))
def corporate(request):
	arg = {}
	arg['corporate'] = True
	return render_to_response('base/pages/corporate.html', arg, context_instance=RequestContext(request))
def privacy(request):
	arg = {}
	return render_to_response('base/pages/privacy.html', arg, context_instance=RequestContext(request))
def tnc(request):
	arg = {}
	return render_to_response('base/pages/tnc.html', arg, context_instance=RequestContext(request))

def faq(request):
	arg = {}
	return render_to_response('base/pages/faq.html', arg, context_instance=RequestContext(request))

def contact_us(request):
	arg = {}
	form = ContactUsForm()
	if request.method == 'POST':
		form = ContactUsForm(request.POST)
		if form.is_valid():
			save_to_db = form.save(commit=False)
			first_name = form.cleaned_data['first_name']
			last_name = form.cleaned_data['last_name']
			email_address = form.cleaned_data['email_address']
			message = form.cleaned_data['message']
			track_id = form.cleaned_data['track_id']
			Subject = 'UncleFrank contact form'
			from_email = 'uncle.frank2020@gmail.com'
			content_email = "Dear %s Your email has been received. Here is your email %s" %(first_name, message)
			to_email = [email_address]#['ridwan_rahman@ymail.com']
			send_mail(
    			Subject,
    			content_email,
    			from_email,
    			to_email,
    			fail_silently=True,
				)
			save_to_db.save() 
			return render_to_response('base/pages/contact_us_success.html', arg, context_instance=RequestContext(request))

	arg = {'form': form}
	arg['contact_us'] = True
	return render_to_response('base/pages/contact_us.html', arg, context_instance=RequestContext(request))
def career(request):
	arg = {}
	return render_to_response('base/pages/career.html', arg, context_instance=RequestContext(request))
	

