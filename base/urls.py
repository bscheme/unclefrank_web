from django.conf.urls import include,url

urlpatterns = [
    url(r'^about/$', 'base.views.about', name="about"),
    url(r'^service/$', 'base.views.service', name="service"),
    url(r'^join_us/$', 'base.views.join_us', name="joinus"),
    url(r'^corporate/$', 'base.views.corporate', name="corporate"),
    url(r'^contact_us/$', 'base.views.contact_us', name="contact_us"),
    url(r'^career/$', 'base.views.career', name="career"),
    url(r'^privacy/$', 'base.views.privacy', name="privacy"),
    url(r'^tnc/$', 'base.views.tnc', name="tnc"),
    url(r'^faq/$', 'base.views.faq', name="faq"),


    ]