from superadmin.models import HomeCMS, AboutPage, Services, JoinUs, Careers, Corporate, ContactUsPage, SliderImage, Terms, Privacy, Faq, AccessControl
from customer.models import Customer
from django.core.urlresolvers import resolve


def context(request):
	arg = {}
	try:
		url_name = resolve(request.path_info).url_name
		if url_name == 'home':
			home = HomeCMS.objects.all()
			arg['home'] = home[0]
		else:
			home = None
	except:
		home = None
	try:
		url_name = resolve(request.path_info).url_name
		if url_name == 'about':
			about = AboutPage.objects.all()
			slider = SliderImage.objects.all()
			arg['about_page'] = about[0]
			arg['slider'] = slider
	except:
		about = None
	try:
		services = Services.objects.all()
		arg['services'] = services[0]
	except:
		services = None

	try:
		services = Services.objects.all()
		arg['services'] = services[0]
	except:
		services = None
	try:
		joinus = JoinUs.objects.all()
		arg['joinus'] = joinus[0]
	except:
		joinus = None

	try:
		contactus = ContactUsPage.objects.all()
		arg['contactus'] = contactus[0]
	except:
		about = None

	try:
		corporate = Corporate.objects.all()
		arg['corporate_page'] = corporate[0]
	except:
		about = None

	try:
		careers = Careers.objects.all()
		arg['careers'] = careers[0]
	except:
		about = None

	try:
		arg['customer'] = Customer.objects.get(user=request.user)
	except:
		arg['customer'] = None
	try:
		arg['terms'] = Terms.objects.all()[0]
	except:
		arg['terms'] = None
	try:
		arg['faq'] = Faq.objects.all()[0]
	except:
		arg['faq'] = None
	try:
		arg['privacy'] = Privacy.objects.all()[0]
	except:
		arg['privacy'] = None
	
	
	return arg

def access(request):
    arg = {}
    try:
        # print request.user
        permission = AccessControl.objects.get(user=request.user)
        # print str(permission.user)
    except Exception, e:
        permission = None

    arg = {
        'permissions': permission
    }
    # for testing only
    permission = {}
    permission['is_cms'] = True
    permission['is_settings'] = True
    permission['is_courier'] = True
    permission['is_pickup'] = True
    permission['is_driver_info'] = True
    permission['is_sales_report'] = True
    permission['is_customer_list'] = True
    permission['is_account'] = True
    permission['is_add_coupon'] = True
    permission['is_window_setup'] = True
    permission['is_address_setup'] = True
    arg = {
	    'permissions': permission
	}
    return arg
