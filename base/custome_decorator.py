from functools import wraps
from customer.models import Customer
from threading import Thread


# Create your views here.
from django.http import HttpResponse, HttpResponseRedirect

def customer_only(view_func):
    """This is The custome decorator for checking if the user is a cuatomer"""
    def _decorator(request, *args, **kwargs):
        user=request.user
        try:
            customer = Customer.objects.get(user=user)
        except:
            return HttpResponseRedirect('/customer/accounts/logout/')
        response = view_func(request, *args, **kwargs)
        return response
    return wraps(view_func)(_decorator)


def postpone(function):
  def decorator(*args, **kwargs):
    t = Thread(target = function, args=args, kwargs=kwargs)
    t.daemon = True
    t.start()
  return decorator