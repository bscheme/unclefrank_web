# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0003_audittrile'),
    ]

    operations = [
        migrations.AddField(
            model_name='audittrile',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, null=True),
        ),
    ]
