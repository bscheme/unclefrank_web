# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0002_auto_20161023_1839'),
    ]

    operations = [
        migrations.CreateModel(
            name='AuditTrile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('table', models.CharField(default=None, max_length=255, null=True)),
                ('old_data', models.TextField(default=None, null=True)),
                ('new_data', models.TextField(default=None, null=True)),
                ('operation', models.CharField(default=None, max_length=255, null=True)),
                ('user_name', models.CharField(default=None, max_length=255, null=True)),
            ],
        ),
    ]
