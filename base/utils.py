from django.conf import settings
# from django.core.mail import EmailMessage
from django.core.mail import EmailMultiAlternatives, EmailMessage
from django.template.loader import get_template
from .custome_decorator import postpone
from django.core.mail import send_mail


# @postpone
def send_email_notification(user, message):
    subject = "New Message form Unclefrank"

    text_template = get_template('email/email.txt')
    html_template = get_template('email/email.html')

    to = user.email
    from_email = from_email = "UncleFrank <admin@unclefrank.com.sg>"

    context = {
        'user': user,
        'message': message
    }

    text_content = text_template.render(context)
    html_content = html_template.render(context)

    msg = EmailMultiAlternatives(subject, html_content, from_email, [user.email])
    msg.attach_alternative(html_content, 'text/html')
    msg.send(fail_silently=True)

def send_email_invoice_for_pickup(user, message, invoice):
    subject = "Invoice for your recent passenger pickup order"

    text_template = get_template('email/email_invoice_pickup.txt')
    html_template = get_template('email/email_invoice_pickup.html')

    to = user.email
    from_email = from_email = "UncleFrank <admin@unclefrank.com.sg>"

    context = {
        'user': user,
        'message': message,
        'invoice': invoice
    }

    text_content = text_template.render(context)
    html_content = html_template.render(context)

    msg = EmailMultiAlternatives(subject, text_content, from_email, [user.email])
    msg.attach_alternative(html_content, 'text/html')
    msg.send(fail_silently=True)