from django import forms
from models import Address

class AddressForm(forms.ModelForm):
	post_code = forms.CharField(label='Postal Code',
		widget=forms.NumberInput(attrs={'class': 'form-control huge', 'placeholder': 'Postal Code'}))
	bldg_no = forms.CharField(required=False, label='Building Number',
		widget=forms.TextInput(attrs={'class': 'form-control huge', 'placeholder': 'Building Number'}))
	street_name = forms.CharField(label='Street Name', 
		widget=forms.TextInput(attrs={'class': 'form-control huge', 'placeholder': 'Street Name'}))
	bldg_name = forms.CharField(required=False, label='Building Name',
		widget=forms.TextInput(attrs={'class': 'form-control huge', 'placeholder': 'Building Name'}))
	floor = forms.CharField(required=False, label='Floor',
		widget=forms.TextInput(attrs={'class': 'form-control huge', 'placeholder': 'Floor'}))
	unit = forms.CharField(required=False, label='Unit',
		widget=forms.TextInput(attrs={'class': 'form-control huge', 'placeholder': 'Unit'}))

	class Meta:
		model = Address
		fields = ('post_code','bldg_no', 'street_name', 'bldg_name','floor', 'unit')
