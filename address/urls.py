from django.conf.urls import include,url
from django.core.urlresolvers import reverse_lazy
from django.views.generic import TemplateView
from .import views

urlpatterns = [
	url(r'^setup/$', 'address.views.address_setup'),
	url(r'^update/(?P<address_id>\d+)/$', 'address.views.address_update'),
	url(r'^delete/(?P<address_id>\d+)/$', 'address.views.address_delete'),
	url(r'^upladCSV/$', 'address.views.upload_csv'),
	url(r'^check/$', 'address.views.address_check'),
	url(r'^postcode_txt/$', 'address.views.postcode_txt'),
	url(r'^street_txt/$', 'address.views.street_txt'),
]