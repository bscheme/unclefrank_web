# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('address', '0003_auto_20161114_1352'),
    ]

    operations = [
        migrations.CreateModel(
            name='Postalcode',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('post_code', models.IntegerField(default=0)),
                ('address_type', models.CharField(default=None, max_length=1, null=True)),
                ('buliding_no', models.CharField(default=None, max_length=7, null=True)),
                ('street_key', models.CharField(default=None, max_length=7, null=True)),
                ('building_key', models.CharField(default=None, max_length=6, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Street',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('street_key', models.CharField(default=None, max_length=7, null=True)),
                ('street_name', models.CharField(default=None, max_length=32, null=True)),
                ('filler', models.CharField(default=None, max_length=6, null=True)),
            ],
        ),
    ]
