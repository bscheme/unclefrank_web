# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('address', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='address',
            name='bldg_name',
            field=models.CharField(max_length=45, null=True),
        ),
        migrations.AlterField(
            model_name='address',
            name='bldg_no',
            field=models.CharField(max_length=7, null=True),
        ),
        migrations.AlterField(
            model_name='address',
            name='floor',
            field=models.CharField(max_length=5, null=True),
        ),
        migrations.AlterField(
            model_name='address',
            name='post_code',
            field=models.CharField(max_length=6, null=True),
        ),
        migrations.AlterField(
            model_name='address',
            name='street_name',
            field=models.CharField(max_length=32, null=True),
        ),
        migrations.AlterField(
            model_name='address',
            name='unit',
            field=models.CharField(max_length=5, null=True),
        ),
    ]
